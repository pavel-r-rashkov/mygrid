'use strict';

const fs = require('fs');
const fsExtra = require("fs-extra");
const path = require('path');
const glob = require('glob');
const camelCase = require('camelcase');
const ngc = require('@angular/compiler-cli/src/main').main;
const rollup = require('rollup');
const uglify = require('rollup-plugin-uglify');
const sourcemaps = require('rollup-plugin-sourcemaps');
const nodeResolve = require('rollup-plugin-node-resolve');
const commonjs = require('rollup-plugin-commonjs');
const nodeSass = require('node-sass');
const inlineResources = require('./inline-resources');

const libName = require('./package.json').name;
const rootFolder = path.join(__dirname);
const compilationFolder = path.join(rootFolder, 'out-tsc');
const srcFolder = path.join(rootFolder, 'src/lib');
const distFolder = path.join(rootFolder, 'dist');
const tempLibFolder = path.join(compilationFolder, 'lib');
const es5OutputFolder = path.join(compilationFolder, 'lib-es5');
const es2015OutputFolder = path.join(compilationFolder, 'lib-es2015');

const themesSrcFolder = path.join(srcFolder, 'src/theming');
const themesDistFolder = path.join(distFolder, 'src/theming');

const assetsSrcFolder = path.join(rootFolder, 'src/lib/assets');
const assetsDestFolder = path.join(distFolder, 'assets');

return Promise.resolve()
  // Copy library to temporary folder and inline html/css.
  .then(() => _relativeCopy(`**/*`, srcFolder, tempLibFolder)
    .then(() => inlineResources(tempLibFolder))
    .then(() => console.log('Inlining succeeded.'))
  )
  // Compile to ES2015.
  .then(() => ngc(['-p', `${tempLibFolder}/tsconfig.lib.json`], (error) => {
		if (error) {
			throw new Error('ES2015 compilation error: ' + error);
		}
	})
  )
  // Compile to ES5.
  .then(() => ngc(['-p', `${tempLibFolder}/tsconfig.es5.json`], (error) => {
		if (error) {
			throw new Error('ES5 compilation error: ' + error);
		}
	})
  )
  // Copy typings and metadata to `dist/` folder.
  .then(() => Promise.resolve()
    .then(() => _relativeCopy('**/*.d.ts', es2015OutputFolder, distFolder))
    .then(() => _relativeCopy('**/*.metadata.json', es2015OutputFolder, distFolder))
    .then(() => console.log('Typings and metadata copy succeeded.'))
  )
  // Bundle lib.
  .then(() => {
    // Base configuration.
    const es5Entry = path.join(es5OutputFolder, `${libName}.js`);
    const es2015Entry = path.join(es2015OutputFolder, `${libName}.js`);
    const rollupBaseConfig = {
      moduleName: camelCase(libName),
      sourceMap: true,
      // ATTENTION:
      // Add any dependency or peer dependency your library to `globals` and `external`.
      // This is required for UMD bundle users.
      globals: {
        // The key here is library name, and the value is the the name of the global variable name
        // the window object.
        // See https://github.com/rollup/rollup/wiki/JavaScript-API#globals for more.
        '@angular/core': 'ng.core'
      },
      external: [
        // List of dependencies
        // See https://github.com/rollup/rollup/wiki/JavaScript-API#external for more.
        '@angular/core',
		'@angular/common',
		'@angular/compiler',
		'@angular/platform-browser',
		'@angular/platform-browser-dynamic',
		'@angular/http',
		'@angular/router',
		'@angular/forms',
		
		'@angular/animations',
		'@angular/animations/browser',
		'@angular/platform-browser/animations',
		
		'rxjs',
		'rxjs/Rx',
		'rxjs/Subject',
		'rxjs/ReplaySubject',
		'rxjs/Observable',
		'rxjs/BehaviorSubject',
		'rxjs/add/operator/catch'
      ],
      plugins: [
        commonjs({
          include: ['node_modules/rxjs/**']
        }),
        sourcemaps(),
        nodeResolve({ jsnext: true, module: true })
      ]
    };

    // UMD bundle.
    const umdConfig = Object.assign({}, rollupBaseConfig, {
      entry: es5Entry,
      dest: path.join(distFolder, `bundles`, `${libName}.umd.js`),
      format: 'umd',
    });

    // Minified UMD bundle.
    const minifiedUmdConfig = Object.assign({}, rollupBaseConfig, {
      entry: es5Entry,
      dest: path.join(distFolder, `bundles`, `${libName}.umd.min.js`),
      format: 'umd',
      plugins: rollupBaseConfig.plugins.concat([uglify({})])
    });

    // ESM+ES5 flat module bundle.
    const fesm5config = Object.assign({}, rollupBaseConfig, {
      entry: es5Entry,
      dest: path.join(distFolder, `${libName}.es5.js`),
      format: 'es'
    });

    // ESM+ES2015 flat module bundle.
    const fesm2015config = Object.assign({}, rollupBaseConfig, {
      entry: es2015Entry,
      dest: path.join(distFolder, `${libName}.js`),
      format: 'es'
    });

    const allBundles = [
      umdConfig,
      minifiedUmdConfig,
      fesm5config,
      fesm2015config
    ].map(cfg => rollup.rollup(cfg).then(bundle => bundle.write(cfg)));

    return Promise.all(allBundles)
      .then(() => console.log('All bundles generated successfully.'))
  })
  // Build themes
  .then(() => {
	console.log('Start building themes');
	fs.mkdirSync(themesDistFolder);
	fs.readdir(themesSrcFolder, (err, files) => {
	  if (err != null) {
		console.log('Error reading themes folder', err);
	  }
		
	  files.forEach(file => {
	    nodeSass.render({
		  file: path.join(themesSrcFolder, file),
		  outputStyle: 'compressed'
		}, function(err, result) {
		  if (err != null) {
		    console.log('Error compiling theme scss', file, err);
			  return;
			}
			
			var  fileName = file.substring(0, file.lastIndexOf('.'));
			fs.writeFileSync(path.join(themesDistFolder, fileName + '.css'), result.css);
		});
	  });
	});
  })
  // Copy assets
  .then(() => {
    fsExtra.copy(assetsSrcFolder, assetsDestFolder, function (err) {
	  if (err) {
		console.log('Error copying assets', err);
		return;
	  }
	  console.log('Copy assets succeeded');
	});
  })
  // Copy package files
  .then(() => {
	  var files = [ 'README.md', 'package.json', 'LICENSE.MIT' ];
	  files.forEach((file) => {
		fsExtra.copySync(path.join(rootFolder, file), path.join(distFolder, file));
	  });
	  console.log('Package files copy succeeded.')
  })
  .catch(e => {
    console.error('\Build failed. See below for errors.\n');
    console.error(e);
    process.exit(1);
  });


// Copy files maintaining relative paths.
function _relativeCopy(fileGlob, from, to) {
  return new Promise((resolve, reject) => {
    glob(fileGlob, { cwd: from, nodir: true }, (err, files) => {
      if (err) reject(err);
      files.forEach(file => {
        const origin = path.join(from, file);
        const dest = path.join(to, file);
        const data = fs.readFileSync(origin, 'utf-8');
        _recursiveMkDir(path.dirname(dest));
        fs.writeFileSync(dest, data);
        resolve();
      })
    })
  });
}

// Recursively create a dir.
function _recursiveMkDir(dir) {
  if (!fs.existsSync(dir)) {
    _recursiveMkDir(path.dirname(dir));
    fs.mkdirSync(dir);
  }
}
