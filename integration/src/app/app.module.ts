import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { MyGridModule } from 'my-grid';
import { AppComponent }  from './app.component';

@NgModule({
  imports: [ 
    BrowserModule,
    MyGridModule
  ],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
