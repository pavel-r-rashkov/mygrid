import { Component } from '@angular/core';

@Component({
  selector: 'integration-app',
  templateUrl: './app.component.html',
})
export class AppComponent {
  items: any[] = [
    { id: 1, name: 'asdf' },
    { id: 2, name: 'bsad' },
    { id: 3, name: 'cdaw' }
  ];

  constructor() {
  }
}
