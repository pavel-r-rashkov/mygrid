import { Injectable } from '@angular/core';

@Injectable()
export class GridSettingsService {
  public key: string;
  public enableServerSideMode: boolean;
}