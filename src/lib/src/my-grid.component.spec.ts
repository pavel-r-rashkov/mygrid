import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild,
  Injectable
} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from './my-grid.component';
import { MyGridModule } from './my-grid.module';
import { FilterStateService } from './filter-state.service';
import { OrderStateService } from './order-state.service';
import { PaginationService } from './pagination/pagination.service';
import { GroupService } from './grouping/group.service';
import { ColumnOrderingService } from './column-ordering/column-ordering.service';
import { RowEditionService } from './edition/row-edition.service';
import { RowDeletionService } from './edition/row-deletion.service';
import { GridSettingsService } from './grid-settings.service';
import { HtmlTableElementService } from './html-table-elements/html-table-element.service';
import { MasterDetailService } from './master-detail/master-detail.service';
import { SelectionService } from './selection/selection.service';
import { ColumnsService } from './columns.service';
import { GridApiService } from './grid-api.service';
import { ScrollMeasureService } from './scroll-measure.service';
import { TextColumnDirective } from './grid-columns/text-column/text-column.directive';
import { GuidService } from './guid';
import { GroupState } from './grouping/group-state';
import { GridColumn } from './grid-columns/grid-column';
import { RowEditionState } from './edition/row-edition-state';
import { DetailsState } from './master-detail/details-state';
import { FilterState } from './states/filter-state';
import { GridState } from './states/grid-state';
import { DefaultComparerService } from './grid-columns/shared/default-comparer.service';
import { LifeCycleState } from './states/life-cycle-state';
import { OrderState } from './states/order-state';

@Injectable()
class FakeScrollMeasureService {
  measureScrollWidth() {
    return 30;
  }
}

@Component({
  selector: 'mgt-test-grid',
  template: `
    <mg-grid id="test-header-component" #testGrid>
      <mg-text-column></mg-text-column>
      <mg-text-column [isVisible]="isVisible"></mg-text-column>
      <mg-text-column></mg-text-column>
    </mg-grid>
  `
})
class TestGridComponent {
  isVisible: boolean = false;
  @ViewChild('testGrid') testGrid: MyGridComponent;
}

describe('Component: MyGridComponent', () => {
  let gridComponent: MyGridComponent;
  let fixture: ComponentFixture<MyGridComponent>;
  let filterStateService: FilterStateService;
  let orderStateService: OrderStateService;
  let paginationService: PaginationService;
  let groupService: GroupService;
  let columnOrderingService: ColumnOrderingService;
  let rowEditionService: RowEditionService;
  let rowDeletionService: RowDeletionService;
  let gridSettingsService: GridSettingsService;
  let htmlTableElementService: HtmlTableElementService;
  let masterDetailService: MasterDetailService;
  let selectionService: SelectionService;
  let columnsService: ColumnsService;
  let gridApiService: GridApiService;
  let scrollMeasureService: FakeScrollMeasureService = new FakeScrollMeasureService();

  beforeEach(async(() => {
    TestBed.overrideComponent(MyGridComponent, {
      remove: { 
        providers: [ScrollMeasureService]
      },
      add: {
        providers: [
          { provide: ScrollMeasureService, useValue: scrollMeasureService }
        ]
      }
    });

    TestBed.configureTestingModule({
        declarations: [
          TestGridComponent
        ],
        providers: [],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(MyGridComponent);
        gridComponent = fixture.componentInstance;

        filterStateService = fixture.debugElement.injector.get(FilterStateService);
        orderStateService = fixture.debugElement.injector.get(OrderStateService);
        paginationService = fixture.debugElement.injector.get(PaginationService);
        groupService = fixture.debugElement.injector.get(GroupService);
        columnOrderingService = fixture.debugElement.injector.get(ColumnOrderingService);
        rowEditionService = fixture.debugElement.injector.get(RowEditionService);
        rowDeletionService = fixture.debugElement.injector.get(RowDeletionService);
        gridSettingsService = fixture.debugElement.injector.get(GridSettingsService);
        htmlTableElementService = fixture.debugElement.injector.get(HtmlTableElementService);
        masterDetailService = fixture.debugElement.injector.get(MasterDetailService);
        selectionService = fixture.debugElement.injector.get(SelectionService);
        columnsService = fixture.debugElement.injector.get(ColumnsService);
        gridApiService = fixture.debugElement.injector.get(GridApiService);
      });
  }));

  it('Grid scroll width should be set on browser\'s scroll width - 1', () => {
    gridComponent.ngOnInit();

    var fakeScrollWidth = scrollMeasureService.measureScrollWidth();
    expect(gridComponent.scrollWidth).toEqual(fakeScrollWidth - 1 + 'px');
  });

  it('getItemRows should return array with the item when the items are not grouped', () => {
    var item = {};

    gridComponent.ngOnInit();
    var itemRows = gridComponent.getItemRows(item);

    expect(itemRows.length).toEqual(1);
    expect(itemRows[0]).toEqual(item);
  });

  it('getItemRows should return array with all items in the group', () => {
    var item = {};
    var column = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    columnsService.columns = [column];
    var groupItems: any[] = [];
    item[groupService.GROUPED_ITEMS_KEY] = groupItems;

    gridComponent.ngOnInit();
    groupService.groupState.next(new GroupState('bar', 'foo', true));

    fixture.whenStable().then(() => {
      var itemRows = gridComponent.getItemRows(item);
      expect(itemRows).toEqual(groupItems);
    });
  });

  it('shouldRenderGroupedLayout should return false when the items are not grouped', () => {
    var item = {};

    gridComponent.ngOnInit();
    var isGroupedLayout = gridComponent.shouldRenderGroupedLayout();

    expect(isGroupedLayout).toBeFalsy();
  });

  it('shouldRenderGroupedLayout should return true when the items are grouped', () => {
    var item = {};
    var column = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    columnsService.columns = [column];
    var groupItems: any[] = [];
    item[groupService.GROUPED_ITEMS_KEY] = groupItems;

    gridComponent.ngOnInit();
    groupService.groupState.next(new GroupState('bar', 'foo', true));

    fixture.whenStable().then(() => {
      var isGroupedLayout = gridComponent.shouldRenderGroupedLayout();
      expect(isGroupedLayout).toBeTruthy();
    });
  });

  it('getVisibleIndex should return grid item index on page + 1 when the items are not grouped', () => {
    var gridItemIndex = 10;
    var rowIndex = 10;

    gridComponent.ngOnInit();
    var visibleIndex = gridComponent.getVisibleIndex(gridItemIndex, rowIndex);

    expect(visibleIndex).toEqual(gridItemIndex + 1);
  });

  it('getVisibleIndex should return grid item index on page + 1 when the items are grouped', () => {
    var column = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    column.propertyName = 'foo';
    columnsService.columns = [column];
    var groupItems: any[] = [];
    var gridItemIndex = 1;
    var rowIndex = 2;
    gridComponent.items = [
      { foo: 'G1' },
      { foo: 'G1' },
      { foo: 'G2' },
      { foo: 'G2' },
      { foo: 'G2' }
    ];    

    gridComponent.ngOnInit();
    groupService.groupState.next(new GroupState(column.columnId, column.propertyName, true));

    fixture.whenStable().then(() => {
      var visibleIndex = gridComponent.getVisibleIndex(gridItemIndex, rowIndex);
      expect(visibleIndex).toEqual(5);
    });
  });

  it('toggleGroup with closed group should open the group', () => {
    var itemsGroup: any = {};

    gridComponent.ngOnInit();
    gridComponent.toggleGroup(itemsGroup);

    expect(itemsGroup.isOpened).toBeTruthy();
  });

  it('toggleGroup with opened group should close the group', () => {
    var itemsGroup = { isOpened: true };

    gridComponent.ngOnInit();
    gridComponent.toggleGroup(itemsGroup);

    expect(itemsGroup.isOpened).toBeFalsy();
  });

  it('shouldRenderColumn should return true for visible non grouped column', () => {
    var column: GridColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());

    gridComponent.ngOnInit();
    var shouldRenderColumn = gridComponent.shouldRenderColumn(column);

    expect(shouldRenderColumn).toBeTruthy();
  });

  it('shouldRenderColumn should return false for column with visibility set to false', () => {
    var column: GridColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    column.isVisible = false;

    gridComponent.ngOnInit();
    var shouldRenderColumn = gridComponent.shouldRenderColumn(column);

    expect(shouldRenderColumn).toBeFalsy();
  });

  it('shouldRenderColumn should return false for column that is grouped', () => {
    var column: GridColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    columnsService.columns = [column];
    column.isVisible = true;

    gridComponent.ngOnInit();
    groupService.groupState.next(new GroupState(column.columnId, 'foo', true));

    fixture.whenStable().then(() => {
      var shouldRenderColumn = gridComponent.shouldRenderColumn(column);
      expect(shouldRenderColumn).toBeFalsy();
    });
  });

  it('getRenderableColumns should return all visible columns', () => {
    var textGridFixture = TestBed.createComponent(TestGridComponent);
    var testGridComponent: TestGridComponent = textGridFixture.componentInstance;

    textGridFixture.detectChanges();
    var renderableColumns = testGridComponent.testGrid.getRenderableColumns();

    expect(renderableColumns.length).toEqual(2);
  });

  it('getRenderableColumnCount should return all visible columns count', () => {
    var textGridFixture = TestBed.createComponent(TestGridComponent);
    var testGridComponent: TestGridComponent = textGridFixture.componentInstance;

    textGridFixture.detectChanges();
    var renderableColumnsCount = testGridComponent.testGrid.getRenderableColumnCount();

    expect(renderableColumnsCount).toEqual(2);
  });

  it('isRowEdited should return false when the item is not being edited', () => {
    var key = 'foo';
    var item = { foo: 1 };
    gridComponent.key = key;

    gridComponent.ngOnInit();
    rowEditionService.rowEdition.next(new RowEditionState([2, 3]));

    fixture.whenStable().then(() => {
      var isRowEdited = gridComponent.isRowEdited(item);
      expect(isRowEdited).toBeFalsy();
    });
  });

  it('isRowEdited should return true when the item is being edited', () => {
    var key = 'foo';
    var item = { foo: 1 };
    gridComponent.key = key;

    gridComponent.ngOnInit();
    rowEditionService.rowEdition.next(new RowEditionState([2, 1]));

    fixture.whenStable().then(() => {
      var isRowEdited = gridComponent.isRowEdited(item);
      expect(isRowEdited).toBeTruthy();
    });
  });

  it('shouldRenderDetailGrid should return false when the item\'s details are not opened', () => {
    var key = 'foo';
    var item = { foo: 1 };
    gridComponent.key = key;

    gridComponent.ngOnInit();
    masterDetailService.detailsStateSubject.next(new DetailsState([2, 3]));

    fixture.whenStable().then(() => {
      var detailsOpened = gridComponent.shouldRenderDetailGrid(item);
      expect(detailsOpened).toBeFalsy();
    });
  });

  it('shouldRenderDetailGrid should return true when the item\'s details are opened', () => {
    var key = 'foo';
    var item = { foo: 1 };
    gridComponent.key = key;

    gridComponent.ngOnInit();
    masterDetailService.detailsStateSubject.next(new DetailsState([2, 1]));

    fixture.whenStable().then(() => {
      var detailsOpened = gridComponent.shouldRenderDetailGrid(item);
      expect(detailsOpened).toBeTruthy();
    });
  });

  it('clearFilters should remove all filters', () => {
    var filterStateSpy: jasmine.Spy = spyOn(filterStateService.filterState, 'next').and.callThrough();

    gridComponent.ngOnInit();
    filterStateService.filterState.next(new FilterState({}));

    fixture.whenStable().then(() => {
      filterStateSpy.calls.reset();      
      gridComponent.clearFilters();

      fixture.whenStable().then(() => {
        expect(filterStateSpy).toHaveBeenCalled();
        expect(filterStateSpy.calls.first().args[0]).toBeNull();
      });
    });
  });

  it('toggleAddRow with closed addition row should open the addition row', () => {
    gridComponent.showAddRow = false;

    gridComponent.ngOnInit();
    gridComponent.toggleAddRow();

    expect(gridComponent.showAddRow).toBeTruthy();
  });

  it('toggleAddRow with opened addition row should close the addition row', () => {
    gridComponent.showAddRow = true;

    gridComponent.ngOnInit();
    gridComponent.toggleAddRow();

    expect(gridComponent.showAddRow).toBeFalsy();
  });

  it('getGroupedColumnData should return grouped column data', () => {
    var column: GridColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    columnsService.columns = [column];

    gridComponent.ngOnInit();
    groupService.groupState.next(new GroupState(column.columnId, 'foo', true));

    fixture.whenStable().then(() => {
      var groupedColumnData = gridComponent.getGroupedColumnData();
      expect(groupedColumnData).toEqual(column.getGroupData());
    });
  });

  it('getGroupedColumnDataComponentType should return grouped column data type', () => {
    var column: GridColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    columnsService.columns = [column];

    gridComponent.ngOnInit();
    groupService.groupState.next(new GroupState(column.columnId, 'foo', true));

    fixture.whenStable().then(() => {
      var groupedColumnType = gridComponent.getGroupedColumnDataComponentType();
      expect(groupedColumnType).toEqual(column.dataComponentType);
    });
  });



  it('getItemsToSummarize with enabled server side mode and global summary should return empty array', () => {
    var items = [ { foo: 'foo', bar: 'bar' } ];
    gridComponent.enableServerSideMode = true;
    gridComponent.onGridStateChange = (gridState: GridState) => { 
      return Observable.of({ items: items, count: 2, summaryResults: [] });
    };

    gridComponent.ngOnInit();
    gridComponent.ngAfterContentInit();

    fixture.whenStable().then(() => {
      //var items = gridComponent.getItemsToSummarize(false);
      //expect(items.length).toEqual(0);
    });
  });

  it('getItemsToSummarize with non grouped items and per page summary should return all items on the page', () => {
    gridComponent.items = [ 
      { foo: 'foo1', bar: 'bar1' },
      { foo: 'foo2', bar: 'bar2' },
      { foo: 'foo3', bar: 'bar3' },
      { foo: 'foo4', bar: 'bar4' },
      { foo: 'foo5', bar: 'bar5' },
      { foo: 'foo6', bar: 'bar6' }
    ];
    var pageSize = 2;
    gridComponent.pageSize = pageSize;

    gridComponent.ngOnInit();
    gridComponent.ngAfterContentInit();
    var itemsToSummarize = gridComponent.getItemsToSummarize(true);
    
    expect(itemsToSummarize.length).toEqual(pageSize);
    expect(itemsToSummarize[0]).toEqual(gridComponent.items[0]);
    expect(itemsToSummarize[1]).toEqual(gridComponent.items[1]);
  });

  it('getItemsToSummarize with non grouped items and global summary should return all items', () => {
    gridComponent.items = [ 
      { foo: 'foo1', bar: 'bar1' },
      { foo: 'foo2', bar: 'bar2' },
      { foo: 'foo3', bar: 'bar3' },
      { foo: 'foo4', bar: 'bar4' },
      { foo: 'foo5', bar: 'bar5' },
      { foo: 'foo6', bar: 'bar6' }
    ];
    var pageSize = 2;
    gridComponent.pageSize = pageSize;

    gridComponent.ngOnInit();
    gridComponent.ngAfterContentInit();
    var itemsToSummarize = gridComponent.getItemsToSummarize(false);
    
    expect(itemsToSummarize.length).toEqual(gridComponent.items.length);
    expect(itemsToSummarize[0]).toEqual(gridComponent.items[0]);
    expect(itemsToSummarize[5]).toEqual(gridComponent.items[5]);
  });

  it('getItemsToSummarize with grouped items and global summary should return all items', () => {
    var column: GridColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    columnsService.columns = [column];
    gridComponent.items = [ 
      { foo: 'foo1', bar: 'bar1' },
      { foo: 'foo2', bar: 'bar2' },
      { foo: 'foo3', bar: 'bar3' },
      { foo: 'foo4', bar: 'bar4' },
      { foo: 'foo5', bar: 'bar5' },
      { foo: 'foo6', bar: 'bar6' }
    ];
    var pageSize = 2;
    gridComponent.pageSize = pageSize;

    gridComponent.ngOnInit();
    groupService.groupState.next(new GroupState(column.columnId, 'foo', true));

    fixture.whenStable().then(() => {
      var itemsToSummarize = gridComponent.getItemsToSummarize(false);
      expect(itemsToSummarize.length).toEqual(gridComponent.items.length);
    });
  });

  it('getItemsToSummarize with grouped items and per page summary should return all items on the page', () => {
    var column: GridColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    columnsService.columns = [column];
    gridComponent.items = [ 
      { foo: 'foo1', bar: 'bar1' },
      { foo: 'foo2', bar: 'bar2' },
      { foo: 'foo3', bar: 'bar3' },
      { foo: 'foo4', bar: 'bar4' },
      { foo: 'foo5', bar: 'bar5' },
      { foo: 'foo6', bar: 'bar6' }
    ];
    var pageSize = 2;
    gridComponent.pageSize = pageSize;

    gridComponent.ngOnInit();
    groupService.groupState.next(new GroupState(column.columnId, 'foo', true));

    fixture.whenStable().then(() => {
      var itemsToSummarize = gridComponent.getItemsToSummarize(true);
      expect(itemsToSummarize.length).toEqual(pageSize);
    });
  });

  it('deleteRow with disabled server-side mode and no deleteItemCallback should remove the item', () => {
    var column: TextColumnDirective = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    column.propertyName = 'foo';
    columnsService.columns = [column];
    var key = 'foo';
    gridComponent.key = key;
    gridComponent.items = [ 
      { foo: 'foo1', bar: 'bar1' },
      { foo: 'foo2', bar: 'bar2' },
      { foo: 'foo3', bar: 'bar3' },
      { foo: 'foo4', bar: 'bar4' },
      { foo: 'foo5', bar: 'bar5' },
      { foo: 'foo6', bar: 'bar6' }
    ];
    var deletedRowKey = 'foo4';

    gridComponent.ngOnInit();
    gridComponent.ngAfterContentInit();
    rowDeletionService.beforeRowDelete.next(deletedRowKey);

    fixture.whenStable().then(() => {
      expect(gridComponent.gridItems.length).toEqual(5);
      var deletedRows = gridComponent.gridItems.filter((item) => { 
        return item[key] == deletedRowKey; 
      });

      expect(deletedRows.length).toEqual(0);
    });
  });

  it('deleteRow with disabled server-side mode with successful deleteItemCallback should remove the item', () => {
    var key = 'foo';
    gridComponent.key = key;
    gridComponent.items = [ 
      { foo: 'foo1', bar: 'bar1' },
      { foo: 'foo2', bar: 'bar2' },
      { foo: 'foo3', bar: 'bar3' },
      { foo: 'foo4', bar: 'bar4' },
      { foo: 'foo5', bar: 'bar5' },
      { foo: 'foo6', bar: 'bar6' }
    ];
    gridComponent.deleteItemCallback = (deletedRowKey) => { return Observable.of(); };
    var deletedRowKey = 'foo4';

    gridComponent.ngOnInit();
    gridComponent.ngAfterContentInit();
    rowDeletionService.beforeRowDelete.next(deletedRowKey);

    fixture.whenStable().then(() => {
      expect(gridComponent.gridItems.length).toEqual(5);
      var deletedRows = gridComponent.gridItems.filter((item) => { 
        return item[key] == deletedRowKey; 
      });

      expect(deletedRows.length).toEqual(0);
    });
  });

  it('deleteRow with disabled server-side mode with unsuccessful deleteItemCallback should not remove the item', () => {
    var key = 'foo';
    gridComponent.key = key;
    gridComponent.items = [ 
      { foo: 'foo1', bar: 'bar1' },
      { foo: 'foo2', bar: 'bar2' },
      { foo: 'foo3', bar: 'bar3' },
      { foo: 'foo4', bar: 'bar4' },
      { foo: 'foo5', bar: 'bar5' },
      { foo: 'foo6', bar: 'bar6' }
    ];
    gridComponent.deleteItemCallback = (deletedRowKey) => { return Observable.throw('sample error'); };
    var deletedRowKey = 'foo4';

    gridComponent.ngOnInit();
    gridComponent.ngAfterContentInit();
    rowDeletionService.beforeRowDelete.next(deletedRowKey);

    fixture.whenStable().then(() => {
      expect(gridComponent.gridItems.length).toEqual(6);
      var deletedRows = gridComponent.gridItems.filter((item) => { 
        return item[key] == deletedRowKey; 
      });

      expect(deletedRows.length).toEqual(1);
    });
  });

  it('deleteRow with enabled server-side mode with unsuccessful deleteItemCallback should not refresh', () => {
    gridComponent.enableServerSideMode = true;
    var key = 'foo';
    gridComponent.key = key;
    gridComponent.items = [ 
      { foo: 'foo1', bar: 'bar1' },
      { foo: 'foo2', bar: 'bar2' },
      { foo: 'foo3', bar: 'bar3' },
      { foo: 'foo4', bar: 'bar4' },
      { foo: 'foo5', bar: 'bar5' },
      { foo: 'foo6', bar: 'bar6' }
    ];
    gridComponent.deleteItemCallback = (deletedRowKey) => { return Observable.throw('sample error'); };
    gridComponent.onGridStateChange = (gridState: GridState) => { 
      return Observable.of({ items: [], count: 0, summaryResults: [] }) 
    };
    var stateChangeSpy: jasmine.Spy = spyOn(gridComponent, 'onGridStateChange').and.callThrough();
    var deletedRowKey = 'foo4';

    gridComponent.ngOnInit();
    gridComponent.ngAfterContentInit();
    stateChangeSpy.calls.reset();
    rowDeletionService.beforeRowDelete.next(deletedRowKey);

    fixture.whenStable().then(() => {
      expect(stateChangeSpy).not.toHaveBeenCalled();
    });
  });

  it('deleteRow with enabled server-side mode with successful deleteItemCallback should refresh', () => {
    gridComponent.enableServerSideMode = true;
    var key = 'foo';
    gridComponent.key = key;
    gridComponent.items = [ 
      { foo: 'foo1', bar: 'bar1' },
      { foo: 'foo2', bar: 'bar2' },
      { foo: 'foo3', bar: 'bar3' },
      { foo: 'foo4', bar: 'bar4' },
      { foo: 'foo5', bar: 'bar5' },
      { foo: 'foo6', bar: 'bar6' }
    ];
    gridComponent.deleteItemCallback = (deletedRowKey) => { return Observable.of(); };
    gridComponent.onGridStateChange = (gridState: GridState) => { 
      return Observable.of({ items: [], count: 0, summaryResults: [] }) 
    };
    var stateChangeSpy: jasmine.Spy = spyOn(gridComponent, 'onGridStateChange').and.callThrough();
    var deletedRowKey = 'foo4';

    gridComponent.ngOnInit();
    gridComponent.ngAfterContentInit();
    stateChangeSpy.calls.reset();
    rowDeletionService.beforeRowDelete.next(deletedRowKey);

    fixture.whenStable().then(() => {
      expect(stateChangeSpy).toHaveBeenCalled();
    });
  });

  it('addItem should add the item and refresh', () => {
    var key = 'foo';
    gridComponent.key = key;
    gridComponent.items = [ 
      { foo: 'foo1', bar: 'bar1' },
      { foo: 'foo2', bar: 'bar2' },
      { foo: 'foo3', bar: 'bar3' },
      { foo: 'foo4', bar: 'bar4' },
      { foo: 'foo5', bar: 'bar5' },
      { foo: 'foo6', bar: 'bar6' }
    ];
    var newItem = { foo: 'foo7', bar: 'bar7' };

    gridComponent.ngOnInit();
    gridComponent.ngAfterContentInit();
    rowEditionService.itemAdded.next(newItem);

    fixture.whenStable().then(() => {
      expect(gridComponent.items.length).toEqual(7);
    });
  });

  it('life cycle state events should be fired when the grid is in client side mode', () => {
    var key = 'foo';
    gridComponent.key = key;
    gridComponent.items = [];
    gridComponent.enableServerSideMode = false;
    var beforeFilterSpy: jasmine.Spy = spyOn(gridComponent.beforeFiltering, 'next');
    var beforeGroupingSpy: jasmine.Spy = spyOn(gridComponent.beforeGrouping, 'next');
    var beforeSortingSpy: jasmine.Spy = spyOn(gridComponent.beforeSorting, 'next');
    var beforePagingSpy: jasmine.Spy = spyOn(gridComponent.beforePaging, 'next');

    fixture.detectChanges();
    gridComponent.api.refresh();
    
    expect(beforeFilterSpy).toHaveBeenCalled();
    expect(beforeGroupingSpy).toHaveBeenCalled();
    expect(beforeSortingSpy).toHaveBeenCalled();
    expect(beforePagingSpy).toHaveBeenCalled();
  });

  it('only life cycle state events after the specified one should be fired when the grid is in client side mode', () => {
    var key = 'foo';
    gridComponent.key = key;
    gridComponent.items = [];
    gridComponent.enableServerSideMode = false;
    var beforeFilterSpy: jasmine.Spy = spyOn(gridComponent.beforeFiltering, 'next');
    var beforeGroupingSpy: jasmine.Spy = spyOn(gridComponent.beforeGrouping, 'next');
    var beforeSortingSpy: jasmine.Spy = spyOn(gridComponent.beforeSorting, 'next');
    var beforePagingSpy: jasmine.Spy = spyOn(gridComponent.beforePaging, 'next');

    fixture.detectChanges();
    beforeFilterSpy.calls.reset();
    beforeGroupingSpy.calls.reset();
    beforeSortingSpy.calls.reset();
    beforePagingSpy.calls.reset();
    orderStateService.orderState.next(new OrderState('123', 'foo', true));

    fixture.whenStable().then(() => {
      expect(beforeFilterSpy).not.toHaveBeenCalled();
      expect(beforeGroupingSpy).not.toHaveBeenCalled();
      expect(beforeSortingSpy).toHaveBeenCalled();
      expect(beforePagingSpy).toHaveBeenCalled();
    });
  });

  it('refresh with server-side mode enabled should set the grid items and items count to the values returned by onGridStateChange callback', () => {
    var key = 'foo';
    gridComponent.key = key;
    gridComponent.enableServerSideMode = true;
    gridComponent.pageSize = 5;
    var items: any[] = [];
    var itemsCount = 10;
    gridComponent.onGridStateChange = (gridState: GridState) => {
      return Observable.of({
        items: items,
        count: itemsCount
      });
    };
    var itemsCountSpy: jasmine.Spy = spyOn(paginationService.itemsCount, 'next');

    fixture.detectChanges();
    gridComponent.api.refresh();
    
    fixture.whenStable().then(() => {
      expect(itemsCountSpy).toHaveBeenCalled();
      expect(itemsCountSpy.calls.first().args[0]).toEqual(itemsCount);
      expect(gridComponent.gridItems).toEqual(items);
    });
  });
});