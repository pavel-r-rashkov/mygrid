import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { ColumnReorderEvent } from './column-reorder-event';
import { GridColumn } from '../grid-columns/grid-column';

@Injectable()
export class ColumnOrderingService {
  public columnReorder: Subject<ColumnReorderEvent>;
  
  constructor() {
    this.columnReorder = new Subject<ColumnReorderEvent>();
  }

  raiseColumnReorderedEvent(reorderEvent: ColumnReorderEvent) {
    this.columnReorder.next(reorderEvent);
  }

  reorderColumns(reorderEvent: ColumnReorderEvent, columns: GridColumn[]) {
    let movedColumnIndex: number = null;
    let movedColumn = columns.find((column, index) => {
      movedColumnIndex = index;
      return column.columnId == reorderEvent.movedColumn.columnId;
    });

    let targetColumnIndex: number = null;
    let targetColumn = columns.find((column, index) => {
      targetColumnIndex = index;
      return column.columnId == reorderEvent.targetColumn.columnId;
    });

    if (movedColumnIndex < targetColumnIndex) {
      if (reorderEvent.isMovedInFront) {
        for (var i: number = movedColumnIndex; i < targetColumnIndex - 1; ++i) {
          columns[i] = columns[i + 1];
        }
        columns[targetColumnIndex - 1] = movedColumn;
      } else {
        for (var i = movedColumnIndex; i < targetColumnIndex; ++i) {
          columns[i] = columns[i + 1];
        }
        columns[targetColumnIndex] = movedColumn;
      }
    } else if (movedColumnIndex > targetColumnIndex) {
      if (reorderEvent.isMovedInFront) {
        for (var i = movedColumnIndex; i > targetColumnIndex; --i) {
          columns[i] = columns[i - 1];
        }
        columns[targetColumnIndex] = movedColumn;
      } else {
        for (var i = movedColumnIndex; i > (targetColumnIndex + 1); --i) {
          columns[i] = columns[i - 1];
        }
        columns[targetColumnIndex + 1] = movedColumn;
      }
    }
  }
}