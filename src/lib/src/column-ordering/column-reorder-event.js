"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ColumnReorderEvent = (function () {
    function ColumnReorderEvent(movedColumn, targetColumn, isMovedInFront) {
        this.movedColumn = movedColumn;
        this.targetColumn = targetColumn;
        this.isMovedInFront = isMovedInFront;
    }
    return ColumnReorderEvent;
}());
exports.ColumnReorderEvent = ColumnReorderEvent;
//# sourceMappingURL=column-reorder-event.js.map