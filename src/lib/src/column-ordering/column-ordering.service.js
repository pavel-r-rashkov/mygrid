"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var ColumnOrderingService = (function () {
    function ColumnOrderingService() {
        this.columnReorder = new Subject_1.Subject();
    }
    ColumnOrderingService.prototype.raiseColumnReorderedEvent = function (reorderEvent) {
        this.columnReorder.next(reorderEvent);
    };
    ColumnOrderingService.prototype.reorderColumns = function (reorderEvent, columns) {
        var movedColumnIndex = null;
        var movedColumn = columns.find(function (column, index) {
            movedColumnIndex = index;
            return column.columnId == reorderEvent.movedColumn.columnId;
        });
        var targetColumnIndex = null;
        var targetColumn = columns.find(function (column, index) {
            targetColumnIndex = index;
            return column.columnId == reorderEvent.targetColumn.columnId;
        });
        if (movedColumnIndex < targetColumnIndex) {
            if (reorderEvent.isMovedInFront) {
                for (var i = movedColumnIndex; i < targetColumnIndex - 1; ++i) {
                    columns[i] = columns[i + 1];
                }
                columns[targetColumnIndex - 1] = movedColumn;
            }
            else {
                for (var i = movedColumnIndex; i < targetColumnIndex; ++i) {
                    columns[i] = columns[i + 1];
                }
                columns[targetColumnIndex] = movedColumn;
            }
        }
        else if (movedColumnIndex > targetColumnIndex) {
            if (reorderEvent.isMovedInFront) {
                for (var i = movedColumnIndex; i > targetColumnIndex; --i) {
                    columns[i] = columns[i - 1];
                }
                columns[targetColumnIndex] = movedColumn;
            }
            else {
                for (var i = movedColumnIndex; i > (targetColumnIndex + 1); --i) {
                    columns[i] = columns[i - 1];
                }
                columns[targetColumnIndex + 1] = movedColumn;
            }
        }
    };
    ColumnOrderingService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], ColumnOrderingService);
    return ColumnOrderingService;
}());
exports.ColumnOrderingService = ColumnOrderingService;
//# sourceMappingURL=column-ordering.service.js.map