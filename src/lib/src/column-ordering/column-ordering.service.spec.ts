import { ColumnOrderingService } from './column-ordering.service';
import { ColumnReorderEvent } from './column-reorder-event';
import { GridColumn } from '../grid-columns/grid-column';
import { TextColumnDirective } from '../grid-columns/text-column/text-column.directive';
import { GuidService } from '../guid';
import { DefaultColumnHeaderComponent } from '../grid-columns/shared/default-column-header.component';
import { TextColumnFilterComponent } from '../grid-columns/text-column/text-column-filter.component';
import { TextColumnDataComponent } from '../grid-columns/text-column/text-column-data.component';
import { TextColumnEditComponent } from '../grid-columns/text-column/text-column-edit.component';
import { DefaultComparerService } from '../grid-columns/shared/default-comparer.service';

describe('Service: ColumnOrderingService', () => {
  let columnOrderingService: ColumnOrderingService;
  let columns: GridColumn[];
  let firstColumn: GridColumn;
  let secondColumn: GridColumn;
  let thirdColumn: GridColumn;

  beforeEach(() => {
    columnOrderingService = new ColumnOrderingService();
    firstColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    secondColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    thirdColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());

    columns = [
      firstColumn,
      secondColumn,
      thirdColumn
    ];
  });

  it('Move last column in front of first', () => {
    var movedColumn = {
      columnId: columns[2].columnId,
      propertyName: ''
    };

    var targedColumn = {
      columnId: columns[0].columnId,
      propertyName: ''
    };

    var reorderEvent = new ColumnReorderEvent(movedColumn, targedColumn, true);
    var middleColumn = columns[1];
    columnOrderingService.reorderColumns(reorderEvent, columns);
    expect(columns[0].columnId).toEqual(movedColumn.columnId);
    expect(columns[1].columnId).toEqual(targedColumn.columnId);
    expect(columns[2].columnId).toEqual(middleColumn.columnId);
  });

  it('Move last column after first', () => {
    var movedColumn = {
      columnId: columns[2].columnId,
      propertyName: ''
    };

    var targedColumn = {
      columnId: columns[0].columnId,
      propertyName: ''
    };

    var reorderEvent = new ColumnReorderEvent(movedColumn, targedColumn, false);
    var middleColumn = columns[1];
    columnOrderingService.reorderColumns(reorderEvent, columns);
    expect(columns[0].columnId).toEqual(targedColumn.columnId);
    expect(columns[1].columnId).toEqual(movedColumn.columnId);
    expect(columns[2].columnId).toEqual(middleColumn.columnId);
  });

  it('Move first column in front of last', () => {
    var movedColumn = {
      columnId: columns[0].columnId,
      propertyName: ''
    };

    var targedColumn = {
      columnId: columns[2].columnId,
      propertyName: ''
    };

    var reorderEvent = new ColumnReorderEvent(movedColumn, targedColumn, true);
    var middleColumn = columns[1];
    columnOrderingService.reorderColumns(reorderEvent, columns);
    expect(columns[0].columnId).toEqual(middleColumn.columnId);
    expect(columns[1].columnId).toEqual(movedColumn.columnId);
    expect(columns[2].columnId).toEqual(targedColumn.columnId);
  });

  it('Move first column after last', () => {
    var movedColumn = {
      columnId: columns[0].columnId,
      propertyName: ''
    };

    var targedColumn = {
      columnId: columns[2].columnId,
      propertyName: ''
    };

    var reorderEvent = new ColumnReorderEvent(movedColumn, targedColumn, false);
    var middleColumn = columns[1];
    columnOrderingService.reorderColumns(reorderEvent, columns);
    expect(columns[0].columnId).toEqual(middleColumn.columnId);
    expect(columns[1].columnId).toEqual(targedColumn.columnId);
    expect(columns[2].columnId).toEqual(movedColumn.columnId);
  });
});