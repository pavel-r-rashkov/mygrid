"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var column_ordering_service_1 = require("./column-ordering.service");
var column_reorder_event_1 = require("./column-reorder-event");
var text_column_directive_1 = require("../grid-columns/text-column/text-column.directive");
var guid_1 = require("../guid");
var default_comparer_service_1 = require("../grid-columns/shared/default-comparer.service");
describe('Service: ColumnOrderingService', function () {
    var columnOrderingService;
    var columns;
    var firstColumn;
    var secondColumn;
    var thirdColumn;
    beforeEach(function () {
        columnOrderingService = new column_ordering_service_1.ColumnOrderingService();
        firstColumn = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        secondColumn = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        thirdColumn = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        columns = [
            firstColumn,
            secondColumn,
            thirdColumn
        ];
    });
    it('Move last column in front of first', function () {
        var movedColumn = {
            columnId: columns[2].columnId,
            propertyName: ''
        };
        var targedColumn = {
            columnId: columns[0].columnId,
            propertyName: ''
        };
        var reorderEvent = new column_reorder_event_1.ColumnReorderEvent(movedColumn, targedColumn, true);
        var middleColumn = columns[1];
        columnOrderingService.reorderColumns(reorderEvent, columns);
        expect(columns[0].columnId).toEqual(movedColumn.columnId);
        expect(columns[1].columnId).toEqual(targedColumn.columnId);
        expect(columns[2].columnId).toEqual(middleColumn.columnId);
    });
    it('Move last column after first', function () {
        var movedColumn = {
            columnId: columns[2].columnId,
            propertyName: ''
        };
        var targedColumn = {
            columnId: columns[0].columnId,
            propertyName: ''
        };
        var reorderEvent = new column_reorder_event_1.ColumnReorderEvent(movedColumn, targedColumn, false);
        var middleColumn = columns[1];
        columnOrderingService.reorderColumns(reorderEvent, columns);
        expect(columns[0].columnId).toEqual(targedColumn.columnId);
        expect(columns[1].columnId).toEqual(movedColumn.columnId);
        expect(columns[2].columnId).toEqual(middleColumn.columnId);
    });
    it('Move first column in front of last', function () {
        var movedColumn = {
            columnId: columns[0].columnId,
            propertyName: ''
        };
        var targedColumn = {
            columnId: columns[2].columnId,
            propertyName: ''
        };
        var reorderEvent = new column_reorder_event_1.ColumnReorderEvent(movedColumn, targedColumn, true);
        var middleColumn = columns[1];
        columnOrderingService.reorderColumns(reorderEvent, columns);
        expect(columns[0].columnId).toEqual(middleColumn.columnId);
        expect(columns[1].columnId).toEqual(movedColumn.columnId);
        expect(columns[2].columnId).toEqual(targedColumn.columnId);
    });
    it('Move first column after last', function () {
        var movedColumn = {
            columnId: columns[0].columnId,
            propertyName: ''
        };
        var targedColumn = {
            columnId: columns[2].columnId,
            propertyName: ''
        };
        var reorderEvent = new column_reorder_event_1.ColumnReorderEvent(movedColumn, targedColumn, false);
        var middleColumn = columns[1];
        columnOrderingService.reorderColumns(reorderEvent, columns);
        expect(columns[0].columnId).toEqual(middleColumn.columnId);
        expect(columns[1].columnId).toEqual(targedColumn.columnId);
        expect(columns[2].columnId).toEqual(movedColumn.columnId);
    });
});
//# sourceMappingURL=column-ordering.service.spec.js.map