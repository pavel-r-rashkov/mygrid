export class ColumnReorderEvent {
  public movedColumn: any;
  public targetColumn: any;
  public isMovedInFront: boolean;

  constructor(movedColumn: any, targetColumn: any, isMovedInFront: boolean) {
    this.movedColumn = movedColumn;
    this.targetColumn = targetColumn;
    this.isMovedInFront = isMovedInFront;
  }
}