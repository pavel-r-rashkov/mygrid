"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var grid_column_1 = require("./grid-columns/grid-column");
var filter_state_service_1 = require("./filter-state.service");
var order_state_service_1 = require("./order-state.service");
var pagination_service_1 = require("./pagination/pagination.service");
var pagination_state_1 = require("./pagination/pagination-state");
var group_service_1 = require("./grouping/group.service");
var summary_item_directive_1 = require("./summary/summary-item.directive");
var column_ordering_service_1 = require("./column-ordering/column-ordering.service");
var row_edition_service_1 = require("./edition/row-edition.service");
var row_deletion_service_1 = require("./edition/row-deletion.service");
var detail_grid_directive_1 = require("./master-detail/detail-grid.directive");
var grid_settings_service_1 = require("./grid-settings.service");
var html_table_element_service_1 = require("./html-table-elements/html-table-element.service");
var life_cycle_state_1 = require("./states/life-cycle-state");
var grid_state_1 = require("./states/grid-state");
var summary_state_1 = require("./states/summary-state");
var master_detail_service_1 = require("./master-detail/master-detail.service");
var selection_service_1 = require("./selection/selection.service");
var columns_service_1 = require("./columns.service");
var grid_api_service_1 = require("./grid-api.service");
var html_element_type_1 = require("./html-table-elements/html-element-type");
var scroll_measure_service_1 = require("./scroll-measure.service");
var guid_1 = require("./guid");
var summary_service_1 = require("./summary/summary.service");
var default_comparer_service_1 = require("./grid-columns/shared/default-comparer.service");
var MyGridComponent = (function () {
    function MyGridComponent(filterStateService, orderStateService, paginationService, groupService, columnOrderingService, rowEditionService, rowDeletionService, gridSettingsService, tableElementService, masterDetailService, selectionService, columnsService, gridApi, scrollMeasureService) {
        var _this = this;
        this.filterStateService = filterStateService;
        this.orderStateService = orderStateService;
        this.paginationService = paginationService;
        this.groupService = groupService;
        this.columnOrderingService = columnOrderingService;
        this.rowEditionService = rowEditionService;
        this.rowDeletionService = rowDeletionService;
        this.gridSettingsService = gridSettingsService;
        this.tableElementService = tableElementService;
        this.masterDetailService = masterDetailService;
        this.selectionService = selectionService;
        this.columnsService = columnsService;
        this.gridApi = gridApi;
        this.scrollMeasureService = scrollMeasureService;
        this.lifeCycleStateOrder = [];
        this.isLoading = false;
        this.serverSideSummaryResults = [];
        this.filteredItems = [];
        this.groupedItems = [];
        this.gridItems = [];
        this.htmlElementType = html_element_type_1.HtmlElementType;
        this.items = [];
        this.enableGrouping = false;
        this.showFilterRow = true;
        this.showAddRow = false;
        this.showAddRowToggle = false;
        this.showClearFilterButton = true;
        this.dropColumnText = 'drag and drop columns here to group';
        this.removeGroupingButtonText = 'Remove grouping';
        this.noDataText = 'No data to display';
        this.showLoadingBar = false;
        this.enableHighlighting = false;
        this.selectionChanged = new core_1.EventEmitter();
        this.beforeFiltering = new core_1.EventEmitter();
        this.beforeGrouping = new core_1.EventEmitter();
        this.beforeSorting = new core_1.EventEmitter();
        this.beforePaging = new core_1.EventEmitter();
        this.columns = new core_1.QueryList();
        this.summaryItems = new core_1.QueryList();
        this.scrollWidth = (this.scrollMeasureService.measureScrollWidth() - 1).toString() + 'px';
        this.initLyfeCycleStates();
        this.columns = new core_1.QueryList();
        this.filterStateSubscription = this.filterStateService.filterState.subscribe(function (newState) {
            _this.currentFilterState = newState;
            _this.refresh();
        });
        this.orderStateSubscription = this.orderStateService.orderState.subscribe(function (newState) {
            _this.currentOrderState = newState;
            _this.refresh(life_cycle_state_1.LifeCycleState.Sorting);
        });
        this.paginationStateSubscription = this.paginationService.paginationState.subscribe(function (newState) {
            _this.currentPaginationState = newState;
            _this.refresh(life_cycle_state_1.LifeCycleState.Pagination);
        });
        this.groupStateSubscription = this.groupService.groupState.subscribe(function (newState) {
            _this.currentGroupState = newState;
            _this.refresh();
        });
        this.columnOrderingSubscription = this.columnOrderingService.columnReorder.subscribe(function (columnReorderEvent) {
            _this.reorderColumns(columnReorderEvent);
        });
        this.currentEditionState = this.rowEditionService.rowEdition.getValue();
        this.editionStateSubscription = this.rowEditionService.rowEdition.subscribe(function (newState) {
            _this.currentEditionState = newState;
        });
        this.beforeRowDeleteSubscription = this.rowDeletionService.beforeRowDelete.subscribe(function (deletedRowKey) {
            _this.deleteRow(deletedRowKey);
        });
        this.itemAddedSubscription = this.rowEditionService.itemAdded.subscribe(function (newItem) {
            _this.addItem(newItem);
        });
        this.detailsStateSubscription = this.masterDetailService.detailsStateSubject.subscribe(function (newState) {
            _this.currentDetailState = newState;
        });
        this.selectionStateSubscription = this.selectionService.selectionSubject.subscribe(function (newState) {
            _this.selectionChanged.next(newState);
        });
    }
    ;
    Object.defineProperty(MyGridComponent.prototype, "api", {
        get: function () {
            return this.gridApi;
        },
        enumerable: true,
        configurable: true
    });
    MyGridComponent.prototype.ngOnInit = function () {
        this.gridSettingsService.key = this.key;
        this.gridSettingsService.enableServerSideMode = this.enableServerSideMode == null ? false : this.enableServerSideMode;
        this.rowEditionService.saveChangesCallback = this.saveChangesCallback;
        this.rowEditionService.initFormCallback = this.initFormCallback;
        this.rowEditionService.getErrorMessages = this.getErrorMessages;
        this.tableElementService.initCellCallback = this.initCellCallback;
        this.paginationService.initialPageSize = this.pageSize;
        this.currentPaginationState = this.pageSize != null ? new pagination_state_1.PaginationState(1, this.pageSize) : null;
        this.createApi();
    };
    MyGridComponent.prototype.ngAfterContentInit = function () {
        this.columnsService.columns = this.columns.toArray();
        this.refresh();
    };
    MyGridComponent.prototype.ngOnChanges = function (changes) {
        if (changes != null
            && changes['pageSize'] != null
            && !changes['pageSize'].isFirstChange()) {
            this.paginationService.paginationState.next(new pagination_state_1.PaginationState(this.currentPaginationState.currentPage, this.pageSize));
        }
    };
    MyGridComponent.prototype.ngOnDestroy = function () {
        this.filterStateSubscription.unsubscribe();
        this.orderStateSubscription.unsubscribe();
        this.paginationStateSubscription.unsubscribe();
        this.groupStateSubscription.unsubscribe();
        this.columnOrderingSubscription.unsubscribe();
        this.editionStateSubscription.unsubscribe();
        this.beforeRowDeleteSubscription.unsubscribe();
        this.itemAddedSubscription.unsubscribe();
        this.detailsStateSubscription.unsubscribe();
        this.selectionStateSubscription.unsubscribe();
    };
    MyGridComponent.prototype.getItemRows = function (item) {
        if (this.shouldRenderGroupedLayout()) {
            return item[this.groupService.GROUPED_ITEMS_KEY];
        }
        return [item];
    };
    MyGridComponent.prototype.shouldRenderGroupedLayout = function () {
        return this.currentGroupState != null;
    };
    MyGridComponent.prototype.getVisibleIndex = function (gridItemIndex, rowIndex) {
        if (this.shouldRenderGroupedLayout()) {
            var previousGroupsRowCount = 0;
            for (var i = 0; i < gridItemIndex; i++) {
                previousGroupsRowCount += this.gridItems[i][this.groupService.GROUPED_ITEMS_KEY].length;
            }
            return previousGroupsRowCount + rowIndex + 1;
        }
        else {
            return gridItemIndex + 1;
        }
    };
    MyGridComponent.prototype.toggleGroup = function (group) {
        group.isOpened = !group.isOpened;
    };
    MyGridComponent.prototype.shouldRenderColumn = function (column) {
        var isColumnGrouped = this.currentGroupState != null && this.currentGroupState.columnId == column.columnId;
        return column.isVisible && !isColumnGrouped;
    };
    MyGridComponent.prototype.getRenderableColumns = function () {
        var _this = this;
        var renderableColumns = this.columns.filter(function (column) {
            return _this.shouldRenderColumn(column);
        });
        return renderableColumns;
    };
    MyGridComponent.prototype.getRenderableColumnCount = function () {
        return this.getRenderableColumns().length;
    };
    MyGridComponent.prototype.isRowEdited = function (item) {
        var itemKey = item[this.key];
        var isRowEdited = this.currentEditionState.currentlyEditedRows.indexOf(itemKey) >= 0;
        return isRowEdited;
    };
    MyGridComponent.prototype.getSummaryItems = function (propertyName) {
        var _this = this;
        var summaryItems = [];
        this.summaryItems.forEach(function (summaryItem) {
            if (summaryItem.propertyName != propertyName) {
                return;
            }
            var serverSideResults = _this.serverSideSummaryResults.filter(function (serverSideSummaryResult) {
                return serverSideSummaryResult.propertyName == propertyName
                    && serverSideSummaryResult.summaryType == summaryItem.summaryType;
            });
            var serverSideSummary = null;
            if (serverSideResults.length > 0) {
                serverSideSummary = serverSideResults[0].summaryValue;
            }
            summaryItems.push({
                serverSideSummary: serverSideSummary,
                summaryItem: summaryItem
            });
        });
        return summaryItems;
    };
    MyGridComponent.prototype.getItemsToSummarize = function (isPerPageSummary) {
        if (this.gridSettingsService.enableServerSideMode && !isPerPageSummary) {
            return [];
        }
        if (this.currentGroupState == null) {
            return isPerPageSummary ? this.gridItems : this.groupedItems;
        }
        else {
            var itemsToSummarize = [];
            var sourceItems = isPerPageSummary ? this.gridItems : this.groupedItems;
            for (var i = 0; i < sourceItems.length; ++i) {
                itemsToSummarize.push.apply(itemsToSummarize, sourceItems[i][this.groupService.GROUPED_ITEMS_KEY]);
            }
            return itemsToSummarize;
        }
    };
    MyGridComponent.prototype.shouldRenderDetailGrid = function (item) {
        var key = item[this.gridSettingsService.key];
        return this.currentDetailState.openedDetails.indexOf(key) >= 0;
    };
    MyGridComponent.prototype.clearFilters = function () {
        this.filterStateService.filterState.next(null);
    };
    MyGridComponent.prototype.toggleAddRow = function () {
        this.showAddRow = !this.showAddRow;
    };
    MyGridComponent.prototype.getGroupedColumnData = function () {
        var _this = this;
        var groupedColumn = this.columnsService.columns.find(function (col) {
            return col.columnId == _this.currentGroupState.columnId;
        });
        return groupedColumn.getGroupData();
    };
    MyGridComponent.prototype.getGroupedColumnDataComponentType = function () {
        var _this = this;
        var groupedColumn = this.columnsService.columns.find(function (col) {
            return col.columnId == _this.currentGroupState.columnId;
        });
        return groupedColumn.dataComponentType;
    };
    MyGridComponent.prototype.refresh = function (startState) {
        var _this = this;
        if (startState === void 0) { startState = life_cycle_state_1.LifeCycleState.Filtering; }
        if (this.gridSettingsService.enableServerSideMode) {
            var summaryStates_1 = [];
            var globalSummaryItems = this.summaryItems.filter(function (summaryItem) {
                return !summaryItem.isPerPageSummary;
            }).forEach(function (summaryItem) {
                summaryStates_1.push(new summary_state_1.SummaryState(summaryItem.propertyName, summaryItem.summaryType));
            });
            var gridState = new grid_state_1.GridState(this.currentFilterState, this.currentGroupState, this.currentOrderState, this.currentPaginationState, summaryStates_1);
            this.isLoading = true;
            this.onGridStateChange(gridState)
                .toPromise()
                .then(function (result) {
                _this.gridItems = result.items;
                if (_this.pageSize != null) {
                    _this.paginationService.itemsCount.next(result.count);
                }
                _this.serverSideSummaryResults = result.summaryResults || [];
                _this.isLoading = false;
            })
                .catch(function (err) {
                console.error(err);
                _this.isLoading = false;
            });
        }
        else {
            for (var i = startState; i < this.lifeCycleStateOrder.length; ++i) {
                this.lifeCycleStateOrder[i]();
            }
        }
    };
    MyGridComponent.prototype.getFilteredItems = function (items) {
        if (this.currentFilterState == null) {
            return items;
        }
        var filteredItems = this.filterStateService.filterItems(items, this.currentFilterState, this.columns.toArray());
        return filteredItems;
    };
    MyGridComponent.prototype.sortItems = function (itemsToSort) {
        if (this.currentOrderState == null) {
            return;
        }
        if (this.currentGroupState != null && this.currentOrderState.columnId == this.currentGroupState.columnId) {
            return;
        }
        this.orderStateService.sortItems(itemsToSort, this.currentOrderState, this.currentGroupState, this.columns.toArray());
    };
    MyGridComponent.prototype.groupItems = function (itemsToGroup) {
        if (this.currentGroupState != null) {
            return this.groupService.groupItems(itemsToGroup, this.currentGroupState, this.columnsService.columns);
        }
        return itemsToGroup;
    };
    MyGridComponent.prototype.applyPagination = function (itemsToPage) {
        if (this.currentPaginationState == null) {
            return itemsToPage;
        }
        else {
            return this.paginationService.applyPagination(itemsToPage, this.currentPaginationState);
        }
    };
    MyGridComponent.prototype.initLyfeCycleStates = function () {
        var _this = this;
        this.lifeCycleStateOrder[life_cycle_state_1.LifeCycleState.Filtering] = function () {
            console.log('filtering');
            _this.beforeFiltering.next(_this.items);
            _this.filteredItems = _this.getFilteredItems(_this.items);
        };
        this.lifeCycleStateOrder[life_cycle_state_1.LifeCycleState.Grouping] = function () {
            _this.beforeGrouping.next(_this.filteredItems);
            _this.groupedItems = _this.groupItems(_this.filteredItems);
            _this.paginationService.itemsCount.next(_this.groupedItems.length);
        };
        this.lifeCycleStateOrder[life_cycle_state_1.LifeCycleState.Sorting] = function () {
            _this.beforeSorting.next(_this.groupedItems);
            _this.sortItems(_this.groupedItems);
        };
        this.lifeCycleStateOrder[life_cycle_state_1.LifeCycleState.Pagination] = function () {
            _this.beforePaging.next(_this.groupedItems);
            _this.gridItems = _this.applyPagination(_this.groupedItems);
        };
    };
    MyGridComponent.prototype.reorderColumns = function (columnReorderEvent) {
        var columnsToReorder = this.columns.toArray();
        this.columnOrderingService.reorderColumns(columnReorderEvent, columnsToReorder);
        this.columns.reset(columnsToReorder);
        if (this.currentGroupState != null
            && columnReorderEvent.movedColumn.columnId == this.currentGroupState.columnId) {
            // while single grouped column is supported
            this.groupService.groupState.next(null);
        }
    };
    MyGridComponent.prototype.addItem = function (newItem) {
        this.items.push(newItem);
        this.refresh();
    };
    MyGridComponent.prototype.removeRow = function (rowKey) {
        var itemsLength = this.items.length;
        for (var i = 0; i < itemsLength; ++i) {
            if (this.items[i][this.key] == rowKey) {
                this.items.splice(i, 1);
                break;
            }
        }
    };
    MyGridComponent.prototype.deleteRow = function (deletedRowKey) {
        var _this = this;
        if (this.gridSettingsService.enableServerSideMode) {
            this.deleteItemCallback(deletedRowKey)
                .toPromise()
                .then(function () {
                _this.refresh();
            })
                .catch(function (error) {
                console.error('Error deleting item', error);
            });
        }
        else {
            if (this.deleteItemCallback != null) {
                this.deleteItemCallback(deletedRowKey)
                    .toPromise()
                    .then(function () {
                    _this.removeRow(deletedRowKey);
                    _this.refresh();
                })
                    .catch(function (error) {
                    console.error('Error deleting item', error);
                });
            }
            else {
                this.removeRow(deletedRowKey);
                this.refresh();
            }
        }
    };
    MyGridComponent.prototype.createApi = function () {
        this.gridApi.refresh = this.refresh.bind(this);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], MyGridComponent.prototype, "key", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], MyGridComponent.prototype, "width", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], MyGridComponent.prototype, "items", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], MyGridComponent.prototype, "enableServerSideMode", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], MyGridComponent.prototype, "enableGrouping", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], MyGridComponent.prototype, "showFilterRow", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], MyGridComponent.prototype, "showAddRow", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], MyGridComponent.prototype, "showAddRowToggle", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], MyGridComponent.prototype, "showClearFilterButton", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], MyGridComponent.prototype, "pageSize", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], MyGridComponent.prototype, "dropColumnText", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], MyGridComponent.prototype, "removeGroupingButtonText", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], MyGridComponent.prototype, "noDataText", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], MyGridComponent.prototype, "paginationTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], MyGridComponent.prototype, "showLoadingBar", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], MyGridComponent.prototype, "scrollHeight", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], MyGridComponent.prototype, "enableHighlighting", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], MyGridComponent.prototype, "saveChangesCallback", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], MyGridComponent.prototype, "deleteItemCallback", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], MyGridComponent.prototype, "initFormCallback", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], MyGridComponent.prototype, "getDetailItems", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], MyGridComponent.prototype, "initCellCallback", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], MyGridComponent.prototype, "getErrorMessages", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], MyGridComponent.prototype, "onGridStateChange", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], MyGridComponent.prototype, "selectionChanged", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], MyGridComponent.prototype, "beforeFiltering", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], MyGridComponent.prototype, "beforeGrouping", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], MyGridComponent.prototype, "beforeSorting", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], MyGridComponent.prototype, "beforePaging", void 0);
    __decorate([
        core_1.ContentChild(detail_grid_directive_1.DetailGridDirective),
        __metadata("design:type", detail_grid_directive_1.DetailGridDirective)
    ], MyGridComponent.prototype, "detailGrid", void 0);
    __decorate([
        core_1.ContentChildren(grid_column_1.GridColumn),
        __metadata("design:type", core_1.QueryList)
    ], MyGridComponent.prototype, "columns", void 0);
    __decorate([
        core_1.ContentChildren(summary_item_directive_1.SummaryItemDirective),
        __metadata("design:type", core_1.QueryList)
    ], MyGridComponent.prototype, "summaryItems", void 0);
    MyGridComponent = __decorate([
        core_1.Component({
            selector: 'mg-grid',
            templateUrl: './my-grid.component.html',
            providers: [
                grid_settings_service_1.GridSettingsService,
                filter_state_service_1.FilterStateService,
                order_state_service_1.OrderStateService,
                pagination_service_1.PaginationService,
                group_service_1.GroupService,
                column_ordering_service_1.ColumnOrderingService,
                row_edition_service_1.RowEditionService,
                row_deletion_service_1.RowDeletionService,
                html_table_element_service_1.HtmlTableElementService,
                master_detail_service_1.MasterDetailService,
                selection_service_1.SelectionService,
                columns_service_1.ColumnsService,
                grid_api_service_1.GridApiService,
                scroll_measure_service_1.ScrollMeasureService,
                guid_1.GuidService,
                summary_service_1.SummaryService,
                default_comparer_service_1.DefaultComparerService
            ]
        }),
        __metadata("design:paramtypes", [filter_state_service_1.FilterStateService,
            order_state_service_1.OrderStateService,
            pagination_service_1.PaginationService,
            group_service_1.GroupService,
            column_ordering_service_1.ColumnOrderingService,
            row_edition_service_1.RowEditionService,
            row_deletion_service_1.RowDeletionService,
            grid_settings_service_1.GridSettingsService,
            html_table_element_service_1.HtmlTableElementService,
            master_detail_service_1.MasterDetailService,
            selection_service_1.SelectionService,
            columns_service_1.ColumnsService,
            grid_api_service_1.GridApiService,
            scroll_measure_service_1.ScrollMeasureService])
    ], MyGridComponent);
    return MyGridComponent;
}());
exports.MyGridComponent = MyGridComponent;
//# sourceMappingURL=my-grid.component.js.map