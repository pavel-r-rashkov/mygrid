"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
var forms_2 = require("@angular/forms");
var animations_1 = require("@angular/platform-browser/animations");
var my_grid_component_1 = require("./my-grid.component");
var header_wrapper_component_1 = require("./wrappers/header-wrapper.component");
var component_host_directive_1 = require("./component-host.directive");
var data_wrapper_component_1 = require("./wrappers/data-wrapper.component");
var filter_wrapper_component_1 = require("./wrappers/filter-wrapper.component");
var pager_component_1 = require("./pagination/pager.component");
var group_drop_target_component_1 = require("./grouping/group-drop-target.component");
var edit_wrapper_component_1 = require("./wrappers/edit-wrapper.component");
var edit_row_component_1 = require("./edition/edit-row.component");
var html_table_element_directive_1 = require("./html-table-elements/html-table-element.directive");
var detail_grid_directive_1 = require("./master-detail/detail-grid.directive");
var detail_grid_wrapper_component_1 = require("./master-detail/detail-grid-wrapper.component");
var summary_item_directive_1 = require("./summary/summary-item.directive");
var summary_component_1 = require("./summary/summary.component");
var text_column_directive_1 = require("./grid-columns/text-column/text-column.directive");
var text_column_filter_component_1 = require("./grid-columns/text-column/text-column-filter.component");
var text_column_data_component_1 = require("./grid-columns/text-column/text-column-data.component");
var text_column_edit_component_1 = require("./grid-columns/text-column/text-column-edit.component");
var spin_column_directive_1 = require("./grid-columns/spin-column/spin-column.directive");
var spin_column_filter_component_1 = require("./grid-columns/spin-column/spin-column-filter.component");
var spin_column_data_component_1 = require("./grid-columns/spin-column/spin-column-data.component");
var spin_column_edit_component_1 = require("./grid-columns/spin-column/spin-column-edit.component");
var combo_box_column_directive_1 = require("./grid-columns/combo-box-column/combo-box-column.directive");
var combo_box_column_filter_component_1 = require("./grid-columns/combo-box-column/combo-box-column-filter.component");
var combo_box_column_data_component_1 = require("./grid-columns/combo-box-column/combo-box-column-data.component");
var combo_box_column_edit_component_1 = require("./grid-columns/combo-box-column/combo-box-column-edit.component");
var button_column_directive_1 = require("./grid-columns/button-column/button-column.directive");
var button_column_data_component_1 = require("./grid-columns/button-column/button-column-data.component");
var mark_column_directive_1 = require("./grid-columns/mark-column/mark-column.directive");
var mark_column_filter_component_1 = require("./grid-columns/mark-column/mark-column-filter.component");
var mark_column_data_component_1 = require("./grid-columns/mark-column/mark-column-data.component");
var mark_column_edit_component_1 = require("./grid-columns/mark-column/mark-column-edit.component");
var date_time_column_directive_1 = require("./grid-columns/date-time-column/date-time-column.directive");
var date_time_column_filter_component_1 = require("./grid-columns/date-time-column/date-time-column-filter.component");
var date_time_column_data_component_1 = require("./grid-columns/date-time-column/date-time-column-data.component");
var date_time_column_edit_component_1 = require("./grid-columns/date-time-column/date-time-column-edit.component");
var edit_column_directive_1 = require("./grid-columns/edit-column/edit-column.directive");
var edit_column_data_component_1 = require("./grid-columns/edit-column/edit-column-data.component");
var edit_column_edit_component_1 = require("./grid-columns/edit-column/edit-column-edit.component");
var delete_column_directive_1 = require("./grid-columns/delete-column/delete-column.directive");
var delete_column_data_component_1 = require("./grid-columns/delete-column/delete-column-data.component");
var details_button_column_directive_1 = require("./grid-columns/details-button-column/details-button-column.directive");
var details_button_column_data_component_1 = require("./grid-columns/details-button-column/details-button-column-data.component");
var selection_column_directive_1 = require("./grid-columns/selection-column/selection-column.directive");
var selection_column_data_component_1 = require("./grid-columns/selection-column/selection-column-data.component");
var index_column_directive_1 = require("./grid-columns/index-column/index-column.directive");
var index_column_data_component_1 = require("./grid-columns/index-column/index-column-data.component");
var default_column_header_component_1 = require("./grid-columns/shared/default-column-header.component");
var MyGridModule = (function () {
    function MyGridModule() {
    }
    MyGridModule = __decorate([
        core_1.NgModule({
            entryComponents: [
                text_column_filter_component_1.TextColumnFilterComponent,
                text_column_data_component_1.TextColumnDataComponent,
                text_column_edit_component_1.TextColumnEditComponent,
                spin_column_filter_component_1.SpinColumnFilterComponent,
                spin_column_data_component_1.SpinColumnDataComponent,
                spin_column_edit_component_1.SpinColumnEditComponent,
                combo_box_column_filter_component_1.ComboBoxColumnFilterComponent,
                combo_box_column_data_component_1.ComboBoxColumnDataComponent,
                combo_box_column_edit_component_1.ComboBoxColumnEditComponent,
                button_column_data_component_1.ButtonColumnDataComponent,
                mark_column_filter_component_1.MarkColumnFilterComponent,
                mark_column_data_component_1.MarkColumnDataComponent,
                mark_column_edit_component_1.MarkColumnEditComponent,
                date_time_column_filter_component_1.DateTimeColumnFilterComponent,
                date_time_column_data_component_1.DateTimeColumnDataComponent,
                date_time_column_edit_component_1.DateTimeColumnEditComponent,
                edit_column_data_component_1.EditColumnDataComponent,
                edit_column_edit_component_1.EditColumnEditComponent,
                delete_column_data_component_1.DeleteColumnDataComponent,
                default_column_header_component_1.DefaultColumnHeaderComponent,
                details_button_column_data_component_1.DetailsButtonColumnDataComponent,
                selection_column_data_component_1.SelectionColumnDataComponent,
                index_column_data_component_1.IndexColumnDataComponent
            ],
            declarations: [
                my_grid_component_1.MyGridComponent,
                header_wrapper_component_1.HeaderWrapperComponent,
                filter_wrapper_component_1.FilterWrapperComponent,
                data_wrapper_component_1.DataWrapperComponent,
                component_host_directive_1.ComponentHostDirective,
                html_table_element_directive_1.HtmlTableElementDirective,
                pager_component_1.PagerComponent,
                group_drop_target_component_1.GroupDropTargetComponent,
                summary_item_directive_1.SummaryItemDirective,
                summary_component_1.SummaryComponent,
                edit_wrapper_component_1.EditWrapperComponent,
                edit_row_component_1.EditRowComponent,
                detail_grid_directive_1.DetailGridDirective,
                detail_grid_wrapper_component_1.DetailGridWrapperComponent,
                text_column_directive_1.TextColumnDirective,
                text_column_filter_component_1.TextColumnFilterComponent,
                text_column_data_component_1.TextColumnDataComponent,
                text_column_edit_component_1.TextColumnEditComponent,
                spin_column_directive_1.SpinColumnDirective,
                spin_column_filter_component_1.SpinColumnFilterComponent,
                spin_column_data_component_1.SpinColumnDataComponent,
                spin_column_edit_component_1.SpinColumnEditComponent,
                combo_box_column_directive_1.ComboBoxColumnDirective,
                combo_box_column_filter_component_1.ComboBoxColumnFilterComponent,
                combo_box_column_data_component_1.ComboBoxColumnDataComponent,
                combo_box_column_edit_component_1.ComboBoxColumnEditComponent,
                button_column_directive_1.ButtonColumnDirective,
                button_column_data_component_1.ButtonColumnDataComponent,
                mark_column_directive_1.MarkColumnDirective,
                mark_column_filter_component_1.MarkColumnFilterComponent,
                mark_column_data_component_1.MarkColumnDataComponent,
                mark_column_edit_component_1.MarkColumnEditComponent,
                date_time_column_directive_1.DateTimeColumnDirective,
                date_time_column_filter_component_1.DateTimeColumnFilterComponent,
                date_time_column_data_component_1.DateTimeColumnDataComponent,
                date_time_column_edit_component_1.DateTimeColumnEditComponent,
                edit_column_directive_1.EditColumnDirective,
                edit_column_data_component_1.EditColumnDataComponent,
                edit_column_edit_component_1.EditColumnEditComponent,
                delete_column_directive_1.DeleteColumnDirective,
                delete_column_data_component_1.DeleteColumnDataComponent,
                details_button_column_directive_1.DetailsButtonColumnDirective,
                details_button_column_data_component_1.DetailsButtonColumnDataComponent,
                selection_column_directive_1.SelectionColumnDirective,
                selection_column_data_component_1.SelectionColumnDataComponent,
                index_column_directive_1.IndexColumnDirective,
                index_column_data_component_1.IndexColumnDataComponent,
                default_column_header_component_1.DefaultColumnHeaderComponent
            ],
            imports: [
                forms_1.FormsModule,
                forms_2.ReactiveFormsModule,
                common_1.CommonModule,
                animations_1.BrowserAnimationsModule
            ],
            exports: [
                my_grid_component_1.MyGridComponent,
                summary_item_directive_1.SummaryItemDirective,
                text_column_directive_1.TextColumnDirective,
                spin_column_directive_1.SpinColumnDirective,
                combo_box_column_directive_1.ComboBoxColumnDirective,
                button_column_directive_1.ButtonColumnDirective,
                mark_column_directive_1.MarkColumnDirective,
                date_time_column_directive_1.DateTimeColumnDirective,
                edit_column_directive_1.EditColumnDirective,
                delete_column_directive_1.DeleteColumnDirective,
                detail_grid_directive_1.DetailGridDirective,
                details_button_column_directive_1.DetailsButtonColumnDirective,
                selection_column_directive_1.SelectionColumnDirective,
                index_column_directive_1.IndexColumnDirective
            ],
            providers: []
        })
    ], MyGridModule);
    return MyGridModule;
}());
exports.MyGridModule = MyGridModule;
//# sourceMappingURL=my-grid.module.js.map