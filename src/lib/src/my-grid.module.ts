import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MyGridComponent } from './my-grid.component';
import { HeaderWrapperComponent } from './wrappers/header-wrapper.component';
import { ComponentHostDirective } from './component-host.directive';
import { DataWrapperComponent } from './wrappers/data-wrapper.component';
import { FilterWrapperComponent } from './wrappers/filter-wrapper.component';
import { FilterStateService } from './filter-state.service';
import { OrderStateService } from './order-state.service';
import { PaginationService } from './pagination/pagination.service';
import { GroupService } from './grouping/group.service';
import { PagerComponent } from './pagination/pager.component';
import { GroupDropTargetComponent } from './grouping/group-drop-target.component';
import { EditWrapperComponent } from './wrappers/edit-wrapper.component';

import { EditRowComponent } from './edition/edit-row.component';
import { RowEditionService } from './edition/row-edition.service';
import { RowDeletionService } from './edition/row-deletion.service';

import { ColumnOrderingService } from './column-ordering/column-ordering.service';

import { HtmlTableElementDirective } from './html-table-elements/html-table-element.directive';

import { DetailGridDirective } from './master-detail/detail-grid.directive';
import { DetailGridWrapperComponent } from './master-detail/detail-grid-wrapper.component';

import { SummaryItemDirective } from './summary/summary-item.directive';
import { SummaryComponent } from './summary/summary.component';

import { TextColumnDirective } from './grid-columns/text-column/text-column.directive';
import { TextColumnFilterComponent } from './grid-columns/text-column/text-column-filter.component';
import { TextColumnDataComponent } from './grid-columns/text-column/text-column-data.component';
import { TextColumnEditComponent } from './grid-columns/text-column/text-column-edit.component';

import { SpinColumnDirective } from './grid-columns/spin-column/spin-column.directive';
import { SpinColumnFilterComponent } from './grid-columns/spin-column/spin-column-filter.component';
import { SpinColumnDataComponent } from './grid-columns/spin-column/spin-column-data.component';
import { SpinColumnEditComponent } from './grid-columns/spin-column/spin-column-edit.component';

import { ComboBoxColumnDirective } from './grid-columns/combo-box-column/combo-box-column.directive';
import { ComboBoxColumnFilterComponent } from './grid-columns/combo-box-column/combo-box-column-filter.component';
import { ComboBoxColumnDataComponent } from './grid-columns/combo-box-column/combo-box-column-data.component';
import { ComboBoxColumnEditComponent } from './grid-columns/combo-box-column/combo-box-column-edit.component';

import { ButtonColumnDirective } from './grid-columns/button-column/button-column.directive';
import { ButtonColumnDataComponent } from './grid-columns/button-column/button-column-data.component';

import { MarkColumnDirective } from './grid-columns/mark-column/mark-column.directive';
import { MarkColumnFilterComponent } from './grid-columns/mark-column/mark-column-filter.component';
import { MarkColumnDataComponent } from './grid-columns/mark-column/mark-column-data.component';
import { MarkColumnEditComponent } from './grid-columns/mark-column/mark-column-edit.component';

import { DateTimeColumnDirective } from './grid-columns/date-time-column/date-time-column.directive';
import { DateTimeColumnFilterComponent } from './grid-columns/date-time-column/date-time-column-filter.component';
import { DateTimeColumnDataComponent } from './grid-columns/date-time-column/date-time-column-data.component';
import { DateTimeColumnEditComponent } from './grid-columns/date-time-column/date-time-column-edit.component';

import { EditColumnDirective } from './grid-columns/edit-column/edit-column.directive';
import { EditColumnDataComponent } from './grid-columns/edit-column/edit-column-data.component';
import { EditColumnEditComponent } from './grid-columns/edit-column/edit-column-edit.component';

import { DeleteColumnDirective } from './grid-columns/delete-column/delete-column.directive';
import { DeleteColumnDataComponent } from './grid-columns/delete-column/delete-column-data.component';

import { DetailsButtonColumnDirective } from './grid-columns/details-button-column/details-button-column.directive';
import { DetailsButtonColumnDataComponent } from './grid-columns/details-button-column/details-button-column-data.component';

import { SelectionColumnDirective } from './grid-columns/selection-column/selection-column.directive';
import { SelectionColumnDataComponent } from './grid-columns/selection-column/selection-column-data.component';

import { IndexColumnDirective } from './grid-columns/index-column/index-column.directive';
import { IndexColumnDataComponent } from './grid-columns/index-column/index-column-data.component';

import { DefaultColumnHeaderComponent } from './grid-columns/shared/default-column-header.component';

@NgModule({
  entryComponents: [
    TextColumnFilterComponent,
    TextColumnDataComponent,
    TextColumnEditComponent,
    SpinColumnFilterComponent,
    SpinColumnDataComponent,
    SpinColumnEditComponent,
    ComboBoxColumnFilterComponent,
    ComboBoxColumnDataComponent,
    ComboBoxColumnEditComponent,
    ButtonColumnDataComponent,
    MarkColumnFilterComponent,
    MarkColumnDataComponent,
    MarkColumnEditComponent,
    DateTimeColumnFilterComponent,
    DateTimeColumnDataComponent,
    DateTimeColumnEditComponent,
    EditColumnDataComponent,
    EditColumnEditComponent,
    DeleteColumnDataComponent,
    DefaultColumnHeaderComponent,
    DetailsButtonColumnDataComponent,
    SelectionColumnDataComponent,
    IndexColumnDataComponent
  ],
  declarations: [
    MyGridComponent,
    HeaderWrapperComponent,
    FilterWrapperComponent,
    DataWrapperComponent,
    ComponentHostDirective,
    HtmlTableElementDirective,
    PagerComponent,
    GroupDropTargetComponent,
    SummaryItemDirective,
    SummaryComponent,
    EditWrapperComponent,
    EditRowComponent,
    DetailGridDirective,
    DetailGridWrapperComponent,
    TextColumnDirective,
    TextColumnFilterComponent,
    TextColumnDataComponent,
    TextColumnEditComponent,
    SpinColumnDirective,
    SpinColumnFilterComponent,
    SpinColumnDataComponent,
    SpinColumnEditComponent,
    ComboBoxColumnDirective,
    ComboBoxColumnFilterComponent,
    ComboBoxColumnDataComponent,
    ComboBoxColumnEditComponent,
    ButtonColumnDirective,
    ButtonColumnDataComponent,
    MarkColumnDirective,
    MarkColumnFilterComponent,
    MarkColumnDataComponent,
    MarkColumnEditComponent,
    DateTimeColumnDirective,
    DateTimeColumnFilterComponent,
    DateTimeColumnDataComponent,
    DateTimeColumnEditComponent,
    EditColumnDirective,
    EditColumnDataComponent,
    EditColumnEditComponent,
    DeleteColumnDirective,
    DeleteColumnDataComponent,
    DetailsButtonColumnDirective,
    DetailsButtonColumnDataComponent,
    SelectionColumnDirective,
    SelectionColumnDataComponent,
    IndexColumnDirective,
    IndexColumnDataComponent,
    DefaultColumnHeaderComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserAnimationsModule
  ],
  exports: [
    MyGridComponent,
    SummaryItemDirective,
    TextColumnDirective,
    SpinColumnDirective,
    ComboBoxColumnDirective,
    ButtonColumnDirective,
    MarkColumnDirective,
    DateTimeColumnDirective,
    EditColumnDirective,
    DeleteColumnDirective,
    DetailGridDirective,
    DetailsButtonColumnDirective,
    SelectionColumnDirective,
    IndexColumnDirective
  ],
  providers: [
  ]
})
export class MyGridModule { }
