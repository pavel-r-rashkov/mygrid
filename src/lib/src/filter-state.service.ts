import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';

import { FilterState } from './states/filter-state';
import { GridColumn } from './grid-columns/grid-column';

@Injectable()
export class FilterStateService {
  private currentState: FilterState = new FilterState({});
  filterState = new ReplaySubject<FilterState>(1);
  
  constructor() {
    this.filterState.subscribe((newState) => {
      this.currentState = newState;
    });
  }  

  raiseFilterChanged(columnId: any, propertyName: string, filterData: any, filterKey: string) {
    let columnFilter = {
      propertyName: propertyName,
      filterData: filterData,
      filterKey: filterKey
    };

    this.currentState.columnFilters[columnId] = columnFilter;
    this.filterState.next(new FilterState(this.currentState.columnFilters));
  }

  clearColumnFilter(columnId: any) {
    this.currentState.columnFilters[columnId] = undefined;
    this.filterState.next(new FilterState(this.currentState.columnFilters)); 
  }

  filterItems(items: any[], filterState: FilterState, columns: GridColumn[]): any[] {
    let filteredItems = items.filter((item) => {
      let isFiltered = columns.some((col) => {
        if (filterState.columnFilters[col.columnId] == null) {
          return false;
        }

        let isPassingFilter = col.filter(col.getValue(item), filterState.columnFilters[col.columnId].filterData, item);
        return !isPassingFilter;
      });

      return !isFiltered;
    });

    return filteredItems;
  }
}