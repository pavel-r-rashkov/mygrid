import { 
  Component, 
  OnInit,
  OnDestroy,
  Input,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import { PaginationService } from './pagination.service';
import { PaginationState } from './pagination-state';

@Component({
  selector: 'mg-pager',
  templateUrl: './pager.component.html'
})
export class PagerComponent implements OnInit, OnDestroy {
  private paginationStateSubscription: Subscription;
  private itemsCountSubscription: Subscription;
  paginationState: PaginationState;
  itemsCount: number;
  @Input() paginationTemplate: TemplateRef<any>;
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) paginationTemplatePlaceholder: ViewContainerRef;

  constructor(
    private paginationService: PaginationService) {

    this.paginationState = new PaginationState(1, this.paginationService.initialPageSize);
    this.paginationStateSubscription = this.paginationService.paginationState.subscribe((newState) => {
      this.paginationState = newState;
      this.renderCustomPaginationTemplate();
    });

    this.itemsCount = this.paginationService.itemsCount.getValue();
    this.itemsCountSubscription = this.paginationService.itemsCount.subscribe((newItemsCount) => {
      this.itemsCount = newItemsCount; 
      this.renderCustomPaginationTemplate();
      let maxPages = Math.ceil(this.itemsCount / this.paginationState.pageSize);
      
      if (maxPages < this.paginationState.currentPage && maxPages > 0) {
        this.paginationService.paginationState.next(new PaginationState(maxPages, this.paginationState.pageSize));
      }
    });
  }

  ngOnInit() {
    this.renderCustomPaginationTemplate();
  }

  ngOnDestroy() {
    this.paginationStateSubscription.unsubscribe();
    this.itemsCountSubscription.unsubscribe();
  }

  changePage(newPage: number) {
    this.paginationService.paginationState.next(new PaginationState(newPage, this.paginationState.pageSize));
  }

  ceil(num: number) {
    return Math.ceil(num);
  }

  private renderCustomPaginationTemplate() {
    if (this.paginationTemplate != null) {
      let changePage = (page: number, size: number) => {
        this.paginationService.paginationState.next(new PaginationState(page, size));
      };

      let templateContext = {
        paginationState: this.paginationState,
        itemsCount: this.itemsCount,
        changePage: changePage
      };

      this.paginationTemplatePlaceholder.clear();
      this.paginationTemplatePlaceholder.createEmbeddedView(this.paginationTemplate, { $implicit: templateContext });
    }
  }
}