export class PaginationState {
  constructor(
    public currentPage: number,
    public pageSize: number) {
  }
}