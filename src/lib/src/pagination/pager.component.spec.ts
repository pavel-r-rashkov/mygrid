import { 
  Component,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {
  ComponentFixture, 
  TestBed,
  async
} from '@angular/core/testing';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import { Observable } from "rxjs/Observable";
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PagerComponent } from './pager.component';
import { PaginationService } from './pagination.service';
import { PaginationState } from './pagination-state';

@Component({
  selector: 'mgt-pager-template',
  template: `
    <ng-template #pagerTemplate let-context>
      <div id="custom-pager"></div>
    </ng-template>
  `
})
class PagerTemplateComponent {
  @ViewChild('pagerTemplate') pagerTemplate: TemplateRef<any>;
}

describe('Component: PagerComponent', () => {
  let fixture: ComponentFixture<PagerComponent>;
  let pagerComponent: PagerComponent;
  let paginationService: PaginationService;
  let paginationStateSpy: jasmine.Spy;
  let initialPageSize = 3;
  let itemsCount = 10;
  let customPagerTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
      ],
      declarations: [
        PagerComponent,
        PagerTemplateComponent
      ],
      providers: [
        PaginationService      
      ]
    })
    .compileComponents()
    .then(() => {
      paginationService = TestBed.get(PaginationService);
      paginationStateSpy = spyOn(paginationService.paginationState, 'next').and.callThrough();
      paginationService.initialPageSize = initialPageSize;

      fixture = TestBed.createComponent(PagerComponent);
      pagerComponent = fixture.componentInstance;

      customPagerTemplate = TestBed
        .createComponent(PagerTemplateComponent)
        .componentInstance
        .pagerTemplate;
    });
  }));

  it('new PagerComponent should set pagination state to first page and initial page size', () => {
    expect(pagerComponent.paginationState.currentPage).toEqual(1);
    expect(pagerComponent.paginationState.pageSize).toEqual(initialPageSize);
  });

  it('new PagerComponent should set items count', () => {
    expect(pagerComponent.itemsCount).toEqual(paginationService.itemsCount.getValue());
  });

  it('PagerComponent should update its pagination state', async(() => {
    var newPaginationState = new PaginationState(2, 3)
    paginationService.paginationState.next(newPaginationState);

    fixture.whenStable().then(() => {
      expect(pagerComponent.paginationState.currentPage).toEqual(newPaginationState.currentPage);
      expect(pagerComponent.paginationState.pageSize).toEqual(newPaginationState.pageSize);
    });
  }));

  it('changePage should update pagination state', async(() => {
    var newPage = 2;
    var currentPageSize = pagerComponent.paginationState.pageSize;
    pagerComponent.changePage(newPage);

    fixture.whenStable().then(() => {
      expect(pagerComponent.paginationState.currentPage).toEqual(newPage);
      expect(pagerComponent.paginationState.pageSize).toEqual(currentPageSize);
    });
  }));

  it('Pager should update items count', async(() => {
    var newItemsCount = 20;

    paginationService.itemsCount.next(newItemsCount);
    
    fixture.whenStable().then(() => {
      expect(pagerComponent.itemsCount).toEqual(newItemsCount);
    });
  }));

  it('Pager should change pagination state to last page when items count is reduced and current page does not exist anymore', async(() => {
    paginationService.paginationState.next(new PaginationState(3, 3));
    
    fixture.whenStable().then(() => {
      paginationStateSpy.calls.reset();
      var currentPageSize = pagerComponent.paginationState.pageSize;
      var newItemsCount = 4;
      paginationService.itemsCount.next(newItemsCount);
      
      fixture.whenStable().then(() => {
        expect(paginationStateSpy.calls.count()).toEqual(1);
        var newPaginationState: PaginationState = paginationStateSpy.calls.first().args[0];
        expect(newPaginationState.currentPage).toEqual(2);
        expect(newPaginationState.pageSize).toEqual(currentPageSize);
      });
    });
  }));

  it('Custom pager template should be rendered when PagerComponent is initialized', () => {
    pagerComponent.paginationTemplate = customPagerTemplate;

    pagerComponent.ngOnInit();
    
    var pagerTemplateElements = fixture.debugElement.queryAll(By.css('#custom-pager'));
    expect(pagerTemplateElements.length).toEqual(1);  
  });

  it('Custom pager template should be rerendered when new pagination state is triggered', async(() => {
    pagerComponent.paginationTemplate = customPagerTemplate;

    pagerComponent.ngOnInit();
    paginationService.paginationState.next(new PaginationState(2, 3));

    fixture.whenStable().then(() => {
      var pagerTemplateElements = fixture.debugElement.queryAll(By.css('#custom-pager'));
      expect(pagerTemplateElements.length).toEqual(1);  
    });
  }));
});