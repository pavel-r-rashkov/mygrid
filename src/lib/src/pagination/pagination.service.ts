import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

import { PaginationState } from './pagination-state';

@Injectable()
export class PaginationService {
  public paginationState: Subject<PaginationState>;
  public itemsCount: BehaviorSubject<number>;
  public initialPageSize: number;

  constructor() {
    this.paginationState = new Subject<PaginationState>();
    this.itemsCount = new BehaviorSubject<number>(0);
  }

  public applyPagination(items: any[], paginationState: PaginationState): any[] {
    let pagedResult = [];
    let startIndex = (paginationState.currentPage - 1) * paginationState.pageSize;
    for (var i = startIndex; i < startIndex + paginationState.pageSize; ++i) {
      var item = items[i];
      if (item != null) {
        pagedResult.push(item);
      }
    }

    return pagedResult;
  }
}