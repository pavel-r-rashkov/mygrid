"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var Subject_1 = require("rxjs/Subject");
var PaginationService = (function () {
    function PaginationService() {
        this.paginationState = new Subject_1.Subject();
        this.itemsCount = new BehaviorSubject_1.BehaviorSubject(0);
    }
    PaginationService.prototype.applyPagination = function (items, paginationState) {
        var pagedResult = [];
        var startIndex = (paginationState.currentPage - 1) * paginationState.pageSize;
        for (var i = startIndex; i < startIndex + paginationState.pageSize; ++i) {
            var item = items[i];
            if (item != null) {
                pagedResult.push(item);
            }
        }
        return pagedResult;
    };
    PaginationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], PaginationService);
    return PaginationService;
}());
exports.PaginationService = PaginationService;
//# sourceMappingURL=pagination.service.js.map