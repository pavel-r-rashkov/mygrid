"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var testing_1 = require("@angular/core/testing");
require("rxjs/add/observable/empty");
require("rxjs/add/observable/throw");
require("rxjs/add/operator/toPromise");
var platform_browser_1 = require("@angular/platform-browser");
var pager_component_1 = require("./pager.component");
var pagination_service_1 = require("./pagination.service");
var pagination_state_1 = require("./pagination-state");
var PagerTemplateComponent = (function () {
    function PagerTemplateComponent() {
    }
    __decorate([
        core_1.ViewChild('pagerTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], PagerTemplateComponent.prototype, "pagerTemplate", void 0);
    PagerTemplateComponent = __decorate([
        core_1.Component({
            selector: 'mgt-pager-template',
            template: "\n    <ng-template #pagerTemplate let-context>\n      <div id=\"custom-pager\"></div>\n    </ng-template>\n  "
        })
    ], PagerTemplateComponent);
    return PagerTemplateComponent;
}());
describe('Component: PagerComponent', function () {
    var fixture;
    var pagerComponent;
    var paginationService;
    var paginationStateSpy;
    var initialPageSize = 3;
    var itemsCount = 10;
    var customPagerTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [],
            declarations: [
                pager_component_1.PagerComponent,
                PagerTemplateComponent
            ],
            providers: [
                pagination_service_1.PaginationService
            ]
        })
            .compileComponents()
            .then(function () {
            paginationService = testing_1.TestBed.get(pagination_service_1.PaginationService);
            paginationStateSpy = spyOn(paginationService.paginationState, 'next').and.callThrough();
            paginationService.initialPageSize = initialPageSize;
            fixture = testing_1.TestBed.createComponent(pager_component_1.PagerComponent);
            pagerComponent = fixture.componentInstance;
            customPagerTemplate = testing_1.TestBed
                .createComponent(PagerTemplateComponent)
                .componentInstance
                .pagerTemplate;
        });
    }));
    it('new PagerComponent should set pagination state to first page and initial page size', function () {
        expect(pagerComponent.paginationState.currentPage).toEqual(1);
        expect(pagerComponent.paginationState.pageSize).toEqual(initialPageSize);
    });
    it('new PagerComponent should set items count', function () {
        expect(pagerComponent.itemsCount).toEqual(paginationService.itemsCount.getValue());
    });
    it('PagerComponent should update its pagination state', testing_1.async(function () {
        var newPaginationState = new pagination_state_1.PaginationState(2, 3);
        paginationService.paginationState.next(newPaginationState);
        fixture.whenStable().then(function () {
            expect(pagerComponent.paginationState.currentPage).toEqual(newPaginationState.currentPage);
            expect(pagerComponent.paginationState.pageSize).toEqual(newPaginationState.pageSize);
        });
    }));
    it('changePage should update pagination state', testing_1.async(function () {
        var newPage = 2;
        var currentPageSize = pagerComponent.paginationState.pageSize;
        pagerComponent.changePage(newPage);
        fixture.whenStable().then(function () {
            expect(pagerComponent.paginationState.currentPage).toEqual(newPage);
            expect(pagerComponent.paginationState.pageSize).toEqual(currentPageSize);
        });
    }));
    it('Pager should update items count', testing_1.async(function () {
        var newItemsCount = 20;
        paginationService.itemsCount.next(newItemsCount);
        fixture.whenStable().then(function () {
            expect(pagerComponent.itemsCount).toEqual(newItemsCount);
        });
    }));
    it('Pager should change pagination state to last page when items count is reduced and current page does not exist anymore', testing_1.async(function () {
        paginationService.paginationState.next(new pagination_state_1.PaginationState(3, 3));
        fixture.whenStable().then(function () {
            paginationStateSpy.calls.reset();
            var currentPageSize = pagerComponent.paginationState.pageSize;
            var newItemsCount = 4;
            paginationService.itemsCount.next(newItemsCount);
            fixture.whenStable().then(function () {
                expect(paginationStateSpy.calls.count()).toEqual(1);
                var newPaginationState = paginationStateSpy.calls.first().args[0];
                expect(newPaginationState.currentPage).toEqual(2);
                expect(newPaginationState.pageSize).toEqual(currentPageSize);
            });
        });
    }));
    it('Custom pager template should be rendered when PagerComponent is initialized', function () {
        pagerComponent.paginationTemplate = customPagerTemplate;
        pagerComponent.ngOnInit();
        var pagerTemplateElements = fixture.debugElement.queryAll(platform_browser_1.By.css('#custom-pager'));
        expect(pagerTemplateElements.length).toEqual(1);
    });
    it('Custom pager template should be rerendered when new pagination state is triggered', testing_1.async(function () {
        pagerComponent.paginationTemplate = customPagerTemplate;
        pagerComponent.ngOnInit();
        paginationService.paginationState.next(new pagination_state_1.PaginationState(2, 3));
        fixture.whenStable().then(function () {
            var pagerTemplateElements = fixture.debugElement.queryAll(platform_browser_1.By.css('#custom-pager'));
            expect(pagerTemplateElements.length).toEqual(1);
        });
    }));
});
//# sourceMappingURL=pager.component.spec.js.map