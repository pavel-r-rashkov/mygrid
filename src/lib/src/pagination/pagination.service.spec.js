"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var pagination_service_1 = require("./pagination.service");
var pagination_state_1 = require("./pagination-state");
describe('Service: PaginationService', function () {
    var paginationService;
    var items;
    beforeEach(function () {
        paginationService = new pagination_service_1.PaginationService();
        items = [
            { id: 1 },
            { id: 2 },
            { id: 3 },
            { id: 4 },
            { id: 5 },
            { id: 6 },
            { id: 7 },
            { id: 8 }
        ];
    });
    it('applyPagination with pagination state set to first page', function () {
        var paginationState = new pagination_state_1.PaginationState(1, 3);
        var pagedItems = paginationService.applyPagination(items, paginationState);
        expect(pagedItems.length).toEqual(paginationState.pageSize);
        expect(items.indexOf(pagedItems[0])).toEqual(0);
        expect(items.indexOf(pagedItems[1])).toEqual(1);
        expect(items.indexOf(pagedItems[2])).toEqual(2);
    });
    it('applyPagination with pagination state set to last page', function () {
        var paginationState = new pagination_state_1.PaginationState(2, 4);
        var pagedItems = paginationService.applyPagination(items, paginationState);
        expect(pagedItems.length).toEqual(paginationState.pageSize);
        for (var i = 0; i < pagedItems.length; i++) {
            expect(items.indexOf(pagedItems[i])).toEqual((paginationState.currentPage - 1) * paginationState.pageSize + i);
        }
    });
    it('applyPagination with last page and last page containing less items than page size', function () {
        var paginationState = new pagination_state_1.PaginationState(3, 3);
        var pagedItems = paginationService.applyPagination(items, paginationState);
        expect(pagedItems.length).toEqual(items.length % paginationState.pageSize);
        for (var i = 0; i < pagedItems.length; i++) {
            expect(items.indexOf(pagedItems[i])).toEqual((paginationState.currentPage - 1) * paginationState.pageSize + i);
        }
    });
    it('applyPagination with page greater than max pages count should return empty array', function () {
        var paginationState = new pagination_state_1.PaginationState(4, 4);
        var pagedItems = paginationService.applyPagination(items, paginationState);
        expect(pagedItems.length).toEqual(0);
    });
});
//# sourceMappingURL=pagination.service.spec.js.map