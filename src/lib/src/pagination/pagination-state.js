"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PaginationState = (function () {
    function PaginationState(currentPage, pageSize) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
    }
    return PaginationState;
}());
exports.PaginationState = PaginationState;
//# sourceMappingURL=pagination-state.js.map