"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var pagination_service_1 = require("./pagination.service");
var pagination_state_1 = require("./pagination-state");
var PagerComponent = (function () {
    function PagerComponent(paginationService) {
        var _this = this;
        this.paginationService = paginationService;
        this.paginationState = new pagination_state_1.PaginationState(1, this.paginationService.initialPageSize);
        this.paginationStateSubscription = this.paginationService.paginationState.subscribe(function (newState) {
            _this.paginationState = newState;
            _this.renderCustomPaginationTemplate();
        });
        this.itemsCount = this.paginationService.itemsCount.getValue();
        this.itemsCountSubscription = this.paginationService.itemsCount.subscribe(function (newItemsCount) {
            _this.itemsCount = newItemsCount;
            _this.renderCustomPaginationTemplate();
            var maxPages = Math.ceil(_this.itemsCount / _this.paginationState.pageSize);
            if (maxPages < _this.paginationState.currentPage && maxPages > 0) {
                _this.paginationService.paginationState.next(new pagination_state_1.PaginationState(maxPages, _this.paginationState.pageSize));
            }
        });
    }
    PagerComponent.prototype.ngOnInit = function () {
        this.renderCustomPaginationTemplate();
    };
    PagerComponent.prototype.ngOnDestroy = function () {
        this.paginationStateSubscription.unsubscribe();
        this.itemsCountSubscription.unsubscribe();
    };
    PagerComponent.prototype.changePage = function (newPage) {
        this.paginationService.paginationState.next(new pagination_state_1.PaginationState(newPage, this.paginationState.pageSize));
    };
    PagerComponent.prototype.ceil = function (num) {
        return Math.ceil(num);
    };
    PagerComponent.prototype.renderCustomPaginationTemplate = function () {
        var _this = this;
        if (this.paginationTemplate != null) {
            var changePage = function (page, size) {
                _this.paginationService.paginationState.next(new pagination_state_1.PaginationState(page, size));
            };
            var templateContext = {
                paginationState: this.paginationState,
                itemsCount: this.itemsCount,
                changePage: changePage
            };
            this.paginationTemplatePlaceholder.clear();
            this.paginationTemplatePlaceholder.createEmbeddedView(this.paginationTemplate, { $implicit: templateContext });
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], PagerComponent.prototype, "paginationTemplate", void 0);
    __decorate([
        core_1.ViewChild('customTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], PagerComponent.prototype, "paginationTemplatePlaceholder", void 0);
    PagerComponent = __decorate([
        core_1.Component({
            selector: 'mg-pager',
            templateUrl: './pager.component.html'
        }),
        __metadata("design:paramtypes", [pagination_service_1.PaginationService])
    ], PagerComponent);
    return PagerComponent;
}());
exports.PagerComponent = PagerComponent;
//# sourceMappingURL=pager.component.js.map