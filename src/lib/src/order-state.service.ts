import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { OrderState } from './states/order-state';
import { GridColumn } from './grid-columns/grid-column';
import { GroupState } from './grouping/group-state';
import { GroupService } from './grouping/group.service';

@Injectable()
export class OrderStateService {
  orderState: ReplaySubject<OrderState> = new ReplaySubject<OrderState>(1);

  constructor(
    private groupService: GroupService) {
  }

  raiseOrderChanged(state: OrderState) {
    this.orderState.next(state);
  }

  sortItems(
    items: any[],
    orderState: OrderState,
    groupState: GroupState,
    columns: GridColumn[]): void {
 
    let sort = (items: any[]) => {
      items.sort((firstItem, secondItem) => {
        var orderedColumn = columns.filter((col) => {
          return col.columnId == orderState.columnId;
        })[0];
        
        let firstValue = orderedColumn.getValue(firstItem);
        let secondValue = orderedColumn.getValue(secondItem);

        let sortResult = orderedColumn.order(firstValue, secondValue, orderState.isAscending, firstItem, secondItem);
        return sortResult;
      });
    };

    if (groupState != null) {
      for (var i = 0; i < items.length; ++i) {
        sort(items[i][this.groupService.GROUPED_ITEMS_KEY]);
      }
    } else {
      sort(items);
    }
  }
}