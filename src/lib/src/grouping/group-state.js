"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GroupState = (function () {
    function GroupState(columnId, propertyName, isAscending) {
        this.columnId = columnId;
        this.propertyName = propertyName;
        this.isAscending = isAscending;
    }
    return GroupState;
}());
exports.GroupState = GroupState;
//# sourceMappingURL=group-state.js.map