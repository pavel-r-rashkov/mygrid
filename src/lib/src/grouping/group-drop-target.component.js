"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var group_service_1 = require("./group.service");
var group_state_1 = require("./group-state");
var columns_service_1 = require("../columns.service");
var GroupDropTargetComponent = (function () {
    function GroupDropTargetComponent(groupService, columnsService, zone, renderer) {
        this.groupService = groupService;
        this.columnsService = columnsService;
        this.zone = zone;
        this.renderer = renderer;
        this.currentGroupState = null;
    }
    GroupDropTargetComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.groupStateSubscription = this.groupService.groupState.subscribe(function (newState) {
            _this.currentGroupState = newState;
        });
        this.zone.runOutsideAngular(function () {
            var handler = function ($event) {
                $event.preventDefault();
            };
            _this.renderer.listen(_this.dropTarget.nativeElement, 'dragover', handler);
        });
    };
    GroupDropTargetComponent.prototype.ngOnDestroy = function () {
        this.groupStateSubscription.unsubscribe();
    };
    GroupDropTargetComponent.prototype.columnDrop = function ($event) {
        var groupingData = JSON.parse($event.dataTransfer.getData('draggedColumnData'));
        if (groupingData.enableGrouping) {
            var groupedColumn = this.columnsService.columns.find(function (col) {
                return col.columnId == groupingData.columnId;
            });
            this.groupService.groupState.next(new group_state_1.GroupState(groupingData.columnId, groupingData.propertyName, true));
        }
    };
    GroupDropTargetComponent.prototype.allowDrop = function ($event) {
        $event.preventDefault();
    };
    GroupDropTargetComponent.prototype.removeGrouping = function () {
        this.groupService.groupState.next(null);
    };
    GroupDropTargetComponent.prototype.getType = function () {
        var _this = this;
        var groupedColumn = this.columnsService.columns.find(function (col) {
            return col.columnId == _this.currentGroupState.columnId;
        });
        return groupedColumn.headerComponentType;
    };
    GroupDropTargetComponent.prototype.getData = function () {
        var _this = this;
        var groupedColumn = this.columnsService.columns.find(function (col) {
            return col.columnId == _this.currentGroupState.columnId;
        });
        return groupedColumn.getHeaderData();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], GroupDropTargetComponent.prototype, "dropColumnText", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], GroupDropTargetComponent.prototype, "removeGroupingButtonText", void 0);
    __decorate([
        core_1.ViewChild('dropTarget'),
        __metadata("design:type", core_1.ElementRef)
    ], GroupDropTargetComponent.prototype, "dropTarget", void 0);
    GroupDropTargetComponent = __decorate([
        core_1.Component({
            selector: 'mg-group-drop-target',
            templateUrl: './group-drop-target.component.html'
        }),
        __metadata("design:paramtypes", [group_service_1.GroupService,
            columns_service_1.ColumnsService,
            core_1.NgZone,
            core_1.Renderer2])
    ], GroupDropTargetComponent);
    return GroupDropTargetComponent;
}());
exports.GroupDropTargetComponent = GroupDropTargetComponent;
//# sourceMappingURL=group-drop-target.component.js.map