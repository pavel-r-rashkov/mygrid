import { GridColumn } from '../grid-columns/grid-column';

export class GroupState {
  constructor(
    public columnId: string,
    public propertyName: string,
    public isAscending: boolean) {
  }
}