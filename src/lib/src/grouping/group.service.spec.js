"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var group_service_1 = require("./group.service");
var group_state_1 = require("./group-state");
var text_column_directive_1 = require("../grid-columns/text-column/text-column.directive");
var guid_1 = require("../guid");
var default_comparer_service_1 = require("../grid-columns/shared/default-comparer.service");
describe('Service: GroupService', function () {
    var groupService;
    var columns;
    var groupedColumn;
    beforeEach(function () {
        groupService = new group_service_1.GroupService();
        groupedColumn = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        groupedColumn.propertyName = 'firstName';
        columns = [groupedColumn];
    });
    it('groupItems with 1 item per group', function () {
        var groupState = new group_state_1.GroupState(groupedColumn.columnId, groupedColumn.propertyName, true);
        var items = [
            { firstName: 'A1', secondName: 'B1' },
            { firstName: 'A2', secondName: 'B2' },
            { firstName: 'A3', secondName: 'B3' }
        ];
        var groupedItems = groupService.groupItems(items, groupState, columns);
        expect(areCorrectlyGrouped(groupedItems, groupState, groupedColumn)).toBeTruthy();
        expect(areAllGroupsClosed(groupedItems)).toBeTruthy();
    });
    it('groupItems with more than 1 item per group', function () {
        var groupState = new group_state_1.GroupState(groupedColumn.columnId, groupedColumn.propertyName, true);
        var items = [
            { firstName: 'A1', secondName: 'B1' },
            { firstName: 'A2', secondName: 'B2' },
            { firstName: 'A3', secondName: 'B3' },
            { firstName: 'A3', secondName: 'B3' },
            { firstName: 'A1', secondName: 'B3' },
            { firstName: 'A4', secondName: 'B3' }
        ];
        var groupedItems = groupService.groupItems(items, groupState, columns);
        expect(areCorrectlyGrouped(groupedItems, groupState, groupedColumn)).toBeTruthy();
        expect(areAllGroupsClosed(groupedItems)).toBeTruthy();
    });
    it('groupItems with no value for grouped property', function () {
        var groupState = new group_state_1.GroupState(groupedColumn.columnId, groupedColumn.propertyName, true);
        var items = [
            { firstName: 'A1', secondName: 'B1' },
            { firstName: 'A2', secondName: 'B2' },
            { secondName: 'B3' },
            { firstName: 'A3', secondName: 'B3' },
            { firstName: 'A1', secondName: 'B3' },
            { firstName: null, secondName: 'B3' }
        ];
        var groupedItems = groupService.groupItems(items, groupState, columns);
        expect(areCorrectlyGrouped(groupedItems, groupState, groupedColumn)).toBeTruthy();
        expect(areAllGroupsClosed(groupedItems)).toBeTruthy();
    });
    function areCorrectlyGrouped(groupedItems, groupState, groupedColumn) {
        for (var groupIndex in groupedItems) {
            var group = groupedItems[groupIndex];
            for (var i = parseInt(groupIndex) + 1; i < groupedItems.length; i++) {
                var groupResult = groupedColumn.group(group[groupState.propertyName], groupedItems[i][groupState.propertyName], groupState.isAscending, null, null);
                if (groupResult == 0) {
                    return false;
                }
            }
            for (var itemIndex in group[groupService.GROUPED_ITEMS_KEY]) {
                var item = group[groupService.GROUPED_ITEMS_KEY][itemIndex];
                var groupResult = groupedColumn.group(group[groupState.propertyName], item[groupState.propertyName], groupState.isAscending, null, item);
                if (groupResult != 0) {
                    return false;
                }
            }
        }
        return true;
    }
    function areAllGroupsClosed(groupedItems) {
        for (var i in groupedItems) {
            if (groupedItems[i].isOpened) {
                return false;
            }
        }
        return true;
    }
});
//# sourceMappingURL=group.service.spec.js.map