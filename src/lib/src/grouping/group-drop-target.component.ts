import { 
  Component,
  Type,
  OnInit,
  OnDestroy,
  Input,
  ViewChild,
  ElementRef,
  NgZone,
  Renderer2
} from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import { GroupService } from './group.service';
import { GroupState } from './group-state';
import { ColumnsService } from '../columns.service';

@Component({
  selector: 'mg-group-drop-target',
  templateUrl: './group-drop-target.component.html'
})
export class GroupDropTargetComponent implements OnInit, OnDestroy {
  private groupStateSubscription: Subscription;
  @Input() dropColumnText: string;
  @Input() removeGroupingButtonText: string;
  @ViewChild('dropTarget') dropTarget: ElementRef;
  currentGroupState: GroupState = null;

  constructor(
    private groupService: GroupService,
    private columnsService: ColumnsService,
    private zone: NgZone,
    private renderer: Renderer2) {
  }

  ngOnInit() {
    this.groupStateSubscription = this.groupService.groupState.subscribe((newState) => {
      this.currentGroupState = newState;
    });

    this.zone.runOutsideAngular(() => {
      let handler = ($event: any) => {
        $event.preventDefault();
      };
      this.renderer.listen(this.dropTarget.nativeElement, 'dragover', handler);
    });
  }

  ngOnDestroy() {
    this.groupStateSubscription.unsubscribe();
  }

  columnDrop($event: any) {
    let groupingData = JSON.parse($event.dataTransfer.getData('draggedColumnData'));

    if (groupingData.enableGrouping) {
      let groupedColumn = this.columnsService.columns.find((col) => {
        return col.columnId == groupingData.columnId;
      });

      this.groupService.groupState.next(new GroupState(
        groupingData.columnId,
        groupingData.propertyName,
        true));
    }
  }

  allowDrop($event: any) {
    $event.preventDefault();
  }

  removeGrouping() {
    this.groupService.groupState.next(null);
  }

  getType(): Type<any> {
    let groupedColumn = this.columnsService.columns.find((col) => {
      return col.columnId == this.currentGroupState.columnId;
    });

    return groupedColumn.headerComponentType;
  }

  getData(): any {
    let groupedColumn = this.columnsService.columns.find((col) => {
      return col.columnId == this.currentGroupState.columnId;
    });

    return groupedColumn.getHeaderData();
  }
}