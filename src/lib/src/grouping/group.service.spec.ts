import { GroupService } from './group.service';
import { GroupState } from './group-state';
import { GridColumn } from '../grid-columns/grid-column';
import { TextColumnDirective } from '../grid-columns/text-column/text-column.directive';
import { GuidService } from '../guid';
import { DefaultComparerService } from '../grid-columns/shared/default-comparer.service';

describe('Service: GroupService', () => {
  let groupService: GroupService;
  let columns: GridColumn[];
  let groupedColumn: TextColumnDirective;

  beforeEach(() => {
    groupService = new GroupService();
    groupedColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    groupedColumn.propertyName = 'firstName';
    columns = [groupedColumn];
  });

  it('groupItems with 1 item per group', () => {
    var groupState = new GroupState(groupedColumn.columnId, groupedColumn.propertyName, true);
    var items: any[] = [
      { firstName: 'A1', secondName: 'B1' },
      { firstName: 'A2', secondName: 'B2' },
      { firstName: 'A3', secondName: 'B3' }
    ];

    var groupedItems = groupService.groupItems(items, groupState, columns);

    expect(areCorrectlyGrouped(groupedItems, groupState, groupedColumn)).toBeTruthy();
    expect(areAllGroupsClosed(groupedItems)).toBeTruthy();
  });

  it('groupItems with more than 1 item per group', () => {
    var groupState = new GroupState(groupedColumn.columnId, groupedColumn.propertyName, true);
    var items: any[] = [
      { firstName: 'A1', secondName: 'B1' },
      { firstName: 'A2', secondName: 'B2' },
      { firstName: 'A3', secondName: 'B3' },
      { firstName: 'A3', secondName: 'B3' },
      { firstName: 'A1', secondName: 'B3' },
      { firstName: 'A4', secondName: 'B3' }
    ];

    var groupedItems = groupService.groupItems(items, groupState, columns);

    expect(areCorrectlyGrouped(groupedItems, groupState, groupedColumn)).toBeTruthy();
    expect(areAllGroupsClosed(groupedItems)).toBeTruthy();
  });

  it('groupItems with no value for grouped property', () => {
    var groupState = new GroupState(groupedColumn.columnId, groupedColumn.propertyName, true);
    var items: any[] = [
      { firstName: 'A1', secondName: 'B1' },
      { firstName: 'A2', secondName: 'B2' },
      { secondName: 'B3' },
      { firstName: 'A3', secondName: 'B3' },
      { firstName: 'A1', secondName: 'B3' },
      { firstName: null, secondName: 'B3' }
    ];

    var groupedItems = groupService.groupItems(items, groupState, columns);

    expect(areCorrectlyGrouped(groupedItems, groupState, groupedColumn)).toBeTruthy();
    expect(areAllGroupsClosed(groupedItems)).toBeTruthy();
  });

  function areCorrectlyGrouped(groupedItems: any[], groupState: GroupState, groupedColumn: GridColumn) {
    for (let groupIndex in groupedItems) {
      var group = groupedItems[groupIndex];
      for (let i = parseInt(groupIndex) + 1; i < groupedItems.length; i++) {
        let groupResult = groupedColumn.group(
          group[groupState.propertyName],
          groupedItems[i][groupState.propertyName],
          groupState.isAscending,
          null, 
          null);

        if (groupResult == 0) {
          return false;
        }
      }

      for (let itemIndex in group[groupService.GROUPED_ITEMS_KEY]) {
        let item = group[groupService.GROUPED_ITEMS_KEY][itemIndex];
        let groupResult = groupedColumn.group(
          group[groupState.propertyName],
          item[groupState.propertyName], 
          groupState.isAscending, 
          null, 
          item);

        if (groupResult != 0) {
          return false;
        }
      }
    }

    return true;
  }

  function areAllGroupsClosed(groupedItems: any[]) {
    for (var i in groupedItems) {
      if (groupedItems[i].isOpened) {
        return false;
      }
    }

    return true;
  }
});