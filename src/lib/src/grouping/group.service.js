"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ReplaySubject_1 = require("rxjs/ReplaySubject");
var GroupService = (function () {
    function GroupService() {
        this.GROUPED_ITEMS_KEY = 'groupItems';
        this.groupState = new ReplaySubject_1.ReplaySubject(1);
    }
    GroupService.prototype.groupItems = function (items, groupState, columns) {
        var _this = this;
        var groupByProperty = groupState.propertyName;
        var groupByColumn = columns.find(function (col) {
            return col.columnId == groupState.columnId;
        });
        var grouped = items.reduce(function (accumulator, item) {
            var group = accumulator.find(function (accumulatedGroup) {
                return groupByColumn.group(accumulatedGroup[groupByProperty], item[groupByProperty], groupState.isAscending, null, item) == 0;
            });
            if (group == null) {
                group = {
                    isOpened: false
                };
                group[groupByProperty] = item[groupByProperty];
                group[_this.GROUPED_ITEMS_KEY] = [];
                accumulator.push(group);
            }
            group[_this.GROUPED_ITEMS_KEY].push(item);
            return accumulator;
        }, []);
        grouped.sort(function (firstGroup, secondGroup) {
            return groupByColumn.group(firstGroup[groupByProperty], secondGroup[groupByProperty], groupState.isAscending, null, null);
        });
        return grouped;
    };
    GroupService = __decorate([
        core_1.Injectable()
    ], GroupService);
    return GroupService;
}());
exports.GroupService = GroupService;
//# sourceMappingURL=group.service.js.map