import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { GroupState } from './group-state';
import { GridColumn } from '../grid-columns/grid-column';

@Injectable()
export class GroupService {
  public readonly GROUPED_ITEMS_KEY: string = 'groupItems';
  public groupState: ReplaySubject<GroupState> = new ReplaySubject<GroupState>(1);

  groupItems(items: any[], groupState: GroupState, columns: GridColumn[]): any[] {
    let groupByProperty = groupState.propertyName;
    let groupByColumn = columns.find((col: GridColumn) => {
      return col.columnId == groupState.columnId;
    });

    let grouped = items.reduce((accumulator, item) => {
      let group = accumulator.find((accumulatedGroup: any) => {
        return groupByColumn.group(
          accumulatedGroup[groupByProperty], 
          item[groupByProperty], 
          groupState.isAscending, 
          null, 
          item) == 0;
      });

      if (group == null) {
        group = {
          isOpened: false
        };
        group[groupByProperty] = item[groupByProperty];
        group[this.GROUPED_ITEMS_KEY] = [];
        accumulator.push(group);
      }

      group[this.GROUPED_ITEMS_KEY].push(item);
      return accumulator;
    }, []);

    grouped.sort((firstGroup: any, secondGroup: any) => {
      return groupByColumn.group(
        firstGroup[groupByProperty],
        secondGroup[groupByProperty], 
        groupState.isAscending, 
        null, 
        null);
    });

    return grouped;
  }
}