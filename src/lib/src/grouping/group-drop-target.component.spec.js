"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var group_drop_target_component_1 = require("./group-drop-target.component");
var group_service_1 = require("./group.service");
var group_state_1 = require("./group-state");
var columns_service_1 = require("../columns.service");
var header_wrapper_component_1 = require("../wrappers/header-wrapper.component");
var text_column_directive_1 = require("../grid-columns/text-column/text-column.directive");
var guid_1 = require("../guid");
var default_comparer_service_1 = require("../grid-columns/shared/default-comparer.service");
describe('Component: GroupDropTargetComponent', function () {
    var groupDropTargetComponent;
    var fixture;
    var groupService;
    var groupStateSpy;
    var groupStateSubscribtionSpy;
    var groupedColumn;
    var columnsService;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                header_wrapper_component_1.HeaderWrapperComponent,
                group_drop_target_component_1.GroupDropTargetComponent
            ],
            providers: [
                group_service_1.GroupService,
                columns_service_1.ColumnsService
            ]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(group_drop_target_component_1.GroupDropTargetComponent);
            groupDropTargetComponent = fixture.componentInstance;
            groupService = fixture.debugElement.injector.get(group_service_1.GroupService);
            groupStateSpy = spyOn(groupService.groupState, 'next').and.callThrough();
            groupStateSubscribtionSpy = spyOn(groupService.groupState, 'subscribe').and.callThrough();
            columnsService = fixture.debugElement.injector.get(columns_service_1.ColumnsService);
            groupedColumn = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        });
    }));
    it('currentGroupState should equal most recent published grouped state', function () {
        groupDropTargetComponent.ngOnInit();
        var newGroupState = new group_state_1.GroupState(groupedColumn.columnId, groupedColumn.propertyName, true);
        groupService.groupState.next(newGroupState);
        expect(groupStateSubscribtionSpy.calls.count()).toEqual(1);
        expect(groupDropTargetComponent.currentGroupState).toEqual(newGroupState);
    });
    it('allowDrop should call event.preventDefault', function () {
        var eventMock = { preventDefault: function () { } };
        var preventDefaultSpy = spyOn(eventMock, 'preventDefault');
        groupDropTargetComponent.ngOnInit();
        groupDropTargetComponent.allowDrop(eventMock);
        expect(preventDefaultSpy.calls.count()).toEqual(1);
    });
    it('getType should return currently grouped column type', function () {
        columnsService.columns = [groupedColumn];
        groupDropTargetComponent.ngOnInit();
        groupService.groupState.next(new group_state_1.GroupState(groupedColumn.columnId, groupedColumn.propertyName, true));
        var groupedColumnHeaderType = groupDropTargetComponent.getType();
        expect(groupedColumnHeaderType).toEqual(groupedColumn.headerComponentType);
    });
    it('getData should return currently grouped column header data', function () {
        columnsService.columns = [groupedColumn];
        groupDropTargetComponent.ngOnInit();
        groupService.groupState.next(new group_state_1.GroupState(groupedColumn.columnId, groupedColumn.propertyName, true));
        var groupedColumnHeaderData = groupDropTargetComponent.getData();
        expect(groupedColumnHeaderData).toEqual(groupedColumn.getHeaderData());
    });
    it('removeGrouping should publish new group state', function () {
        groupDropTargetComponent.ngOnInit();
        groupService.groupState.next(new group_state_1.GroupState(groupedColumn.columnId, groupedColumn.propertyName, true));
        groupStateSpy.calls.reset();
        groupDropTargetComponent.removeGrouping();
        expect(groupDropTargetComponent.currentGroupState).toBeNull();
        expect(groupStateSpy.calls.count()).toEqual(1);
    });
    it('dropping column with disabled grouping should not change the current group state', function () {
        groupDropTargetComponent.ngOnInit();
        var dropEventMock = {
            dataTransfer: {
                getData: function (key) {
                    var draggedColumn = {
                        columnId: groupedColumn.columnId,
                        propertyName: groupedColumn.propertyName,
                        enableGrouping: false
                    };
                    return JSON.stringify(draggedColumn);
                }
            }
        };
        groupDropTargetComponent.columnDrop(dropEventMock);
        expect(groupDropTargetComponent.currentGroupState).toBeNull();
        expect(groupStateSpy.calls.count()).toEqual(0);
    });
    it('dropping column with enabled grouping should change the current group state', function () {
        columnsService.columns = [groupedColumn];
        var dropEventMock = {
            dataTransfer: {
                getData: function (key) {
                    var draggedColumn = {
                        columnId: groupedColumn.columnId,
                        propertyName: groupedColumn.propertyName,
                        enableGrouping: true
                    };
                    return JSON.stringify(draggedColumn);
                }
            }
        };
        groupDropTargetComponent.ngOnInit();
        groupDropTargetComponent.columnDrop(dropEventMock);
        expect(groupDropTargetComponent.currentGroupState).not.toBeNull();
        expect(groupStateSpy.calls.count()).toEqual(1);
        expect(groupDropTargetComponent.currentGroupState.columnId).toEqual(groupedColumn.columnId);
        expect(groupDropTargetComponent.currentGroupState.propertyName).toEqual(groupedColumn.propertyName);
        expect(groupDropTargetComponent.currentGroupState.isAscending).toBeTruthy();
    });
});
//# sourceMappingURL=group-drop-target.component.spec.js.map