import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { 
  DebugElement, 
  Type 
} from '@angular/core';

import { GroupDropTargetComponent } from './group-drop-target.component';
import { GroupService } from './group.service';
import { GroupState } from './group-state';
import { ColumnsService } from '../columns.service';
import { HeaderWrapperComponent } from '../wrappers/header-wrapper.component';
import { TextColumnDirective } from '../grid-columns/text-column/text-column.directive';
import { GuidService } from '../guid';
import { DefaultComparerService } from '../grid-columns/shared/default-comparer.service';

describe('Component: GroupDropTargetComponent', () => {
  let groupDropTargetComponent: GroupDropTargetComponent;
  let fixture: ComponentFixture<GroupDropTargetComponent>;
  let groupService: GroupService;
  let groupStateSpy: jasmine.Spy;
  let groupStateSubscribtionSpy: jasmine.Spy;
  let groupedColumn: TextColumnDirective;
  let columnsService: ColumnsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeaderWrapperComponent,
        GroupDropTargetComponent
      ],
      providers: [
        GroupService,
        ColumnsService
      ]
    })
    .compileComponents()
    .then(() => {
      fixture = TestBed.createComponent(GroupDropTargetComponent);
      groupDropTargetComponent = fixture.componentInstance;

      groupService = fixture.debugElement.injector.get(GroupService);
      groupStateSpy = spyOn(groupService.groupState, 'next').and.callThrough();
      groupStateSubscribtionSpy = spyOn(groupService.groupState, 'subscribe').and.callThrough();

      columnsService = fixture.debugElement.injector.get(ColumnsService);

      groupedColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    });
  }));

  it('currentGroupState should equal most recent published grouped state', () => {
    groupDropTargetComponent.ngOnInit();
    var newGroupState = new GroupState(groupedColumn.columnId, groupedColumn.propertyName, true);
    groupService.groupState.next(newGroupState);

    expect(groupStateSubscribtionSpy.calls.count()).toEqual(1);
    expect(groupDropTargetComponent.currentGroupState).toEqual(newGroupState);
  });

  it('allowDrop should call event.preventDefault', () => {
    var eventMock = { preventDefault: function() {} };
    var preventDefaultSpy = spyOn(eventMock, 'preventDefault');

    groupDropTargetComponent.ngOnInit();
    groupDropTargetComponent.allowDrop(eventMock);

    expect(preventDefaultSpy.calls.count()).toEqual(1);
  });

  it('getType should return currently grouped column type', () => {
    columnsService.columns = [groupedColumn];
    
    groupDropTargetComponent.ngOnInit();
    groupService.groupState.next(new GroupState(groupedColumn.columnId, groupedColumn.propertyName, true));
    var groupedColumnHeaderType = groupDropTargetComponent.getType();

    expect(groupedColumnHeaderType).toEqual(groupedColumn.headerComponentType);
  });

  it('getData should return currently grouped column header data', () => {
    columnsService.columns = [groupedColumn];

    groupDropTargetComponent.ngOnInit();
    groupService.groupState.next(new GroupState(groupedColumn.columnId, groupedColumn.propertyName, true));
    var groupedColumnHeaderData = groupDropTargetComponent.getData();

    expect(groupedColumnHeaderData).toEqual(groupedColumn.getHeaderData());
  });

  it('removeGrouping should publish new group state', () => {
    groupDropTargetComponent.ngOnInit();
    groupService.groupState.next(new GroupState(groupedColumn.columnId, groupedColumn.propertyName, true));
    groupStateSpy.calls.reset();
    groupDropTargetComponent.removeGrouping();

    expect(groupDropTargetComponent.currentGroupState).toBeNull();
    expect(groupStateSpy.calls.count()).toEqual(1);
  });

  it('dropping column with disabled grouping should not change the current group state', () => {
    groupDropTargetComponent.ngOnInit();
    var dropEventMock = {
      dataTransfer: {
        getData: function(key: string) {
          var draggedColumn = {
            columnId: groupedColumn.columnId,
            propertyName: groupedColumn.propertyName,
            enableGrouping: false
          }
          return JSON.stringify(draggedColumn);
        }
      }
    };

    groupDropTargetComponent.columnDrop(dropEventMock);

    expect(groupDropTargetComponent.currentGroupState).toBeNull();
    expect(groupStateSpy.calls.count()).toEqual(0);
  });

  it('dropping column with enabled grouping should change the current group state', () => {
    columnsService.columns = [groupedColumn];
    var dropEventMock = {
      dataTransfer: {
        getData: function(key: string) {
          var draggedColumn = {
            columnId: groupedColumn.columnId,
            propertyName: groupedColumn.propertyName,
            enableGrouping: true
          }
          return JSON.stringify(draggedColumn);
        }
      }
    };

    groupDropTargetComponent.ngOnInit();
    groupDropTargetComponent.columnDrop(dropEventMock);

    expect(groupDropTargetComponent.currentGroupState).not.toBeNull();
    expect(groupStateSpy.calls.count()).toEqual(1);
    expect(groupDropTargetComponent.currentGroupState.columnId).toEqual(groupedColumn.columnId);
    expect(groupDropTargetComponent.currentGroupState.propertyName).toEqual(groupedColumn.propertyName);
    expect(groupDropTargetComponent.currentGroupState.isAscending).toBeTruthy();
  });
});