"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var ScrollMeasureService = (function () {
    function ScrollMeasureService(renderer, document) {
        this.renderer = renderer;
        this.document = document;
    }
    ScrollMeasureService.prototype.measureScrollWidth = function () {
        if (this.scrollCache == null) {
            var scrollDiv = this.renderer.createElement('div');
            this.renderer.addClass(scrollDiv, 'scrollbar-measure');
            this.renderer.appendChild(this.document.body, scrollDiv);
            this.scrollCache = scrollDiv.offsetWidth - scrollDiv.clientWidth;
            this.renderer.removeChild(this.document.body, scrollDiv);
        }
        return this.scrollCache;
    };
    ScrollMeasureService.prototype.ngOnDestroy = function () {
        this.renderer.destroy();
    };
    ScrollMeasureService = __decorate([
        core_1.Injectable(),
        __param(1, core_1.Inject(platform_browser_1.DOCUMENT)),
        __metadata("design:paramtypes", [core_1.Renderer2, Object])
    ], ScrollMeasureService);
    return ScrollMeasureService;
}());
exports.ScrollMeasureService = ScrollMeasureService;
//# sourceMappingURL=scroll-measure.service.js.map