"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var filter_state_service_1 = require("./filter-state.service");
var filter_state_1 = require("./states/filter-state");
var text_column_directive_1 = require("./grid-columns/text-column/text-column.directive");
var guid_1 = require("./guid");
var default_comparer_service_1 = require("./grid-columns/shared/default-comparer.service");
describe('Service: FilterStateService', function () {
    var filterStateService;
    var columns;
    var firstColumn;
    var secondColumn;
    beforeEach(function () {
        filterStateService = new filter_state_service_1.FilterStateService();
        firstColumn = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        firstColumn.propertyName = 'foo';
        secondColumn = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        secondColumn.propertyName = 'bar';
        columns = [
            firstColumn,
            secondColumn
        ];
    });
    it('filterItems with all items passing the filter', function () {
        var items = [
            { foo: 'Hsdawdd', bar: 'Asd' },
            { foo: 'Asd', bar: 'koq' },
            { foo: null, bar: 'Olqk' },
            { foo: 'Poqk' },
            { foo: '', bar: null },
            { foo: 'oqw', bar: 'Lkqkq' }
        ];
        var filters = [];
        filters[firstColumn.columnId] = { filterData: {} };
        filters[secondColumn.columnId] = { filterData: {} };
        var filterState = new filter_state_1.FilterState(filters);
        var firstColumnFilterSpy = spyOn(firstColumn, 'filter').and.callFake(function (value, columnFilter, item) {
            return true;
        });
        var secondColumnFilterSpy = spyOn(secondColumn, 'filter').and.callFake(function (value, columnFilter, item) {
            return true;
        });
        var filteredItems = filterStateService.filterItems(items, filterState, columns);
        expect(filteredItems.length).toEqual(items.length);
    });
    it('filterItems with items not passing a single column filter', function () {
        var items = [
            { foo: 'Hsdawdd', bar: 'Asd' },
            { foo: 'Asd', bar: 'koq' },
            { foo: null, bar: 'Olqk' },
            { foo: 'Poqk' },
            { foo: '', bar: null },
            { foo: 'oqw', bar: 'Lkqkq' }
        ];
        var filters = [];
        filters[firstColumn.columnId] = { filterData: {} };
        filters[secondColumn.columnId] = { filterData: {} };
        var filterState = new filter_state_1.FilterState(filters);
        var firstColumnFilterSpy = spyOn(firstColumn, 'filter').and.callFake(function (value, columnFilter, item) {
            return item != items[0];
        });
        var secondColumnFilterSpy = spyOn(secondColumn, 'filter').and.callFake(function (value, columnFilter, item) {
            return item != items[1];
        });
        var filteredItems = filterStateService.filterItems(items, filterState, columns);
        expect(filteredItems.length).toEqual(items.length - 2);
    });
    it('filterItems with items not passing a multiple column filters', function () {
        var items = [
            { foo: 'Hsdawdd', bar: 'Asd' },
            { foo: 'Asd', bar: 'koq' },
            { foo: null, bar: 'Olqk' },
            { foo: 'Poqk' },
            { foo: '', bar: null },
            { foo: 'oqw', bar: 'Lkqkq' }
        ];
        var filters = [];
        filters[firstColumn.columnId] = { filterData: {} };
        filters[secondColumn.columnId] = { filterData: {} };
        var filterState = new filter_state_1.FilterState(filters);
        var firstColumnFilterSpy = spyOn(firstColumn, 'filter').and.callFake(function (value, columnFilter, item) {
            return item != items[0];
        });
        var secondColumnFilterSpy = spyOn(secondColumn, 'filter').and.callFake(function (value, columnFilter, item) {
            return item != items[0];
        });
        var filteredItems = filterStateService.filterItems(items, filterState, columns);
        expect(filteredItems.length).toEqual(items.length - 1);
    });
    it('raiseFilterChanged should add the specified column filter to the filter state', function () {
        var nextFilterSpy = spyOn(filterStateService.filterState, 'next');
        var columnId = '12345';
        var propertyName = 'baz';
        var filterData = {};
        var filterKey = 'bar';
        filterStateService.raiseFilterChanged(columnId, propertyName, filterData, filterKey);
        expect(nextFilterSpy).toHaveBeenCalled();
        var call = nextFilterSpy.calls.first();
        var newFilterState = call.args[0];
        expect(newFilterState.columnFilters[columnId]).not.toBeNull();
        expect(newFilterState.columnFilters[columnId].propertyName).toEqual(propertyName);
        expect(newFilterState.columnFilters[columnId].filterData).toEqual(filterData);
        expect(newFilterState.columnFilters[columnId].filterKey).toEqual(filterKey);
    });
});
//# sourceMappingURL=filter-state.service.spec.js.map