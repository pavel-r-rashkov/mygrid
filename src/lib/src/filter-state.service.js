"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ReplaySubject_1 = require("rxjs/ReplaySubject");
var filter_state_1 = require("./states/filter-state");
var FilterStateService = (function () {
    function FilterStateService() {
        var _this = this;
        this.currentState = new filter_state_1.FilterState({});
        this.filterState = new ReplaySubject_1.ReplaySubject(1);
        this.filterState.subscribe(function (newState) {
            _this.currentState = newState;
        });
    }
    FilterStateService.prototype.raiseFilterChanged = function (columnId, propertyName, filterData, filterKey) {
        var columnFilter = {
            propertyName: propertyName,
            filterData: filterData,
            filterKey: filterKey
        };
        this.currentState.columnFilters[columnId] = columnFilter;
        this.filterState.next(new filter_state_1.FilterState(this.currentState.columnFilters));
    };
    FilterStateService.prototype.clearColumnFilter = function (columnId) {
        this.currentState.columnFilters[columnId] = undefined;
        this.filterState.next(new filter_state_1.FilterState(this.currentState.columnFilters));
    };
    FilterStateService.prototype.filterItems = function (items, filterState, columns) {
        var filteredItems = items.filter(function (item) {
            var isFiltered = columns.some(function (col) {
                if (filterState.columnFilters[col.columnId] == null) {
                    return false;
                }
                var isPassingFilter = col.filter(col.getValue(item), filterState.columnFilters[col.columnId].filterData, item);
                return !isPassingFilter;
            });
            return !isFiltered;
        });
        return filteredItems;
    };
    FilterStateService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], FilterStateService);
    return FilterStateService;
}());
exports.FilterStateService = FilterStateService;
//# sourceMappingURL=filter-state.service.js.map