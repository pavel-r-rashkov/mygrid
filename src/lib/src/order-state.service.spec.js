"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var order_state_service_1 = require("./order-state.service");
var order_state_1 = require("./states/order-state");
var group_service_1 = require("./grouping/group.service");
var group_state_1 = require("./grouping/group-state");
var text_column_directive_1 = require("./grid-columns/text-column/text-column.directive");
var guid_1 = require("./guid");
var default_comparer_service_1 = require("./grid-columns/shared/default-comparer.service");
describe('Service: OrderStateService', function () {
    var orderStateService;
    var groupService;
    var columns;
    var firstColumn;
    var secondColumn;
    var sortedPropertyName = 'foo';
    beforeEach(function () {
        groupService = new group_service_1.GroupService();
        orderStateService = new order_state_service_1.OrderStateService(groupService);
        firstColumn = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        firstColumn.propertyName = sortedPropertyName;
        secondColumn = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        columns = [
            firstColumn,
            secondColumn
        ];
    });
    it('Sort non grouped items', function () {
        var items = [
            { foo: 'Hsdawdd' },
            { foo: 'Asd' },
            { foo: null },
            { foo: 'Poqk' },
            { foo: '' },
            { foo: 'oqw' }
        ];
        var orderState = new order_state_1.OrderState(firstColumn.columnId, sortedPropertyName, true);
        orderStateService.sortItems(items, orderState, null, columns);
        for (var i = 1; i < items.length; i++) {
            var firstItem = items[i - 1];
            var secondItem = items[i];
            var itemOrder = firstColumn.order(firstItem[sortedPropertyName], secondItem[sortedPropertyName], orderState.isAscending, firstItem, secondItem);
            var isOrderCorrect = itemOrder == -1 || itemOrder == 0;
            expect(isOrderCorrect).toBeTruthy();
        }
    });
    it('Sort grouped items', function () {
        var items = [
            {
                groupItems: [
                    { foo: 'Hsdawdd' },
                    { foo: 'Asd' },
                    { foo: null },
                    { foo: 'Poqk' },
                    { foo: '' },
                    { foo: 'oqw' }
                ]
            }
        ];
        var groupedColumnPropertyName = 'bar';
        secondColumn.propertyName = groupedColumnPropertyName;
        var orderState = new order_state_1.OrderState(firstColumn.columnId, sortedPropertyName, true);
        var groupState = new group_state_1.GroupState(secondColumn.columnId, groupedColumnPropertyName, true);
        orderStateService.sortItems(items, orderState, groupState, columns);
        for (var groupIndex = 0; groupIndex < items.length; groupIndex++) {
            for (var i = 1; i < items[groupIndex][groupService.GROUPED_ITEMS_KEY].length; i++) {
                var firstItem = items[groupIndex][groupService.GROUPED_ITEMS_KEY][i - 1];
                var secondItem = items[groupIndex][groupService.GROUPED_ITEMS_KEY][i];
                var itemOrder = firstColumn.order(firstItem[sortedPropertyName], secondItem[sortedPropertyName], orderState.isAscending, firstItem, secondItem);
                var isOrderCorrect = itemOrder == -1 || itemOrder == 0;
                expect(isOrderCorrect).toBeTruthy();
            }
        }
    });
});
//# sourceMappingURL=order-state.service.spec.js.map