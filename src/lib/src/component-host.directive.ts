import { 
  Directive,
  ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[my-grid-component-host]'
})
export class ComponentHostDirective {
  constructor(
    public viewContainerRef: ViewContainerRef) {
  }
}