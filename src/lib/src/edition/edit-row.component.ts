import { 
  Component,
  Input,
  OnInit,
  OnDestroy
} from '@angular/core';

import { 
  FormControl, 
  FormGroup,
  FormBuilder
} from '@angular/forms';

import {
  Observable,
  Subscription
} from 'rxjs';

import 'rxjs/add/operator/catch';

import { GridColumn } from '../grid-columns/grid-column';
import { RowEditionService } from '../edition/row-edition.service';
import { GridSettingsService } from '../grid-settings.service';
import { GridApiService } from '../grid-api.service';
import { HtmlElementType } from '../html-table-elements/html-element-type';

@Component({
  selector: '[my-grid-edit-row]',
  templateUrl: './edit-row.component.html'
})
export class EditRowComponent implements OnInit, OnDestroy {
  private endRowEditionSubscription: Subscription;
  @Input() columns: GridColumn[];
  @Input() item: any;
  editRowForm: FormGroup;
  htmlElementType: typeof HtmlElementType = HtmlElementType;

  constructor(
    private rowEditionService: RowEditionService,
    private settingsService: GridSettingsService,
    private gridApi: GridApiService) {

    this.endRowEditionSubscription = this.rowEditionService.endRowEditionSubject.subscribe((rowKey) => {
      if ((this.item == null && rowKey == null)
        || (this.item != null && rowKey == this.item[this.settingsService.key])) {
        this.saveEditChanges(rowKey);
      }
    });
  }

  ngOnInit() {
    this.initEditForm();
    
    let form = {
      form: this.editRowForm,
      isNewItem: this.item == null
    };
    this.rowEditionService.initForm(form);
  }

  ngOnDestroy() {
    this.endRowEditionSubscription.unsubscribe();
  }

  private initEditForm() {
    let formGroup = {};
    let formValues = {};

    this.columns.forEach((column) => {
      if (column.editComponentType != null && column.getData().propertyName != null) {
        formGroup[column.getData().propertyName] = new FormControl();
        if (this.item != null) {
          formValues[column.getData().propertyName] = column.getValue(this.item);
        }
      }
    });
  
    this.editRowForm = new FormGroup(formGroup);
    if (this.item != null) {
      this.editRowForm.setValue(formValues);
    }
  }

  private saveEditChanges(rowKey: any) {
    let formValues = this.editRowForm.value;

    // edit row
    if (this.item != null) {
      let changes = { item: formValues, isNewItem: false };

      if (this.settingsService.enableServerSideMode) {
        this.rowEditionService.saveChangesCallback(changes)
          .toPromise()
          .then(() => {
            this.rowEditionService.cancelRowEdition(rowKey);
            this.gridApi.refresh();
          })
          .catch((error) => { 
            console.error('Error updating item:', error);
          });
      } else {
        if (this.rowEditionService.saveChangesCallback != null) {
          this.rowEditionService.saveChangesCallback(changes)
            .toPromise()
            .then(() => {
              this.updateItem();
              this.gridApi.refresh();
            })
            .catch((error) => {
              console.error('Error updating item:', error);
            })
        } else {
          this.updateItem();
          this.gridApi.refresh();
        }
      }
    // new row
    } else {
      let changes = { item: formValues, isNewItem: true };

      if (this.settingsService.enableServerSideMode) {
        this.rowEditionService.saveChangesCallback(changes)
          .toPromise()
          .then(() => {
            this.editRowForm.reset();
            this.gridApi.refresh();
          })
          .catch((error) => {
            console.error('Error adding item:', error);
          });
      } else {
        if (this.rowEditionService.saveChangesCallback != null) {
          this.rowEditionService.saveChangesCallback(changes)
            .toPromise()
            .then(() => {
              this.editRowForm.reset();
              this.rowEditionService.itemAdded.next(changes.item);
            })
            .catch((error) => { 
              console.error('Error adding item:', error);
            });
        } else {
          this.editRowForm.reset();
          this.rowEditionService.itemAdded.next(changes.item);
        }
      }
    }
  }

  private updateItem() {
    let formValues = this.editRowForm.value;

    for (var key in formValues) {
      this.item[key] = formValues[key];
    }

    this.rowEditionService.cancelRowEdition(this.item[this.settingsService.key]);
  }
}