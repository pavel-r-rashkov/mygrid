"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RowEditionState = (function () {
    function RowEditionState(currentlyEditedRows) {
        this.currentlyEditedRows = currentlyEditedRows;
    }
    return RowEditionState;
}());
exports.RowEditionState = RowEditionState;
//# sourceMappingURL=row-edition-state.js.map