import { FormGroup } from '@angular/forms';
import { RowEditionService } from './row-edition.service';
import { RowEditionState } from './row-edition-state';


describe('Service: RowEditionService', () => {
  let rowEditionService: RowEditionService;
  let rowEditionSpy: jasmine.Spy;
  let endRowEditionSpy: jasmine.Spy;

  beforeEach(() => {
    rowEditionService = new RowEditionService();
    rowEditionSpy = spyOn(rowEditionService.rowEdition, 'next');
    endRowEditionSpy = spyOn(rowEditionService.endRowEditionSubject, 'next');
  });

  it('startRowEdition should add the row key to the current row edition state', () => {
    var editedRowKey = 20;

    rowEditionService.startRowEdition(editedRowKey);

    var currentRowEditionState: RowEditionState = rowEditionService.rowEdition.getValue();
    expect(currentRowEditionState.currentlyEditedRows).toEqual([editedRowKey]);
    expect(rowEditionSpy.calls.count()).toEqual(1);
  });

  it('startRowEdition should add the row key to the current row edition state without removing existing row keys', () => {
    var existingKey = 10;
    var editedRowKey = 20;

    rowEditionService.startRowEdition(existingKey);
    rowEditionService.startRowEdition(editedRowKey);

    var currentRowEditionState: RowEditionState = rowEditionService.rowEdition.getValue();
    expect(currentRowEditionState.currentlyEditedRows).toEqual([existingKey, editedRowKey]);
    expect(rowEditionSpy.calls.count()).toEqual(2);
  });

  it('startRowEdition with already edited row should not publish new row edition state', () => {
    var rowKey = 10;
  
    rowEditionService.startRowEdition(rowKey);
    rowEditionService.startRowEdition(rowKey);

    var currentRowEditionState: RowEditionState = rowEditionService.rowEdition.getValue();
    expect(currentRowEditionState.currentlyEditedRows).toEqual([rowKey]);
    expect(rowEditionSpy.calls.count()).toEqual(1);
  });

  it('endRowEdition should publish the key of the row', () => {
    var rowKey = 10;
  
    rowEditionService.endRowEdition(rowKey);

    expect(endRowEditionSpy.calls.count()).toEqual(1);
    expect(endRowEditionSpy.calls.first().args[0]).toEqual(rowKey);
  });

  it('cancelRowEdition should publish new row edition state without the canceled row key', () => {
    var rowKey = 10;
  
    rowEditionService.startRowEdition(rowKey);
    rowEditionService.cancelRowEdition(rowKey);

    var currentRowEditionState: RowEditionState = rowEditionService.rowEdition.getValue();
    expect(currentRowEditionState.currentlyEditedRows).toEqual([]);
    expect(rowEditionSpy.calls.count()).toEqual(2);
  });

  it('cancelRowEdition should publish new row edition state with row keys not equal to the calceled row key', () => {
    var existingKey = 20;
    var rowKey = 10;
  
    rowEditionService.startRowEdition(existingKey);
    rowEditionService.startRowEdition(rowKey);
    rowEditionService.cancelRowEdition(rowKey);

    var currentRowEditionState: RowEditionState = rowEditionService.rowEdition.getValue();
    expect(currentRowEditionState.currentlyEditedRows).toEqual([existingKey]);
  });

  it('cancelRowEdition should not publish new row edition state when the canceled row key does not exist in the current row edition state', () => {
    var existingKey = 20;
    var rowKey = 10;
  
    rowEditionService.startRowEdition(existingKey);
    rowEditionService.cancelRowEdition(rowKey);

    var currentRowEditionState: RowEditionState = rowEditionService.rowEdition.getValue();
    expect(currentRowEditionState.currentlyEditedRows).toEqual([existingKey]);
    expect(rowEditionSpy.calls.count()).toEqual(1);
  });

  it('initForm should call user provided initFormCallback if it exists', () => {
    var form = new FormGroup({});
    rowEditionService.initFormCallback = (editForm: FormGroup) => {};
    var initFormSpy = spyOn(rowEditionService, 'initFormCallback');

    rowEditionService.initForm(form);

    expect(initFormSpy.calls.count()).toEqual(1);
    expect(initFormSpy.calls.first().args[0]).toEqual(form);
  });
});