"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var forms_1 = require("@angular/forms");
var testing_1 = require("@angular/core/testing");
require("rxjs/add/observable/empty");
require("rxjs/add/observable/throw");
require("rxjs/add/operator/toPromise");
var Observable_1 = require("rxjs/Observable");
var edit_row_component_1 = require("./edit-row.component");
var row_edition_service_1 = require("./row-edition.service");
var grid_settings_service_1 = require("../grid-settings.service");
var grid_api_service_1 = require("../grid-api.service");
var text_column_directive_1 = require("../grid-columns/text-column/text-column.directive");
var guid_1 = require("../guid");
var html_table_element_directive_1 = require("../html-table-elements/html-table-element.directive");
var edit_wrapper_component_1 = require("../wrappers/edit-wrapper.component");
var default_comparer_service_1 = require("../grid-columns/shared/default-comparer.service");
describe('Component: EditRowComponent', function () {
    var editRowComponent;
    var fixture;
    var rowEditionService;
    var rowEditionServiceInitSpy;
    var itemAddedSpy;
    var cancelRowEditionSpy;
    var gridApiService;
    var refreshSpy;
    var settingsService;
    var ROW_KEY = 'id';
    var editedItem;
    var columns;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule
            ],
            declarations: [
                edit_row_component_1.EditRowComponent,
                html_table_element_directive_1.HtmlTableElementDirective,
                edit_wrapper_component_1.EditWrapperComponent
            ],
            providers: [
                row_edition_service_1.RowEditionService,
                grid_settings_service_1.GridSettingsService,
                grid_api_service_1.GridApiService
            ]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(edit_row_component_1.EditRowComponent);
            editRowComponent = fixture.componentInstance;
            settingsService = testing_1.TestBed.get(grid_settings_service_1.GridSettingsService);
            settingsService.key = ROW_KEY;
            rowEditionService = fixture.debugElement.injector.get(row_edition_service_1.RowEditionService);
            rowEditionServiceInitSpy = spyOn(rowEditionService, 'initForm');
            itemAddedSpy = spyOn(rowEditionService.itemAdded, 'next');
            cancelRowEditionSpy = spyOn(rowEditionService, 'cancelRowEdition');
            gridApiService = fixture.debugElement.injector.get(grid_api_service_1.GridApiService);
            gridApiService.refresh = function () { };
            refreshSpy = spyOn(gridApiService, 'refresh');
            editedItem = { firstName: 'Foo', secondName: 'Bar' };
            editedItem[ROW_KEY] = '1';
            var firstNameColumn = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
            firstNameColumn.propertyName = 'firstName';
            var secondNameColumn = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
            secondNameColumn.propertyName = 'secondName';
            columns = [
                firstNameColumn,
                secondNameColumn
            ];
            editRowComponent.columns = columns;
        });
    }));
    it('editRowForm should be initialized with empty values when EditRowComponent is created', function () {
        editRowComponent.ngOnInit();
        var formValues = editRowComponent.editRowForm.value;
        var initArgs = rowEditionServiceInitSpy.calls.first().args[0];
        expect(formValues.firstName).toBeNull();
        expect(formValues.secondName).toBeNull();
        expect(rowEditionServiceInitSpy.calls.count()).toEqual(1);
        expect(initArgs.form).toEqual(editRowComponent.editRowForm);
        expect(initArgs.isNewItem).toBeTruthy();
    });
    it('editRowForm should be initialized with item\'s values when EditRowComponent is created', function () {
        editRowComponent.item = editedItem;
        editRowComponent.ngOnInit();
        var formValues = editRowComponent.editRowForm.value;
        var initArgs = rowEditionServiceInitSpy.calls.first().args[0];
        expect(formValues.firstName).toEqual(editedItem.firstName);
        expect(formValues.secondName).toEqual(editedItem.secondName);
        expect(rowEditionServiceInitSpy.calls.count()).toEqual(1);
        expect(initArgs.form).toEqual(editRowComponent.editRowForm);
        expect(initArgs.isNewItem).toBeFalsy();
    });
    it('EditRowComponent for new item should not save changes when end row edition subject emits row key different than null', function () {
        settingsService.enableServerSideMode = false;
        editRowComponent.ngOnInit();
        rowEditionService.endRowEditionSubject.next(123);
        expect(itemAddedSpy.calls.count()).toEqual(0);
    });
    it('EditRowComponent for existing item should not save changes when end row edition subject emits row key different than item\'s key', function () {
        settingsService.enableServerSideMode = false;
        editRowComponent.item = editedItem;
        editRowComponent.ngOnInit();
        rowEditionService.endRowEditionSubject.next(123);
        expect(refreshSpy.calls.count()).toEqual(0);
    });
    it('EditRowComponent for new item with enabled server-side mode should call saveChangesCallback, reset the form and refresh', testing_1.async(function () {
        settingsService.enableServerSideMode = true;
        rowEditionService.saveChangesCallback = function (changes) { return Observable_1.Observable.empty(); };
        var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
        editRowComponent.ngOnInit();
        var editRowFormResetSpy = spyOn(editRowComponent.editRowForm, 'reset');
        rowEditionService.endRowEditionSubject.next(null);
        var saveChangesArg = rowEditionSaveChangesSpy.calls.first().args[0];
        expect(saveChangesArg.isNewItem).toEqual(true);
        expect(saveChangesArg.item).toEqual(editRowComponent.editRowForm.value);
        expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
        fixture.whenStable().then(function () {
            expect(editRowFormResetSpy.calls.count()).toEqual(1);
            expect(refreshSpy.calls.count()).toEqual(1);
        });
    }));
    it('EditRowComponent for new item with enabled server-side mode with unsuccessful call to saveChangesCallback should not reset the form and refresh', testing_1.async(function () {
        settingsService.enableServerSideMode = true;
        var errorMessage = 'sample message';
        rowEditionService.saveChangesCallback = function (changes) { return Observable_1.Observable.throw(errorMessage); };
        var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
        editRowComponent.ngOnInit();
        var editRowFormResetSpy = spyOn(editRowComponent.editRowForm, 'reset');
        rowEditionService.endRowEditionSubject.next(null);
        expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
        fixture.whenStable().then(function () {
            expect(editRowFormResetSpy.calls.count()).toEqual(0);
            expect(refreshSpy.calls.count()).toEqual(0);
            // assert log is called
        });
    }));
    it('EditRowComponent for new item with disabled server-side mode with no saveChangesCallback should reset the form and publish itemAdded', function () {
        settingsService.enableServerSideMode = false;
        editRowComponent.ngOnInit();
        var editRowFormResetSpy = spyOn(editRowComponent.editRowForm, 'reset');
        rowEditionService.endRowEditionSubject.next(null);
        expect(editRowFormResetSpy.calls.count()).toEqual(1);
        expect(itemAddedSpy.calls.count()).toEqual(1);
        expect(itemAddedSpy.calls.first().args[0]).toEqual(editRowComponent.editRowForm.value);
    });
    it('EditRowComponent for new item with disabled server-side mode with successful call to saveChangesCallback should reset the form and publish item added', testing_1.async(function () {
        settingsService.enableServerSideMode = false;
        rowEditionService.saveChangesCallback = function (changes) { return Observable_1.Observable.empty(); };
        var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
        editRowComponent.ngOnInit();
        var editRowFormResetSpy = spyOn(editRowComponent.editRowForm, 'reset');
        rowEditionService.endRowEditionSubject.next(null);
        expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
        fixture.whenStable().then(function () {
            expect(editRowFormResetSpy.calls.count()).toEqual(1);
            expect(itemAddedSpy.calls.count()).toEqual(1);
            expect(itemAddedSpy.calls.first().args[0]).toEqual(editRowComponent.editRowForm.value);
        });
    }));
    it('EditRowComponent for new item with disabled server-side mode with unsuccessful call to saveChangesCallback should not reset the form and publish item added', testing_1.async(function () {
        settingsService.enableServerSideMode = false;
        var errorMessage = 'sample message';
        rowEditionService.saveChangesCallback = function (changes) { return Observable_1.Observable.throw(errorMessage); };
        var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
        editRowComponent.ngOnInit();
        var editRowFormResetSpy = spyOn(editRowComponent.editRowForm, 'reset');
        rowEditionService.endRowEditionSubject.next(null);
        expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
        fixture.whenStable().then(function () {
            expect(editRowFormResetSpy.calls.count()).toEqual(0);
            expect(itemAddedSpy.calls.count()).toEqual(0);
            // assert error is logged
        });
    }));
    it('EditRowComponent for existing item with enabled server-side mode should call saveChangesCallback, cancel row edition and refresh', testing_1.async(function () {
        settingsService.enableServerSideMode = true;
        editRowComponent.item = editedItem;
        rowEditionService.saveChangesCallback = function (changes) { return Observable_1.Observable.empty(); };
        var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
        editRowComponent.ngOnInit();
        rowEditionService.endRowEditionSubject.next(editedItem[ROW_KEY]);
        var saveChangesArg = rowEditionSaveChangesSpy.calls.first().args[0];
        expect(saveChangesArg.isNewItem).toEqual(false);
        expect(saveChangesArg.item).toEqual(editRowComponent.editRowForm.value);
        expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
        fixture.whenStable().then(function () {
            expect(cancelRowEditionSpy.calls.count()).toEqual(1);
            expect(cancelRowEditionSpy.calls.first().args[0]).toEqual(editedItem[ROW_KEY]);
            expect(refreshSpy.calls.count()).toEqual(1);
        });
    }));
    it('EditRowComponent for existing item with enabled server-side mode and unsuccessful call to saveChangesCallback should not cancel row edition and refresh', testing_1.async(function () {
        settingsService.enableServerSideMode = true;
        var errorMessage = 'sample message';
        editRowComponent.item = editedItem;
        rowEditionService.saveChangesCallback = function (changes) { return Observable_1.Observable.throw(errorMessage); };
        var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
        editRowComponent.ngOnInit();
        rowEditionService.endRowEditionSubject.next(editedItem[ROW_KEY]);
        var saveChangesArg = rowEditionSaveChangesSpy.calls.first().args[0];
        expect(saveChangesArg.isNewItem).toEqual(false);
        expect(saveChangesArg.item).toEqual(editRowComponent.editRowForm.value);
        expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
        fixture.whenStable().then(function () {
            expect(cancelRowEditionSpy.calls.count()).toEqual(0);
            expect(refreshSpy.calls.count()).toEqual(0);
            // assert error is logged
        });
    }));
    it('EditRowComponent for existing item with disabled server-side mode and no saveChangesCallback should update the item, cancel row edition and refresh', function () {
        settingsService.enableServerSideMode = false;
        var editedFirstName = 'Edited';
        editRowComponent.item = editedItem;
        editRowComponent.ngOnInit();
        editRowComponent.editRowForm.patchValue({ firstName: editedFirstName });
        rowEditionService.endRowEditionSubject.next(editedItem[ROW_KEY]);
        expect(editedItem.firstName).toEqual(editedFirstName);
        expect(cancelRowEditionSpy.calls.count()).toEqual(1);
        expect(cancelRowEditionSpy.calls.first().args[0]).toEqual(editedItem[ROW_KEY]);
        expect(refreshSpy.calls.count()).toEqual(1);
    });
    it('EditRowComponent for existing item with disabled server-side mode and successful call to saveChangesCallback should cancel row edition and refresh', testing_1.async(function () {
        settingsService.enableServerSideMode = false;
        var editedFirstName = 'Edited';
        editRowComponent.item = editedItem;
        rowEditionService.saveChangesCallback = function (changes) { return Observable_1.Observable.empty(); };
        var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
        editRowComponent.ngOnInit();
        editRowComponent.editRowForm.patchValue({ firstName: editedFirstName });
        rowEditionService.endRowEditionSubject.next(editedItem[ROW_KEY]);
        var saveChangesArg = rowEditionSaveChangesSpy.calls.first().args[0];
        expect(saveChangesArg.isNewItem).toEqual(false);
        expect(saveChangesArg.item).toEqual(editRowComponent.editRowForm.value);
        expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
        fixture.whenStable().then(function () {
            expect(editedItem.firstName).toEqual(editedFirstName);
            expect(cancelRowEditionSpy.calls.count()).toEqual(1);
            expect(cancelRowEditionSpy.calls.first().args[0]).toEqual(editedItem[ROW_KEY]);
            expect(refreshSpy.calls.count()).toEqual(1);
        });
    }));
    it('EditRowComponent for existing item with disabled server-side mode and unsuccessful call to saveChangesCallback should not cancel row edition and refresh', testing_1.async(function () {
        settingsService.enableServerSideMode = false;
        var editedFirstName = 'Edited';
        var errorMessage = 'sample message';
        editRowComponent.item = editedItem;
        rowEditionService.saveChangesCallback = function (changes) { return Observable_1.Observable.throw(errorMessage); };
        var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
        editRowComponent.ngOnInit();
        editRowComponent.editRowForm.patchValue({ firstName: editedFirstName });
        rowEditionService.endRowEditionSubject.next(editedItem[ROW_KEY]);
        var saveChangesArg = rowEditionSaveChangesSpy.calls.first().args[0];
        expect(saveChangesArg.isNewItem).toEqual(false);
        expect(saveChangesArg.item).toEqual(editRowComponent.editRowForm.value);
        expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
        fixture.whenStable().then(function () {
            expect(editedItem.firstName).not.toEqual(editedFirstName);
            expect(cancelRowEditionSpy.calls.count()).toEqual(0);
            expect(refreshSpy.calls.count()).toEqual(0);
            // assert error is logged
        });
    }));
});
//# sourceMappingURL=edit-row.component.spec.js.map