"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var Subject_1 = require("rxjs/Subject");
var row_edition_state_1 = require("./row-edition-state");
var RowEditionService = (function () {
    function RowEditionService() {
        this.rowEdition = new BehaviorSubject_1.BehaviorSubject(new row_edition_state_1.RowEditionState([]));
        this.endRowEditionSubject = new Subject_1.Subject();
        this.itemAdded = new Subject_1.Subject();
    }
    RowEditionService.prototype.startRowEdition = function (rowKey) {
        var currentState = this.rowEdition.getValue();
        if (currentState.currentlyEditedRows.indexOf(rowKey) < 0) {
            currentState.currentlyEditedRows.push(rowKey);
            this.rowEdition.next(currentState);
        }
    };
    RowEditionService.prototype.endRowEdition = function (rowKey) {
        this.endRowEditionSubject.next(rowKey);
    };
    RowEditionService.prototype.cancelRowEdition = function (rowKey) {
        var currentState = this.rowEdition.getValue();
        var canceledRowKeyIndex = currentState.currentlyEditedRows.indexOf(rowKey);
        if (canceledRowKeyIndex >= 0) {
            currentState.currentlyEditedRows.splice(canceledRowKeyIndex, 1);
            this.rowEdition.next(currentState);
        }
    };
    RowEditionService.prototype.initForm = function (form) {
        if (this.initFormCallback != null) {
            this.initFormCallback(form);
        }
    };
    RowEditionService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], RowEditionService);
    return RowEditionService;
}());
exports.RowEditionService = RowEditionService;
//# sourceMappingURL=row-edition.service.js.map