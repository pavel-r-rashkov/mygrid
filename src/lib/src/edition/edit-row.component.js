"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
require("rxjs/add/operator/catch");
var row_edition_service_1 = require("../edition/row-edition.service");
var grid_settings_service_1 = require("../grid-settings.service");
var grid_api_service_1 = require("../grid-api.service");
var html_element_type_1 = require("../html-table-elements/html-element-type");
var EditRowComponent = (function () {
    function EditRowComponent(rowEditionService, settingsService, gridApi) {
        var _this = this;
        this.rowEditionService = rowEditionService;
        this.settingsService = settingsService;
        this.gridApi = gridApi;
        this.htmlElementType = html_element_type_1.HtmlElementType;
        this.endRowEditionSubscription = this.rowEditionService.endRowEditionSubject.subscribe(function (rowKey) {
            if ((_this.item == null && rowKey == null)
                || (_this.item != null && rowKey == _this.item[_this.settingsService.key])) {
                _this.saveEditChanges(rowKey);
            }
        });
    }
    EditRowComponent.prototype.ngOnInit = function () {
        this.initEditForm();
        var form = {
            form: this.editRowForm,
            isNewItem: this.item == null
        };
        this.rowEditionService.initForm(form);
    };
    EditRowComponent.prototype.ngOnDestroy = function () {
        this.endRowEditionSubscription.unsubscribe();
    };
    EditRowComponent.prototype.initEditForm = function () {
        var _this = this;
        var formGroup = {};
        var formValues = {};
        this.columns.forEach(function (column) {
            if (column.editComponentType != null && column.getData().propertyName != null) {
                formGroup[column.getData().propertyName] = new forms_1.FormControl();
                if (_this.item != null) {
                    formValues[column.getData().propertyName] = column.getValue(_this.item);
                }
            }
        });
        this.editRowForm = new forms_1.FormGroup(formGroup);
        if (this.item != null) {
            this.editRowForm.setValue(formValues);
        }
    };
    EditRowComponent.prototype.saveEditChanges = function (rowKey) {
        var _this = this;
        var formValues = this.editRowForm.value;
        // edit row
        if (this.item != null) {
            var changes = { item: formValues, isNewItem: false };
            if (this.settingsService.enableServerSideMode) {
                this.rowEditionService.saveChangesCallback(changes)
                    .toPromise()
                    .then(function () {
                    _this.rowEditionService.cancelRowEdition(rowKey);
                    _this.gridApi.refresh();
                })
                    .catch(function (error) {
                    console.error('Error updating item:', error);
                });
            }
            else {
                if (this.rowEditionService.saveChangesCallback != null) {
                    this.rowEditionService.saveChangesCallback(changes)
                        .toPromise()
                        .then(function () {
                        _this.updateItem();
                        _this.gridApi.refresh();
                    })
                        .catch(function (error) {
                        console.error('Error updating item:', error);
                    });
                }
                else {
                    this.updateItem();
                    this.gridApi.refresh();
                }
            }
            // new row
        }
        else {
            var changes_1 = { item: formValues, isNewItem: true };
            if (this.settingsService.enableServerSideMode) {
                this.rowEditionService.saveChangesCallback(changes_1)
                    .toPromise()
                    .then(function () {
                    _this.editRowForm.reset();
                    _this.gridApi.refresh();
                })
                    .catch(function (error) {
                    console.error('Error adding item:', error);
                });
            }
            else {
                if (this.rowEditionService.saveChangesCallback != null) {
                    this.rowEditionService.saveChangesCallback(changes_1)
                        .toPromise()
                        .then(function () {
                        _this.editRowForm.reset();
                        _this.rowEditionService.itemAdded.next(changes_1.item);
                    })
                        .catch(function (error) {
                        console.error('Error adding item:', error);
                    });
                }
                else {
                    this.editRowForm.reset();
                    this.rowEditionService.itemAdded.next(changes_1.item);
                }
            }
        }
    };
    EditRowComponent.prototype.updateItem = function () {
        var formValues = this.editRowForm.value;
        for (var key in formValues) {
            this.item[key] = formValues[key];
        }
        this.rowEditionService.cancelRowEdition(this.item[this.settingsService.key]);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], EditRowComponent.prototype, "columns", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], EditRowComponent.prototype, "item", void 0);
    EditRowComponent = __decorate([
        core_1.Component({
            selector: '[my-grid-edit-row]',
            templateUrl: './edit-row.component.html'
        }),
        __metadata("design:paramtypes", [row_edition_service_1.RowEditionService,
            grid_settings_service_1.GridSettingsService,
            grid_api_service_1.GridApiService])
    ], EditRowComponent);
    return EditRowComponent;
}());
exports.EditRowComponent = EditRowComponent;
//# sourceMappingURL=edit-row.component.js.map