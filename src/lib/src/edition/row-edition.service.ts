import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

import { RowEditionState } from './row-edition-state';

@Injectable()
export class RowEditionService {
  public rowEdition: BehaviorSubject<RowEditionState>;
  public endRowEditionSubject: Subject<any>;
  public itemAdded: Subject<any>;
  public saveChangesCallback: (changes: any) => Observable<any>;
  public initFormCallback: (editForm: any) => void;
  public getErrorMessages: (errorKeys: any, propertyName: string) => string[];

  constructor() {
    this.rowEdition = new BehaviorSubject<RowEditionState>(new RowEditionState([]));
    this.endRowEditionSubject = new Subject<any>();
    this.itemAdded = new Subject<any>();
  }

  public startRowEdition(rowKey: any) {
    let currentState = this.rowEdition.getValue();
    if (currentState.currentlyEditedRows.indexOf(rowKey) < 0) {
      currentState.currentlyEditedRows.push(rowKey);
      this.rowEdition.next(currentState);
    }
  }

  public endRowEdition(rowKey: any) {
    this.endRowEditionSubject.next(rowKey);
  }

  public cancelRowEdition(rowKey: any) {
    let currentState = this.rowEdition.getValue();
    var canceledRowKeyIndex = currentState.currentlyEditedRows.indexOf(rowKey);
    if (canceledRowKeyIndex >= 0) {
      currentState.currentlyEditedRows.splice(canceledRowKeyIndex, 1);
      this.rowEdition.next(currentState);
    }
  }

  public initForm(form: any) {
    if (this.initFormCallback != null) {
      this.initFormCallback(form);
    }
  }
}