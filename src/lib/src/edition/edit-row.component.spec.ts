import { 
  FormsModule, 
  ReactiveFormsModule 
} from '@angular/forms';
import {
  ComponentFixture, 
  TestBed,
  async
} from '@angular/core/testing';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import { Observable } from "rxjs/Observable";
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { EditRowComponent } from './edit-row.component';
import { RowEditionService } from './row-edition.service';
import { GridSettingsService } from '../grid-settings.service';
import { GridApiService } from '../grid-api.service';
import { GridColumn } from '../grid-columns/grid-column';
import { IndexColumnDirective } from '../grid-columns/index-column/index-column.directive';
import { TextColumnDirective } from '../grid-columns/text-column/text-column.directive';
import { GuidService } from '../guid';
import { HtmlTableElementDirective } from '../html-table-elements/html-table-element.directive';
import { EditWrapperComponent } from '../wrappers/edit-wrapper.component';
import { DefaultComparerService } from '../grid-columns/shared/default-comparer.service';

describe('Component: EditRowComponent', () => {
  let editRowComponent: EditRowComponent;
  let fixture: ComponentFixture<EditRowComponent>;
  let rowEditionService: RowEditionService;
  let rowEditionServiceInitSpy: jasmine.Spy;
  let itemAddedSpy: jasmine.Spy;
  let cancelRowEditionSpy: jasmine.Spy;
  let gridApiService: GridApiService;
  let refreshSpy: jasmine.Spy;
  let settingsService: GridSettingsService;
  const ROW_KEY: string = 'id';
  let editedItem: any;
  let columns: GridColumn[];
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [
        EditRowComponent,
        HtmlTableElementDirective,
        EditWrapperComponent
      ],
      providers: [
        RowEditionService,
        GridSettingsService,
        GridApiService
      ]
    })
    .compileComponents()
    .then(() => {
      fixture = TestBed.createComponent(EditRowComponent);
      editRowComponent = fixture.componentInstance;

      settingsService = TestBed.get(GridSettingsService);
      settingsService.key = ROW_KEY; 

      rowEditionService = fixture.debugElement.injector.get(RowEditionService);
      rowEditionServiceInitSpy = spyOn(rowEditionService, 'initForm');
      itemAddedSpy = spyOn(rowEditionService.itemAdded, 'next');
      cancelRowEditionSpy = spyOn(rowEditionService, 'cancelRowEdition');

      gridApiService = fixture.debugElement.injector.get(GridApiService);
      gridApiService.refresh = () => {};
      refreshSpy = spyOn(gridApiService, 'refresh');

      editedItem = { firstName: 'Foo', secondName: 'Bar' };
      editedItem[ROW_KEY] = '1';

      let firstNameColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
      firstNameColumn.propertyName = 'firstName';

      let secondNameColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
      secondNameColumn.propertyName = 'secondName';

      columns = [
        firstNameColumn,
        secondNameColumn
      ];

      editRowComponent.columns = columns;
    });
  }));

  it('editRowForm should be initialized with empty values when EditRowComponent is created', () => {
    editRowComponent.ngOnInit();

    var formValues = editRowComponent.editRowForm.value;
    var initArgs = rowEditionServiceInitSpy.calls.first().args[0];

    expect(formValues.firstName).toBeNull();
    expect(formValues.secondName).toBeNull();
    expect(rowEditionServiceInitSpy.calls.count()).toEqual(1);
    expect(initArgs.form).toEqual(editRowComponent.editRowForm);
    expect(initArgs.isNewItem).toBeTruthy();
  });

  it('editRowForm should be initialized with item\'s values when EditRowComponent is created', () => {
    editRowComponent.item = editedItem;

    editRowComponent.ngOnInit();
    var formValues = editRowComponent.editRowForm.value;
    var initArgs = rowEditionServiceInitSpy.calls.first().args[0];
    
    expect(formValues.firstName).toEqual(editedItem.firstName);
    expect(formValues.secondName).toEqual(editedItem.secondName);
    expect(rowEditionServiceInitSpy.calls.count()).toEqual(1);
    expect(initArgs.form).toEqual(editRowComponent.editRowForm);
    expect(initArgs.isNewItem).toBeFalsy();
  });

  it('EditRowComponent for new item should not save changes when end row edition subject emits row key different than null', () => {
     settingsService.enableServerSideMode = false;
     
    editRowComponent.ngOnInit();
    rowEditionService.endRowEditionSubject.next(123);

    expect(itemAddedSpy.calls.count()).toEqual(0);
  });

  it('EditRowComponent for existing item should not save changes when end row edition subject emits row key different than item\'s key', () => {
    settingsService.enableServerSideMode = false;
    editRowComponent.item = editedItem;
    
    editRowComponent.ngOnInit();
    rowEditionService.endRowEditionSubject.next(123);

    expect(refreshSpy.calls.count()).toEqual(0);
  });

  it('EditRowComponent for new item with enabled server-side mode should call saveChangesCallback, reset the form and refresh', async(() => {
    settingsService.enableServerSideMode = true;
    rowEditionService.saveChangesCallback = (changes) => { return Observable.empty(); };
    var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
    
    editRowComponent.ngOnInit();
    var editRowFormResetSpy = spyOn(editRowComponent.editRowForm, 'reset');
    rowEditionService.endRowEditionSubject.next(null);

    var saveChangesArg = rowEditionSaveChangesSpy.calls.first().args[0];
    expect(saveChangesArg.isNewItem).toEqual(true);
    expect(saveChangesArg.item).toEqual(editRowComponent.editRowForm.value);
    expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
    fixture.whenStable().then(() => {
      expect(editRowFormResetSpy.calls.count()).toEqual(1);
      expect(refreshSpy.calls.count()).toEqual(1);
    });
  }));

  it('EditRowComponent for new item with enabled server-side mode with unsuccessful call to saveChangesCallback should not reset the form and refresh', async(() => {
    settingsService.enableServerSideMode = true;
    var errorMessage: string = 'sample message';
    rowEditionService.saveChangesCallback = (changes) => { return Observable.throw(errorMessage); };
    var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
    
    editRowComponent.ngOnInit();
    var editRowFormResetSpy = spyOn(editRowComponent.editRowForm, 'reset');
    rowEditionService.endRowEditionSubject.next(null);

    expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
    fixture.whenStable().then(() => {
      expect(editRowFormResetSpy.calls.count()).toEqual(0);
      expect(refreshSpy.calls.count()).toEqual(0);
      // assert log is called
    });
  }));

  it('EditRowComponent for new item with disabled server-side mode with no saveChangesCallback should reset the form and publish itemAdded', () => {
    settingsService.enableServerSideMode = false;
    
    editRowComponent.ngOnInit();
    var editRowFormResetSpy = spyOn(editRowComponent.editRowForm, 'reset');
    rowEditionService.endRowEditionSubject.next(null);

    expect(editRowFormResetSpy.calls.count()).toEqual(1);
    expect(itemAddedSpy.calls.count()).toEqual(1);
    expect(itemAddedSpy.calls.first().args[0]).toEqual(editRowComponent.editRowForm.value);
  });

  it('EditRowComponent for new item with disabled server-side mode with successful call to saveChangesCallback should reset the form and publish item added', async(() => {
    settingsService.enableServerSideMode = false;
    
    rowEditionService.saveChangesCallback = (changes) => { return Observable.empty(); };
    var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
    
    editRowComponent.ngOnInit();
    var editRowFormResetSpy = spyOn(editRowComponent.editRowForm, 'reset');
    rowEditionService.endRowEditionSubject.next(null);

    expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
    fixture.whenStable().then(() => {
      expect(editRowFormResetSpy.calls.count()).toEqual(1);
      expect(itemAddedSpy.calls.count()).toEqual(1);
      expect(itemAddedSpy.calls.first().args[0]).toEqual(editRowComponent.editRowForm.value);
    });
  }));

  it('EditRowComponent for new item with disabled server-side mode with unsuccessful call to saveChangesCallback should not reset the form and publish item added', async(() => {
    settingsService.enableServerSideMode = false;
    var errorMessage: string = 'sample message';
    rowEditionService.saveChangesCallback = (changes) => { return Observable.throw(errorMessage); };
    var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
    
    editRowComponent.ngOnInit();
    var editRowFormResetSpy = spyOn(editRowComponent.editRowForm, 'reset');
    rowEditionService.endRowEditionSubject.next(null);

    expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
    fixture.whenStable().then(() => {
      expect(editRowFormResetSpy.calls.count()).toEqual(0);
      expect(itemAddedSpy.calls.count()).toEqual(0);
      // assert error is logged
    });
  }));

  it('EditRowComponent for existing item with enabled server-side mode should call saveChangesCallback, cancel row edition and refresh', async(() => {
    settingsService.enableServerSideMode = true;
    editRowComponent.item = editedItem;
    rowEditionService.saveChangesCallback = (changes) => { return Observable.empty(); };
    var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
    
    editRowComponent.ngOnInit();
    rowEditionService.endRowEditionSubject.next(editedItem[ROW_KEY]);

    var saveChangesArg = rowEditionSaveChangesSpy.calls.first().args[0];
    expect(saveChangesArg.isNewItem).toEqual(false);
    expect(saveChangesArg.item).toEqual(editRowComponent.editRowForm.value);
    expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
    fixture.whenStable().then(() => {
      expect(cancelRowEditionSpy.calls.count()).toEqual(1);
      expect(cancelRowEditionSpy.calls.first().args[0]).toEqual(editedItem[ROW_KEY]);
      expect(refreshSpy.calls.count()).toEqual(1);
    });
  }));

  it('EditRowComponent for existing item with enabled server-side mode and unsuccessful call to saveChangesCallback should not cancel row edition and refresh', async(() => {
    settingsService.enableServerSideMode = true;
    var errorMessage: string = 'sample message';
    editRowComponent.item = editedItem;
    rowEditionService.saveChangesCallback = (changes) => { return Observable.throw(errorMessage); };
    var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
    
    editRowComponent.ngOnInit();
    rowEditionService.endRowEditionSubject.next(editedItem[ROW_KEY]);

    var saveChangesArg = rowEditionSaveChangesSpy.calls.first().args[0];
    expect(saveChangesArg.isNewItem).toEqual(false);
    expect(saveChangesArg.item).toEqual(editRowComponent.editRowForm.value);
    expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
    fixture.whenStable().then(() => {
      expect(cancelRowEditionSpy.calls.count()).toEqual(0);
      expect(refreshSpy.calls.count()).toEqual(0);
      // assert error is logged
    });
  }));

  it('EditRowComponent for existing item with disabled server-side mode and no saveChangesCallback should update the item, cancel row edition and refresh', () => {
    settingsService.enableServerSideMode = false;
    var editedFirstName = 'Edited';
    editRowComponent.item = editedItem;
    
    editRowComponent.ngOnInit();
    editRowComponent.editRowForm.patchValue({ firstName: editedFirstName });
    rowEditionService.endRowEditionSubject.next(editedItem[ROW_KEY]);

    expect(editedItem.firstName).toEqual(editedFirstName);
    expect(cancelRowEditionSpy.calls.count()).toEqual(1);
    expect(cancelRowEditionSpy.calls.first().args[0]).toEqual(editedItem[ROW_KEY]);
    expect(refreshSpy.calls.count()).toEqual(1);
  });

  it('EditRowComponent for existing item with disabled server-side mode and successful call to saveChangesCallback should cancel row edition and refresh', async(() => {
    settingsService.enableServerSideMode = false;
    var editedFirstName = 'Edited';
    editRowComponent.item = editedItem;
    rowEditionService.saveChangesCallback = (changes) => { return Observable.empty(); };
    var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
    
    editRowComponent.ngOnInit();
    editRowComponent.editRowForm.patchValue({ firstName: editedFirstName });
    rowEditionService.endRowEditionSubject.next(editedItem[ROW_KEY]);

    var saveChangesArg = rowEditionSaveChangesSpy.calls.first().args[0];
    expect(saveChangesArg.isNewItem).toEqual(false);
    expect(saveChangesArg.item).toEqual(editRowComponent.editRowForm.value);
    expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
    fixture.whenStable().then(() => {
      expect(editedItem.firstName).toEqual(editedFirstName);
      expect(cancelRowEditionSpy.calls.count()).toEqual(1);
      expect(cancelRowEditionSpy.calls.first().args[0]).toEqual(editedItem[ROW_KEY]);
      expect(refreshSpy.calls.count()).toEqual(1);
    });
  }));

  it('EditRowComponent for existing item with disabled server-side mode and unsuccessful call to saveChangesCallback should not cancel row edition and refresh', async(() => {
    settingsService.enableServerSideMode = false;
    var editedFirstName = 'Edited';
    var errorMessage: string = 'sample message';
    editRowComponent.item = editedItem;
    rowEditionService.saveChangesCallback = (changes) => { return Observable.throw(errorMessage); };
    var rowEditionSaveChangesSpy = spyOn(rowEditionService, 'saveChangesCallback').and.callThrough();
    
    editRowComponent.ngOnInit();
    editRowComponent.editRowForm.patchValue({ firstName: editedFirstName });
    rowEditionService.endRowEditionSubject.next(editedItem[ROW_KEY]);

    var saveChangesArg = rowEditionSaveChangesSpy.calls.first().args[0];
    expect(saveChangesArg.isNewItem).toEqual(false);
    expect(saveChangesArg.item).toEqual(editRowComponent.editRowForm.value);
    expect(rowEditionSaveChangesSpy.calls.count()).toEqual(1);
    fixture.whenStable().then(() => {
      expect(editedItem.firstName).not.toEqual(editedFirstName);
      expect(cancelRowEditionSpy.calls.count()).toEqual(0);
      expect(refreshSpy.calls.count()).toEqual(0);
      // assert error is logged
    });
  }));
});