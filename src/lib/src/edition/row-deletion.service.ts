import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class RowDeletionService {
	public beforeRowDelete: Subject<any>;

	constructor() {
		this.beforeRowDelete = new Subject<any>();
	}
}