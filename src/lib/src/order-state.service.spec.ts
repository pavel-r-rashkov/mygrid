import { OrderStateService } from './order-state.service';
import { OrderState } from './states/order-state';
import { GroupService } from './grouping/group.service';
import { GroupState } from './grouping/group-state';
import { GridColumn } from './grid-columns/grid-column';
import { TextColumnDirective } from './grid-columns/text-column/text-column.directive';
import { GuidService } from './guid';
import { DefaultComparerService } from './grid-columns/shared/default-comparer.service';

describe('Service: OrderStateService', () => {
  let orderStateService: OrderStateService;
  let groupService: GroupService;
  let columns: GridColumn[];
  let firstColumn: TextColumnDirective;
  let secondColumn: TextColumnDirective;
  let sortedPropertyName = 'foo';

  beforeEach(() => {
    groupService = new GroupService();
    orderStateService = new OrderStateService(groupService);
    firstColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    firstColumn.propertyName = sortedPropertyName;
    secondColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    columns = [
      firstColumn,
      secondColumn
    ];
  });

  it('Sort non grouped items', () => {
    var items = [
      { foo: 'Hsdawdd' },
      { foo: 'Asd' },
      { foo: null },
      { foo: 'Poqk' },
      { foo: '' },
      { foo: 'oqw' }
    ];
    var orderState = new OrderState(firstColumn.columnId, sortedPropertyName, true);
    
    orderStateService.sortItems(items, orderState, null, columns);

    for (var i = 1; i < items.length; i++) {
      let firstItem = items[i - 1];
      let secondItem = items[i];
      let itemOrder = firstColumn.order(firstItem[sortedPropertyName], secondItem[sortedPropertyName], orderState.isAscending, firstItem, secondItem);
      let isOrderCorrect = itemOrder == -1 || itemOrder == 0;

      expect(isOrderCorrect).toBeTruthy();
    }
  });

  it('Sort grouped items', () => {
    var items = [
      { 
        groupItems: [
          { foo: 'Hsdawdd' },
          { foo: 'Asd' },
          { foo: null },
          { foo: 'Poqk' },
          { foo: '' },
          { foo: 'oqw' }
        ] 
      }
    ];
    var groupedColumnPropertyName = 'bar';
    secondColumn.propertyName = groupedColumnPropertyName;
    var orderState = new OrderState(firstColumn.columnId, sortedPropertyName, true);
    var groupState = new GroupState(secondColumn.columnId, groupedColumnPropertyName, true);
    
    orderStateService.sortItems(items, orderState, groupState, columns);

    for (var groupIndex = 0; groupIndex < items.length; groupIndex++) {
      for (var i = 1; i < items[groupIndex][groupService.GROUPED_ITEMS_KEY].length; i++) {
        let firstItem = items[groupIndex][groupService.GROUPED_ITEMS_KEY][i - 1];
        let secondItem = items[groupIndex][groupService.GROUPED_ITEMS_KEY][i];
        let itemOrder = firstColumn.order(firstItem[sortedPropertyName], secondItem[sortedPropertyName], orderState.isAscending, firstItem, secondItem);
        let isOrderCorrect = itemOrder == -1 || itemOrder == 0;

        expect(isOrderCorrect).toBeTruthy();
      }  
    }
  });
});