import { FilterStateService } from './filter-state.service';
import { FilterState } from './states/filter-state';
import { GridColumn } from './grid-columns/grid-column';
import { TextColumnDirective } from './grid-columns/text-column/text-column.directive';
import { GuidService } from './guid';
import { DefaultComparerService } from './grid-columns/shared/default-comparer.service';

describe('Service: FilterStateService', () => {
  let filterStateService: FilterStateService;
  let columns: GridColumn[];
  let firstColumn: TextColumnDirective;
  let secondColumn: TextColumnDirective;

  beforeEach(() => {
    filterStateService = new FilterStateService();
    firstColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    firstColumn.propertyName = 'foo';
    secondColumn = new TextColumnDirective(new GuidService(), new DefaultComparerService());
    secondColumn.propertyName = 'bar';

    columns = [
      firstColumn,
      secondColumn
    ];
  });

  it('filterItems with all items passing the filter', () => {
    var items = [
      { foo: 'Hsdawdd', bar: 'Asd' },
      { foo: 'Asd', bar: 'koq' },
      { foo: null, bar: 'Olqk' },
      { foo: 'Poqk' },
      { foo: '', bar: null },
      { foo: 'oqw', bar: 'Lkqkq' }
    ];
    var filters: any[] = [];
    filters[firstColumn.columnId] = { filterData: {} };
    filters[secondColumn.columnId] = { filterData: {} };
    var filterState = new FilterState(filters);
    var firstColumnFilterSpy = spyOn(firstColumn, 'filter').and.callFake((value: any, columnFilter: any, item: any) => {
      return true;
    });
    var secondColumnFilterSpy = spyOn(secondColumn, 'filter').and.callFake((value: any, columnFilter: any, item: any) => {
      return true;
    });
    
    var filteredItems = filterStateService.filterItems(items, filterState, columns);

    expect(filteredItems.length).toEqual(items.length);
  });

  it('filterItems with items not passing a single column filter', () => {
    var items = [
      { foo: 'Hsdawdd', bar: 'Asd' },
      { foo: 'Asd', bar: 'koq' },
      { foo: null, bar: 'Olqk' },
      { foo: 'Poqk' },
      { foo: '', bar: null },
      { foo: 'oqw', bar: 'Lkqkq' }
    ];
    var filters: any[] = [];
    filters[firstColumn.columnId] = { filterData: {} };
    filters[secondColumn.columnId] = { filterData: {} };
    var filterState = new FilterState(filters);
    var firstColumnFilterSpy = spyOn(firstColumn, 'filter').and.callFake((value: any, columnFilter: any, item: any) => {
      return item != items[0];
    });
    var secondColumnFilterSpy = spyOn(secondColumn, 'filter').and.callFake((value: any, columnFilter: any, item: any) => {
      return item != items[1];
    });
    
    var filteredItems = filterStateService.filterItems(items, filterState, columns);

    expect(filteredItems.length).toEqual(items.length - 2);
  });

  it('filterItems with items not passing a multiple column filters', () => {
    var items = [
      { foo: 'Hsdawdd', bar: 'Asd' },
      { foo: 'Asd', bar: 'koq' },
      { foo: null, bar: 'Olqk' },
      { foo: 'Poqk' },
      { foo: '', bar: null },
      { foo: 'oqw', bar: 'Lkqkq' }
    ];
    var filters: any[] = [];
    filters[firstColumn.columnId] = { filterData: {} };
    filters[secondColumn.columnId] = { filterData: {} };
    var filterState = new FilterState(filters);
    var firstColumnFilterSpy = spyOn(firstColumn, 'filter').and.callFake((value: any, columnFilter: any, item: any) => {
      return item != items[0];
    });
    var secondColumnFilterSpy = spyOn(secondColumn, 'filter').and.callFake((value: any, columnFilter: any, item: any) => {
      return item != items[0];
    });
    
    var filteredItems = filterStateService.filterItems(items, filterState, columns);

    expect(filteredItems.length).toEqual(items.length - 1);
  });

  it('raiseFilterChanged should add the specified column filter to the filter state', () => {
    var nextFilterSpy = spyOn(filterStateService.filterState, 'next');
    var columnId = '12345';
    var propertyName = 'baz';
    var filterData = {};
    var filterKey = 'bar';

    filterStateService.raiseFilterChanged(columnId, propertyName, filterData, filterKey);

    expect(nextFilterSpy).toHaveBeenCalled();
    var call = nextFilterSpy.calls.first();
    var newFilterState = call.args[0];
    expect(newFilterState.columnFilters[columnId]).not.toBeNull();
    expect(newFilterState.columnFilters[columnId].propertyName).toEqual(propertyName);
    expect(newFilterState.columnFilters[columnId].filterData).toEqual(filterData);
    expect(newFilterState.columnFilters[columnId].filterKey).toEqual(filterKey);
  });
});