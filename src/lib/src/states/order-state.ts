export class OrderState {
  constructor(
    public columnId: string,
    public propertyName: string,
    public isAscending: boolean) {
  }
}