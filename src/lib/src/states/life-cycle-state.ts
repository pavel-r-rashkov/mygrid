export enum LifeCycleState {
  Filtering = 0,
  Grouping = 1,
  Sorting = 2,
  Pagination = 3
}