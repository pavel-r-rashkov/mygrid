"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FilterState = (function () {
    function FilterState(columnFilters) {
        this.columnFilters = columnFilters;
    }
    return FilterState;
}());
exports.FilterState = FilterState;
//# sourceMappingURL=filter-state.js.map