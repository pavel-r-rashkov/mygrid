"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LifeCycleState;
(function (LifeCycleState) {
    LifeCycleState[LifeCycleState["Filtering"] = 0] = "Filtering";
    LifeCycleState[LifeCycleState["Grouping"] = 1] = "Grouping";
    LifeCycleState[LifeCycleState["Sorting"] = 2] = "Sorting";
    LifeCycleState[LifeCycleState["Pagination"] = 3] = "Pagination";
})(LifeCycleState = exports.LifeCycleState || (exports.LifeCycleState = {}));
//# sourceMappingURL=life-cycle-state.js.map