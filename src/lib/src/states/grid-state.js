"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GridState = (function () {
    function GridState(filterState, groupingState, orderState, paginationState, summaryStates) {
        this.filterState = filterState;
        this.groupingState = groupingState;
        this.orderState = orderState;
        this.paginationState = paginationState;
        this.summaryStates = summaryStates;
    }
    return GridState;
}());
exports.GridState = GridState;
//# sourceMappingURL=grid-state.js.map