//import {} from 'filter-state';
import { GroupState } from '../grouping/group-state';
import { OrderState } from './order-state';
import { PaginationState } from '../pagination/pagination-state';
import { SummaryState } from './summary-state';

export class GridState {
	constructor(
		public filterState: any,
		public groupingState: GroupState,
		public orderState: OrderState,
		public paginationState: PaginationState,
    public summaryStates: SummaryState[]) {
	}
}