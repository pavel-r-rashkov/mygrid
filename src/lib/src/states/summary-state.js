"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SummaryState = (function () {
    function SummaryState(propertyName, summaryType) {
        this.propertyName = propertyName;
        this.summaryType = summaryType;
    }
    return SummaryState;
}());
exports.SummaryState = SummaryState;
//# sourceMappingURL=summary-state.js.map