"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrderState = (function () {
    function OrderState(columnId, propertyName, isAscending) {
        this.columnId = columnId;
        this.propertyName = propertyName;
        this.isAscending = isAscending;
    }
    return OrderState;
}());
exports.OrderState = OrderState;
//# sourceMappingURL=order-state.js.map