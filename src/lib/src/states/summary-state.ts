export class SummaryState {
  constructor(
    public propertyName: string,
    public summaryType: string) {
  }
}