"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
require("rxjs/add/observable/of");
var Observable_1 = require("rxjs/Observable");
var my_grid_component_1 = require("./my-grid.component");
var my_grid_module_1 = require("./my-grid.module");
var filter_state_service_1 = require("./filter-state.service");
var order_state_service_1 = require("./order-state.service");
var pagination_service_1 = require("./pagination/pagination.service");
var group_service_1 = require("./grouping/group.service");
var column_ordering_service_1 = require("./column-ordering/column-ordering.service");
var row_edition_service_1 = require("./edition/row-edition.service");
var row_deletion_service_1 = require("./edition/row-deletion.service");
var grid_settings_service_1 = require("./grid-settings.service");
var html_table_element_service_1 = require("./html-table-elements/html-table-element.service");
var master_detail_service_1 = require("./master-detail/master-detail.service");
var selection_service_1 = require("./selection/selection.service");
var columns_service_1 = require("./columns.service");
var grid_api_service_1 = require("./grid-api.service");
var scroll_measure_service_1 = require("./scroll-measure.service");
var text_column_directive_1 = require("./grid-columns/text-column/text-column.directive");
var guid_1 = require("./guid");
var group_state_1 = require("./grouping/group-state");
var row_edition_state_1 = require("./edition/row-edition-state");
var details_state_1 = require("./master-detail/details-state");
var filter_state_1 = require("./states/filter-state");
var default_comparer_service_1 = require("./grid-columns/shared/default-comparer.service");
var order_state_1 = require("./states/order-state");
var FakeScrollMeasureService = (function () {
    function FakeScrollMeasureService() {
    }
    FakeScrollMeasureService.prototype.measureScrollWidth = function () {
        return 30;
    };
    FakeScrollMeasureService = __decorate([
        core_1.Injectable()
    ], FakeScrollMeasureService);
    return FakeScrollMeasureService;
}());
var TestGridComponent = (function () {
    function TestGridComponent() {
        this.isVisible = false;
    }
    __decorate([
        core_1.ViewChild('testGrid'),
        __metadata("design:type", my_grid_component_1.MyGridComponent)
    ], TestGridComponent.prototype, "testGrid", void 0);
    TestGridComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-grid',
            template: "\n    <mg-grid id=\"test-header-component\" #testGrid>\n      <mg-text-column></mg-text-column>\n      <mg-text-column [isVisible]=\"isVisible\"></mg-text-column>\n      <mg-text-column></mg-text-column>\n    </mg-grid>\n  "
        })
    ], TestGridComponent);
    return TestGridComponent;
}());
describe('Component: MyGridComponent', function () {
    var gridComponent;
    var fixture;
    var filterStateService;
    var orderStateService;
    var paginationService;
    var groupService;
    var columnOrderingService;
    var rowEditionService;
    var rowDeletionService;
    var gridSettingsService;
    var htmlTableElementService;
    var masterDetailService;
    var selectionService;
    var columnsService;
    var gridApiService;
    var scrollMeasureService = new FakeScrollMeasureService();
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.overrideComponent(my_grid_component_1.MyGridComponent, {
            remove: {
                providers: [scroll_measure_service_1.ScrollMeasureService]
            },
            add: {
                providers: [
                    { provide: scroll_measure_service_1.ScrollMeasureService, useValue: scrollMeasureService }
                ]
            }
        });
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestGridComponent
            ],
            providers: [],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(my_grid_component_1.MyGridComponent);
            gridComponent = fixture.componentInstance;
            filterStateService = fixture.debugElement.injector.get(filter_state_service_1.FilterStateService);
            orderStateService = fixture.debugElement.injector.get(order_state_service_1.OrderStateService);
            paginationService = fixture.debugElement.injector.get(pagination_service_1.PaginationService);
            groupService = fixture.debugElement.injector.get(group_service_1.GroupService);
            columnOrderingService = fixture.debugElement.injector.get(column_ordering_service_1.ColumnOrderingService);
            rowEditionService = fixture.debugElement.injector.get(row_edition_service_1.RowEditionService);
            rowDeletionService = fixture.debugElement.injector.get(row_deletion_service_1.RowDeletionService);
            gridSettingsService = fixture.debugElement.injector.get(grid_settings_service_1.GridSettingsService);
            htmlTableElementService = fixture.debugElement.injector.get(html_table_element_service_1.HtmlTableElementService);
            masterDetailService = fixture.debugElement.injector.get(master_detail_service_1.MasterDetailService);
            selectionService = fixture.debugElement.injector.get(selection_service_1.SelectionService);
            columnsService = fixture.debugElement.injector.get(columns_service_1.ColumnsService);
            gridApiService = fixture.debugElement.injector.get(grid_api_service_1.GridApiService);
        });
    }));
    it('Grid scroll width should be set on browser\'s scroll width - 1', function () {
        gridComponent.ngOnInit();
        var fakeScrollWidth = scrollMeasureService.measureScrollWidth();
        expect(gridComponent.scrollWidth).toEqual(fakeScrollWidth - 1 + 'px');
    });
    it('getItemRows should return array with the item when the items are not grouped', function () {
        var item = {};
        gridComponent.ngOnInit();
        var itemRows = gridComponent.getItemRows(item);
        expect(itemRows.length).toEqual(1);
        expect(itemRows[0]).toEqual(item);
    });
    it('getItemRows should return array with all items in the group', function () {
        var item = {};
        var column = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        columnsService.columns = [column];
        var groupItems = [];
        item[groupService.GROUPED_ITEMS_KEY] = groupItems;
        gridComponent.ngOnInit();
        groupService.groupState.next(new group_state_1.GroupState('bar', 'foo', true));
        fixture.whenStable().then(function () {
            var itemRows = gridComponent.getItemRows(item);
            expect(itemRows).toEqual(groupItems);
        });
    });
    it('shouldRenderGroupedLayout should return false when the items are not grouped', function () {
        var item = {};
        gridComponent.ngOnInit();
        var isGroupedLayout = gridComponent.shouldRenderGroupedLayout();
        expect(isGroupedLayout).toBeFalsy();
    });
    it('shouldRenderGroupedLayout should return true when the items are grouped', function () {
        var item = {};
        var column = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        columnsService.columns = [column];
        var groupItems = [];
        item[groupService.GROUPED_ITEMS_KEY] = groupItems;
        gridComponent.ngOnInit();
        groupService.groupState.next(new group_state_1.GroupState('bar', 'foo', true));
        fixture.whenStable().then(function () {
            var isGroupedLayout = gridComponent.shouldRenderGroupedLayout();
            expect(isGroupedLayout).toBeTruthy();
        });
    });
    it('getVisibleIndex should return grid item index on page + 1 when the items are not grouped', function () {
        var gridItemIndex = 10;
        var rowIndex = 10;
        gridComponent.ngOnInit();
        var visibleIndex = gridComponent.getVisibleIndex(gridItemIndex, rowIndex);
        expect(visibleIndex).toEqual(gridItemIndex + 1);
    });
    it('getVisibleIndex should return grid item index on page + 1 when the items are grouped', function () {
        var column = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        column.propertyName = 'foo';
        columnsService.columns = [column];
        var groupItems = [];
        var gridItemIndex = 1;
        var rowIndex = 2;
        gridComponent.items = [
            { foo: 'G1' },
            { foo: 'G1' },
            { foo: 'G2' },
            { foo: 'G2' },
            { foo: 'G2' }
        ];
        gridComponent.ngOnInit();
        groupService.groupState.next(new group_state_1.GroupState(column.columnId, column.propertyName, true));
        fixture.whenStable().then(function () {
            var visibleIndex = gridComponent.getVisibleIndex(gridItemIndex, rowIndex);
            expect(visibleIndex).toEqual(5);
        });
    });
    it('toggleGroup with closed group should open the group', function () {
        var itemsGroup = {};
        gridComponent.ngOnInit();
        gridComponent.toggleGroup(itemsGroup);
        expect(itemsGroup.isOpened).toBeTruthy();
    });
    it('toggleGroup with opened group should close the group', function () {
        var itemsGroup = { isOpened: true };
        gridComponent.ngOnInit();
        gridComponent.toggleGroup(itemsGroup);
        expect(itemsGroup.isOpened).toBeFalsy();
    });
    it('shouldRenderColumn should return true for visible non grouped column', function () {
        var column = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        gridComponent.ngOnInit();
        var shouldRenderColumn = gridComponent.shouldRenderColumn(column);
        expect(shouldRenderColumn).toBeTruthy();
    });
    it('shouldRenderColumn should return false for column with visibility set to false', function () {
        var column = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        column.isVisible = false;
        gridComponent.ngOnInit();
        var shouldRenderColumn = gridComponent.shouldRenderColumn(column);
        expect(shouldRenderColumn).toBeFalsy();
    });
    it('shouldRenderColumn should return false for column that is grouped', function () {
        var column = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        columnsService.columns = [column];
        column.isVisible = true;
        gridComponent.ngOnInit();
        groupService.groupState.next(new group_state_1.GroupState(column.columnId, 'foo', true));
        fixture.whenStable().then(function () {
            var shouldRenderColumn = gridComponent.shouldRenderColumn(column);
            expect(shouldRenderColumn).toBeFalsy();
        });
    });
    it('getRenderableColumns should return all visible columns', function () {
        var textGridFixture = testing_1.TestBed.createComponent(TestGridComponent);
        var testGridComponent = textGridFixture.componentInstance;
        textGridFixture.detectChanges();
        var renderableColumns = testGridComponent.testGrid.getRenderableColumns();
        expect(renderableColumns.length).toEqual(2);
    });
    it('getRenderableColumnCount should return all visible columns count', function () {
        var textGridFixture = testing_1.TestBed.createComponent(TestGridComponent);
        var testGridComponent = textGridFixture.componentInstance;
        textGridFixture.detectChanges();
        var renderableColumnsCount = testGridComponent.testGrid.getRenderableColumnCount();
        expect(renderableColumnsCount).toEqual(2);
    });
    it('isRowEdited should return false when the item is not being edited', function () {
        var key = 'foo';
        var item = { foo: 1 };
        gridComponent.key = key;
        gridComponent.ngOnInit();
        rowEditionService.rowEdition.next(new row_edition_state_1.RowEditionState([2, 3]));
        fixture.whenStable().then(function () {
            var isRowEdited = gridComponent.isRowEdited(item);
            expect(isRowEdited).toBeFalsy();
        });
    });
    it('isRowEdited should return true when the item is being edited', function () {
        var key = 'foo';
        var item = { foo: 1 };
        gridComponent.key = key;
        gridComponent.ngOnInit();
        rowEditionService.rowEdition.next(new row_edition_state_1.RowEditionState([2, 1]));
        fixture.whenStable().then(function () {
            var isRowEdited = gridComponent.isRowEdited(item);
            expect(isRowEdited).toBeTruthy();
        });
    });
    it('shouldRenderDetailGrid should return false when the item\'s details are not opened', function () {
        var key = 'foo';
        var item = { foo: 1 };
        gridComponent.key = key;
        gridComponent.ngOnInit();
        masterDetailService.detailsStateSubject.next(new details_state_1.DetailsState([2, 3]));
        fixture.whenStable().then(function () {
            var detailsOpened = gridComponent.shouldRenderDetailGrid(item);
            expect(detailsOpened).toBeFalsy();
        });
    });
    it('shouldRenderDetailGrid should return true when the item\'s details are opened', function () {
        var key = 'foo';
        var item = { foo: 1 };
        gridComponent.key = key;
        gridComponent.ngOnInit();
        masterDetailService.detailsStateSubject.next(new details_state_1.DetailsState([2, 1]));
        fixture.whenStable().then(function () {
            var detailsOpened = gridComponent.shouldRenderDetailGrid(item);
            expect(detailsOpened).toBeTruthy();
        });
    });
    it('clearFilters should remove all filters', function () {
        var filterStateSpy = spyOn(filterStateService.filterState, 'next').and.callThrough();
        gridComponent.ngOnInit();
        filterStateService.filterState.next(new filter_state_1.FilterState({}));
        fixture.whenStable().then(function () {
            filterStateSpy.calls.reset();
            gridComponent.clearFilters();
            fixture.whenStable().then(function () {
                expect(filterStateSpy).toHaveBeenCalled();
                expect(filterStateSpy.calls.first().args[0]).toBeNull();
            });
        });
    });
    it('toggleAddRow with closed addition row should open the addition row', function () {
        gridComponent.showAddRow = false;
        gridComponent.ngOnInit();
        gridComponent.toggleAddRow();
        expect(gridComponent.showAddRow).toBeTruthy();
    });
    it('toggleAddRow with opened addition row should close the addition row', function () {
        gridComponent.showAddRow = true;
        gridComponent.ngOnInit();
        gridComponent.toggleAddRow();
        expect(gridComponent.showAddRow).toBeFalsy();
    });
    it('getGroupedColumnData should return grouped column data', function () {
        var column = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        columnsService.columns = [column];
        gridComponent.ngOnInit();
        groupService.groupState.next(new group_state_1.GroupState(column.columnId, 'foo', true));
        fixture.whenStable().then(function () {
            var groupedColumnData = gridComponent.getGroupedColumnData();
            expect(groupedColumnData).toEqual(column.getGroupData());
        });
    });
    it('getGroupedColumnDataComponentType should return grouped column data type', function () {
        var column = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        columnsService.columns = [column];
        gridComponent.ngOnInit();
        groupService.groupState.next(new group_state_1.GroupState(column.columnId, 'foo', true));
        fixture.whenStable().then(function () {
            var groupedColumnType = gridComponent.getGroupedColumnDataComponentType();
            expect(groupedColumnType).toEqual(column.dataComponentType);
        });
    });
    it('getItemsToSummarize with enabled server side mode and global summary should return empty array', function () {
        var items = [{ foo: 'foo', bar: 'bar' }];
        gridComponent.enableServerSideMode = true;
        gridComponent.onGridStateChange = function (gridState) {
            return Observable_1.Observable.of({ items: items, count: 2, summaryResults: [] });
        };
        gridComponent.ngOnInit();
        gridComponent.ngAfterContentInit();
        fixture.whenStable().then(function () {
            //var items = gridComponent.getItemsToSummarize(false);
            //expect(items.length).toEqual(0);
        });
    });
    it('getItemsToSummarize with non grouped items and per page summary should return all items on the page', function () {
        gridComponent.items = [
            { foo: 'foo1', bar: 'bar1' },
            { foo: 'foo2', bar: 'bar2' },
            { foo: 'foo3', bar: 'bar3' },
            { foo: 'foo4', bar: 'bar4' },
            { foo: 'foo5', bar: 'bar5' },
            { foo: 'foo6', bar: 'bar6' }
        ];
        var pageSize = 2;
        gridComponent.pageSize = pageSize;
        gridComponent.ngOnInit();
        gridComponent.ngAfterContentInit();
        var itemsToSummarize = gridComponent.getItemsToSummarize(true);
        expect(itemsToSummarize.length).toEqual(pageSize);
        expect(itemsToSummarize[0]).toEqual(gridComponent.items[0]);
        expect(itemsToSummarize[1]).toEqual(gridComponent.items[1]);
    });
    it('getItemsToSummarize with non grouped items and global summary should return all items', function () {
        gridComponent.items = [
            { foo: 'foo1', bar: 'bar1' },
            { foo: 'foo2', bar: 'bar2' },
            { foo: 'foo3', bar: 'bar3' },
            { foo: 'foo4', bar: 'bar4' },
            { foo: 'foo5', bar: 'bar5' },
            { foo: 'foo6', bar: 'bar6' }
        ];
        var pageSize = 2;
        gridComponent.pageSize = pageSize;
        gridComponent.ngOnInit();
        gridComponent.ngAfterContentInit();
        var itemsToSummarize = gridComponent.getItemsToSummarize(false);
        expect(itemsToSummarize.length).toEqual(gridComponent.items.length);
        expect(itemsToSummarize[0]).toEqual(gridComponent.items[0]);
        expect(itemsToSummarize[5]).toEqual(gridComponent.items[5]);
    });
    it('getItemsToSummarize with grouped items and global summary should return all items', function () {
        var column = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        columnsService.columns = [column];
        gridComponent.items = [
            { foo: 'foo1', bar: 'bar1' },
            { foo: 'foo2', bar: 'bar2' },
            { foo: 'foo3', bar: 'bar3' },
            { foo: 'foo4', bar: 'bar4' },
            { foo: 'foo5', bar: 'bar5' },
            { foo: 'foo6', bar: 'bar6' }
        ];
        var pageSize = 2;
        gridComponent.pageSize = pageSize;
        gridComponent.ngOnInit();
        groupService.groupState.next(new group_state_1.GroupState(column.columnId, 'foo', true));
        fixture.whenStable().then(function () {
            var itemsToSummarize = gridComponent.getItemsToSummarize(false);
            expect(itemsToSummarize.length).toEqual(gridComponent.items.length);
        });
    });
    it('getItemsToSummarize with grouped items and per page summary should return all items on the page', function () {
        var column = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        columnsService.columns = [column];
        gridComponent.items = [
            { foo: 'foo1', bar: 'bar1' },
            { foo: 'foo2', bar: 'bar2' },
            { foo: 'foo3', bar: 'bar3' },
            { foo: 'foo4', bar: 'bar4' },
            { foo: 'foo5', bar: 'bar5' },
            { foo: 'foo6', bar: 'bar6' }
        ];
        var pageSize = 2;
        gridComponent.pageSize = pageSize;
        gridComponent.ngOnInit();
        groupService.groupState.next(new group_state_1.GroupState(column.columnId, 'foo', true));
        fixture.whenStable().then(function () {
            var itemsToSummarize = gridComponent.getItemsToSummarize(true);
            expect(itemsToSummarize.length).toEqual(pageSize);
        });
    });
    it('deleteRow with disabled server-side mode and no deleteItemCallback should remove the item', function () {
        var column = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), new default_comparer_service_1.DefaultComparerService());
        column.propertyName = 'foo';
        columnsService.columns = [column];
        var key = 'foo';
        gridComponent.key = key;
        gridComponent.items = [
            { foo: 'foo1', bar: 'bar1' },
            { foo: 'foo2', bar: 'bar2' },
            { foo: 'foo3', bar: 'bar3' },
            { foo: 'foo4', bar: 'bar4' },
            { foo: 'foo5', bar: 'bar5' },
            { foo: 'foo6', bar: 'bar6' }
        ];
        var deletedRowKey = 'foo4';
        gridComponent.ngOnInit();
        gridComponent.ngAfterContentInit();
        rowDeletionService.beforeRowDelete.next(deletedRowKey);
        fixture.whenStable().then(function () {
            expect(gridComponent.gridItems.length).toEqual(5);
            var deletedRows = gridComponent.gridItems.filter(function (item) {
                return item[key] == deletedRowKey;
            });
            expect(deletedRows.length).toEqual(0);
        });
    });
    it('deleteRow with disabled server-side mode with successful deleteItemCallback should remove the item', function () {
        var key = 'foo';
        gridComponent.key = key;
        gridComponent.items = [
            { foo: 'foo1', bar: 'bar1' },
            { foo: 'foo2', bar: 'bar2' },
            { foo: 'foo3', bar: 'bar3' },
            { foo: 'foo4', bar: 'bar4' },
            { foo: 'foo5', bar: 'bar5' },
            { foo: 'foo6', bar: 'bar6' }
        ];
        gridComponent.deleteItemCallback = function (deletedRowKey) { return Observable_1.Observable.of(); };
        var deletedRowKey = 'foo4';
        gridComponent.ngOnInit();
        gridComponent.ngAfterContentInit();
        rowDeletionService.beforeRowDelete.next(deletedRowKey);
        fixture.whenStable().then(function () {
            expect(gridComponent.gridItems.length).toEqual(5);
            var deletedRows = gridComponent.gridItems.filter(function (item) {
                return item[key] == deletedRowKey;
            });
            expect(deletedRows.length).toEqual(0);
        });
    });
    it('deleteRow with disabled server-side mode with unsuccessful deleteItemCallback should not remove the item', function () {
        var key = 'foo';
        gridComponent.key = key;
        gridComponent.items = [
            { foo: 'foo1', bar: 'bar1' },
            { foo: 'foo2', bar: 'bar2' },
            { foo: 'foo3', bar: 'bar3' },
            { foo: 'foo4', bar: 'bar4' },
            { foo: 'foo5', bar: 'bar5' },
            { foo: 'foo6', bar: 'bar6' }
        ];
        gridComponent.deleteItemCallback = function (deletedRowKey) { return Observable_1.Observable.throw('sample error'); };
        var deletedRowKey = 'foo4';
        gridComponent.ngOnInit();
        gridComponent.ngAfterContentInit();
        rowDeletionService.beforeRowDelete.next(deletedRowKey);
        fixture.whenStable().then(function () {
            expect(gridComponent.gridItems.length).toEqual(6);
            var deletedRows = gridComponent.gridItems.filter(function (item) {
                return item[key] == deletedRowKey;
            });
            expect(deletedRows.length).toEqual(1);
        });
    });
    it('deleteRow with enabled server-side mode with unsuccessful deleteItemCallback should not refresh', function () {
        gridComponent.enableServerSideMode = true;
        var key = 'foo';
        gridComponent.key = key;
        gridComponent.items = [
            { foo: 'foo1', bar: 'bar1' },
            { foo: 'foo2', bar: 'bar2' },
            { foo: 'foo3', bar: 'bar3' },
            { foo: 'foo4', bar: 'bar4' },
            { foo: 'foo5', bar: 'bar5' },
            { foo: 'foo6', bar: 'bar6' }
        ];
        gridComponent.deleteItemCallback = function (deletedRowKey) { return Observable_1.Observable.throw('sample error'); };
        gridComponent.onGridStateChange = function (gridState) {
            return Observable_1.Observable.of({ items: [], count: 0, summaryResults: [] });
        };
        var stateChangeSpy = spyOn(gridComponent, 'onGridStateChange').and.callThrough();
        var deletedRowKey = 'foo4';
        gridComponent.ngOnInit();
        gridComponent.ngAfterContentInit();
        stateChangeSpy.calls.reset();
        rowDeletionService.beforeRowDelete.next(deletedRowKey);
        fixture.whenStable().then(function () {
            expect(stateChangeSpy).not.toHaveBeenCalled();
        });
    });
    it('deleteRow with enabled server-side mode with successful deleteItemCallback should refresh', function () {
        gridComponent.enableServerSideMode = true;
        var key = 'foo';
        gridComponent.key = key;
        gridComponent.items = [
            { foo: 'foo1', bar: 'bar1' },
            { foo: 'foo2', bar: 'bar2' },
            { foo: 'foo3', bar: 'bar3' },
            { foo: 'foo4', bar: 'bar4' },
            { foo: 'foo5', bar: 'bar5' },
            { foo: 'foo6', bar: 'bar6' }
        ];
        gridComponent.deleteItemCallback = function (deletedRowKey) { return Observable_1.Observable.of(); };
        gridComponent.onGridStateChange = function (gridState) {
            return Observable_1.Observable.of({ items: [], count: 0, summaryResults: [] });
        };
        var stateChangeSpy = spyOn(gridComponent, 'onGridStateChange').and.callThrough();
        var deletedRowKey = 'foo4';
        gridComponent.ngOnInit();
        gridComponent.ngAfterContentInit();
        stateChangeSpy.calls.reset();
        rowDeletionService.beforeRowDelete.next(deletedRowKey);
        fixture.whenStable().then(function () {
            expect(stateChangeSpy).toHaveBeenCalled();
        });
    });
    it('addItem should add the item and refresh', function () {
        var key = 'foo';
        gridComponent.key = key;
        gridComponent.items = [
            { foo: 'foo1', bar: 'bar1' },
            { foo: 'foo2', bar: 'bar2' },
            { foo: 'foo3', bar: 'bar3' },
            { foo: 'foo4', bar: 'bar4' },
            { foo: 'foo5', bar: 'bar5' },
            { foo: 'foo6', bar: 'bar6' }
        ];
        var newItem = { foo: 'foo7', bar: 'bar7' };
        gridComponent.ngOnInit();
        gridComponent.ngAfterContentInit();
        rowEditionService.itemAdded.next(newItem);
        fixture.whenStable().then(function () {
            expect(gridComponent.items.length).toEqual(7);
        });
    });
    it('life cycle state events should be fired when the grid is in client side mode', function () {
        var key = 'foo';
        gridComponent.key = key;
        gridComponent.items = [];
        gridComponent.enableServerSideMode = false;
        var beforeFilterSpy = spyOn(gridComponent.beforeFiltering, 'next');
        var beforeGroupingSpy = spyOn(gridComponent.beforeGrouping, 'next');
        var beforeSortingSpy = spyOn(gridComponent.beforeSorting, 'next');
        var beforePagingSpy = spyOn(gridComponent.beforePaging, 'next');
        fixture.detectChanges();
        gridComponent.api.refresh();
        expect(beforeFilterSpy).toHaveBeenCalled();
        expect(beforeGroupingSpy).toHaveBeenCalled();
        expect(beforeSortingSpy).toHaveBeenCalled();
        expect(beforePagingSpy).toHaveBeenCalled();
    });
    it('only life cycle state events after the specified one should be fired when the grid is in client side mode', function () {
        var key = 'foo';
        gridComponent.key = key;
        gridComponent.items = [];
        gridComponent.enableServerSideMode = false;
        var beforeFilterSpy = spyOn(gridComponent.beforeFiltering, 'next');
        var beforeGroupingSpy = spyOn(gridComponent.beforeGrouping, 'next');
        var beforeSortingSpy = spyOn(gridComponent.beforeSorting, 'next');
        var beforePagingSpy = spyOn(gridComponent.beforePaging, 'next');
        fixture.detectChanges();
        beforeFilterSpy.calls.reset();
        beforeGroupingSpy.calls.reset();
        beforeSortingSpy.calls.reset();
        beforePagingSpy.calls.reset();
        orderStateService.orderState.next(new order_state_1.OrderState('123', 'foo', true));
        fixture.whenStable().then(function () {
            expect(beforeFilterSpy).not.toHaveBeenCalled();
            expect(beforeGroupingSpy).not.toHaveBeenCalled();
            expect(beforeSortingSpy).toHaveBeenCalled();
            expect(beforePagingSpy).toHaveBeenCalled();
        });
    });
    it('refresh with server-side mode enabled should set the grid items and items count to the values returned by onGridStateChange callback', function () {
        var key = 'foo';
        gridComponent.key = key;
        gridComponent.enableServerSideMode = true;
        gridComponent.pageSize = 5;
        var items = [];
        var itemsCount = 10;
        gridComponent.onGridStateChange = function (gridState) {
            return Observable_1.Observable.of({
                items: items,
                count: itemsCount
            });
        };
        var itemsCountSpy = spyOn(paginationService.itemsCount, 'next');
        fixture.detectChanges();
        gridComponent.api.refresh();
        fixture.whenStable().then(function () {
            expect(itemsCountSpy).toHaveBeenCalled();
            expect(itemsCountSpy.calls.first().args[0]).toEqual(itemsCount);
            expect(gridComponent.gridItems).toEqual(items);
        });
    });
});
//# sourceMappingURL=my-grid.component.spec.js.map