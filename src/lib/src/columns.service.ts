import { Injectable } from '@angular/core';

import { GridColumn } from './grid-columns/grid-column';

@Injectable()
export class ColumnsService {
  public columns: GridColumn[];
}