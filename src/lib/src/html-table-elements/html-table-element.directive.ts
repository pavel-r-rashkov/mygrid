import { 
  Directive, 
  OnInit,
  ElementRef,
  Input
} from '@angular/core';

import { HtmlTableElementService } from './html-table-element.service';
import { HtmlElementType } from './html-element-type';

@Directive({
  selector: '[mg-html-table-element]'
})
export class HtmlTableElementDirective implements OnInit {
  @Input() elementContext: any;
  @Input() elementType: HtmlElementType;

  constructor(
    private elementRef: ElementRef,
    private tableElementService: HtmlTableElementService) {
  }

  ngOnInit() {
    if (this.tableElementService.initCellCallback != null) {
      this.tableElementService.initCellCallback(this.elementContext, this.elementRef, this.elementType);
    }
  }
}