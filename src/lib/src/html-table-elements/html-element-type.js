"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HtmlElementType;
(function (HtmlElementType) {
    HtmlElementType[HtmlElementType["HeaderCell"] = 0] = "HeaderCell";
    HtmlElementType[HtmlElementType["FilterCell"] = 1] = "FilterCell";
    HtmlElementType[HtmlElementType["DataCell"] = 2] = "DataCell";
    HtmlElementType[HtmlElementType["EditCell"] = 3] = "EditCell";
    HtmlElementType[HtmlElementType["SummaryCell"] = 4] = "SummaryCell";
})(HtmlElementType = exports.HtmlElementType || (exports.HtmlElementType = {}));
//# sourceMappingURL=html-element-type.js.map