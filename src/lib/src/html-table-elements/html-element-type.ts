export enum HtmlElementType {
  HeaderCell,
  FilterCell,
  DataCell,
  EditCell,
  SummaryCell
}