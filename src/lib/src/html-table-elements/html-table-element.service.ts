import { 
  Injectable, 
  ElementRef
} from '@angular/core';

import { HtmlElementType } from './html-element-type';

@Injectable()
export class HtmlTableElementService {
  initCellCallback: (elementData: any, elementRef: ElementRef, elementType: HtmlElementType) => void;
}