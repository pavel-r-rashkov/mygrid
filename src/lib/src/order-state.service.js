"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ReplaySubject_1 = require("rxjs/ReplaySubject");
var group_service_1 = require("./grouping/group.service");
var OrderStateService = (function () {
    function OrderStateService(groupService) {
        this.groupService = groupService;
        this.orderState = new ReplaySubject_1.ReplaySubject(1);
    }
    OrderStateService.prototype.raiseOrderChanged = function (state) {
        this.orderState.next(state);
    };
    OrderStateService.prototype.sortItems = function (items, orderState, groupState, columns) {
        var sort = function (items) {
            items.sort(function (firstItem, secondItem) {
                var orderedColumn = columns.filter(function (col) {
                    return col.columnId == orderState.columnId;
                })[0];
                var firstValue = orderedColumn.getValue(firstItem);
                var secondValue = orderedColumn.getValue(secondItem);
                var sortResult = orderedColumn.order(firstValue, secondValue, orderState.isAscending, firstItem, secondItem);
                return sortResult;
            });
        };
        if (groupState != null) {
            for (var i = 0; i < items.length; ++i) {
                sort(items[i][this.groupService.GROUPED_ITEMS_KEY]);
            }
        }
        else {
            sort(items);
        }
    };
    OrderStateService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [group_service_1.GroupService])
    ], OrderStateService);
    return OrderStateService;
}());
exports.OrderStateService = OrderStateService;
//# sourceMappingURL=order-state.service.js.map