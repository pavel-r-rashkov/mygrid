"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var component_host_directive_1 = require("../component-host.directive");
var DataWrapperComponent = (function () {
    function DataWrapperComponent(componentFactoryResolver) {
        this.componentFactoryResolver = componentFactoryResolver;
    }
    DataWrapperComponent.prototype.ngAfterContentInit = function () {
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.dataType);
        var viewContainerRef = this.dataComponentHost.viewContainerRef;
        viewContainerRef.clear();
        var componentRef = viewContainerRef.createComponent(componentFactory);
        var componentInstance = componentRef.instance;
        componentInstance.data = this.data;
        componentInstance.item = this.item;
        componentInstance.visibleIndex = this.visibleIndex;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], DataWrapperComponent.prototype, "data", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.Type)
    ], DataWrapperComponent.prototype, "dataType", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], DataWrapperComponent.prototype, "item", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], DataWrapperComponent.prototype, "visibleIndex", void 0);
    __decorate([
        core_1.ViewChild(component_host_directive_1.ComponentHostDirective),
        __metadata("design:type", component_host_directive_1.ComponentHostDirective)
    ], DataWrapperComponent.prototype, "dataComponentHost", void 0);
    DataWrapperComponent = __decorate([
        core_1.Component({
            selector: 'mg-data-wrapper',
            templateUrl: './data-wrapper.component.html'
        }),
        __metadata("design:paramtypes", [core_1.ComponentFactoryResolver])
    ], DataWrapperComponent);
    return DataWrapperComponent;
}());
exports.DataWrapperComponent = DataWrapperComponent;
//# sourceMappingURL=data-wrapper.component.js.map