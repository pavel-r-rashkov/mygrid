"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
require("rxjs/add/observable/of");
var my_grid_module_1 = require("../my-grid.module");
var filter_component_1 = require("../grid-columns/contracts/filter-component");
var filter_wrapper_component_1 = require("./filter-wrapper.component");
var TestFilterComponent = (function (_super) {
    __extends(TestFilterComponent, _super);
    function TestFilterComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TestFilterComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-filter',
            template: "\n    <div id=\"test-filter-component\"></div>\n  "
        })
    ], TestFilterComponent);
    return TestFilterComponent;
}(filter_component_1.FilterComponent));
describe('Component: FilterWrapperComponent', function () {
    var filterWrapperComponent;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.overrideModule(testing_2.BrowserDynamicTestingModule, {
            set: {
                entryComponents: [TestFilterComponent]
            }
        });
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestFilterComponent
            ],
            providers: [],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(filter_wrapper_component_1.FilterWrapperComponent);
            filterWrapperComponent = fixture.componentInstance;
        });
    }));
    it('FilterWrapperComponent should create filter component and set its input properties', function () {
        var data = {};
        var columnId = '123';
        filterWrapperComponent.filterData = data;
        filterWrapperComponent.columnId = columnId;
        filterWrapperComponent.filterType = TestFilterComponent;
        fixture.detectChanges();
        var testFilterComponent = fixture.debugElement.query(platform_browser_1.By.css('#test-filter-component'));
        expect(testFilterComponent).not.toBeNull();
        expect(testFilterComponent.context.filterData).toEqual(data);
        expect(testFilterComponent.context.columnId).toEqual(columnId);
    });
    it('FilterWrapperComponent should not create filter component when filterType is not provided', function () {
        var data = {};
        var columnId = '123';
        filterWrapperComponent.filterData = data;
        filterWrapperComponent.columnId = columnId;
        fixture.detectChanges();
        var testFilterComponent = fixture.debugElement.query(platform_browser_1.By.css('#test-filter-component'));
        expect(testFilterComponent).toBeNull();
    });
});
//# sourceMappingURL=filter-wrapper.component.spec.js.map