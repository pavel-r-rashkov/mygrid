import { 
  Component,
  ComponentFactoryResolver,
  ViewContainerRef,
  Input,
  Type,
  ViewChild,
  ElementRef,
  NgZone,
  ComponentRef,
  OnInit,
  OnDestroy,
  Renderer2
} from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import { ComponentHostDirective } from '../component-host.directive';
import { HeaderComponent } from '../grid-columns/contracts/header-component';
import { ColumnOrderingService } from '../column-ordering/column-ordering.service';
import { ColumnReorderEvent } from '../column-ordering/column-reorder-event';
import { GroupService } from '../grouping/group.service';
import { GroupState } from '../grouping/group-state';

@Component({
  selector: 'mg-header-wrapper',
  templateUrl: './header-wrapper.component.html'
})
export class HeaderWrapperComponent implements OnInit, OnDestroy {
  private readonly DRAGGED_HEADER_DATA_TRANSFER_KEY: string = 'draggedColumnData';
  private groupStateSubscription: Subscription;
  private currentGroupState: GroupState;
  private headerComponent: ComponentRef<any>;
  private showLeftDropColumnMarker = false;
  private showRightDropColumnMarker = false;
  @Input() columnId: string;
  @Input() headerData: any;
  @Input() headerType: Type<any>;
  @ViewChild(ComponentHostDirective) headerComponentHost: ComponentHostDirective;
  @ViewChild('headerWrapper') headerWrapper: ElementRef;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private columnOrderingService: ColumnOrderingService,
    private groupService: GroupService,
    private zone: NgZone,
    private renderer: Renderer2) {

    this.groupStateSubscription = this.groupService.groupState.subscribe((newState) => {
      this.currentGroupState = newState;
    });
  }

  ngOnInit() {
    this.zone.runOutsideAngular(() => {
      let handler = ($event: any) => {
        this.columnDragOver($event);
      };
      this.renderer.listen(this.headerWrapper.nativeElement, 'dragover', handler);
    });

    this.updateHeaderComponent();
  }

  ngOnChanges() {
    this.updateHeaderComponent();
  }

  ngOnDestroy() {
    this.groupStateSubscription.unsubscribe();
  }

  columnDragStart($event: any) {
    let draggedColumnData = {
      columnId: this.columnId,
      propertyName: this.headerData.propertyName,
      enableGrouping: this.headerData.enableGrouping
    };
    
    $event.dataTransfer.setData(this.DRAGGED_HEADER_DATA_TRANSFER_KEY, JSON.stringify(draggedColumnData));
  }

  columnDrop($event: any) {
    $event.stopPropagation(); // when multiple group columns are supported
    let columnDataTransfer = JSON.parse($event.dataTransfer.getData(this.DRAGGED_HEADER_DATA_TRANSFER_KEY));
    let isMovedInFront = this.showLeftDropColumnMarker;
    this.showLeftDropColumnMarker = false;
    this.showRightDropColumnMarker = false;

    if (this.currentGroupState != null && this.currentGroupState.columnId == this.columnId) {
      if (columnDataTransfer.enableGrouping) {
        this.groupService.groupState.next(new GroupState(
          columnDataTransfer.columnId,
          columnDataTransfer.propertyName,
          true));
      }

      return;
    }

    if (columnDataTransfer != null && columnDataTransfer.columnId != this.columnId) {
      let targetColumn = { 
        columnId: this.columnId, 
        propertyName: this.headerData.propertyName
      };

      let columnReorderEvent = new ColumnReorderEvent(columnDataTransfer, targetColumn, isMovedInFront);
      this.columnOrderingService.raiseColumnReorderedEvent(columnReorderEvent);
    }
  }

  columnDragLeave($event: any) {
    this.showLeftDropColumnMarker = false;
    this.showRightDropColumnMarker = false;
  }

  columnDragOver($event: any) {
    $event.preventDefault();

    // remove when multiple group columns are supported
    if (this.currentGroupState != null && this.currentGroupState.columnId == this.columnId) {
      return;
    }    

    let targetRect = this.headerWrapper.nativeElement.getClientRects()[0];
    let leftOffset = $event.clientX - targetRect.left;

    let showLeft = this.showLeftDropColumnMarker;
    let showRight = this.showRightDropColumnMarker;

    if (leftOffset < (targetRect.width / 2)) {
      showLeft = true;
      showRight = false;
    } else {
      showLeft = false;
      showRight = true;
    }

    if (showLeft != this.showLeftDropColumnMarker || showRight != this.showRightDropColumnMarker) {
      this.zone.run(() => {
        this.showLeftDropColumnMarker = showLeft;
        this.showRightDropColumnMarker = showRight;
      });
    }
  }

  private updateHeaderComponent() {
    if (this.headerType != null) {
      if (this.headerComponent) {
        this.headerComponent.destroy();
      }
      
      let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.headerType);
      let viewContainerRef = this.headerComponentHost.viewContainerRef;
      viewContainerRef.clear();
      this.headerComponent = viewContainerRef.createComponent(componentFactory);
      let headerComponentInstance = <HeaderComponent>this.headerComponent.instance;
      headerComponentInstance.headerData = this.headerData;
      headerComponentInstance.columnId = this.columnId;
    }
  }
}