"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
require("rxjs/add/operator/toPromise");
require("rxjs/add/observable/of");
var my_grid_module_1 = require("../my-grid.module");
var edit_component_1 = require("../grid-columns/contracts/edit-component");
var edit_wrapper_component_1 = require("./edit-wrapper.component");
var row_edition_service_1 = require("../edition/row-edition.service");
var TestEditComponent = (function (_super) {
    __extends(TestEditComponent, _super);
    function TestEditComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TestEditComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-edit',
            template: "\n    <div id=\"test-edit-component\"></div>\n  "
        })
    ], TestEditComponent);
    return TestEditComponent;
}(edit_component_1.EditComponent));
var ValidationErrorsTemplateComponent = (function () {
    function ValidationErrorsTemplateComponent() {
    }
    __decorate([
        core_1.ViewChild('errorsTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], ValidationErrorsTemplateComponent.prototype, "errorsTemplate", void 0);
    ValidationErrorsTemplateComponent = __decorate([
        core_1.Component({
            selector: 'mgt-validation-errors-template',
            template: "\n    <ng-template #errorsTemplate let-context>\n      <div id=\"validation-errors-template\"></div>\n    </ng-template>\n  "
        })
    ], ValidationErrorsTemplateComponent);
    return ValidationErrorsTemplateComponent;
}());
describe('Component: EditWrapperComponent', function () {
    var editWrapperComponent;
    var fixture;
    var validationErrorsTemplate;
    var rowEditionService;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.overrideModule(testing_2.BrowserDynamicTestingModule, {
            set: {
                entryComponents: [TestEditComponent]
            }
        });
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestEditComponent,
                ValidationErrorsTemplateComponent
            ],
            providers: [
                row_edition_service_1.RowEditionService
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(edit_wrapper_component_1.EditWrapperComponent);
            editWrapperComponent = fixture.componentInstance;
            rowEditionService = fixture.debugElement.injector.get(row_edition_service_1.RowEditionService);
            validationErrorsTemplate = testing_1.TestBed
                .createComponent(ValidationErrorsTemplateComponent)
                .componentInstance
                .errorsTemplate;
        });
    }));
    it('EditWrapperComponent should create edit component and set its input properties', function () {
        var data = {};
        var item = {};
        var form = new forms_1.FormGroup({});
        editWrapperComponent.data = data;
        editWrapperComponent.item = item;
        editWrapperComponent.editForm = form;
        editWrapperComponent.editType = TestEditComponent;
        fixture.detectChanges();
        var testEditComponent = fixture.debugElement.query(platform_browser_1.By.css('#test-edit-component'));
        expect(testEditComponent).not.toBeNull();
        expect(testEditComponent.context.data).toEqual(data);
        expect(testEditComponent.context.item).toEqual(item);
        expect(testEditComponent.context.editForm).toEqual(form);
    });
    it('EditWrapperComponent should not create edit component when editType is not provided', function () {
        var data = {};
        var item = {};
        var form = new forms_1.FormGroup({});
        editWrapperComponent.data = data;
        editWrapperComponent.item = item;
        editWrapperComponent.editForm = form;
        fixture.detectChanges();
        var testEditComponent = fixture.debugElement.query(platform_browser_1.By.css('#test-edit-component'));
        expect(testEditComponent).toBeNull();
    });
    it('Custom validation errors template should be rendered', function () {
        var data = {
            validationErrorsTemplate: validationErrorsTemplate
        };
        var item = {};
        var form = new forms_1.FormGroup({});
        editWrapperComponent.data = data;
        editWrapperComponent.item = item;
        editWrapperComponent.editForm = form;
        editWrapperComponent.editType = TestEditComponent;
        fixture.detectChanges();
        var errorsTemplate = fixture.debugElement.query(platform_browser_1.By.css('#validation-errors-template'));
        expect(errorsTemplate).not.toBeNull();
        expect(errorsTemplate.context.$implicit.data).toEqual(data);
        expect(errorsTemplate.context.$implicit.item).toEqual(item);
        expect(errorsTemplate.context.$implicit.editForm).toEqual(form);
        expect(errorsTemplate.context.$implicit.getErrors).toEqual(editWrapperComponent.getErrors);
    });
    it('shouldRenderErrors should return false when there is no propertyName', function () {
        var data = {};
        var form = new forms_1.FormGroup({});
        editWrapperComponent.data = data;
        editWrapperComponent.editForm = form;
        editWrapperComponent.editType = TestEditComponent;
        fixture.detectChanges();
        expect(editWrapperComponent.shouldRenderErrors()).toBeFalsy();
    });
    it('shouldRenderErrors should return false when the form control for the propertyName is not invalid', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName
        };
        var controls = {};
        controls[propertyName] = new forms_1.FormControl();
        var form = new forms_1.FormGroup(controls);
        form.controls.foo.validator = forms_1.Validators.required;
        form.patchValue({ foo: 'asdf' });
        editWrapperComponent.data = data;
        editWrapperComponent.editForm = form;
        editWrapperComponent.editType = TestEditComponent;
        fixture.detectChanges();
        expect(editWrapperComponent.shouldRenderErrors()).toBeFalsy();
    });
    it('shouldRenderErrors should return false when the form control for the propertyName is not dirty', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName
        };
        var controls = {};
        controls[propertyName] = new forms_1.FormControl();
        var form = new forms_1.FormGroup(controls);
        editWrapperComponent.data = data;
        editWrapperComponent.editForm = form;
        editWrapperComponent.editType = TestEditComponent;
        fixture.detectChanges();
        expect(editWrapperComponent.shouldRenderErrors()).toBeFalsy();
    });
    it('shouldRenderErrors should return true when the form control for the propertyName is dirty and invalid', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName
        };
        var controls = {};
        controls[propertyName] = new forms_1.FormControl();
        var form = new forms_1.FormGroup(controls);
        form.controls[propertyName].validator = forms_1.Validators.required;
        form.patchValue({ foo: null });
        form.controls[propertyName].markAsDirty();
        editWrapperComponent.data = data;
        editWrapperComponent.editForm = form;
        editWrapperComponent.editType = TestEditComponent;
        fixture.detectChanges();
        expect(editWrapperComponent.shouldRenderErrors()).toBeTruthy();
    });
    it('getErrors should return empty array when getErrorMessages is null', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName
        };
        var form = new forms_1.FormGroup({
            foo: new forms_1.FormControl()
        });
        form.controls[propertyName].validator = forms_1.Validators.required;
        form.patchValue({ foo: null });
        editWrapperComponent.data = data;
        editWrapperComponent.editForm = form;
        editWrapperComponent.editType = TestEditComponent;
        fixture.detectChanges();
        var errors = editWrapperComponent.getErrors();
        expect(errors).not.toBeNull();
        expect(errors.length).toEqual(0);
    });
    it('getErrors should return empty array when there is no propertyName', function () {
        var data = {};
        var form = new forms_1.FormGroup({});
        editWrapperComponent.data = data;
        editWrapperComponent.editForm = form;
        editWrapperComponent.editType = TestEditComponent;
        rowEditionService.getErrorMessages = function (formGroup) { return []; };
        var getErrorsSpy = spyOn(rowEditionService, 'getErrorMessages').and.callThrough();
        fixture.detectChanges();
        var errors = editWrapperComponent.getErrors();
        expect(getErrorsSpy).not.toHaveBeenCalled();
        expect(errors).not.toBeNull();
        expect(errors.length).toEqual(0);
    });
    it('getErrors should return empty array when there are no errors for the control', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName
        };
        var form = new forms_1.FormGroup({
            foo: new forms_1.FormControl()
        });
        form.patchValue({ foo: null });
        editWrapperComponent.data = data;
        editWrapperComponent.editForm = form;
        editWrapperComponent.editType = TestEditComponent;
        rowEditionService.getErrorMessages = function (formGroup) { return []; };
        var getErrorsSpy = spyOn(rowEditionService, 'getErrorMessages').and.callThrough();
        fixture.detectChanges();
        var errors = editWrapperComponent.getErrors();
        expect(getErrorsSpy).not.toHaveBeenCalled();
        expect(errors).not.toBeNull();
        expect(errors.length).toEqual(0);
    });
    it('getErrors should call getErrorMessages and return the error messages when the control is invalid', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName
        };
        var form = new forms_1.FormGroup({
            foo: new forms_1.FormControl()
        });
        form.controls[propertyName].validator = forms_1.Validators.required;
        form.patchValue({ foo: null });
        editWrapperComponent.data = data;
        editWrapperComponent.editForm = form;
        editWrapperComponent.editType = TestEditComponent;
        var errorMessages = ['bar'];
        rowEditionService.getErrorMessages = function (formGroup) { return errorMessages; };
        var getErrorsSpy = spyOn(rowEditionService, 'getErrorMessages').and.callThrough();
        fixture.detectChanges();
        var errors = editWrapperComponent.getErrors();
        expect(getErrorsSpy).toHaveBeenCalled();
        expect(errors).toEqual(errorMessages);
    });
});
//# sourceMappingURL=edit-wrapper.component.spec.js.map