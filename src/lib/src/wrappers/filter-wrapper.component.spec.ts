import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from '../my-grid.component';
import { MyGridModule } from '../my-grid.module';
import { FilterComponent } from '../grid-columns/contracts/filter-component';
import { FilterWrapperComponent } from './filter-wrapper.component';

@Component({
  selector: 'mgt-test-filter',
  template: `
    <div id="test-filter-component"></div>
  `
})
class TestFilterComponent extends FilterComponent {
}

describe('Component: FilterWrapperComponent', () => {
  let filterWrapperComponent: FilterWrapperComponent;
  let fixture: ComponentFixture<FilterWrapperComponent>;
  
  beforeEach(async(() => {
    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [TestFilterComponent]
      }
    });

    TestBed.configureTestingModule({
        declarations: [
          TestFilterComponent
        ],
        providers: [
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(FilterWrapperComponent);
        filterWrapperComponent = fixture.componentInstance;
      });
  }));


  it('FilterWrapperComponent should create filter component and set its input properties', () => {
    var data = {};
    var columnId = '123';
    filterWrapperComponent.filterData = data;
    filterWrapperComponent.columnId = columnId;
    filterWrapperComponent.filterType = TestFilterComponent;

    fixture.detectChanges();

    var testFilterComponent: DebugElement = fixture.debugElement.query(By.css('#test-filter-component'));
    expect(testFilterComponent).not.toBeNull();
    expect(testFilterComponent.context.filterData).toEqual(data);
    expect(testFilterComponent.context.columnId).toEqual(columnId);
  });

  it('FilterWrapperComponent should not create filter component when filterType is not provided', () => {
    var data = {};
    var columnId = '123';
    filterWrapperComponent.filterData = data;
    filterWrapperComponent.columnId = columnId;

    fixture.detectChanges();

    var testFilterComponent: DebugElement = fixture.debugElement.query(By.css('#test-filter-component'));
    expect(testFilterComponent).toBeNull();
  });
});