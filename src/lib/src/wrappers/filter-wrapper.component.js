"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var component_host_directive_1 = require("../component-host.directive");
var FilterWrapperComponent = (function () {
    function FilterWrapperComponent(componentFactoryResolver) {
        this.componentFactoryResolver = componentFactoryResolver;
    }
    FilterWrapperComponent.prototype.ngAfterContentInit = function () {
        if (this.filterType != null) {
            var componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.filterType);
            var viewContainerRef = this.dataComponentHost.viewContainerRef;
            viewContainerRef.clear();
            var componentRef = viewContainerRef.createComponent(componentFactory);
            var componentInstance = componentRef.instance;
            componentInstance.filterData = this.filterData;
            componentInstance.columnId = this.columnId;
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], FilterWrapperComponent.prototype, "columnId", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], FilterWrapperComponent.prototype, "filterData", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.Type)
    ], FilterWrapperComponent.prototype, "filterType", void 0);
    __decorate([
        core_1.ViewChild(component_host_directive_1.ComponentHostDirective),
        __metadata("design:type", component_host_directive_1.ComponentHostDirective)
    ], FilterWrapperComponent.prototype, "dataComponentHost", void 0);
    FilterWrapperComponent = __decorate([
        core_1.Component({
            selector: 'mg-filter-wrapper',
            templateUrl: './filter-wrapper.component.html'
        }),
        __metadata("design:paramtypes", [core_1.ComponentFactoryResolver])
    ], FilterWrapperComponent);
    return FilterWrapperComponent;
}());
exports.FilterWrapperComponent = FilterWrapperComponent;
//# sourceMappingURL=filter-wrapper.component.js.map