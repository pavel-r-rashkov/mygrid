import { 
  Component,
  ComponentFactoryResolver,
  Input,
  Type,
  ViewChild,
  AfterContentInit
} from '@angular/core';

import { ComponentHostDirective } from '../component-host.directive';
import { FilterComponent } from '../grid-columns/contracts/filter-component';

@Component({
  selector: 'mg-filter-wrapper',
  templateUrl: './filter-wrapper.component.html'
})
export class FilterWrapperComponent implements AfterContentInit {
  @Input() columnId: string;
  @Input() filterData: any;
  @Input() filterType: Type<any>;
  @ViewChild(ComponentHostDirective) dataComponentHost: ComponentHostDirective;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngAfterContentInit() {
    if (this.filterType != null) {
      let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.filterType);
      let viewContainerRef = this.dataComponentHost.viewContainerRef;
      viewContainerRef.clear();
      let componentRef = viewContainerRef.createComponent(componentFactory);
      let componentInstance = <FilterComponent>componentRef.instance;
      componentInstance.filterData = this.filterData;
      componentInstance.columnId = this.columnId;
    }
  }
}