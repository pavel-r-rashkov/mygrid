import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from '../my-grid.component';
import { MyGridModule } from '../my-grid.module';
import { EditComponent } from '../grid-columns/contracts/edit-component';
import { EditWrapperComponent } from './edit-wrapper.component';
import { RowEditionService } from '../edition/row-edition.service';

@Component({
  selector: 'mgt-test-edit',
  template: `
    <div id="test-edit-component"></div>
  `
})
class TestEditComponent extends EditComponent {
}

@Component({
  selector: 'mgt-validation-errors-template',
  template: `
    <ng-template #errorsTemplate let-context>
      <div id="validation-errors-template"></div>
    </ng-template>
  `
})
class ValidationErrorsTemplateComponent {
  @ViewChild('errorsTemplate') errorsTemplate: TemplateRef<any>;
}

describe('Component: EditWrapperComponent', () => {
  var editWrapperComponent: EditWrapperComponent;
  var fixture: ComponentFixture<EditWrapperComponent>;
  var validationErrorsTemplate: TemplateRef<any>;
  var rowEditionService: RowEditionService;

  beforeEach(async(() => {
    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [TestEditComponent]
      }
    });

    TestBed.configureTestingModule({
        declarations: [
          TestEditComponent,
          ValidationErrorsTemplateComponent
        ],
        providers: [
          RowEditionService
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(EditWrapperComponent);
        editWrapperComponent = fixture.componentInstance;

        rowEditionService = fixture.debugElement.injector.get(RowEditionService);

        validationErrorsTemplate = TestBed
          .createComponent(ValidationErrorsTemplateComponent)
          .componentInstance
          .errorsTemplate;
      });
  }));


  it('EditWrapperComponent should create edit component and set its input properties', () => {
    var data: any = {};
    var item: any = {};
    var form: FormGroup = new FormGroup({});
    editWrapperComponent.data = data;
    editWrapperComponent.item = item;
    editWrapperComponent.editForm = form;
    editWrapperComponent.editType = TestEditComponent;

    fixture.detectChanges();

    var testEditComponent: DebugElement = fixture.debugElement.query(By.css('#test-edit-component'));
    expect(testEditComponent).not.toBeNull();
    expect(testEditComponent.context.data).toEqual(data);
    expect(testEditComponent.context.item).toEqual(item);
    expect(testEditComponent.context.editForm).toEqual(form);
  });

  it('EditWrapperComponent should not create edit component when editType is not provided', () => {
    var data = {};
    var item = {};
    var form: FormGroup = new FormGroup({});
    editWrapperComponent.data = data;
    editWrapperComponent.item = item;
    editWrapperComponent.editForm = form;

    fixture.detectChanges();

    var testEditComponent: DebugElement = fixture.debugElement.query(By.css('#test-edit-component'));
    expect(testEditComponent).toBeNull();
  });

  it('Custom validation errors template should be rendered', () => {
    var data: any = {
      validationErrorsTemplate: validationErrorsTemplate
    };
    var item: any = {};
    var form: FormGroup = new FormGroup({});
    editWrapperComponent.data = data;
    editWrapperComponent.item = item;
    editWrapperComponent.editForm = form;
    editWrapperComponent.editType = TestEditComponent;

    fixture.detectChanges();

    var errorsTemplate: DebugElement = fixture.debugElement.query(By.css('#validation-errors-template'));
    expect(errorsTemplate).not.toBeNull();
    expect(errorsTemplate.context.$implicit.data).toEqual(data);
    expect(errorsTemplate.context.$implicit.item).toEqual(item);
    expect(errorsTemplate.context.$implicit.editForm).toEqual(form);
    expect(errorsTemplate.context.$implicit.getErrors).toEqual(editWrapperComponent.getErrors);
  });

  it('shouldRenderErrors should return false when there is no propertyName', () => {
    var data: any = {};
    var form: FormGroup = new FormGroup({});
    editWrapperComponent.data = data;
    editWrapperComponent.editForm = form;
    editWrapperComponent.editType = TestEditComponent;

    fixture.detectChanges();

    expect(editWrapperComponent.shouldRenderErrors()).toBeFalsy();
  });

  it('shouldRenderErrors should return false when the form control for the propertyName is not invalid', () => {
    var propertyName = 'foo';
    var data: any = {
      propertyName: propertyName
    };
    var controls = {};
    controls[propertyName] = new FormControl();
    var form: FormGroup = new FormGroup(controls);
    form.controls.foo.validator = Validators.required;
    form.patchValue({ foo: 'asdf' });
    editWrapperComponent.data = data;
    editWrapperComponent.editForm = form;
    editWrapperComponent.editType = TestEditComponent;

    fixture.detectChanges();

    expect(editWrapperComponent.shouldRenderErrors()).toBeFalsy();
  });

  it('shouldRenderErrors should return false when the form control for the propertyName is not dirty', () => {
    var propertyName = 'foo';
    var data: any = {
      propertyName: propertyName
    };
    var controls = {};
    controls[propertyName] = new FormControl();
    var form: FormGroup = new FormGroup(controls);
    editWrapperComponent.data = data;
    editWrapperComponent.editForm = form;
    editWrapperComponent.editType = TestEditComponent;

    fixture.detectChanges();

    expect(editWrapperComponent.shouldRenderErrors()).toBeFalsy();
  });

  it('shouldRenderErrors should return true when the form control for the propertyName is dirty and invalid', () => {
    var propertyName = 'foo';
    var data: any = {
      propertyName: propertyName
    };
    var controls = {};
    controls[propertyName] = new FormControl();
    var form: FormGroup = new FormGroup(controls);
    form.controls[propertyName].validator = Validators.required;
    form.patchValue({ foo: null });
    (form.controls[propertyName] as FormControl).markAsDirty();
    editWrapperComponent.data = data;
    editWrapperComponent.editForm = form;
    editWrapperComponent.editType = TestEditComponent;

    fixture.detectChanges();

    expect(editWrapperComponent.shouldRenderErrors()).toBeTruthy();
  });

  it('getErrors should return empty array when getErrorMessages is null', () => {
    var propertyName: string = 'foo';
    var data: any = {
      propertyName: propertyName
    };
    var form: FormGroup = new FormGroup({
      foo: new FormControl()
    });
    form.controls[propertyName].validator = Validators.required;
    form.patchValue({ foo: null });
    editWrapperComponent.data = data;
    editWrapperComponent.editForm = form;
    editWrapperComponent.editType = TestEditComponent;

    fixture.detectChanges();
    var errors = editWrapperComponent.getErrors();

    expect(errors).not.toBeNull();
    expect(errors.length).toEqual(0);
  });

  it('getErrors should return empty array when there is no propertyName', () => {
    var data: any = {};
    var form: FormGroup = new FormGroup({});
    editWrapperComponent.data = data;
    editWrapperComponent.editForm = form;
    editWrapperComponent.editType = TestEditComponent;
    rowEditionService.getErrorMessages = (formGroup: FormGroup): string[] => { return []; };
    var getErrorsSpy = spyOn(rowEditionService, 'getErrorMessages').and.callThrough();

    fixture.detectChanges();
    var errors = editWrapperComponent.getErrors();

    expect(getErrorsSpy).not.toHaveBeenCalled();
    expect(errors).not.toBeNull();
    expect(errors.length).toEqual(0);
  });

  it('getErrors should return empty array when there are no errors for the control', () => {
    var propertyName: string = 'foo';
    var data: any = {
      propertyName: propertyName
    };
    var form: FormGroup = new FormGroup({
      foo: new FormControl()
    });
    form.patchValue({ foo: null });
    editWrapperComponent.data = data;
    editWrapperComponent.editForm = form;
    editWrapperComponent.editType = TestEditComponent;
    rowEditionService.getErrorMessages = (formGroup: FormGroup): string[] => { return []; };
    var getErrorsSpy = spyOn(rowEditionService, 'getErrorMessages').and.callThrough();

    fixture.detectChanges();
    var errors = editWrapperComponent.getErrors();

    expect(getErrorsSpy).not.toHaveBeenCalled();
    expect(errors).not.toBeNull();
    expect(errors.length).toEqual(0);
  });
  
  it('getErrors should call getErrorMessages and return the error messages when the control is invalid', () => {
    var propertyName: string = 'foo';
    var data: any = {
      propertyName: propertyName
    };
    var form: FormGroup = new FormGroup({
      foo: new FormControl()
    });
    form.controls[propertyName].validator = Validators.required;
    form.patchValue({ foo: null });
    editWrapperComponent.data = data;
    editWrapperComponent.editForm = form;
    editWrapperComponent.editType = TestEditComponent;
    var errorMessages = ['bar'];
    rowEditionService.getErrorMessages = (formGroup: FormGroup): string[] => { return errorMessages; };
    var getErrorsSpy = spyOn(rowEditionService, 'getErrorMessages').and.callThrough();

    fixture.detectChanges();
    var errors = editWrapperComponent.getErrors();

    expect(getErrorsSpy).toHaveBeenCalled();
    expect(errors).toEqual(errorMessages);
  });
});