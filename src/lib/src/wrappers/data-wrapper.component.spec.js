"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
require("rxjs/add/observable/of");
var my_grid_module_1 = require("../my-grid.module");
var data_component_1 = require("../grid-columns/contracts/data-component");
var data_wrapper_component_1 = require("./data-wrapper.component");
var TestDataComponent = (function (_super) {
    __extends(TestDataComponent, _super);
    function TestDataComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TestDataComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-data',
            template: "\n    <div id=\"test-data-component\"></div>\n  "
        })
    ], TestDataComponent);
    return TestDataComponent;
}(data_component_1.DataComponent));
describe('Component: DataWrapperComponent', function () {
    var dataWrapperComponent;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.overrideModule(testing_2.BrowserDynamicTestingModule, {
            set: {
                entryComponents: [TestDataComponent]
            }
        });
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestDataComponent
            ],
            providers: [],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(data_wrapper_component_1.DataWrapperComponent);
            dataWrapperComponent = fixture.componentInstance;
        });
    }));
    it('DataWrapperComponent should create data component and set its input properties', function () {
        var data = {};
        var item = {};
        var visibleIndex = 10;
        dataWrapperComponent.data = data;
        dataWrapperComponent.item = item;
        dataWrapperComponent.visibleIndex = visibleIndex;
        dataWrapperComponent.dataType = TestDataComponent;
        fixture.detectChanges();
        var testDataComponent = fixture.debugElement.query(platform_browser_1.By.css('#test-data-component'));
        expect(testDataComponent).not.toBeNull();
        expect(testDataComponent.context.data).toEqual(data);
        expect(testDataComponent.context.item).toEqual(item);
        expect(testDataComponent.context.visibleIndex).toEqual(visibleIndex);
    });
});
//# sourceMappingURL=data-wrapper.component.spec.js.map