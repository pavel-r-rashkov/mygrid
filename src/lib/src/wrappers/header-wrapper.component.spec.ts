import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from '../my-grid.component';
import { MyGridModule } from '../my-grid.module';
import { HeaderComponent } from '../grid-columns/contracts/header-component';
import { HeaderWrapperComponent } from './header-wrapper.component';
import { ColumnOrderingService } from '../column-ordering/column-ordering.service';
import { GroupService } from '../grouping/group.service';
import { GroupState } from '../grouping/group-state';
import { ColumnReorderEvent } from '../column-ordering/column-reorder-event';

@Component({
  selector: 'mgt-test-header',
  template: `
    <div id="test-header-component"></div>
  `
})
class TestHeaderComponent extends HeaderComponent {
}

describe('Component: HeaderWrapperComponent', () => {
  let headerWrapperComponent: HeaderWrapperComponent;
  let fixture: ComponentFixture<HeaderWrapperComponent>;
  let orderingService: ColumnOrderingService;
  let groupService: GroupService;

  beforeEach(async(() => {
    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [TestHeaderComponent]
      }
    });

    TestBed.configureTestingModule({
        declarations: [
          TestHeaderComponent
        ],
        providers: [
          ColumnOrderingService,
          GroupService
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(HeaderWrapperComponent);
        headerWrapperComponent = fixture.componentInstance;

        orderingService = fixture.debugElement.injector.get(ColumnOrderingService);
        groupService = fixture.debugElement.injector.get(GroupService);
      });
  }));


  it('HeaderWrapperComponent should create header component and set its input properties', () => {
    var data = {};
    var columnId = '123';
    headerWrapperComponent.columnId = columnId;
    headerWrapperComponent.headerData = data;
    headerWrapperComponent.headerType = TestHeaderComponent;

    fixture.detectChanges();

    var testHeaderComponent: DebugElement = fixture.debugElement.query(By.css('#test-header-component'));
    expect(testHeaderComponent).not.toBeNull();
    expect(testHeaderComponent.context.headerData).toEqual(data);
    expect(testHeaderComponent.context.columnId).toEqual(columnId);
  });

  it('data transfer should be set when drag starts', () => {
    var data = {
      propertyName: 'foo',
      enableGrouping: true
    };
    var columnId = '123';
    headerWrapperComponent.columnId = columnId;
    headerWrapperComponent.headerData = data;
    headerWrapperComponent.headerType = TestHeaderComponent;
    var dragStartEvent = {
      dataTransfer: { setData: () => {} }
    };
    var setDataSpy: jasmine.Spy = spyOn(dragStartEvent.dataTransfer, 'setData');

    fixture.detectChanges();
    headerWrapperComponent.columnDragStart(dragStartEvent);
    
    expect(setDataSpy).toHaveBeenCalled();
    var dataTransferKey = setDataSpy.calls.first().args[0];
    expect(dataTransferKey).toEqual('draggedColumnData');
    var dataTransfer = JSON.parse(setDataSpy.calls.first().args[1]);
    expect(dataTransfer.columnId).toEqual(columnId);
    expect(dataTransfer.propertyName).toEqual(data.propertyName);
    expect(dataTransfer.enableGrouping).toEqual(data.enableGrouping);
  });

  it('columnDrop over grouped column should create new group state with the dropped column when it has grouping enabled', () => {
    var data = {
      propertyName: 'foo',
      enableGrouping: true
    };
    var columnId = '123';
    headerWrapperComponent.columnId = columnId;
    headerWrapperComponent.headerData = data;
    headerWrapperComponent.headerType = TestHeaderComponent;
    var droppedColumnId = '456';
    var droppedPropertyName = 'baz';
    var dropEvent = {
      stopPropagation: function() {},
      dataTransfer: { 
        getData: () => { 
          return JSON.stringify({
            columnId: droppedColumnId,
            propertyName: droppedPropertyName,
            enableGrouping: true
          }); 
        } 
      }
    };
    var groupStateSpy: jasmine.Spy = spyOn(groupService.groupState, 'next').and.callThrough();

    fixture.detectChanges();
    groupService.groupState.next(new GroupState(columnId, data.propertyName, true));

    fixture.whenStable().then(() => {
      groupStateSpy.calls.reset();
      headerWrapperComponent.columnDrop(dropEvent);

      expect(groupStateSpy).toHaveBeenCalled();
      var newGroupState: GroupState = groupStateSpy.calls.first().args[0];
      expect(newGroupState.columnId).toEqual(droppedColumnId);
      expect(newGroupState.propertyName).toEqual(droppedPropertyName);
    });
  });

  it('columnDrop over grouped column should not create new group state when the dropped column has grouping disabled', () => {
    var data = {
      propertyName: 'foo',
      enableGrouping: true
    };
    var columnId = '123';
    headerWrapperComponent.columnId = columnId;
    headerWrapperComponent.headerData = data;
    headerWrapperComponent.headerType = TestHeaderComponent;
    var droppedColumnId = '456';
    var droppedPropertyName = 'baz';
    var dropEvent = {
      stopPropagation: function() {},
      dataTransfer: {
        getData: (key: string) => { 
          return JSON.stringify({
            columnId: droppedColumnId,
            propertyName: droppedPropertyName,
            enableGrouping: false
          }); 
        } 
      }
    };
    var groupStateSpy: jasmine.Spy = spyOn(groupService.groupState, 'next').and.callThrough();

    fixture.detectChanges();
    groupService.groupState.next(new GroupState(columnId, data.propertyName, true));

    fixture.whenStable().then(() => {
      groupStateSpy.calls.reset();
      headerWrapperComponent.columnDrop(dropEvent);

      expect(groupStateSpy).not.toHaveBeenCalled();
    });
  });

  it('columnDrop over non grouped column should raise column reorder event', () => {
    var data = {
      propertyName: 'foo',
      enableGrouping: true
    };
    var columnId = '123';
    headerWrapperComponent.columnId = columnId;
    headerWrapperComponent.headerData = data;
    headerWrapperComponent.headerType = TestHeaderComponent;
    var droppedColumnId = '456';
    var droppedPropertyName = 'baz';
    var dropEvent = {
      stopPropagation: function() {},
      dataTransfer: {
        getData: (key: string) => { 
          return JSON.stringify({
            columnId: droppedColumnId,
            propertyName: droppedPropertyName,
            enableGrouping: false
          }); 
        } 
      }
    };
    var reorderSpy: jasmine.Spy = spyOn(orderingService.columnReorder, 'next');

    fixture.detectChanges();
    headerWrapperComponent.columnDrop(dropEvent);
    
    expect(reorderSpy).toHaveBeenCalled();
    var reorderEvent: ColumnReorderEvent = reorderSpy.calls.first().args[0];

    var movedColumn = reorderEvent.movedColumn;
    expect(movedColumn.columnId).toEqual(droppedColumnId);
    expect(movedColumn.propertyName).toEqual(droppedPropertyName);

    var targetColumn = reorderEvent.targetColumn;
    expect(targetColumn.columnId).toEqual(columnId);
    expect(targetColumn.propertyName).toEqual(data.propertyName);
  });
});