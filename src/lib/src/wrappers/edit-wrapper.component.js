"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var component_host_directive_1 = require("../component-host.directive");
var row_edition_service_1 = require("../edition/row-edition.service");
var EditWrapperComponent = (function () {
    function EditWrapperComponent(componentFactoryResolver, rowEditionService) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.rowEditionService = rowEditionService;
    }
    EditWrapperComponent.prototype.ngOnInit = function () {
        if (this.editType == null) {
            return;
        }
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.editType);
        var viewContainerRef = this.headerComponentHost.viewContainerRef;
        viewContainerRef.clear();
        var componentRef = viewContainerRef.createComponent(componentFactory);
        var editComponentInstance = componentRef.instance;
        editComponentInstance.data = this.data;
        editComponentInstance.item = this.item;
        editComponentInstance.editForm = this.editForm;
        // render custom validation errors template
        if (this.data.validationErrorsTemplate != null) {
            var errorsTemplateContext = {
                data: this.data,
                item: this.item,
                editForm: this.editForm,
                getErrors: this.getErrors
            };
            this.errorsTemplatePlaceholder.createEmbeddedView(this.data.validationErrorsTemplate, { $implicit: errorsTemplateContext });
        }
    };
    EditWrapperComponent.prototype.shouldRenderErrors = function () {
        if (this.data.propertyName == null) {
            return false;
        }
        return this.editForm.get([this.data.propertyName]).invalid
            && (this.editForm.get([this.data.propertyName]).dirty);
    };
    EditWrapperComponent.prototype.getErrors = function () {
        if (this.rowEditionService.getErrorMessages != null
            && this.data.propertyName != null
            && this.editForm.controls[this.data.propertyName].errors != null) {
            var errors = this.rowEditionService.getErrorMessages(this.editForm.controls[this.data.propertyName].errors, this.data.propertyName);
            return errors;
        }
        return [];
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], EditWrapperComponent.prototype, "data", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], EditWrapperComponent.prototype, "item", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.Type)
    ], EditWrapperComponent.prototype, "editType", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", forms_1.FormGroup)
    ], EditWrapperComponent.prototype, "editForm", void 0);
    __decorate([
        core_1.ViewChild(component_host_directive_1.ComponentHostDirective),
        __metadata("design:type", component_host_directive_1.ComponentHostDirective)
    ], EditWrapperComponent.prototype, "headerComponentHost", void 0);
    __decorate([
        core_1.ViewChild('errorsTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], EditWrapperComponent.prototype, "errorsTemplatePlaceholder", void 0);
    EditWrapperComponent = __decorate([
        core_1.Component({
            selector: 'mg-edit-wrapper',
            templateUrl: './edit-wrapper.component.html'
        }),
        __metadata("design:paramtypes", [core_1.ComponentFactoryResolver,
            row_edition_service_1.RowEditionService])
    ], EditWrapperComponent);
    return EditWrapperComponent;
}());
exports.EditWrapperComponent = EditWrapperComponent;
//# sourceMappingURL=edit-wrapper.component.js.map