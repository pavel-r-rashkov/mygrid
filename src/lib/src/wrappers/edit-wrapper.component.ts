import { 
  Component, 
  Input,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  TemplateRef,
  ViewContainerRef,
  Type
} from '@angular/core';

import { FormGroup } from '@angular/forms';

import { ComponentHostDirective } from '../component-host.directive';
import { EditComponent } from '../grid-columns/contracts/edit-component';
import { RowEditionService } from '../edition/row-edition.service';

@Component({
  selector: 'mg-edit-wrapper',
  templateUrl: './edit-wrapper.component.html'
})
export class EditWrapperComponent implements OnInit {
  @Input() data: any;
  @Input() item: any;
  @Input() editType: Type<any>;
  @Input() editForm: FormGroup;
  @ViewChild(ComponentHostDirective) headerComponentHost: ComponentHostDirective;
  @ViewChild('errorsTemplatePlaceholder', { read: ViewContainerRef }) errorsTemplatePlaceholder: ViewContainerRef;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private rowEditionService: RowEditionService) {
  }

  ngOnInit() {
    if (this.editType == null) {
      return;
    }

    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.editType);
    let viewContainerRef = this.headerComponentHost.viewContainerRef;
    viewContainerRef.clear();
    let componentRef = viewContainerRef.createComponent(componentFactory);
    let editComponentInstance = <EditComponent>componentRef.instance;
    editComponentInstance.data = this.data;
    editComponentInstance.item = this.item;
    editComponentInstance.editForm = this.editForm;

    // render custom validation errors template
    if (this.data.validationErrorsTemplate != null) {
      let errorsTemplateContext = {
        data: this.data,
        item: this.item,
        editForm: this.editForm,
        getErrors: this.getErrors
      };

      this.errorsTemplatePlaceholder.createEmbeddedView(this.data.validationErrorsTemplate, { $implicit: errorsTemplateContext });
    }
  }

  shouldRenderErrors() {
    if (this.data.propertyName == null) {
      return false;
    }

    return this.editForm.get([this.data.propertyName]).invalid 
      && (this.editForm.get([this.data.propertyName]).dirty);
  }

  getErrors() {
    if (this.rowEditionService.getErrorMessages != null 
      && this.data.propertyName != null
      && this.editForm.controls[this.data.propertyName].errors != null) {
      let errors: string[] = this.rowEditionService.getErrorMessages(
        this.editForm.controls[this.data.propertyName].errors, 
        this.data.propertyName);

      return errors;
    }

    return [];
  }
}