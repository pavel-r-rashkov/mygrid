"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var component_host_directive_1 = require("../component-host.directive");
var column_ordering_service_1 = require("../column-ordering/column-ordering.service");
var column_reorder_event_1 = require("../column-ordering/column-reorder-event");
var group_service_1 = require("../grouping/group.service");
var group_state_1 = require("../grouping/group-state");
var HeaderWrapperComponent = (function () {
    function HeaderWrapperComponent(componentFactoryResolver, columnOrderingService, groupService, zone, renderer) {
        var _this = this;
        this.componentFactoryResolver = componentFactoryResolver;
        this.columnOrderingService = columnOrderingService;
        this.groupService = groupService;
        this.zone = zone;
        this.renderer = renderer;
        this.DRAGGED_HEADER_DATA_TRANSFER_KEY = 'draggedColumnData';
        this.showLeftDropColumnMarker = false;
        this.showRightDropColumnMarker = false;
        this.groupStateSubscription = this.groupService.groupState.subscribe(function (newState) {
            _this.currentGroupState = newState;
        });
    }
    HeaderWrapperComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.zone.runOutsideAngular(function () {
            var handler = function ($event) {
                _this.columnDragOver($event);
            };
            _this.renderer.listen(_this.headerWrapper.nativeElement, 'dragover', handler);
        });
        this.updateHeaderComponent();
    };
    HeaderWrapperComponent.prototype.ngOnChanges = function () {
        this.updateHeaderComponent();
    };
    HeaderWrapperComponent.prototype.ngOnDestroy = function () {
        this.groupStateSubscription.unsubscribe();
    };
    HeaderWrapperComponent.prototype.columnDragStart = function ($event) {
        var draggedColumnData = {
            columnId: this.columnId,
            propertyName: this.headerData.propertyName,
            enableGrouping: this.headerData.enableGrouping
        };
        $event.dataTransfer.setData(this.DRAGGED_HEADER_DATA_TRANSFER_KEY, JSON.stringify(draggedColumnData));
    };
    HeaderWrapperComponent.prototype.columnDrop = function ($event) {
        $event.stopPropagation(); // when multiple group columns are supported
        var columnDataTransfer = JSON.parse($event.dataTransfer.getData(this.DRAGGED_HEADER_DATA_TRANSFER_KEY));
        var isMovedInFront = this.showLeftDropColumnMarker;
        this.showLeftDropColumnMarker = false;
        this.showRightDropColumnMarker = false;
        if (this.currentGroupState != null && this.currentGroupState.columnId == this.columnId) {
            if (columnDataTransfer.enableGrouping) {
                this.groupService.groupState.next(new group_state_1.GroupState(columnDataTransfer.columnId, columnDataTransfer.propertyName, true));
            }
            return;
        }
        if (columnDataTransfer != null && columnDataTransfer.columnId != this.columnId) {
            var targetColumn = {
                columnId: this.columnId,
                propertyName: this.headerData.propertyName
            };
            var columnReorderEvent = new column_reorder_event_1.ColumnReorderEvent(columnDataTransfer, targetColumn, isMovedInFront);
            this.columnOrderingService.raiseColumnReorderedEvent(columnReorderEvent);
        }
    };
    HeaderWrapperComponent.prototype.columnDragLeave = function ($event) {
        this.showLeftDropColumnMarker = false;
        this.showRightDropColumnMarker = false;
    };
    HeaderWrapperComponent.prototype.columnDragOver = function ($event) {
        var _this = this;
        $event.preventDefault();
        // remove when multiple group columns are supported
        if (this.currentGroupState != null && this.currentGroupState.columnId == this.columnId) {
            return;
        }
        var targetRect = this.headerWrapper.nativeElement.getClientRects()[0];
        var leftOffset = $event.clientX - targetRect.left;
        var showLeft = this.showLeftDropColumnMarker;
        var showRight = this.showRightDropColumnMarker;
        if (leftOffset < (targetRect.width / 2)) {
            showLeft = true;
            showRight = false;
        }
        else {
            showLeft = false;
            showRight = true;
        }
        if (showLeft != this.showLeftDropColumnMarker || showRight != this.showRightDropColumnMarker) {
            this.zone.run(function () {
                _this.showLeftDropColumnMarker = showLeft;
                _this.showRightDropColumnMarker = showRight;
            });
        }
    };
    HeaderWrapperComponent.prototype.updateHeaderComponent = function () {
        if (this.headerType != null) {
            if (this.headerComponent) {
                this.headerComponent.destroy();
            }
            var componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.headerType);
            var viewContainerRef = this.headerComponentHost.viewContainerRef;
            viewContainerRef.clear();
            this.headerComponent = viewContainerRef.createComponent(componentFactory);
            var headerComponentInstance = this.headerComponent.instance;
            headerComponentInstance.headerData = this.headerData;
            headerComponentInstance.columnId = this.columnId;
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], HeaderWrapperComponent.prototype, "columnId", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], HeaderWrapperComponent.prototype, "headerData", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.Type)
    ], HeaderWrapperComponent.prototype, "headerType", void 0);
    __decorate([
        core_1.ViewChild(component_host_directive_1.ComponentHostDirective),
        __metadata("design:type", component_host_directive_1.ComponentHostDirective)
    ], HeaderWrapperComponent.prototype, "headerComponentHost", void 0);
    __decorate([
        core_1.ViewChild('headerWrapper'),
        __metadata("design:type", core_1.ElementRef)
    ], HeaderWrapperComponent.prototype, "headerWrapper", void 0);
    HeaderWrapperComponent = __decorate([
        core_1.Component({
            selector: 'mg-header-wrapper',
            templateUrl: './header-wrapper.component.html'
        }),
        __metadata("design:paramtypes", [core_1.ComponentFactoryResolver,
            column_ordering_service_1.ColumnOrderingService,
            group_service_1.GroupService,
            core_1.NgZone,
            core_1.Renderer2])
    ], HeaderWrapperComponent);
    return HeaderWrapperComponent;
}());
exports.HeaderWrapperComponent = HeaderWrapperComponent;
//# sourceMappingURL=header-wrapper.component.js.map