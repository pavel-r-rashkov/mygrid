import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from '../my-grid.component';
import { MyGridModule } from '../my-grid.module';
import { DataComponent } from '../grid-columns/contracts/data-component';
import { DataWrapperComponent } from './data-wrapper.component';

@Component({
  selector: 'mgt-test-data',
  template: `
    <div id="test-data-component"></div>
  `
})
class TestDataComponent extends DataComponent {
}

describe('Component: DataWrapperComponent', () => {
  let dataWrapperComponent: DataWrapperComponent;
  let fixture: ComponentFixture<DataWrapperComponent>;
  
  beforeEach(async(() => {
    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [TestDataComponent]
      }
    });

    TestBed.configureTestingModule({
        declarations: [
          TestDataComponent
        ],
        providers: [
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(DataWrapperComponent);
        dataWrapperComponent = fixture.componentInstance;
      });
  }));


  it('DataWrapperComponent should create data component and set its input properties', () => {
    var data = {};
    var item = {};
    var visibleIndex = 10;
    dataWrapperComponent.data = data;
    dataWrapperComponent.item = item;
    dataWrapperComponent.visibleIndex = visibleIndex;
    dataWrapperComponent.dataType = TestDataComponent;

    fixture.detectChanges();

    var testDataComponent: DebugElement = fixture.debugElement.query(By.css('#test-data-component'));
    expect(testDataComponent).not.toBeNull();
    expect(testDataComponent.context.data).toEqual(data);
    expect(testDataComponent.context.item).toEqual(item);
    expect(testDataComponent.context.visibleIndex).toEqual(visibleIndex);
  });
});