"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
require("rxjs/add/observable/of");
var my_grid_module_1 = require("../my-grid.module");
var header_component_1 = require("../grid-columns/contracts/header-component");
var header_wrapper_component_1 = require("./header-wrapper.component");
var column_ordering_service_1 = require("../column-ordering/column-ordering.service");
var group_service_1 = require("../grouping/group.service");
var group_state_1 = require("../grouping/group-state");
var TestHeaderComponent = (function (_super) {
    __extends(TestHeaderComponent, _super);
    function TestHeaderComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TestHeaderComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-header',
            template: "\n    <div id=\"test-header-component\"></div>\n  "
        })
    ], TestHeaderComponent);
    return TestHeaderComponent;
}(header_component_1.HeaderComponent));
describe('Component: HeaderWrapperComponent', function () {
    var headerWrapperComponent;
    var fixture;
    var orderingService;
    var groupService;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.overrideModule(testing_2.BrowserDynamicTestingModule, {
            set: {
                entryComponents: [TestHeaderComponent]
            }
        });
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestHeaderComponent
            ],
            providers: [
                column_ordering_service_1.ColumnOrderingService,
                group_service_1.GroupService
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(header_wrapper_component_1.HeaderWrapperComponent);
            headerWrapperComponent = fixture.componentInstance;
            orderingService = fixture.debugElement.injector.get(column_ordering_service_1.ColumnOrderingService);
            groupService = fixture.debugElement.injector.get(group_service_1.GroupService);
        });
    }));
    it('HeaderWrapperComponent should create header component and set its input properties', function () {
        var data = {};
        var columnId = '123';
        headerWrapperComponent.columnId = columnId;
        headerWrapperComponent.headerData = data;
        headerWrapperComponent.headerType = TestHeaderComponent;
        fixture.detectChanges();
        var testHeaderComponent = fixture.debugElement.query(platform_browser_1.By.css('#test-header-component'));
        expect(testHeaderComponent).not.toBeNull();
        expect(testHeaderComponent.context.headerData).toEqual(data);
        expect(testHeaderComponent.context.columnId).toEqual(columnId);
    });
    it('data transfer should be set when drag starts', function () {
        var data = {
            propertyName: 'foo',
            enableGrouping: true
        };
        var columnId = '123';
        headerWrapperComponent.columnId = columnId;
        headerWrapperComponent.headerData = data;
        headerWrapperComponent.headerType = TestHeaderComponent;
        var dragStartEvent = {
            dataTransfer: { setData: function () { } }
        };
        var setDataSpy = spyOn(dragStartEvent.dataTransfer, 'setData');
        fixture.detectChanges();
        headerWrapperComponent.columnDragStart(dragStartEvent);
        expect(setDataSpy).toHaveBeenCalled();
        var dataTransferKey = setDataSpy.calls.first().args[0];
        expect(dataTransferKey).toEqual('draggedColumnData');
        var dataTransfer = JSON.parse(setDataSpy.calls.first().args[1]);
        expect(dataTransfer.columnId).toEqual(columnId);
        expect(dataTransfer.propertyName).toEqual(data.propertyName);
        expect(dataTransfer.enableGrouping).toEqual(data.enableGrouping);
    });
    it('columnDrop over grouped column should create new group state with the dropped column when it has grouping enabled', function () {
        var data = {
            propertyName: 'foo',
            enableGrouping: true
        };
        var columnId = '123';
        headerWrapperComponent.columnId = columnId;
        headerWrapperComponent.headerData = data;
        headerWrapperComponent.headerType = TestHeaderComponent;
        var droppedColumnId = '456';
        var droppedPropertyName = 'baz';
        var dropEvent = {
            stopPropagation: function () { },
            dataTransfer: {
                getData: function () {
                    return JSON.stringify({
                        columnId: droppedColumnId,
                        propertyName: droppedPropertyName,
                        enableGrouping: true
                    });
                }
            }
        };
        var groupStateSpy = spyOn(groupService.groupState, 'next').and.callThrough();
        fixture.detectChanges();
        groupService.groupState.next(new group_state_1.GroupState(columnId, data.propertyName, true));
        fixture.whenStable().then(function () {
            groupStateSpy.calls.reset();
            headerWrapperComponent.columnDrop(dropEvent);
            expect(groupStateSpy).toHaveBeenCalled();
            var newGroupState = groupStateSpy.calls.first().args[0];
            expect(newGroupState.columnId).toEqual(droppedColumnId);
            expect(newGroupState.propertyName).toEqual(droppedPropertyName);
        });
    });
    it('columnDrop over grouped column should not create new group state when the dropped column has grouping disabled', function () {
        var data = {
            propertyName: 'foo',
            enableGrouping: true
        };
        var columnId = '123';
        headerWrapperComponent.columnId = columnId;
        headerWrapperComponent.headerData = data;
        headerWrapperComponent.headerType = TestHeaderComponent;
        var droppedColumnId = '456';
        var droppedPropertyName = 'baz';
        var dropEvent = {
            stopPropagation: function () { },
            dataTransfer: {
                getData: function (key) {
                    return JSON.stringify({
                        columnId: droppedColumnId,
                        propertyName: droppedPropertyName,
                        enableGrouping: false
                    });
                }
            }
        };
        var groupStateSpy = spyOn(groupService.groupState, 'next').and.callThrough();
        fixture.detectChanges();
        groupService.groupState.next(new group_state_1.GroupState(columnId, data.propertyName, true));
        fixture.whenStable().then(function () {
            groupStateSpy.calls.reset();
            headerWrapperComponent.columnDrop(dropEvent);
            expect(groupStateSpy).not.toHaveBeenCalled();
        });
    });
    it('columnDrop over non grouped column should raise column reorder event', function () {
        var data = {
            propertyName: 'foo',
            enableGrouping: true
        };
        var columnId = '123';
        headerWrapperComponent.columnId = columnId;
        headerWrapperComponent.headerData = data;
        headerWrapperComponent.headerType = TestHeaderComponent;
        var droppedColumnId = '456';
        var droppedPropertyName = 'baz';
        var dropEvent = {
            stopPropagation: function () { },
            dataTransfer: {
                getData: function (key) {
                    return JSON.stringify({
                        columnId: droppedColumnId,
                        propertyName: droppedPropertyName,
                        enableGrouping: false
                    });
                }
            }
        };
        var reorderSpy = spyOn(orderingService.columnReorder, 'next');
        fixture.detectChanges();
        headerWrapperComponent.columnDrop(dropEvent);
        expect(reorderSpy).toHaveBeenCalled();
        var reorderEvent = reorderSpy.calls.first().args[0];
        var movedColumn = reorderEvent.movedColumn;
        expect(movedColumn.columnId).toEqual(droppedColumnId);
        expect(movedColumn.propertyName).toEqual(droppedPropertyName);
        var targetColumn = reorderEvent.targetColumn;
        expect(targetColumn.columnId).toEqual(columnId);
        expect(targetColumn.propertyName).toEqual(data.propertyName);
    });
});
//# sourceMappingURL=header-wrapper.component.spec.js.map