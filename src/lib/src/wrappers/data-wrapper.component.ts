import { 
  Component,
  Input,
  ComponentFactoryResolver,
  Type,
  ViewChild,
  AfterContentInit } from '@angular/core';

import { ComponentHostDirective } from '../component-host.directive';
import { DataComponent } from '../grid-columns/contracts/data-component';

@Component({
  selector: 'mg-data-wrapper',
  templateUrl: './data-wrapper.component.html'
})
export class DataWrapperComponent implements AfterContentInit {
  @Input() data: any;
  @Input() dataType: Type<any>;
  @Input() item: any;
  @Input() visibleIndex: number;
  @ViewChild(ComponentHostDirective) dataComponentHost: ComponentHostDirective;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngAfterContentInit() {
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.dataType);
    let viewContainerRef = this.dataComponentHost.viewContainerRef;
    viewContainerRef.clear();
    let componentRef = viewContainerRef.createComponent(componentFactory);
    let componentInstance = <DataComponent>componentRef.instance;
    componentInstance.data = this.data;
    componentInstance.item = this.item;
    componentInstance.visibleIndex = this.visibleIndex;
  }
}