import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { DeleteColumnDirective } from './delete-column.directive';
import { GuidService } from '../../guid';

@Component({
  selector: 'mgt-test-delete-column',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
  `
}) 
class TestDeleteColumnComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
}

describe('Directive: DeleteColumnDirective', () => {
  let deleteColumnDirective: DeleteColumnDirective;
  let dataTemplate: TemplateRef<any>;
  let headerTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestDeleteColumnComponent
        ]
      })
      .compileComponents()
      .then(() => {
        deleteColumnDirective = new DeleteColumnDirective(new GuidService());

        var testComponent = TestBed.createComponent(TestDeleteColumnComponent).componentInstance;
        dataTemplate = testComponent.dataTemplate;
        headerTemplate = testComponent.headerTemplate;
      });
  }));

  it('getHeaderData should return delete column header data', () => {
    var caption = 'foo';
    deleteColumnDirective.caption = caption;
    deleteColumnDirective.headerTemplate = headerTemplate;

    var headerData: any = deleteColumnDirective.getHeaderData();

    expect(headerData.caption).toEqual(caption);
    expect(headerData.headerTemplate).toEqual(headerTemplate);
    expect(headerData.enableOrdering).toBeFalsy();
  });

  it('getData should return delete column data', () => {
    var buttonText: string = 'foo';
    deleteColumnDirective.deleteButtonText = buttonText;
    deleteColumnDirective.dataTemplate = dataTemplate;

    var data: any = deleteColumnDirective.getData();

    expect(data.deleteButtonText).toEqual(buttonText);
    expect(data.dataTemplate).toEqual(dataTemplate);
  });
});