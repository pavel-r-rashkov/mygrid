import { 
  Component, 
  OnInit,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { DataComponent } from '../contracts/data-component';
import { RowDeletionService } from '../../edition/row-deletion.service';
import { GridSettingsService } from '../../grid-settings.service';

@Component({
  selector: 'mg-delete-column-data',
  templateUrl: './delete-column-data.component.html'
})
export class DeleteColumnDataComponent extends DataComponent implements OnInit {
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;
  
  constructor(
    private rowDeletionService: RowDeletionService,
    private gridSettingsService: GridSettingsService) {
    super();
  }

  ngOnInit() {
    if (this.data.dataTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item
      };
      this.containerRef.createEmbeddedView(this.data.dataTemplate, { $implicit: templateContext });
    }
  }

  deleteRow() {
    let itemKey = this.item[this.gridSettingsService.key];
    this.rowDeletionService.beforeRowDelete.next(itemKey); 
  }
}