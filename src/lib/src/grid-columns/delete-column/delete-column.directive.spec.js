"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var delete_column_directive_1 = require("./delete-column.directive");
var guid_1 = require("../../guid");
var TestDeleteColumnComponent = (function () {
    function TestDeleteColumnComponent() {
    }
    __decorate([
        core_1.ViewChild('dataTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestDeleteColumnComponent.prototype, "dataTemplate", void 0);
    __decorate([
        core_1.ViewChild('headerTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestDeleteColumnComponent.prototype, "headerTemplate", void 0);
    TestDeleteColumnComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-delete-column',
            template: "\n    <ng-template #dataTemplate>\n      <div id=\"test-data-template\"></div>\n    </ng-template>\n    <ng-template #headerTemplate>\n      <div id=\"test-header-template\"></div>\n    </ng-template>\n  "
        })
    ], TestDeleteColumnComponent);
    return TestDeleteColumnComponent;
}());
describe('Directive: DeleteColumnDirective', function () {
    var deleteColumnDirective;
    var dataTemplate;
    var headerTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestDeleteColumnComponent
            ]
        })
            .compileComponents()
            .then(function () {
            deleteColumnDirective = new delete_column_directive_1.DeleteColumnDirective(new guid_1.GuidService());
            var testComponent = testing_1.TestBed.createComponent(TestDeleteColumnComponent).componentInstance;
            dataTemplate = testComponent.dataTemplate;
            headerTemplate = testComponent.headerTemplate;
        });
    }));
    it('getHeaderData should return delete column header data', function () {
        var caption = 'foo';
        deleteColumnDirective.caption = caption;
        deleteColumnDirective.headerTemplate = headerTemplate;
        var headerData = deleteColumnDirective.getHeaderData();
        expect(headerData.caption).toEqual(caption);
        expect(headerData.headerTemplate).toEqual(headerTemplate);
        expect(headerData.enableOrdering).toBeFalsy();
    });
    it('getData should return delete column data', function () {
        var buttonText = 'foo';
        deleteColumnDirective.deleteButtonText = buttonText;
        deleteColumnDirective.dataTemplate = dataTemplate;
        var data = deleteColumnDirective.getData();
        expect(data.deleteButtonText).toEqual(buttonText);
        expect(data.dataTemplate).toEqual(dataTemplate);
    });
});
//# sourceMappingURL=delete-column.directive.spec.js.map