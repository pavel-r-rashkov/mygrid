import { 
  Directive,
  TemplateRef,
  Input,
  ContentChild,
  forwardRef 
} from '@angular/core';

import { GridColumn } from '../grid-column';
import { DefaultColumnHeaderComponent } from '../shared/default-column-header.component';
import { DeleteColumnDataComponent } from './delete-column-data.component';
import { GuidService } from '../../guid';

@Directive({
  selector: 'mg-delete-column',
  providers: [{provide: GridColumn, useExisting: forwardRef(() => DeleteColumnDirective)}]
})
export class DeleteColumnDirective extends GridColumn {
  @Input() deleteButtonText: string = 'DELETE';

  constructor(
    guidService: GuidService) {
    super(
      DefaultColumnHeaderComponent, 
      null, 
      DeleteColumnDataComponent, 
      null,
      guidService.newGuid());
  }

  public getHeaderData(): any {
    return {
      caption: this.caption,
      headerTemplate: this.headerTemplate,
      enableOrdering: false
    };
  }

  public getData(): any {
    return {
      dataTemplate: this.dataTemplate,
      deleteButtonText: this.deleteButtonText
    }
  }
}