import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { DeleteColumnDataComponent } from './delete-column-data.component';
import { RowDeletionService } from '../../edition/row-deletion.service';
import { GridSettingsService } from '../../grid-settings.service';

@Component({
  selector: 'mgt-test-data',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
  `
})
class TestDataComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
}

describe('Component: DeleteColumnDataComponent', () => {
  let deleteColumnDataComponent: DeleteColumnDataComponent;
  let fixture: ComponentFixture<DeleteColumnDataComponent>;
  let dataTemplate: TemplateRef<any>;
  let rowDeletionService: RowDeletionService;
  let gridSettingsService: GridSettingsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestDataComponent
        ],
        providers: [
          RowDeletionService,
          GridSettingsService
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(DeleteColumnDataComponent);
        deleteColumnDataComponent = fixture.componentInstance;

        rowDeletionService = fixture.debugElement.injector.get(RowDeletionService);
        gridSettingsService = fixture.debugElement.injector.get(GridSettingsService);

        var templateComponent: TestDataComponent = TestBed.createComponent(TestDataComponent).componentInstance;
        dataTemplate = templateComponent.dataTemplate;
      });
  }));

  it('deleteRow should raise beforeRowDelete with item\'s key', () => {
    var key = 'foo';
    var item = { foo: 'foo' };
    gridSettingsService.key = key;
    deleteColumnDataComponent.item = item;
    deleteColumnDataComponent.data = {};
    var beforeRowDeleteSpy: jasmine.Spy = spyOn(rowDeletionService.beforeRowDelete, 'next');

    fixture.detectChanges();
    deleteColumnDataComponent.deleteRow();

    expect(beforeRowDeleteSpy).toHaveBeenCalled();
    expect(beforeRowDeleteSpy.calls.first().args[0]).toEqual(item[key]);
  });

  it('custom data template should be rendered when it exists', () => {
    var item = {};
    var data = {
      dataTemplate: dataTemplate
    };
    deleteColumnDataComponent.data = data;
    deleteColumnDataComponent.item = item;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-data-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.data).toEqual(data);
    expect(customTemplate.context.$implicit.item).toEqual(item);
  });
});