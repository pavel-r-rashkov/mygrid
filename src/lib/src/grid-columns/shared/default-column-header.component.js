"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var header_component_1 = require("./../contracts/header-component");
var order_state_1 = require("../../states/order-state");
var order_state_service_1 = require("../../order-state.service");
var group_service_1 = require("../../grouping/group.service");
var group_state_1 = require("../../grouping/group-state");
var DefaultColumnHeaderComponent = (function (_super) {
    __extends(DefaultColumnHeaderComponent, _super);
    function DefaultColumnHeaderComponent(orderStateService, groupService) {
        var _this = _super.call(this) || this;
        _this.orderStateService = orderStateService;
        _this.groupService = groupService;
        _this.orderStateSubscription = _this.orderStateService.orderState.subscribe(function (newState) {
            _this.currentOrderState = newState;
        });
        _this.groupStateSubscription = _this.groupService.groupState.subscribe(function (newState) {
            _this.currentGroupState = newState;
        });
        return _this;
    }
    DefaultColumnHeaderComponent.prototype.ngOnInit = function () {
        if (this.headerData.headerTemplate != null) {
            this.containerRef.createEmbeddedView(this.headerData.headerTemplate, { $implicit: { headerData: this.headerData } });
        }
    };
    DefaultColumnHeaderComponent.prototype.ngOnDestroy = function () {
        this.orderStateSubscription.unsubscribe();
        this.groupStateSubscription.unsubscribe();
    };
    DefaultColumnHeaderComponent.prototype.headerClicked = function () {
        if (this.headerData.enableGrouping
            && this.currentGroupState != null
            && this.currentGroupState.columnId == this.columnId) {
            this.groupService.groupState.next(new group_state_1.GroupState(this.columnId, this.headerData.propertyName, !this.currentGroupState.isAscending));
            return;
        }
        if (!this.headerData.enableOrdering) {
            return;
        }
        var isAscending = true;
        if (this.currentOrderState != null
            && this.currentOrderState.columnId == this.columnId
            && this.currentOrderState.isAscending) {
            isAscending = false;
        }
        this.orderStateService.raiseOrderChanged(new order_state_1.OrderState(this.columnId, this.headerData.propertyName, isAscending));
    };
    DefaultColumnHeaderComponent.prototype.isOrderMarkerVisible = function () {
        var isColumnOrdered = this.currentOrderState != null && this.currentOrderState.columnId == this.columnId;
        var isColumnGrouped = this.currentGroupState != null && this.currentGroupState.columnId == this.columnId;
        return isColumnOrdered || isColumnGrouped;
    };
    DefaultColumnHeaderComponent.prototype.isAscending = function () {
        if (this.currentGroupState != null && this.currentGroupState.columnId == this.columnId) {
            return this.currentGroupState.isAscending;
        }
        if (this.currentOrderState != null && this.currentOrderState.columnId == this.columnId) {
            return this.currentOrderState.isAscending;
        }
    };
    __decorate([
        core_1.ViewChild('customTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], DefaultColumnHeaderComponent.prototype, "containerRef", void 0);
    DefaultColumnHeaderComponent = __decorate([
        core_1.Component({
            selector: 'mg-default-column-header',
            templateUrl: './default-column-header.component.html'
        }),
        __metadata("design:paramtypes", [order_state_service_1.OrderStateService,
            group_service_1.GroupService])
    ], DefaultColumnHeaderComponent);
    return DefaultColumnHeaderComponent;
}(header_component_1.HeaderComponent));
exports.DefaultColumnHeaderComponent = DefaultColumnHeaderComponent;
//# sourceMappingURL=default-column-header.component.js.map