"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DefaultComparerService = (function () {
    function DefaultComparerService() {
    }
    DefaultComparerService.prototype.compare = function (firstValue, secondValue, isAscending) {
        if (firstValue == null && secondValue == null) {
            return 0;
        }
        else if (firstValue == null && secondValue != null) {
            return isAscending ? 1 : -1;
        }
        else if (firstValue != null && secondValue == null) {
            return isAscending ? -1 : 1;
        }
        else {
            if (firstValue.valueOf() == secondValue.valueOf()) {
                return 0;
            }
            var inOrder = firstValue < secondValue;
            if (isAscending) {
                return inOrder ? -1 : 1;
            }
            else {
                return inOrder ? 1 : -1;
            }
        }
    };
    DefaultComparerService = __decorate([
        core_1.Injectable()
    ], DefaultComparerService);
    return DefaultComparerService;
}());
exports.DefaultComparerService = DefaultComparerService;
//# sourceMappingURL=default-comparer.service.js.map