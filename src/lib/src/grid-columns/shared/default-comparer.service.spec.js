"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var default_comparer_service_1 = require("./default-comparer.service");
describe('Service: DefaultComparerService', function () {
    var comparer;
    beforeEach(testing_1.async(function () {
        comparer = new default_comparer_service_1.DefaultComparerService();
    }));
    it('compare with two null values should return 0', function () {
        var result = comparer.compare(null, null, true);
        expect(result).toEqual(0);
    });
    it('compare with first value null and ascending order should return 1', function () {
        var result = comparer.compare(null, 'A', true);
        expect(result).toEqual(1);
    });
    it('compare with first value null and descending order should return -1', function () {
        var result = comparer.compare(null, 'A', false);
        expect(result).toEqual(-1);
    });
    it('compare with second value null and ascending order should return -1', function () {
        var result = comparer.compare('A', null, true);
        expect(result).toEqual(-1);
    });
    it('compare with second value null and descending order should return 1', function () {
        var result = comparer.compare('A', null, false);
        expect(result).toEqual(1);
    });
    it('compare with equal values should return 0', function () {
        var result = comparer.compare('A', 'A', false);
        expect(result).toEqual(0);
    });
    it('compare with first value ordered before second value and asecending order should return -1', function () {
        var result = comparer.compare('A', 'B', true);
        expect(result).toEqual(-1);
    });
    it('compare with first value ordered before second value and desecending order should return 1', function () {
        var result = comparer.compare('A', 'B', false);
        expect(result).toEqual(1);
    });
    it('compare with second value ordered before first value and asecending order should return 1', function () {
        var result = comparer.compare('B', 'A', true);
        expect(result).toEqual(1);
    });
    it('compare with second value ordered before first value and desecending order should return -1', function () {
        var result = comparer.compare('B', 'A', false);
        expect(result).toEqual(-1);
    });
});
//# sourceMappingURL=default-comparer.service.spec.js.map