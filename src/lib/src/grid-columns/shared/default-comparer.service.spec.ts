import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { DefaultComparerService } from './default-comparer.service';

describe('Service: DefaultComparerService', () => {
  let comparer: DefaultComparerService;
 
  beforeEach(async(() => {
    comparer = new DefaultComparerService();
  }));

  it('compare with two null values should return 0', () => {
    var result = comparer.compare(null, null, true);

    expect(result).toEqual(0);
  });

  it('compare with first value null and ascending order should return 1', () => {
    var result = comparer.compare(null, 'A', true);

    expect(result).toEqual(1);
  });

  it('compare with first value null and descending order should return -1', () => {
    var result = comparer.compare(null, 'A', false);

    expect(result).toEqual(-1);
  });

  it('compare with second value null and ascending order should return -1', () => {
    var result = comparer.compare('A', null, true);

    expect(result).toEqual(-1);
  });

  it('compare with second value null and descending order should return 1', () => {
    var result = comparer.compare('A', null, false);

    expect(result).toEqual(1);
  });

  it('compare with equal values should return 0', () => {
    var result = comparer.compare('A', 'A', false);

    expect(result).toEqual(0);
  });

  it('compare with first value ordered before second value and asecending order should return -1', () => {
    var result = comparer.compare('A', 'B', true);

    expect(result).toEqual(-1);
  });

  it('compare with first value ordered before second value and desecending order should return 1', () => {
    var result = comparer.compare('A', 'B', false);

    expect(result).toEqual(1);
  });

  it('compare with second value ordered before first value and asecending order should return 1', () => {
    var result = comparer.compare('B', 'A', true);

    expect(result).toEqual(1);
  });

  it('compare with second value ordered before first value and desecending order should return -1', () => {
    var result = comparer.compare('B', 'A', false);

    expect(result).toEqual(-1);
  });
});