"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var my_grid_module_1 = require("../../my-grid.module");
var default_column_header_component_1 = require("./default-column-header.component");
var order_state_service_1 = require("../../order-state.service");
var group_service_1 = require("../../grouping/group.service");
var order_state_1 = require("../../states/order-state");
var group_state_1 = require("../../grouping/group-state");
var TestHeaderComponent = (function () {
    function TestHeaderComponent() {
    }
    __decorate([
        core_1.ViewChild('headerTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestHeaderComponent.prototype, "headerTemplate", void 0);
    TestHeaderComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-header',
            template: "\n    <ng-template #headerTemplate>\n      <div id=\"test-header-template\"></div>\n    </ng-template>\n  "
        })
    ], TestHeaderComponent);
    return TestHeaderComponent;
}());
describe('Component: DefaultColumnHeaderComponent', function () {
    var defaultColumnHeaderComponent;
    var fixture;
    var headerTemplate;
    var orderService;
    var groupService;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestHeaderComponent
            ],
            providers: [
                order_state_service_1.OrderStateService,
                group_service_1.GroupService
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(default_column_header_component_1.DefaultColumnHeaderComponent);
            defaultColumnHeaderComponent = fixture.componentInstance;
            orderService = fixture.debugElement.injector.get(order_state_service_1.OrderStateService);
            groupService = fixture.debugElement.injector.get(group_service_1.GroupService);
            var templateComponent = testing_1.TestBed.createComponent(TestHeaderComponent).componentInstance;
            headerTemplate = templateComponent.headerTemplate;
        });
    }));
    it('custom header template should be rendered when it exists', function () {
        var headerData = {
            headerTemplate: headerTemplate
        };
        defaultColumnHeaderComponent.headerData = headerData;
        fixture.detectChanges();
        var customTemplate = fixture.debugElement.query(platform_browser_1.By.css('#test-header-template'));
        expect(customTemplate).not.toBeNull();
        expect(customTemplate.context.$implicit.headerData).toEqual(headerData);
    });
    it('isOrderMarkerVisible should return true when the column is ordered', function () {
        var columnId = 'foo';
        defaultColumnHeaderComponent.columnId = columnId;
        defaultColumnHeaderComponent.headerData = {};
        fixture.detectChanges();
        orderService.raiseOrderChanged(new order_state_1.OrderState(columnId, '', true));
        fixture.whenStable().then(function () {
            var isVisible = defaultColumnHeaderComponent.isOrderMarkerVisible();
            expect(isVisible).toBeTruthy();
        });
    });
    it('isOrderMarkerVisible should return true when the column is grouped', function () {
        var columnId = 'foo';
        defaultColumnHeaderComponent.columnId = columnId;
        defaultColumnHeaderComponent.headerData = {};
        fixture.detectChanges();
        groupService.groupState.next(new group_state_1.GroupState(columnId, '', true));
        fixture.whenStable().then(function () {
            var isVisible = defaultColumnHeaderComponent.isOrderMarkerVisible();
            expect(isVisible).toBeTruthy();
        });
    });
    it('isAscending should return true when the column is ordered in asecending order', function () {
        var columnId = 'foo';
        defaultColumnHeaderComponent.columnId = columnId;
        defaultColumnHeaderComponent.headerData = {};
        var isAscendingOrder = true;
        fixture.detectChanges();
        orderService.raiseOrderChanged(new order_state_1.OrderState(columnId, '', isAscendingOrder));
        fixture.whenStable().then(function () {
            var isAscending = defaultColumnHeaderComponent.isAscending();
            expect(isAscending).toEqual(isAscendingOrder);
        });
    });
    it('isAscending should return false when the column is ordered in desecending order', function () {
        var columnId = 'foo';
        defaultColumnHeaderComponent.columnId = columnId;
        defaultColumnHeaderComponent.headerData = {};
        var isAscendingOrder = false;
        fixture.detectChanges();
        orderService.raiseOrderChanged(new order_state_1.OrderState(columnId, '', isAscendingOrder));
        fixture.whenStable().then(function () {
            var isAscending = defaultColumnHeaderComponent.isAscending();
            expect(isAscending).toEqual(isAscendingOrder);
        });
    });
    it('isAscending should return true when the column is grouped in asecending order', function () {
        var columnId = 'foo';
        defaultColumnHeaderComponent.columnId = columnId;
        defaultColumnHeaderComponent.headerData = {};
        var isAscendingOrder = true;
        fixture.detectChanges();
        groupService.groupState.next(new group_state_1.GroupState(columnId, '', isAscendingOrder));
        fixture.whenStable().then(function () {
            var isAscending = defaultColumnHeaderComponent.isAscending();
            expect(isAscending).toEqual(isAscendingOrder);
        });
    });
    it('isAscending should return false when the column is grouped in desecending order', function () {
        var columnId = 'foo';
        defaultColumnHeaderComponent.columnId = columnId;
        defaultColumnHeaderComponent.headerData = {};
        var isAscendingOrder = false;
        fixture.detectChanges();
        groupService.groupState.next(new group_state_1.GroupState(columnId, '', isAscendingOrder));
        fixture.whenStable().then(function () {
            var isAscending = defaultColumnHeaderComponent.isAscending();
            expect(isAscending).toEqual(isAscendingOrder);
        });
    });
    it('headerClicked should create new group state with opposite order when the column is grouped', function () {
        var columnId = 'foo';
        defaultColumnHeaderComponent.columnId = columnId;
        defaultColumnHeaderComponent.headerData = { enableGrouping: true };
        var initialOrder = false;
        var groupStateSpy = spyOn(groupService.groupState, 'next').and.callThrough();
        fixture.detectChanges();
        groupService.groupState.next(new group_state_1.GroupState(columnId, '', initialOrder));
        fixture.whenStable().then(function () {
            groupStateSpy.calls.reset();
            defaultColumnHeaderComponent.headerClicked();
            expect(groupStateSpy).toHaveBeenCalled();
            var newGroupState = groupStateSpy.calls.first().args[0];
            expect(newGroupState.isAscending).toEqual(!initialOrder);
        });
    });
    it('headerClicked should create new order state with opposite order when the column is ordered', function () {
        var columnId = 'foo';
        defaultColumnHeaderComponent.columnId = columnId;
        defaultColumnHeaderComponent.headerData = { enableOrdering: true };
        var initialOrder = false;
        var orderStateSpy = spyOn(orderService.orderState, 'next').and.callThrough();
        fixture.detectChanges();
        orderService.raiseOrderChanged(new order_state_1.OrderState(columnId, '', initialOrder));
        fixture.whenStable().then(function () {
            orderStateSpy.calls.reset();
            defaultColumnHeaderComponent.headerClicked();
            expect(orderStateSpy).toHaveBeenCalled();
            var newOrderState = orderStateSpy.calls.first().args[0];
            expect(newOrderState.isAscending).toEqual(!initialOrder);
        });
    });
    it('headerClicked should not create new order state when ordering for column is disabled', function () {
        var columnId = 'foo';
        defaultColumnHeaderComponent.columnId = columnId;
        var headerData = {
            enableOrdering: false
        };
        defaultColumnHeaderComponent.headerData = headerData;
        var orderStateSpy = spyOn(orderService.orderState, 'next').and.callThrough();
        fixture.detectChanges();
        defaultColumnHeaderComponent.headerClicked();
        expect(orderStateSpy).not.toHaveBeenCalled();
    });
});
//# sourceMappingURL=default-column-header.component.spec.js.map