import { 
  Component, 
  OnInit,
  OnDestroy,
  AfterContentInit,
  TemplateRef,
  ViewContainerRef,
  ViewChild,
  Input 
} from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import { HeaderComponent } from './../contracts/header-component';
import { OrderState } from '../../states/order-state';
import { OrderStateService } from '../../order-state.service';
import { GroupService } from '../../grouping/group.service';
import { GroupState } from '../../grouping/group-state';

@Component({
  selector: 'mg-default-column-header',
  templateUrl: './default-column-header.component.html'
})
export class DefaultColumnHeaderComponent extends HeaderComponent implements OnInit, OnDestroy {
  private currentGroupState: GroupState;
  private orderStateSubscription: Subscription;
  private groupStateSubscription: Subscription;
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;
  currentOrderState: OrderState;

  constructor(
    private orderStateService: OrderStateService,
    private groupService: GroupService) {
    super();
    
    this.orderStateSubscription = this.orderStateService.orderState.subscribe((newState) => {
      this.currentOrderState = newState;
    });

    this.groupStateSubscription = this.groupService.groupState.subscribe((newState) => {
      this.currentGroupState = newState;
    });
  }

  ngOnInit() {
    if (this.headerData.headerTemplate != null) {
      this.containerRef.createEmbeddedView(
        this.headerData.headerTemplate, 
        { $implicit: { headerData: this.headerData } });
    }
  }

  ngOnDestroy() {
    this.orderStateSubscription.unsubscribe();
    this.groupStateSubscription.unsubscribe();
  }

  headerClicked() {
    if (this.headerData.enableGrouping 
      && this.currentGroupState != null 
      && this.currentGroupState.columnId == this.columnId) {

      this.groupService.groupState.next(new GroupState(
        this.columnId, 
        this.headerData.propertyName,
        !this.currentGroupState.isAscending));

      return;
    }

    if (!this.headerData.enableOrdering) {
      return;
    }

    let isAscending = true;
    if (this.currentOrderState != null 
      && this.currentOrderState.columnId == this.columnId
      && this.currentOrderState.isAscending) {
      isAscending = false;
    }

    this.orderStateService.raiseOrderChanged(new OrderState(this.columnId, this.headerData.propertyName, isAscending));
  }

  isOrderMarkerVisible() {
    let isColumnOrdered = this.currentOrderState != null && this.currentOrderState.columnId == this.columnId;
    let isColumnGrouped = this.currentGroupState != null && this.currentGroupState.columnId == this.columnId;

    return isColumnOrdered || isColumnGrouped;
  }

  isAscending() {
    if (this.currentGroupState != null && this.currentGroupState.columnId == this.columnId) {
      return this.currentGroupState.isAscending;
    }
    
    if (this.currentOrderState != null && this.currentOrderState.columnId == this.columnId) {
      return this.currentOrderState.isAscending;
    }
  }
}