import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { DefaultColumnHeaderComponent } from './default-column-header.component';
import { OrderStateService } from '../../order-state.service';
import { GroupService } from '../../grouping/group.service';
import { OrderState } from '../../states/order-state';
import { GroupState } from '../../grouping/group-state';

@Component({
  selector: 'mgt-test-header',
  template: `
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
  `
})
class TestHeaderComponent {
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
}

describe('Component: DefaultColumnHeaderComponent', () => {
  let defaultColumnHeaderComponent: DefaultColumnHeaderComponent;
  let fixture: ComponentFixture<DefaultColumnHeaderComponent>;
  let headerTemplate: TemplateRef<any>;
  let orderService: OrderStateService;
  let groupService: GroupService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestHeaderComponent
        ],
        providers: [
          OrderStateService,
          GroupService
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(DefaultColumnHeaderComponent);
        defaultColumnHeaderComponent = fixture.componentInstance;

        orderService = fixture.debugElement.injector.get(OrderStateService);
        groupService = fixture.debugElement.injector.get(GroupService);

        var templateComponent: TestHeaderComponent = TestBed.createComponent(TestHeaderComponent).componentInstance;
        headerTemplate = templateComponent.headerTemplate;
      });
  }));

  it('custom header template should be rendered when it exists', () => {
    var headerData = {
      headerTemplate: headerTemplate
    };
    defaultColumnHeaderComponent.headerData = headerData;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-header-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.headerData).toEqual(headerData);
  });

  it('isOrderMarkerVisible should return true when the column is ordered', () => {
    var columnId = 'foo';
    defaultColumnHeaderComponent.columnId = columnId;
    defaultColumnHeaderComponent.headerData = {};

    fixture.detectChanges();
    orderService.raiseOrderChanged(new OrderState(columnId, '', true));

    fixture.whenStable().then(() => {
      var isVisible = defaultColumnHeaderComponent.isOrderMarkerVisible();
      expect(isVisible).toBeTruthy();
    });
  });

  it('isOrderMarkerVisible should return true when the column is grouped', () => {
    var columnId = 'foo';
    defaultColumnHeaderComponent.columnId = columnId;
    defaultColumnHeaderComponent.headerData = {};

    fixture.detectChanges();
    groupService.groupState.next(new GroupState(columnId, '', true));

    fixture.whenStable().then(() => {
      var isVisible = defaultColumnHeaderComponent.isOrderMarkerVisible();
      expect(isVisible).toBeTruthy();
    });
  });

  it('isAscending should return true when the column is ordered in asecending order', () => {
    var columnId = 'foo';
    defaultColumnHeaderComponent.columnId = columnId;
    defaultColumnHeaderComponent.headerData = {};
    var isAscendingOrder = true;

    fixture.detectChanges();
    orderService.raiseOrderChanged(new OrderState(columnId, '', isAscendingOrder));

    fixture.whenStable().then(() => {
      var isAscending = defaultColumnHeaderComponent.isAscending();
      expect(isAscending).toEqual(isAscendingOrder);
    });
  });

  it('isAscending should return false when the column is ordered in desecending order', () => {
    var columnId = 'foo';
    defaultColumnHeaderComponent.columnId = columnId;
    defaultColumnHeaderComponent.headerData = {};
    var isAscendingOrder = false;

    fixture.detectChanges();
    orderService.raiseOrderChanged(new OrderState(columnId, '', isAscendingOrder));

    fixture.whenStable().then(() => {
      var isAscending = defaultColumnHeaderComponent.isAscending();
      expect(isAscending).toEqual(isAscendingOrder);
    });
  });

  it('isAscending should return true when the column is grouped in asecending order', () => {
    var columnId = 'foo';
    defaultColumnHeaderComponent.columnId = columnId;
    defaultColumnHeaderComponent.headerData = {};
    var isAscendingOrder = true;

    fixture.detectChanges();
    groupService.groupState.next(new GroupState(columnId, '', isAscendingOrder));

    fixture.whenStable().then(() => {
      var isAscending = defaultColumnHeaderComponent.isAscending();
      expect(isAscending).toEqual(isAscendingOrder);
    });
  });

  it('isAscending should return false when the column is grouped in desecending order', () => {
    var columnId = 'foo';
    defaultColumnHeaderComponent.columnId = columnId;
    defaultColumnHeaderComponent.headerData = {};
    var isAscendingOrder = false;

    fixture.detectChanges();
    groupService.groupState.next(new GroupState(columnId, '', isAscendingOrder));

    fixture.whenStable().then(() => {
      var isAscending = defaultColumnHeaderComponent.isAscending();
      expect(isAscending).toEqual(isAscendingOrder);
    });
  });

  it('headerClicked should create new group state with opposite order when the column is grouped', () => {
    var columnId = 'foo';
    defaultColumnHeaderComponent.columnId = columnId;
    defaultColumnHeaderComponent.headerData = { enableGrouping: true };
    var initialOrder = false;
    var groupStateSpy = spyOn(groupService.groupState, 'next').and.callThrough();

    fixture.detectChanges();
    groupService.groupState.next(new GroupState(columnId, '', initialOrder));

    fixture.whenStable().then(() => {
      groupStateSpy.calls.reset();
      defaultColumnHeaderComponent.headerClicked();

      expect(groupStateSpy).toHaveBeenCalled();
      var newGroupState: GroupState = groupStateSpy.calls.first().args[0];
      expect(newGroupState.isAscending).toEqual(!initialOrder);
    });
  });

  it('headerClicked should create new order state with opposite order when the column is ordered', () => {
    var columnId = 'foo';
    defaultColumnHeaderComponent.columnId = columnId;
    defaultColumnHeaderComponent.headerData = { enableOrdering: true };
    var initialOrder = false;
    var orderStateSpy = spyOn(orderService.orderState, 'next').and.callThrough();

    fixture.detectChanges();
    orderService.raiseOrderChanged(new OrderState(columnId, '', initialOrder));

    fixture.whenStable().then(() => {
      orderStateSpy.calls.reset();
      defaultColumnHeaderComponent.headerClicked();

      expect(orderStateSpy).toHaveBeenCalled();
      var newOrderState: OrderState = orderStateSpy.calls.first().args[0];
      expect(newOrderState.isAscending).toEqual(!initialOrder);
    });
  });

  it('headerClicked should not create new order state when ordering for column is disabled', () => {
    var columnId = 'foo';
    defaultColumnHeaderComponent.columnId = columnId;
    var headerData = {
      enableOrdering: false
    };
    defaultColumnHeaderComponent.headerData = headerData;
    var orderStateSpy = spyOn(orderService.orderState, 'next').and.callThrough();

    fixture.detectChanges();
    defaultColumnHeaderComponent.headerClicked();
    
    expect(orderStateSpy).not.toHaveBeenCalled();
  });
});