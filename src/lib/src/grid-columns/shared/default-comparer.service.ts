import { Injectable } from '@angular/core';

@Injectable()
export class DefaultComparerService {
  public compare(firstValue: any, secondValue: any, isAscending: boolean) {
    if (firstValue == null && secondValue == null) {
      return 0;
    } else if (firstValue == null && secondValue != null) {
      return isAscending ? 1 : -1;
    } else if (firstValue != null && secondValue == null) {
      return isAscending ? -1 : 1;
    } else {
      if (firstValue.valueOf() == secondValue.valueOf()) {
        return 0;
      }

      let inOrder = firstValue < secondValue;
      if (isAscending) {
        return inOrder ? -1 : 1;
      } else {
        return inOrder ? 1 : -1;
      }
    } 
  }
}