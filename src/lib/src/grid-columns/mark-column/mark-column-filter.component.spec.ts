import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { MarkColumnFilterComponent } from './mark-column-filter.component';
import { FilterStateService } from '../../filter-state.service';
import { FilterState } from '../../states/filter-state';

@Component({
  selector: 'mgt-test-filter',
  template: `
    <ng-template #filterTemplate>
      <div id="test-filter-template"></div>
    </ng-template>
  `
})
class TestFilterComponent {
  @ViewChild('filterTemplate') filterTemplate: TemplateRef<any>;
}

describe('Component: MarkColumnFilterComponent', () => {
  let markColumnFilterComponent: MarkColumnFilterComponent;
  let fixture: ComponentFixture<MarkColumnFilterComponent>;
  let filterTemplate: TemplateRef<any>;
  let filterStateService: FilterStateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        providers: [
          FilterStateService
        ],
        declarations: [
          TestFilterComponent
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(MarkColumnFilterComponent);
        markColumnFilterComponent = fixture.componentInstance;

        var templateComponent: TestFilterComponent = TestBed.createComponent(TestFilterComponent).componentInstance;
        filterTemplate = templateComponent.filterTemplate;

        filterStateService = fixture.debugElement.injector.get(FilterStateService);
      });
  }));
  
  it('custom filter template should be rendered when it exists', () => {
    var data = {
      filterTemplate: filterTemplate,
      enableFiltering: true
    };
    markColumnFilterComponent.filterData = data;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-filter-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.filterData).toEqual(data);
    expect(customTemplate.context.$implicit.columnId).toEqual(markColumnFilterComponent.columnId);
    expect(customTemplate.context.$implicit.filter).toEqual(markColumnFilterComponent.customTemplateFilter);
  });

  it('changing filter state should update checkBoxValue', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    markColumnFilterComponent.filterData = data;
    var columnId = '123';
    markColumnFilterComponent.columnId = columnId;
    var isMarked = true;    

    fixture.detectChanges();
    filterStateService.raiseFilterChanged(columnId, propertyName, { isMarked: isMarked }, '');

    fixture.whenStable().then(() => {
      expect(markColumnFilterComponent.checkBoxValue).toEqual(isMarked);
    });
  });

  it('customTemplateFilter should create new filter state', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    markColumnFilterComponent.filterData = data;
    var columnId = '123';
    markColumnFilterComponent.columnId = columnId;
    var templateFilterData = {};
    var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    markColumnFilterComponent.customTemplateFilter(templateFilterData);

    expect(raiseFilterChangeSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangeSpy.calls.first();
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2]).toEqual(templateFilterData);
  });

  it('filterChanged should clear filter for this column when the chech box value is null', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    markColumnFilterComponent.filterData = data;
    var columnId = '123';
    markColumnFilterComponent.columnId = columnId;
    var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');

    fixture.detectChanges();
    markColumnFilterComponent.filterChanged();

    expect(clearColumnFilterSpy).toHaveBeenCalled();
    expect(clearColumnFilterSpy.calls.first().args[0]).toEqual(markColumnFilterComponent.columnId);
  });

  it('filterChanged should create new filter state when the chech box value is not null', () => {
    var propertyName = 'foo';
    var searchTermType = 3;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    markColumnFilterComponent.filterData = data;
    var columnId = '123';
    markColumnFilterComponent.columnId = columnId;
    var checkBoxValue = true;
    markColumnFilterComponent.checkBoxValue = checkBoxValue;
    var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    markColumnFilterComponent.filterChanged();

    expect(raiseFilterChangedSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangedSpy.calls.first()
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2].isMarked).toEqual(checkBoxValue);
  });
});