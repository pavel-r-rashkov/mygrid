"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var mark_column_directive_1 = require("./mark-column.directive");
var guid_1 = require("../../guid");
var default_comparer_service_1 = require("../shared/default-comparer.service");
var TestMarkColumnComponent = (function () {
    function TestMarkColumnComponent() {
    }
    __decorate([
        core_1.ViewChild('dataTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestMarkColumnComponent.prototype, "dataTemplate", void 0);
    __decorate([
        core_1.ViewChild('headerTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestMarkColumnComponent.prototype, "headerTemplate", void 0);
    __decorate([
        core_1.ViewChild('editTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestMarkColumnComponent.prototype, "editTemplate", void 0);
    __decorate([
        core_1.ViewChild('validationErrorsTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestMarkColumnComponent.prototype, "validationErrorsTemplate", void 0);
    __decorate([
        core_1.ViewChild('filterTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestMarkColumnComponent.prototype, "filterTemplate", void 0);
    __decorate([
        core_1.ViewChild('groupHeaderTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestMarkColumnComponent.prototype, "groupHeaderTemplate", void 0);
    TestMarkColumnComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-mark-column',
            template: "\n    <ng-template #dataTemplate>\n      <div id=\"test-data-template\"></div>\n    </ng-template>\n    <ng-template #headerTemplate>\n      <div id=\"test-header-template\"></div>\n    </ng-template>\n    <ng-template #editTemplate>\n      <div id=\"test-edit-template\"></div>\n    </ng-template>\n    <ng-template #validationErrorsTemplate>\n      <div id=\"test-validation-error-template\"></div>\n    </ng-template>\n    <ng-template #filterTemplate>\n      <div id=\"test-filter-template\"></div>\n    </ng-template>\n    <ng-template #groupHeaderTemplate>\n      <div id=\"test-group-header-template\"></div>\n    </ng-template>\n  "
        })
    ], TestMarkColumnComponent);
    return TestMarkColumnComponent;
}());
describe('Directive: MarkColumnDirective', function () {
    var markColumnDirective;
    var comparer;
    var dataTemplate;
    var headerTemplate;
    var editTemplate;
    var validationErrorsTemplate;
    var filterTemplate;
    var groupHeaderTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestMarkColumnComponent
            ]
        })
            .compileComponents()
            .then(function () {
            comparer = new default_comparer_service_1.DefaultComparerService();
            markColumnDirective = new mark_column_directive_1.MarkColumnDirective(new guid_1.GuidService(), comparer);
            var testComponent = testing_1.TestBed.createComponent(TestMarkColumnComponent).componentInstance;
            dataTemplate = testComponent.dataTemplate;
            headerTemplate = testComponent.headerTemplate;
            editTemplate = testComponent.editTemplate;
            validationErrorsTemplate = testComponent.validationErrorsTemplate;
            filterTemplate = testComponent.filterTemplate;
            groupHeaderTemplate = testComponent.groupHeaderTemplate;
        });
    }));
    it('getHeaderData should return mark column header data', function () {
        var caption = 'foo';
        markColumnDirective.caption = caption;
        markColumnDirective.headerTemplate = headerTemplate;
        var propertyName = 'bar';
        markColumnDirective.propertyName = propertyName;
        var orderingEnabled = false;
        markColumnDirective.enableOrdering = orderingEnabled;
        var groupingEnabled = false;
        markColumnDirective.enableGrouping = groupingEnabled;
        var headerData = markColumnDirective.getHeaderData();
        expect(headerData.caption).toEqual(caption);
        expect(headerData.headerTemplate).toEqual(headerTemplate);
        expect(headerData.propertyName).toEqual(propertyName);
        expect(headerData.enableOrdering).toEqual(orderingEnabled);
        expect(headerData.enableGrouping).toEqual(groupingEnabled);
    });
    it('ordering and grouping should be enabled by default', function () {
        var headerData = markColumnDirective.getHeaderData();
        expect(headerData.enableOrdering).toBeTruthy();
        expect(headerData.enableGrouping).toBeTruthy();
    });
    it('getData should return mark column data', function () {
        markColumnDirective.dataTemplate = dataTemplate;
        markColumnDirective.editTemplate = editTemplate;
        markColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
        var propertyName = 'bar';
        markColumnDirective.propertyName = propertyName;
        var editionEnabled = false;
        markColumnDirective.enableEditing = editionEnabled;
        var data = markColumnDirective.getData();
        expect(data.dataTemplate).toEqual(dataTemplate);
        expect(data.editTemplate).toEqual(editTemplate);
        expect(data.validationErrorsTemplate).toEqual(validationErrorsTemplate);
        expect(data.propertyName).toEqual(propertyName);
        expect(data.enableEditing).toEqual(editionEnabled);
    });
    it('editing should be enabled by default', function () {
        var data = markColumnDirective.getData();
        expect(data.enableEditing).toBeTruthy();
    });
    it('getFilterData should return mark column filter data', function () {
        markColumnDirective.filterTemplate = filterTemplate;
        var propertyName = 'bar';
        markColumnDirective.propertyName = propertyName;
        var filteringEnabled = false;
        markColumnDirective.enableFiltering = filteringEnabled;
        var filterData = markColumnDirective.getFilterData();
        expect(filterData.filterTemplate).toEqual(filterTemplate);
        expect(filterData.propertyName).toEqual(propertyName);
        expect(filterData.enableFiltering).toEqual(filteringEnabled);
    });
    it('filtering should be enabled by default', function () {
        var filterData = markColumnDirective.getFilterData();
        expect(filterData.enableFiltering).toBeTruthy();
    });
    it('getGroupData should return mark column group data', function () {
        markColumnDirective.groupHeaderTemplate = groupHeaderTemplate;
        markColumnDirective.editTemplate = editTemplate;
        markColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
        var propertyName = 'bar';
        markColumnDirective.propertyName = propertyName;
        var editionEnabled = false;
        markColumnDirective.enableEditing = editionEnabled;
        var groupData = markColumnDirective.getGroupData();
        expect(groupData.dataTemplate).toEqual(groupHeaderTemplate);
        expect(groupData.editTemplate).toEqual(editTemplate);
        expect(groupData.validationErrorsTemplate).toEqual(validationErrorsTemplate);
        expect(groupData.propertyName).toEqual(propertyName);
        expect(groupData.enableEditing).toEqual(editionEnabled);
    });
    it('getValue should return null when the column is not bound to a property', function () {
        var item = { foo: true };
        var columnValue = markColumnDirective.getValue(item);
        expect(columnValue).toEqual(null);
    });
    it('getValue should return the value of the property that the column is bound to', function () {
        var item = { foo: true };
        var propertyName = 'foo';
        markColumnDirective.propertyName = propertyName;
        var columnValue = markColumnDirective.getValue(item);
        expect(columnValue).toEqual(item[propertyName]);
    });
    it('order should call customOrder callback if it is provided', function () {
        markColumnDirective.customOrder = function (firstValue, secondValue, isAscending, firstItem, secondItem) { return 0; };
        var orderResult = 1;
        var orderSpy = spyOn(markColumnDirective, 'customOrder').and.returnValue(orderResult);
        var result = markColumnDirective.order(true, false, true, {}, {});
        expect(result).toEqual(orderResult);
    });
    it('order should call default comparer', function () {
        var firstValue = false;
        var secondValue = false;
        var isAsecneding = true;
        var comparerSpy = spyOn(comparer, 'compare');
        var result = markColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});
        expect(comparerSpy).toHaveBeenCalled();
        var compareArgs = comparerSpy.calls.first().args;
        expect(compareArgs[0]).toEqual(firstValue);
        expect(compareArgs[1]).toEqual(secondValue);
        expect(compareArgs[2]).toEqual(isAsecneding);
    });
    it('group should call customGroup callback if it is provided', function () {
        markColumnDirective.customGroup = function (firstValue, secondValue, isAscending, firstItem, secondItem) { return 0; };
        var groupResult = 1;
        var orderSpy = spyOn(markColumnDirective, 'customGroup').and.returnValue(groupResult);
        var result = markColumnDirective.group(true, false, true, {}, {});
        expect(result).toEqual(groupResult);
    });
    it('group should call default comparer', function () {
        var firstValue = true;
        var secondValue = false;
        var isAsecneding = true;
        var comparerSpy = spyOn(comparer, 'compare');
        var result = markColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});
        expect(comparerSpy).toHaveBeenCalled();
        var compareArgs = comparerSpy.calls.first().args;
        expect(compareArgs[0]).toEqual(firstValue);
        expect(compareArgs[1]).toEqual(secondValue);
        expect(compareArgs[2]).toEqual(isAsecneding);
    });
    it('filter should call customFilter callback if it is provided', function () {
        markColumnDirective.customFilter = function (val, filterData, item) { return false; };
        var filterResult = true;
        var filterSpy = spyOn(markColumnDirective, 'customFilter').and.returnValue(filterResult);
        var value = true;
        var filterData = {};
        var item = {};
        var result = markColumnDirective.filter(value, filterData, item);
        expect(filterSpy).toHaveBeenCalled();
        var filterArgs = filterSpy.calls.first().args;
        expect(filterArgs[0]).toEqual(value);
        expect(filterArgs[1]).toEqual(filterData);
        expect(filterArgs[2]).toEqual(item);
    });
    it('filter with no filter data should return true', function () {
        var result = markColumnDirective.filter(true, null, {});
        expect(result).toBeTruthy();
    });
    it('filter with no isMarked should return true', function () {
        var result = markColumnDirective.filter(false, {}, {});
        expect(result).toBeTruthy();
    });
    it('filter with no item value should return false', function () {
        var result = markColumnDirective.filter(null, { isMarked: false }, {});
        expect(result).toBeFalsy();
    });
    it('filter with isMarked equal to column value should return true', function () {
        var result = markColumnDirective.filter(false, { isMarked: false }, {});
        expect(result).toBeTruthy();
    });
    it('filter with isMarked not equal to column value should return false', function () {
        var result = markColumnDirective.filter(false, { isMarked: true }, {});
        expect(result).toBeFalsy();
    });
});
//# sourceMappingURL=mark-column.directive.spec.js.map