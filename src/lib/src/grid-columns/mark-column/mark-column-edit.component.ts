import { 
  Component, 
  OnInit,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { EditComponent } from '../contracts/edit-component';

@Component({  
  selector: 'mg-mark-column-edit',
  templateUrl: './mark-column-edit.component.html'
})
export class MarkColumnEditComponent extends EditComponent implements OnInit {
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;

  constructor() {
    super();
  }
  
  ngOnInit() {
    if (this.data.editTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item,
        editForm: this.editForm
      };
      this.containerRef.createEmbeddedView(this.data.editTemplate, { $implicit: templateContext });
    }
  }
}