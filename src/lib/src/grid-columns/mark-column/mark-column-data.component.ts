import { 
  Component, 
  OnInit,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { DataComponent } from '../contracts/data-component';

@Component({
  selector: 'mg-mark-column-data',
  templateUrl: './mark-column-data.component.html'
})
export class MarkColumnDataComponent extends DataComponent implements OnInit {
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;

  constructor() {
    super();
  }
  
  ngOnInit() {
    if (this.data.dataTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item
      };
      this.containerRef.createEmbeddedView(this.data.dataTemplate, { $implicit: templateContext });
    }
  }
}