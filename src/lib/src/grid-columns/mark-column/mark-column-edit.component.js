"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var edit_component_1 = require("../contracts/edit-component");
var MarkColumnEditComponent = (function (_super) {
    __extends(MarkColumnEditComponent, _super);
    function MarkColumnEditComponent() {
        return _super.call(this) || this;
    }
    MarkColumnEditComponent.prototype.ngOnInit = function () {
        if (this.data.editTemplate != null) {
            var templateContext = {
                data: this.data,
                item: this.item,
                editForm: this.editForm
            };
            this.containerRef.createEmbeddedView(this.data.editTemplate, { $implicit: templateContext });
        }
    };
    __decorate([
        core_1.ViewChild('customTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], MarkColumnEditComponent.prototype, "containerRef", void 0);
    MarkColumnEditComponent = __decorate([
        core_1.Component({
            selector: 'mg-mark-column-edit',
            templateUrl: './mark-column-edit.component.html'
        }),
        __metadata("design:paramtypes", [])
    ], MarkColumnEditComponent);
    return MarkColumnEditComponent;
}(edit_component_1.EditComponent));
exports.MarkColumnEditComponent = MarkColumnEditComponent;
//# sourceMappingURL=mark-column-edit.component.js.map