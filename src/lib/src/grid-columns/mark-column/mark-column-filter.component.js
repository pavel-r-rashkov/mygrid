"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var filter_component_1 = require("../contracts/filter-component");
var filter_state_service_1 = require("../../filter-state.service");
var MarkColumnFilterComponent = (function (_super) {
    __extends(MarkColumnFilterComponent, _super);
    function MarkColumnFilterComponent(filterStateService) {
        var _this = _super.call(this) || this;
        _this.filterStateService = filterStateService;
        _this.filterKey = 'Mark';
        return _this;
    }
    MarkColumnFilterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.filterStateSubscription = this.filterStateService.filterState.subscribe(function (newState) {
            var markColumnFilter = newState.columnFilters[_this.columnId];
            _this.checkBoxValue = markColumnFilter == null ? null : markColumnFilter.filterData.isMarked;
        });
    };
    MarkColumnFilterComponent.prototype.ngAfterViewInit = function () {
        if (this.filterData.filterTemplate != null) {
            var templateContext = {
                columnId: this.columnId,
                filterData: this.filterData,
                filter: this.customTemplateFilter
            };
            this.containerRef.createEmbeddedView(this.filterData.filterTemplate, { $implicit: templateContext });
        }
    };
    MarkColumnFilterComponent.prototype.ngOnDestroy = function () {
        this.filterStateSubscription.unsubscribe();
    };
    MarkColumnFilterComponent.prototype.filterChanged = function () {
        if (this.checkBoxValue == null) {
            this.filterStateService.clearColumnFilter(this.columnId);
        }
        else {
            this.filterStateService.raiseFilterChanged(this.columnId, this.filterData.propertyName, { isMarked: this.checkBoxValue }, this.filterKey);
        }
    };
    MarkColumnFilterComponent.prototype.customTemplateFilter = function (filterData) {
        this.filterStateService.raiseFilterChanged(this.columnId, this.filterData.propertyName, filterData, this.filterKey);
    };
    __decorate([
        core_1.ViewChild('customTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], MarkColumnFilterComponent.prototype, "containerRef", void 0);
    MarkColumnFilterComponent = __decorate([
        core_1.Component({
            selector: 'mg-mark-column-filter',
            templateUrl: './mark-column-filter.component.html'
        }),
        __metadata("design:paramtypes", [filter_state_service_1.FilterStateService])
    ], MarkColumnFilterComponent);
    return MarkColumnFilterComponent;
}(filter_component_1.FilterComponent));
exports.MarkColumnFilterComponent = MarkColumnFilterComponent;
//# sourceMappingURL=mark-column-filter.component.js.map