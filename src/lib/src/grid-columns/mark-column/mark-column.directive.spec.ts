import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { MarkColumnDirective } from './mark-column.directive';
import { GuidService } from '../../guid';
import { DefaultComparerService } from '../shared/default-comparer.service';

@Component({
  selector: 'mgt-test-mark-column',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
    <ng-template #editTemplate>
      <div id="test-edit-template"></div>
    </ng-template>
    <ng-template #validationErrorsTemplate>
      <div id="test-validation-error-template"></div>
    </ng-template>
    <ng-template #filterTemplate>
      <div id="test-filter-template"></div>
    </ng-template>
    <ng-template #groupHeaderTemplate>
      <div id="test-group-header-template"></div>
    </ng-template>
  `
}) 
class TestMarkColumnComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
  @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
  @ViewChild('validationErrorsTemplate') validationErrorsTemplate: TemplateRef<any>;
  @ViewChild('filterTemplate') filterTemplate: TemplateRef<any>;
  @ViewChild('groupHeaderTemplate') groupHeaderTemplate: TemplateRef<any>;
}

describe('Directive: MarkColumnDirective', () => {
  let markColumnDirective: MarkColumnDirective;
  let comparer: DefaultComparerService;
  let dataTemplate: TemplateRef<any>;
  let headerTemplate: TemplateRef<any>;
  let editTemplate: TemplateRef<any>;
  let validationErrorsTemplate: TemplateRef<any>;
  let filterTemplate: TemplateRef<any>;
  let groupHeaderTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestMarkColumnComponent
        ]
      })
      .compileComponents()
      .then(() => {
        comparer = new DefaultComparerService();
        markColumnDirective = new MarkColumnDirective(new GuidService(), comparer);

        var testComponent = TestBed.createComponent(TestMarkColumnComponent).componentInstance;
        dataTemplate = testComponent.dataTemplate;
        headerTemplate = testComponent.headerTemplate;
        editTemplate = testComponent.editTemplate;
        validationErrorsTemplate = testComponent.validationErrorsTemplate;
        filterTemplate = testComponent.filterTemplate;
        groupHeaderTemplate = testComponent.groupHeaderTemplate;
      });
  }));

  it('getHeaderData should return mark column header data', () => {
    var caption = 'foo';
    markColumnDirective.caption = caption;
    markColumnDirective.headerTemplate = headerTemplate;
    var propertyName = 'bar';
    markColumnDirective.propertyName = propertyName;
    var orderingEnabled = false;
    markColumnDirective.enableOrdering = orderingEnabled;
    var groupingEnabled = false;
    markColumnDirective.enableGrouping = groupingEnabled;

    var headerData: any = markColumnDirective.getHeaderData();

    expect(headerData.caption).toEqual(caption);
    expect(headerData.headerTemplate).toEqual(headerTemplate);
    expect(headerData.propertyName).toEqual(propertyName);
    expect(headerData.enableOrdering).toEqual(orderingEnabled);
    expect(headerData.enableGrouping).toEqual(groupingEnabled);
  });

  it('ordering and grouping should be enabled by default', () => {
    var headerData: any = markColumnDirective.getHeaderData();

    expect(headerData.enableOrdering).toBeTruthy();
    expect(headerData.enableGrouping).toBeTruthy();
  });

  it('getData should return mark column data', () => {
    markColumnDirective.dataTemplate = dataTemplate;
    markColumnDirective.editTemplate = editTemplate;
    markColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
    var propertyName = 'bar';
    markColumnDirective.propertyName = propertyName;
    var editionEnabled = false;
    markColumnDirective.enableEditing = editionEnabled;

    var data: any = markColumnDirective.getData();

    expect(data.dataTemplate).toEqual(dataTemplate);
    expect(data.editTemplate).toEqual(editTemplate);
    expect(data.validationErrorsTemplate).toEqual(validationErrorsTemplate);
    expect(data.propertyName).toEqual(propertyName);
    expect(data.enableEditing).toEqual(editionEnabled);
  });

  it('editing should be enabled by default', () => {
    var data: any = markColumnDirective.getData();

    expect(data.enableEditing).toBeTruthy();
  });

  it('getFilterData should return mark column filter data', () => {
    markColumnDirective.filterTemplate = filterTemplate;
    var propertyName = 'bar';
    markColumnDirective.propertyName = propertyName;
    var filteringEnabled = false;
    markColumnDirective.enableFiltering = filteringEnabled;

    var filterData: any = markColumnDirective.getFilterData();

    expect(filterData.filterTemplate).toEqual(filterTemplate);
    expect(filterData.propertyName).toEqual(propertyName);
    expect(filterData.enableFiltering).toEqual(filteringEnabled);
  });

  it('filtering should be enabled by default', () => {
    var filterData: any = markColumnDirective.getFilterData();

    expect(filterData.enableFiltering).toBeTruthy();
  });

  it('getGroupData should return mark column group data', () => {
    markColumnDirective.groupHeaderTemplate = groupHeaderTemplate;
    markColumnDirective.editTemplate = editTemplate;
    markColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
    var propertyName = 'bar';
    markColumnDirective.propertyName = propertyName;
    var editionEnabled = false;
    markColumnDirective.enableEditing = editionEnabled;

    var groupData: any = markColumnDirective.getGroupData();

    expect(groupData.dataTemplate).toEqual(groupHeaderTemplate);
    expect(groupData.editTemplate).toEqual(editTemplate);
    expect(groupData.validationErrorsTemplate).toEqual(validationErrorsTemplate);
    expect(groupData.propertyName).toEqual(propertyName);
    expect(groupData.enableEditing).toEqual(editionEnabled);
  });

  it('getValue should return null when the column is not bound to a property', () => {
    var item = { foo: true };

    var columnValue = markColumnDirective.getValue(item);

    expect(columnValue).toEqual(null);
  });

  it('getValue should return the value of the property that the column is bound to', () => {
    var item = { foo: true };
    var propertyName = 'foo';
    markColumnDirective.propertyName = propertyName;

    var columnValue = markColumnDirective.getValue(item);

    expect(columnValue).toEqual(item[propertyName]);
  });

  it('order should call customOrder callback if it is provided', () => {
    markColumnDirective.customOrder = (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any): number => { return 0; };
    var orderResult = 1;
    var orderSpy: jasmine.Spy = spyOn(markColumnDirective, 'customOrder').and.returnValue(orderResult);

    var result = markColumnDirective.order(true, false, true, {}, {});

    expect(result).toEqual(orderResult);
  });

  it('order should call default comparer', () => {
    var firstValue = false;
    var secondValue = false;
    var isAsecneding = true;
    var comparerSpy: jasmine.Spy = spyOn(comparer, 'compare');

    var result = markColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(comparerSpy).toHaveBeenCalled();
    var compareArgs = comparerSpy.calls.first().args;
    expect(compareArgs[0]).toEqual(firstValue);
    expect(compareArgs[1]).toEqual(secondValue);
    expect(compareArgs[2]).toEqual(isAsecneding);
  });

  it('group should call customGroup callback if it is provided', () => {
    markColumnDirective.customGroup = (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any): number => { return 0; };
    var groupResult = 1;
    var orderSpy: jasmine.Spy = spyOn(markColumnDirective, 'customGroup').and.returnValue(groupResult);

    var result = markColumnDirective.group(true, false, true, {}, {});

    expect(result).toEqual(groupResult);
  });

  it('group should call default comparer', () => {
    var firstValue = true;
    var secondValue = false;
    var isAsecneding = true;
    var comparerSpy: jasmine.Spy = spyOn(comparer, 'compare');

    var result = markColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(comparerSpy).toHaveBeenCalled();
    var compareArgs = comparerSpy.calls.first().args;
    expect(compareArgs[0]).toEqual(firstValue);
    expect(compareArgs[1]).toEqual(secondValue);
    expect(compareArgs[2]).toEqual(isAsecneding);
  });

  it('filter should call customFilter callback if it is provided', () => {
    markColumnDirective.customFilter = (val: any, filterData: any, item: any): boolean => { return false; };
    var filterResult = true;
    var filterSpy: jasmine.Spy = spyOn(markColumnDirective, 'customFilter').and.returnValue(filterResult);
    var value = true;
    var filterData = {};
    var item = {};

    var result = markColumnDirective.filter(value, filterData, item);

    expect(filterSpy).toHaveBeenCalled();
    var filterArgs = filterSpy.calls.first().args;
    expect(filterArgs[0]).toEqual(value);
    expect(filterArgs[1]).toEqual(filterData);
    expect(filterArgs[2]).toEqual(item);
  });

  it('filter with no filter data should return true', () => {
    var result = markColumnDirective.filter(true, null, {});

    expect(result).toBeTruthy();
  });

  it('filter with no isMarked should return true', () => {
    var result = markColumnDirective.filter(false, {}, {});

    expect(result).toBeTruthy();
  });

  it('filter with no item value should return false', () => {
    var result = markColumnDirective.filter(null, { isMarked: false }, {});

    expect(result).toBeFalsy();
  });

  it('filter with isMarked equal to column value should return true', () => {
    var result = markColumnDirective.filter(false, { isMarked: false }, {});

    expect(result).toBeTruthy();
  });

  it('filter with isMarked not equal to column value should return false', () => {
    var result = markColumnDirective.filter(false, { isMarked: true }, {});

    expect(result).toBeFalsy();
  });
});