"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var my_grid_module_1 = require("../../my-grid.module");
var mark_column_edit_component_1 = require("./mark-column-edit.component");
var TestEditComponent = (function () {
    function TestEditComponent() {
    }
    __decorate([
        core_1.ViewChild('editTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestEditComponent.prototype, "editTemplate", void 0);
    TestEditComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-edit',
            template: "\n    <ng-template #editTemplate>\n      <div id=\"test-edit-template\"></div>\n    </ng-template>\n  "
        })
    ], TestEditComponent);
    return TestEditComponent;
}());
describe('Component: MarkColumnEditComponent', function () {
    var markColumnEditComponent;
    var fixture;
    var editTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestEditComponent
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(mark_column_edit_component_1.MarkColumnEditComponent);
            markColumnEditComponent = fixture.componentInstance;
            var templateComponent = testing_1.TestBed.createComponent(TestEditComponent).componentInstance;
            editTemplate = templateComponent.editTemplate;
        });
    }));
    it('custom edit template should be rendered when it exists', function () {
        var item = {};
        var data = {
            editTemplate: editTemplate
        };
        var form = new forms_1.FormGroup({});
        markColumnEditComponent.data = data;
        markColumnEditComponent.item = item;
        markColumnEditComponent.editForm = form;
        fixture.detectChanges();
        var customTemplate = fixture.debugElement.query(platform_browser_1.By.css('#test-edit-template'));
        expect(customTemplate).not.toBeNull();
        expect(customTemplate.context.$implicit.data).toEqual(data);
        expect(customTemplate.context.$implicit.item).toEqual(item);
        expect(customTemplate.context.$implicit.editForm).toEqual(form);
    });
});
//# sourceMappingURL=mark-column-edit.component.spec.js.map