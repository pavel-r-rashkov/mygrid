"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var my_grid_module_1 = require("../../my-grid.module");
var mark_column_filter_component_1 = require("./mark-column-filter.component");
var filter_state_service_1 = require("../../filter-state.service");
var TestFilterComponent = (function () {
    function TestFilterComponent() {
    }
    __decorate([
        core_1.ViewChild('filterTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestFilterComponent.prototype, "filterTemplate", void 0);
    TestFilterComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-filter',
            template: "\n    <ng-template #filterTemplate>\n      <div id=\"test-filter-template\"></div>\n    </ng-template>\n  "
        })
    ], TestFilterComponent);
    return TestFilterComponent;
}());
describe('Component: MarkColumnFilterComponent', function () {
    var markColumnFilterComponent;
    var fixture;
    var filterTemplate;
    var filterStateService;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [
                filter_state_service_1.FilterStateService
            ],
            declarations: [
                TestFilterComponent
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(mark_column_filter_component_1.MarkColumnFilterComponent);
            markColumnFilterComponent = fixture.componentInstance;
            var templateComponent = testing_1.TestBed.createComponent(TestFilterComponent).componentInstance;
            filterTemplate = templateComponent.filterTemplate;
            filterStateService = fixture.debugElement.injector.get(filter_state_service_1.FilterStateService);
        });
    }));
    it('custom filter template should be rendered when it exists', function () {
        var data = {
            filterTemplate: filterTemplate,
            enableFiltering: true
        };
        markColumnFilterComponent.filterData = data;
        fixture.detectChanges();
        var customTemplate = fixture.debugElement.query(platform_browser_1.By.css('#test-filter-template'));
        expect(customTemplate).not.toBeNull();
        expect(customTemplate.context.$implicit.filterData).toEqual(data);
        expect(customTemplate.context.$implicit.columnId).toEqual(markColumnFilterComponent.columnId);
        expect(customTemplate.context.$implicit.filter).toEqual(markColumnFilterComponent.customTemplateFilter);
    });
    it('changing filter state should update checkBoxValue', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        markColumnFilterComponent.filterData = data;
        var columnId = '123';
        markColumnFilterComponent.columnId = columnId;
        var isMarked = true;
        fixture.detectChanges();
        filterStateService.raiseFilterChanged(columnId, propertyName, { isMarked: isMarked }, '');
        fixture.whenStable().then(function () {
            expect(markColumnFilterComponent.checkBoxValue).toEqual(isMarked);
        });
    });
    it('customTemplateFilter should create new filter state', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        markColumnFilterComponent.filterData = data;
        var columnId = '123';
        markColumnFilterComponent.columnId = columnId;
        var templateFilterData = {};
        var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        markColumnFilterComponent.customTemplateFilter(templateFilterData);
        expect(raiseFilterChangeSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangeSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2]).toEqual(templateFilterData);
    });
    it('filterChanged should clear filter for this column when the chech box value is null', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        markColumnFilterComponent.filterData = data;
        var columnId = '123';
        markColumnFilterComponent.columnId = columnId;
        var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');
        fixture.detectChanges();
        markColumnFilterComponent.filterChanged();
        expect(clearColumnFilterSpy).toHaveBeenCalled();
        expect(clearColumnFilterSpy.calls.first().args[0]).toEqual(markColumnFilterComponent.columnId);
    });
    it('filterChanged should create new filter state when the chech box value is not null', function () {
        var propertyName = 'foo';
        var searchTermType = 3;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        markColumnFilterComponent.filterData = data;
        var columnId = '123';
        markColumnFilterComponent.columnId = columnId;
        var checkBoxValue = true;
        markColumnFilterComponent.checkBoxValue = checkBoxValue;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        markColumnFilterComponent.filterChanged();
        expect(raiseFilterChangedSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangedSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2].isMarked).toEqual(checkBoxValue);
    });
});
//# sourceMappingURL=mark-column-filter.component.spec.js.map