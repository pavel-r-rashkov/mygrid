import { 
  Directive, 
  forwardRef,
  Input,
  TemplateRef
} from '@angular/core';

import { GridColumn } from '../grid-column';
import { DefaultColumnHeaderComponent } from '../shared/default-column-header.component';
import { MarkColumnFilterComponent } from './mark-column-filter.component';
import { MarkColumnDataComponent } from './mark-column-data.component';
import { MarkColumnEditComponent } from './mark-column-edit.component';
import { GuidService } from '../../guid';
import { DefaultComparerService } from '../shared/default-comparer.service';

@Directive({
  selector: 'mg-mark-column',
  providers: [{provide: GridColumn, useExisting: forwardRef(() => MarkColumnDirective)}]
})
export class MarkColumnDirective extends GridColumn {
  @Input() propertyName: string;
  @Input() enableOrdering: boolean;
  @Input() enableFiltering: boolean;
  @Input() enableEditing: boolean;
  @Input() enableGrouping: boolean;
  @Input() customFilter: (val: any, filterData: any, item: any) => boolean;
  @Input() customOrder: (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) => number;
  @Input() customGroup: (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) => number;

  constructor(
    private guidService: GuidService,
    private comparer: DefaultComparerService) {
    super(
      DefaultColumnHeaderComponent, 
      MarkColumnFilterComponent, 
      MarkColumnDataComponent,
      MarkColumnEditComponent,
      guidService.newGuid());
  }

  public getHeaderData(): any {
    return {
      caption: this.caption,
      headerTemplate: this.headerTemplate,
      propertyName: this.propertyName,
      enableOrdering: this.enableOrdering == null ? true : this.enableOrdering,
      enableGrouping: this.enableGrouping == null ? true : this.enableGrouping
    };
  }

  public getData(): any {
    return {
      propertyName: this.propertyName,
      dataTemplate: this.dataTemplate,
      enableEditing: this.enableEditing == null ? true : this.enableEditing,
      editTemplate: this.editTemplate,
      validationErrorsTemplate: this.validationErrorsTemplate
    }
  }

  public getFilterData(): any {
    return {
      filterTemplate: this.filterTemplate,
      propertyName: this.propertyName,
      enableFiltering: this.enableFiltering == null ? true : this.enableFiltering
    }
  }

  public getGroupData(): any {
    let groupData = this.getData();
    groupData.dataTemplate = this.groupHeaderTemplate;
    return groupData;
  }

  public filter(val: any, filterData: any, item: any): boolean {
    if (this.customFilter != null) {
      return this.customFilter(val, filterData, item);
    }

    if (filterData != null && filterData.isMarked != null) {
      return val == filterData.isMarked;
    }

    return true;
  }

  public order(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    if (this.customOrder != null) {
      return this.customOrder(firstValue, secondValue, isAscending, firstItem, secondItem);
    } else {
      return this.comparer.compare(firstValue, secondValue, isAscending);
    }
  }

  public group(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    if (this.customGroup != null) {
      return this.customGroup(firstValue, secondValue, isAscending, firstItem, secondItem);
    } else {
      return this.comparer.compare(firstValue, secondValue, isAscending);
    }
  }

  public getValue(item: any) {
    if (item[this.propertyName] == undefined) {
      return null;
    }
    
    return item[this.propertyName];
  }
}