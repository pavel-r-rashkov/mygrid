import { 
  Component, 
  OnInit,
  OnDestroy,
  AfterViewInit,
  Input,
  TemplateRef,
  ViewChild,
  ViewContainerRef } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Subscription } from 'rxjs/Rx';

import { FilterComponent } from '../contracts/filter-component';
import { FilterStateService } from '../../filter-state.service';
import { FilterState } from '../../states/filter-state';

@Component({
  selector: 'mg-mark-column-filter',
  templateUrl: './mark-column-filter.component.html'
})
export class MarkColumnFilterComponent extends FilterComponent implements OnInit, AfterViewInit, OnDestroy {
  private readonly filterKey: string = 'Mark';
  private filterStateSubscription: Subscription;
  private currentFilterState: FilterState;
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;
  checkBoxValue: any;

  constructor(
    private filterStateService: FilterStateService) {
    super();
  }

  ngOnInit() {
    this.filterStateSubscription = this.filterStateService.filterState.subscribe((newState) => {
      let markColumnFilter = newState.columnFilters[this.columnId];
      this.checkBoxValue = markColumnFilter == null ? null : markColumnFilter.filterData.isMarked;
    });
  }

  ngAfterViewInit() {
    if (this.filterData.filterTemplate != null) {
      let templateContext = {
        columnId: this.columnId,
        filterData: this.filterData,
        filter: this.customTemplateFilter
      }

      this.containerRef.createEmbeddedView(this.filterData.filterTemplate, { $implicit: templateContext });
    }
  }

  ngOnDestroy() {
    this.filterStateSubscription.unsubscribe();
  }

  filterChanged() {
    if (this.checkBoxValue == null) {
      this.filterStateService.clearColumnFilter(this.columnId);
    } else {
      this.filterStateService.raiseFilterChanged(
        this.columnId,
        this.filterData.propertyName,
        { isMarked: this.checkBoxValue },
        this.filterKey);
    }
  }

  customTemplateFilter(filterData: any) {
    this.filterStateService.raiseFilterChanged(
      this.columnId,
      this.filterData.propertyName,
      filterData,
      this.filterKey);
  }
}