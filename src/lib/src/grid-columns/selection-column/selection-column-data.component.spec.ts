import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { SelectionColumnDataComponent } from './selection-column-data.component';
import { SelectionService } from '../../selection/selection.service';
import { SelectionState } from '../../selection/selection-state';
import { GridSettingsService } from '../../grid-settings.service';

@Component({
  selector: 'mgt-test-data',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
  `
})
class TestDataComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
}

describe('Component: SelectionColumnDataComponent', () => {
  let selectionColumnDataComponent: SelectionColumnDataComponent;
  let fixture: ComponentFixture<SelectionColumnDataComponent>;
  let dataTemplate: TemplateRef<any>;
  let selectionService: SelectionService;
  let gridSettingsService: GridSettingsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestDataComponent
        ],
        providers: [
          SelectionService,
          GridSettingsService
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SelectionColumnDataComponent);
        selectionColumnDataComponent = fixture.componentInstance;

        selectionService = fixture.debugElement.injector.get(SelectionService);
        gridSettingsService = fixture.debugElement.injector.get(GridSettingsService);

        var templateComponent: TestDataComponent = TestBed.createComponent(TestDataComponent).componentInstance;
        dataTemplate = templateComponent.dataTemplate;
      });
  }));

  it('selectionChange should create new SelectionState with item\'s key in it when the item is selected', () => {
    var key = 'foo';
    var item = { foo: 'foo' };
    gridSettingsService.key = key;
    selectionColumnDataComponent.item = item;
    selectionColumnDataComponent.data = {};
    var selectionSpy: jasmine.Spy = spyOn(selectionService.selectionSubject, 'next').and.callThrough();
    
    fixture.detectChanges();
    selectionColumnDataComponent.selectionValue = true;
    selectionColumnDataComponent.selectionChange();

    expect(selectionSpy).toHaveBeenCalled();
    var state: SelectionState = selectionSpy.calls.first().args[0];
    expect(state.selectedItems.indexOf(item[key]) >= 0).toBeTruthy();
  });

  it('selectionChange should create new SelectionState without item\'s key in it when the item is deselected', () => {
    var key = 'foo';
    var item = { foo: 'foo' };
    gridSettingsService.key = key;
    selectionColumnDataComponent.item = item;
    selectionColumnDataComponent.data = {};
    var selectionSpy: jasmine.Spy = spyOn(selectionService.selectionSubject, 'next').and.callThrough();

    fixture.detectChanges();
    selectionService.selectionSubject.next(new SelectionState([item[key]]));

    fixture.whenStable().then(() => {
      selectionSpy.calls.reset();
      selectionColumnDataComponent.selectionValue = false;
      selectionColumnDataComponent.selectionChange();

      expect(selectionSpy).toHaveBeenCalled();
      expect(selectionService.selectionSubject.getValue().selectedItems.indexOf(item[key]) < 0).toBeTruthy();
    });
  });

  it('custom data template should be rendered when it exists', () => {
    var item = {};
    var data = {
      dataTemplate: dataTemplate
    };
    selectionColumnDataComponent.data = data;
    selectionColumnDataComponent.item = item;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-data-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.data).toEqual(data);
    expect(customTemplate.context.$implicit.item).toEqual(item);
    expect(customTemplate.context.$implicit.selectionSubject).toEqual(selectionService.selectionSubject);
  });
});