"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var grid_column_1 = require("../grid-column");
var selection_column_data_component_1 = require("./selection-column-data.component");
var default_column_header_component_1 = require("../shared/default-column-header.component");
var guid_1 = require("../../guid");
var SelectionColumnDirective = (function (_super) {
    __extends(SelectionColumnDirective, _super);
    function SelectionColumnDirective(guidService) {
        return _super.call(this, default_column_header_component_1.DefaultColumnHeaderComponent, null, selection_column_data_component_1.SelectionColumnDataComponent, null, guidService.newGuid()) || this;
    }
    SelectionColumnDirective_1 = SelectionColumnDirective;
    SelectionColumnDirective.prototype.getHeaderData = function () {
        return {
            caption: this.caption,
            headerTemplate: this.headerTemplate,
            enableOrdering: false
        };
    };
    SelectionColumnDirective.prototype.getData = function () {
        return {
            dataTemplate: this.dataTemplate
        };
    };
    SelectionColumnDirective = SelectionColumnDirective_1 = __decorate([
        core_1.Directive({
            selector: 'mg-selection-column',
            providers: [{ provide: grid_column_1.GridColumn, useExisting: core_1.forwardRef(function () { return SelectionColumnDirective_1; }) }]
        }),
        __metadata("design:paramtypes", [guid_1.GuidService])
    ], SelectionColumnDirective);
    return SelectionColumnDirective;
    var SelectionColumnDirective_1;
}(grid_column_1.GridColumn));
exports.SelectionColumnDirective = SelectionColumnDirective;
//# sourceMappingURL=selection-column.directive.js.map