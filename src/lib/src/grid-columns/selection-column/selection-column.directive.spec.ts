import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { SelectionColumnDirective } from './selection-column.directive';
import { GuidService } from '../../guid';

@Component({
  selector: 'mgt-test-selection-column',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
  `
}) 
class TestSelectionColumnComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
}

describe('Directive: SelectionColumnDirective', () => {
  let selectionColumnDirective: SelectionColumnDirective;
  let dataTemplate: TemplateRef<any>;
  let headerTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestSelectionColumnComponent
        ]
      })
      .compileComponents()
      .then(() => {
        selectionColumnDirective = new SelectionColumnDirective(new GuidService());

        var testComponent = TestBed.createComponent(TestSelectionColumnComponent).componentInstance;
        dataTemplate = testComponent.dataTemplate;
        headerTemplate = testComponent.headerTemplate;
      });
  }));

  it('getHeaderData should return selection column header data', () => {
    var caption = 'foo';
    selectionColumnDirective.caption = caption;
    selectionColumnDirective.headerTemplate = headerTemplate;

    var headerData: any = selectionColumnDirective.getHeaderData();

    expect(headerData.caption).toEqual(caption);
    expect(headerData.headerTemplate).toEqual(headerTemplate);
    expect(headerData.enableOrdering).toBeFalsy();
  });

  it('getData should return selection column data', () => {
    selectionColumnDirective.dataTemplate = dataTemplate;

    var data: any = selectionColumnDirective.getData();

    expect(data.dataTemplate).toEqual(dataTemplate);
  });
});