import { 
  Component, 
  OnInit,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { DataComponent } from '../contracts/data-component';
import { SelectionService } from '../../selection/selection.service';
import { GridSettingsService } from '../../grid-settings.service';
import { SelectionState } from '../../selection/selection-state';

@Component({
  selector: 'mg-selection-column-data',
  templateUrl: './selection-column-data.component.html'
})
export class SelectionColumnDataComponent extends DataComponent implements OnInit {
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;
  selectionValue: any;

  constructor(
    private selectionService: SelectionService,
    private settingsService: GridSettingsService) {
    super();
  }

  ngOnInit() {
    if (this.data.dataTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item,
        select: this.select,
        deselect: this.deselect,
        selectionSubject: this.selectionService.selectionSubject
      };
      this.containerRef.createEmbeddedView(this.data.dataTemplate, { $implicit: templateContext });
    }

    this.updateSelectionValue();
    this.selectionService.selectionSubject.subscribe((newState: SelectionState) => {
      this.updateSelectionValue();
    });
  }

  selectionChange() {
    if (this.selectionValue) {
      this.select();
    } else {
      this.deselect();
    }
  }

  private select() {
    let selectedItems = this.selectionService.selectionSubject.getValue().selectedItems;
    let itemKey = this.item[this.settingsService.key];
    selectedItems.push(itemKey);
    this.selectionService.selectionSubject.next(new SelectionState(selectedItems));
  }

  private deselect() {
    let selectedItems = this.selectionService.selectionSubject.getValue().selectedItems;
    let itemKey = this.item[this.settingsService.key];
    let keyIndex = selectedItems.indexOf(itemKey);
    if (keyIndex >= 0) {
      selectedItems.splice(keyIndex, 1);
      this.selectionService.selectionSubject.next(new SelectionState(selectedItems));
    }
  }

  private updateSelectionValue() {
    let selectedItems = this.selectionService.selectionSubject.getValue().selectedItems;
    let itemKey = this.item[this.settingsService.key];
    this.selectionValue = selectedItems.indexOf(itemKey) >= 0;
  }
}