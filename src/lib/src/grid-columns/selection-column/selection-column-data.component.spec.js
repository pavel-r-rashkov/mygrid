"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
require("rxjs/add/observable/of");
var my_grid_module_1 = require("../../my-grid.module");
var selection_column_data_component_1 = require("./selection-column-data.component");
var selection_service_1 = require("../../selection/selection.service");
var selection_state_1 = require("../../selection/selection-state");
var grid_settings_service_1 = require("../../grid-settings.service");
var TestDataComponent = (function () {
    function TestDataComponent() {
    }
    __decorate([
        core_1.ViewChild('dataTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestDataComponent.prototype, "dataTemplate", void 0);
    TestDataComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-data',
            template: "\n    <ng-template #dataTemplate>\n      <div id=\"test-data-template\"></div>\n    </ng-template>\n  "
        })
    ], TestDataComponent);
    return TestDataComponent;
}());
describe('Component: SelectionColumnDataComponent', function () {
    var selectionColumnDataComponent;
    var fixture;
    var dataTemplate;
    var selectionService;
    var gridSettingsService;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestDataComponent
            ],
            providers: [
                selection_service_1.SelectionService,
                grid_settings_service_1.GridSettingsService
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(selection_column_data_component_1.SelectionColumnDataComponent);
            selectionColumnDataComponent = fixture.componentInstance;
            selectionService = fixture.debugElement.injector.get(selection_service_1.SelectionService);
            gridSettingsService = fixture.debugElement.injector.get(grid_settings_service_1.GridSettingsService);
            var templateComponent = testing_1.TestBed.createComponent(TestDataComponent).componentInstance;
            dataTemplate = templateComponent.dataTemplate;
        });
    }));
    it('selectionChange should create new SelectionState with item\'s key in it when the item is selected', function () {
        var key = 'foo';
        var item = { foo: 'foo' };
        gridSettingsService.key = key;
        selectionColumnDataComponent.item = item;
        selectionColumnDataComponent.data = {};
        var selectionSpy = spyOn(selectionService.selectionSubject, 'next').and.callThrough();
        fixture.detectChanges();
        selectionColumnDataComponent.selectionValue = true;
        selectionColumnDataComponent.selectionChange();
        expect(selectionSpy).toHaveBeenCalled();
        var state = selectionSpy.calls.first().args[0];
        expect(state.selectedItems.indexOf(item[key]) >= 0).toBeTruthy();
    });
    it('selectionChange should create new SelectionState without item\'s key in it when the item is deselected', function () {
        var key = 'foo';
        var item = { foo: 'foo' };
        gridSettingsService.key = key;
        selectionColumnDataComponent.item = item;
        selectionColumnDataComponent.data = {};
        var selectionSpy = spyOn(selectionService.selectionSubject, 'next').and.callThrough();
        fixture.detectChanges();
        selectionService.selectionSubject.next(new selection_state_1.SelectionState([item[key]]));
        fixture.whenStable().then(function () {
            selectionSpy.calls.reset();
            selectionColumnDataComponent.selectionValue = false;
            selectionColumnDataComponent.selectionChange();
            expect(selectionSpy).toHaveBeenCalled();
            expect(selectionService.selectionSubject.getValue().selectedItems.indexOf(item[key]) < 0).toBeTruthy();
        });
    });
    it('custom data template should be rendered when it exists', function () {
        var item = {};
        var data = {
            dataTemplate: dataTemplate
        };
        selectionColumnDataComponent.data = data;
        selectionColumnDataComponent.item = item;
        fixture.detectChanges();
        var customTemplate = fixture.debugElement.query(platform_browser_1.By.css('#test-data-template'));
        expect(customTemplate).not.toBeNull();
        expect(customTemplate.context.$implicit.data).toEqual(data);
        expect(customTemplate.context.$implicit.item).toEqual(item);
        expect(customTemplate.context.$implicit.selectionSubject).toEqual(selectionService.selectionSubject);
    });
});
//# sourceMappingURL=selection-column-data.component.spec.js.map