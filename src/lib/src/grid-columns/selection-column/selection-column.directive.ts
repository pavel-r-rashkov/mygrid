import { 
  Directive, 
  forwardRef,
  Input,
  TemplateRef
} from '@angular/core';

import { GridColumn } from '../grid-column';
import { SelectionColumnDataComponent } from './selection-column-data.component';
import { DefaultColumnHeaderComponent } from '../shared/default-column-header.component';
import { GuidService } from '../../guid';

@Directive({
  selector: 'mg-selection-column',
  providers: [{provide: GridColumn, useExisting: forwardRef(() => SelectionColumnDirective)}]
})
export class SelectionColumnDirective extends GridColumn {
  constructor(
    guidService: GuidService) {
    super(
      DefaultColumnHeaderComponent, 
      null, 
      SelectionColumnDataComponent,
      null,
      guidService.newGuid());
  }

  public getHeaderData(): any {
    return {
      caption: this.caption,
      headerTemplate: this.headerTemplate,
      enableOrdering: false
    };
  }

  public getData(): any {
    return {
      dataTemplate: this.dataTemplate
    }
  }
}