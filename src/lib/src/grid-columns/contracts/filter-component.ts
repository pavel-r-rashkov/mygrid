import { Input } from '@angular/core';

export class FilterComponent {
  @Input() columnId: string;
  @Input() filterData: any;
}