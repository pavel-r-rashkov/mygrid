import { Input } from '@angular/core';

export class DataComponent {
  @Input() data: any;
  @Input() item: any;
  @Input() visibleIndex: number;
}