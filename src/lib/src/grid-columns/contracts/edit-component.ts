import { FormGroup } from '@angular/forms';
import { Input } from '@angular/core';

export class EditComponent {
  @Input() data: any;
  @Input() item: any;
  @Input() editForm: FormGroup;
}