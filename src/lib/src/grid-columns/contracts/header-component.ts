import { Input } from '@angular/core';

export class HeaderComponent {
  @Input() columnId: string;
  @Input() headerData: any;
}