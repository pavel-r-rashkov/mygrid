"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var animations_1 = require("@angular/animations");
exports.filterConditionMenuAnimation = animations_1.trigger('filterConditionMenuState', [
    animations_1.state('hidden', animations_1.style({
        top: '50%',
        opacity: '0'
    })),
    animations_1.state('opened', animations_1.style({
        top: '100%',
        opacity: '1'
    })),
    animations_1.transition('hidden => opened', animations_1.animate('150ms ease-in')),
    animations_1.transition('opened => hidden', animations_1.animate('150ms ease-out'))
]);
//# sourceMappingURL=animations.js.map