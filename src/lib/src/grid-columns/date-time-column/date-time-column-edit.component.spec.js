"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var my_grid_module_1 = require("../../my-grid.module");
var date_time_column_edit_component_1 = require("./date-time-column-edit.component");
var TestEditComponent = (function () {
    function TestEditComponent() {
    }
    __decorate([
        core_1.ViewChild('editTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestEditComponent.prototype, "editTemplate", void 0);
    TestEditComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-edit',
            template: "\n    <ng-template #editTemplate>\n      <div id=\"test-edit-template\"></div>\n    </ng-template>\n  "
        })
    ], TestEditComponent);
    return TestEditComponent;
}());
describe('Component: DateTimeColumnEditComponent', function () {
    var dateTimeColumnEditComponent;
    var fixture;
    var editTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestEditComponent
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(date_time_column_edit_component_1.DateTimeColumnEditComponent);
            dateTimeColumnEditComponent = fixture.componentInstance;
            var templateComponent = testing_1.TestBed.createComponent(TestEditComponent).componentInstance;
            editTemplate = templateComponent.editTemplate;
        });
    }));
    it('custom edit template should be rendered when it exists', function () {
        var item = {};
        var data = {
            editTemplate: editTemplate
        };
        var form = new forms_1.FormGroup({});
        dateTimeColumnEditComponent.data = data;
        dateTimeColumnEditComponent.item = item;
        dateTimeColumnEditComponent.editForm = form;
        fixture.detectChanges();
        var customTemplate = fixture.debugElement.query(platform_browser_1.By.css('#test-edit-template'));
        expect(customTemplate).not.toBeNull();
        expect(customTemplate.context.$implicit.data).toEqual(data);
        expect(customTemplate.context.$implicit.item).toEqual(item);
        expect(customTemplate.context.$implicit.editForm).toEqual(form);
    });
    it('getValue should return null when the item is null', function () {
        var data = {
            propertyName: 'foo'
        };
        var form = new forms_1.FormGroup({ foo: new forms_1.FormControl() });
        dateTimeColumnEditComponent.data = data;
        dateTimeColumnEditComponent.editForm = form;
        fixture.detectChanges();
        var columnValue = dateTimeColumnEditComponent.getValue();
        expect(columnValue).toBeNull();
    });
    it('getValue should return null when the value that the column is bound to is null', function () {
        var data = {
            propertyName: 'foo'
        };
        var form = new forms_1.FormGroup({ foo: new forms_1.FormControl() });
        dateTimeColumnEditComponent.data = data;
        dateTimeColumnEditComponent.editForm = form;
        dateTimeColumnEditComponent.item = {};
        fixture.detectChanges();
        var columnValue = dateTimeColumnEditComponent.getValue();
        expect(columnValue).toBeNull();
    });
    it('getValue should return date string of the property that the column is bound to when the type is date', function () {
        var data = {
            propertyName: 'foo',
            type: 'date'
        };
        var item = { foo: new Date(1992, 10, 3) };
        var form = new forms_1.FormGroup({ foo: new forms_1.FormControl() });
        dateTimeColumnEditComponent.data = data;
        dateTimeColumnEditComponent.item = item;
        dateTimeColumnEditComponent.editForm = form;
        fixture.detectChanges();
        var dateString = dateTimeColumnEditComponent.getValue();
        expect(dateString).toEqual('1992-11-03');
    });
    it('getValue should return time string of the property that the column is bound to when the type is time', function () {
        var data = {
            propertyName: 'foo',
            type: 'time',
            step: 0.001
        };
        var item = { foo: new Date(1992, 10, 3, 8, 34, 28, 123) };
        var form = new forms_1.FormGroup({ foo: new forms_1.FormControl() });
        dateTimeColumnEditComponent.data = data;
        dateTimeColumnEditComponent.item = item;
        dateTimeColumnEditComponent.editForm = form;
        fixture.detectChanges();
        var dateString = dateTimeColumnEditComponent.getValue();
        expect(dateString).toEqual('08:34:28.123');
    });
    it('getValue should return date-time string of the property that the column is bound to when the type is datetime-local', function () {
        var data = {
            propertyName: 'foo',
            type: 'datetime-local',
            step: 0.001
        };
        var item = { foo: new Date(1992, 10, 3, 8, 34, 28, 123) };
        var form = new forms_1.FormGroup({ foo: new forms_1.FormControl() });
        dateTimeColumnEditComponent.data = data;
        dateTimeColumnEditComponent.item = item;
        dateTimeColumnEditComponent.editForm = form;
        fixture.detectChanges();
        var dateString = dateTimeColumnEditComponent.getValue();
        expect(dateString).toEqual('1992-11-03T08:34:28.123');
    });
});
//# sourceMappingURL=date-time-column-edit.component.spec.js.map