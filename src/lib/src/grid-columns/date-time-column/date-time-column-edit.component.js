"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var edit_component_1 = require("../contracts/edit-component");
var DateTimeColumnEditComponent = (function (_super) {
    __extends(DateTimeColumnEditComponent, _super);
    function DateTimeColumnEditComponent() {
        return _super.call(this) || this;
    }
    DateTimeColumnEditComponent.prototype.ngOnInit = function () {
        if (this.data.editTemplate != null) {
            var templateContext = {
                data: this.data,
                item: this.item,
                editForm: this.editForm
            };
            this.containerRef.createEmbeddedView(this.data.editTemplate, { $implicit: templateContext });
        }
    };
    DateTimeColumnEditComponent.prototype.getValue = function () {
        if (this.item == null || !this.item[this.data.propertyName]) {
            return null;
        }
        var date = this.item[this.data.propertyName];
        var year = date.getFullYear().toString();
        var month = (date.getMonth() + 1).toString();
        var day = date.getDate().toString();
        var dateString = year + '-' + this.padStart(month, 2, '0') + '-' + this.padStart(day, 2, '0');
        var hours = date.getHours().toString();
        var minutes = date.getMinutes().toString();
        var seconds = date.getSeconds().toString();
        var milliseconds = date.getMilliseconds().toString();
        var timeString = this.padStart(hours, 2, '0') + ':' + this.padStart(minutes, 2, '0');
        if (this.data.step < 60) {
            timeString += ':' + this.padStart(seconds, 2, '0');
        }
        if (this.data.step < 1) {
            timeString += '.' + this.padStart(milliseconds, 3, '0');
        }
        var formatted = '';
        switch (this.data.type) {
            case 'date':
                formatted = dateString;
                break;
            case 'time':
                formatted = timeString;
                break;
            case 'datetime-local':
                formatted = dateString + 'T' + timeString;
                break;
        }
        return formatted;
    };
    DateTimeColumnEditComponent.prototype.padStart = function (target, length, fill) {
        if (target.length >= length) {
            return target;
        }
        var fillLength = length - target.length;
        while (fillLength > fill.length) {
            fill += fill;
        }
        var truncatedFill = fill.substr(0, fillLength);
        return truncatedFill + target;
    };
    __decorate([
        core_1.ViewChild('customTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], DateTimeColumnEditComponent.prototype, "containerRef", void 0);
    DateTimeColumnEditComponent = __decorate([
        core_1.Component({
            selector: 'mg-date-time-column-edit',
            templateUrl: './date-time-column-edit.component.html'
        }),
        __metadata("design:paramtypes", [])
    ], DateTimeColumnEditComponent);
    return DateTimeColumnEditComponent;
}(edit_component_1.EditComponent));
exports.DateTimeColumnEditComponent = DateTimeColumnEditComponent;
//# sourceMappingURL=date-time-column-edit.component.js.map