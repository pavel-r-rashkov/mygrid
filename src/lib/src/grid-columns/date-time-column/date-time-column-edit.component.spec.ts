import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { 
  FormGroup, 
  FormControl 
} from '@angular/forms';

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { DateTimeColumnEditComponent } from './date-time-column-edit.component';

@Component({
  selector: 'mgt-test-edit',
  template: `
    <ng-template #editTemplate>
      <div id="test-edit-template"></div>
    </ng-template>
  `
})
class TestEditComponent {
  @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
}

describe('Component: DateTimeColumnEditComponent', () => {
  let dateTimeColumnEditComponent: DateTimeColumnEditComponent;
  let fixture: ComponentFixture<DateTimeColumnEditComponent>;
  let editTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestEditComponent
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(DateTimeColumnEditComponent);
        dateTimeColumnEditComponent = fixture.componentInstance;

        var templateComponent: TestEditComponent = TestBed.createComponent(TestEditComponent).componentInstance;
        editTemplate = templateComponent.editTemplate;
      });
  }));
  
  it('custom edit template should be rendered when it exists', () => {
    var item = {};
    var data = {
      editTemplate: editTemplate
    };
    var form = new FormGroup({});
    dateTimeColumnEditComponent.data = data;
    dateTimeColumnEditComponent.item = item;
    dateTimeColumnEditComponent.editForm = form;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-edit-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.data).toEqual(data);
    expect(customTemplate.context.$implicit.item).toEqual(item);
    expect(customTemplate.context.$implicit.editForm).toEqual(form);
  });

  it('getValue should return null when the item is null', () => {
    var data = {
      propertyName: 'foo'
    };
    var form = new FormGroup({ foo: new FormControl() });
    dateTimeColumnEditComponent.data = data;
    dateTimeColumnEditComponent.editForm = form;

    fixture.detectChanges();
    var columnValue = dateTimeColumnEditComponent.getValue();    
    
    expect(columnValue).toBeNull();
  });

  it('getValue should return null when the value that the column is bound to is null', () => {
    var data = {
      propertyName: 'foo'
    };
    var form = new FormGroup({ foo: new FormControl() });
    dateTimeColumnEditComponent.data = data;
    dateTimeColumnEditComponent.editForm = form;
    dateTimeColumnEditComponent.item = {};

    fixture.detectChanges();
    var columnValue = dateTimeColumnEditComponent.getValue();    
    
    expect(columnValue).toBeNull();
  });

  it('getValue should return date string of the property that the column is bound to when the type is date', () => {
    var data = {
      propertyName: 'foo',
      type: 'date'
    };
    var item = { foo: new Date(1992, 10, 3) };
    var form = new FormGroup({ foo: new FormControl() });
    dateTimeColumnEditComponent.data = data;
    dateTimeColumnEditComponent.item = item;
    dateTimeColumnEditComponent.editForm = form;

    fixture.detectChanges();
    var dateString: string = dateTimeColumnEditComponent.getValue();
    
    expect(dateString).toEqual('1992-11-03');
  });

  it('getValue should return time string of the property that the column is bound to when the type is time', () => {
    var data = {
      propertyName: 'foo',
      type: 'time',
      step: 0.001
    };
    var item = { foo: new Date(1992, 10, 3, 8, 34, 28, 123) };
    var form = new FormGroup({ foo: new FormControl() });
    dateTimeColumnEditComponent.data = data;
    dateTimeColumnEditComponent.item = item;
    dateTimeColumnEditComponent.editForm = form;

    fixture.detectChanges();
    var dateString: string = dateTimeColumnEditComponent.getValue();
    
    expect(dateString).toEqual('08:34:28.123');
  });

  it('getValue should return date-time string of the property that the column is bound to when the type is datetime-local', () => {
    var data = {
      propertyName: 'foo',
      type: 'datetime-local',
      step: 0.001
    };
    var item = { foo: new Date(1992, 10, 3, 8, 34, 28, 123) };
    var form = new FormGroup({ foo: new FormControl() });
    dateTimeColumnEditComponent.data = data;
    dateTimeColumnEditComponent.item = item;
    dateTimeColumnEditComponent.editForm = form;

    fixture.detectChanges();
    var dateString: string = dateTimeColumnEditComponent.getValue();
    
    expect(dateString).toEqual('1992-11-03T08:34:28.123');
  });
});