"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DateTimeSearchTermType;
(function (DateTimeSearchTermType) {
    DateTimeSearchTermType[DateTimeSearchTermType["LessThan"] = 0] = "LessThan";
    DateTimeSearchTermType[DateTimeSearchTermType["LessThanOrEqual"] = 1] = "LessThanOrEqual";
    DateTimeSearchTermType[DateTimeSearchTermType["Equals"] = 2] = "Equals";
    DateTimeSearchTermType[DateTimeSearchTermType["MoreThanOrEqual"] = 3] = "MoreThanOrEqual";
    DateTimeSearchTermType[DateTimeSearchTermType["MoreThan"] = 4] = "MoreThan";
})(DateTimeSearchTermType = exports.DateTimeSearchTermType || (exports.DateTimeSearchTermType = {}));
//# sourceMappingURL=date-time-search-term-type.js.map