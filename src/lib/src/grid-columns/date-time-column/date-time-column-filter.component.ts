import { 
  Component, 
  OnInit,
  OnDestroy,
  AfterViewInit,
  Input,
  TemplateRef,
  ViewChild,
  ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import { filterConditionMenuAnimation } from '../animations';
import { FilterComponent } from '../contracts/filter-component';
import { FilterStateService } from '../../filter-state.service';
import { FilterState } from '../../states/filter-state';
import { DateTimeSearchTermType } from './date-time-search-term-type';

@Component({
  selector: 'mg-date-time-column-filter',
  templateUrl: './date-time-column-filter.component.html',
  animations: [
    filterConditionMenuAnimation
  ]
})
export class DateTimeColumnFilterComponent extends FilterComponent implements OnInit, AfterViewInit, OnDestroy {
  private readonly filterKey: string = 'Date';
  private filterStateSubscription: Subscription;
  private currentFilterState: FilterState;
  private cachedFilterDate: any;
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;
  filterDate: any;
  menuVisible: boolean;
  selectedSearchType: DateTimeSearchTermType;
  dateTimeSearchTermType = DateTimeSearchTermType;

  constructor(
    private filterStateService: FilterStateService) {
    super();
    this.menuVisible = false;
  }

  ngOnInit() {
    this.selectedSearchType = this.filterData.searchTermType;

    this.filterStateSubscription = this.filterStateService.filterState.subscribe((newState) => {
      let dateTimeColumnFilter = newState.columnFilters[this.columnId];
      this.filterDate = dateTimeColumnFilter == null ? null : dateTimeColumnFilter.filterData.selectedDate;
    });
  }

  ngAfterViewInit() {
    if (this.filterData.filterTemplate != null) {
      let templateContext = {
        columnId: this.columnId,
        filterData: this.filterData,
        filter: this.customTemplateFilter
      }

      this.containerRef.createEmbeddedView(this.filterData.filterTemplate, { $implicit: templateContext });
    }
  }

  ngOnDestroy() {
    this.filterStateSubscription.unsubscribe();
  }

  filterBlur() {
    if (this.filterDate == '') {
      this.filterDate = null;
    }

    if (this.cachedFilterDate == this.filterDate) {
      return;
    }

    if (this.filterDate == null) {
      this.filterStateService.clearColumnFilter(this.columnId);
    } else {
      this.filterStateService.raiseFilterChanged(
        this.columnId,
        this.filterData.propertyName,
        { selectedDate: this.filterDate, searchTermType: this.selectedSearchType },
        this.filterKey);
    }
  }

  filterFocusedOut($event: any) {
    this.cachedFilterDate = this.filterDate;    
  }

  selectSearchType(selectedType: number) {
    if (selectedType != this.selectedSearchType && this.filterDate != null) {
      this.filterStateService.raiseFilterChanged(
        this.columnId,
        this.filterData.propertyName, 
        { selectedDate: this.filterDate, searchTermType: selectedType },
        this.filterKey);
    }

    this.selectedSearchType = selectedType;
  }

  toggleMenu() {
    this.menuVisible = !this.menuVisible;
  }

  buttonFocusedOut() {
    this.menuVisible = false;
  }

  getConditionMenuState(): string {
    return this.menuVisible ? 'opened' : 'hidden';
  }

  customTemplateFilter(filterData: any) {
    this.filterStateService.raiseFilterChanged(
      this.columnId,
      this.filterData.propertyName,
      filterData,
      this.filterKey);
  }
}