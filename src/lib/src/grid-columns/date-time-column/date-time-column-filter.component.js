"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("../animations");
var filter_component_1 = require("../contracts/filter-component");
var filter_state_service_1 = require("../../filter-state.service");
var date_time_search_term_type_1 = require("./date-time-search-term-type");
var DateTimeColumnFilterComponent = (function (_super) {
    __extends(DateTimeColumnFilterComponent, _super);
    function DateTimeColumnFilterComponent(filterStateService) {
        var _this = _super.call(this) || this;
        _this.filterStateService = filterStateService;
        _this.filterKey = 'Date';
        _this.dateTimeSearchTermType = date_time_search_term_type_1.DateTimeSearchTermType;
        _this.menuVisible = false;
        return _this;
    }
    DateTimeColumnFilterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.selectedSearchType = this.filterData.searchTermType;
        this.filterStateSubscription = this.filterStateService.filterState.subscribe(function (newState) {
            var dateTimeColumnFilter = newState.columnFilters[_this.columnId];
            _this.filterDate = dateTimeColumnFilter == null ? null : dateTimeColumnFilter.filterData.selectedDate;
        });
    };
    DateTimeColumnFilterComponent.prototype.ngAfterViewInit = function () {
        if (this.filterData.filterTemplate != null) {
            var templateContext = {
                columnId: this.columnId,
                filterData: this.filterData,
                filter: this.customTemplateFilter
            };
            this.containerRef.createEmbeddedView(this.filterData.filterTemplate, { $implicit: templateContext });
        }
    };
    DateTimeColumnFilterComponent.prototype.ngOnDestroy = function () {
        this.filterStateSubscription.unsubscribe();
    };
    DateTimeColumnFilterComponent.prototype.filterBlur = function () {
        if (this.filterDate == '') {
            this.filterDate = null;
        }
        if (this.cachedFilterDate == this.filterDate) {
            return;
        }
        if (this.filterDate == null) {
            this.filterStateService.clearColumnFilter(this.columnId);
        }
        else {
            this.filterStateService.raiseFilterChanged(this.columnId, this.filterData.propertyName, { selectedDate: this.filterDate, searchTermType: this.selectedSearchType }, this.filterKey);
        }
    };
    DateTimeColumnFilterComponent.prototype.filterFocusedOut = function ($event) {
        this.cachedFilterDate = this.filterDate;
    };
    DateTimeColumnFilterComponent.prototype.selectSearchType = function (selectedType) {
        if (selectedType != this.selectedSearchType && this.filterDate != null) {
            this.filterStateService.raiseFilterChanged(this.columnId, this.filterData.propertyName, { selectedDate: this.filterDate, searchTermType: selectedType }, this.filterKey);
        }
        this.selectedSearchType = selectedType;
    };
    DateTimeColumnFilterComponent.prototype.toggleMenu = function () {
        this.menuVisible = !this.menuVisible;
    };
    DateTimeColumnFilterComponent.prototype.buttonFocusedOut = function () {
        this.menuVisible = false;
    };
    DateTimeColumnFilterComponent.prototype.getConditionMenuState = function () {
        return this.menuVisible ? 'opened' : 'hidden';
    };
    DateTimeColumnFilterComponent.prototype.customTemplateFilter = function (filterData) {
        this.filterStateService.raiseFilterChanged(this.columnId, this.filterData.propertyName, filterData, this.filterKey);
    };
    __decorate([
        core_1.ViewChild('customTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], DateTimeColumnFilterComponent.prototype, "containerRef", void 0);
    DateTimeColumnFilterComponent = __decorate([
        core_1.Component({
            selector: 'mg-date-time-column-filter',
            templateUrl: './date-time-column-filter.component.html',
            animations: [
                animations_1.filterConditionMenuAnimation
            ]
        }),
        __metadata("design:paramtypes", [filter_state_service_1.FilterStateService])
    ], DateTimeColumnFilterComponent);
    return DateTimeColumnFilterComponent;
}(filter_component_1.FilterComponent));
exports.DateTimeColumnFilterComponent = DateTimeColumnFilterComponent;
//# sourceMappingURL=date-time-column-filter.component.js.map