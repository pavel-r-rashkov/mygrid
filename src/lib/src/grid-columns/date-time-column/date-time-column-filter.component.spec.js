"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var my_grid_module_1 = require("../../my-grid.module");
var date_time_column_filter_component_1 = require("./date-time-column-filter.component");
var filter_state_service_1 = require("../../filter-state.service");
var date_time_search_term_type_1 = require("./date-time-search-term-type");
var TestFilterComponent = (function () {
    function TestFilterComponent() {
    }
    __decorate([
        core_1.ViewChild('filterTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestFilterComponent.prototype, "filterTemplate", void 0);
    TestFilterComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-filter',
            template: "\n    <ng-template #filterTemplate>\n      <div id=\"test-filter-template\"></div>\n    </ng-template>\n  "
        })
    ], TestFilterComponent);
    return TestFilterComponent;
}());
describe('Component: DateTimeColumnFilterComponent', function () {
    var dateTimeColumnFilterComponent;
    var fixture;
    var filterTemplate;
    var filterStateService;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [
                filter_state_service_1.FilterStateService
            ],
            declarations: [
                TestFilterComponent
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(date_time_column_filter_component_1.DateTimeColumnFilterComponent);
            dateTimeColumnFilterComponent = fixture.componentInstance;
            var templateComponent = testing_1.TestBed.createComponent(TestFilterComponent).componentInstance;
            filterTemplate = templateComponent.filterTemplate;
            filterStateService = fixture.debugElement.injector.get(filter_state_service_1.FilterStateService);
        });
    }));
    it('custom filter template should be rendered when it exists', function () {
        var data = {
            filterTemplate: filterTemplate,
            enableFiltering: true
        };
        dateTimeColumnFilterComponent.filterData = data;
        fixture.detectChanges();
        var customTemplate = fixture.debugElement.query(platform_browser_1.By.css('#test-filter-template'));
        expect(customTemplate).not.toBeNull();
        expect(customTemplate.context.$implicit.filterData).toEqual(data);
        expect(customTemplate.context.$implicit.columnId).toEqual(dateTimeColumnFilterComponent.columnId);
        expect(customTemplate.context.$implicit.filter).toEqual(dateTimeColumnFilterComponent.customTemplateFilter);
    });
    it('changing filter state should update filterDate', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        var filterDate = new Date();
        fixture.detectChanges();
        filterStateService.raiseFilterChanged(columnId, propertyName, { selectedDate: filterDate }, '');
        fixture.whenStable().then(function () {
            expect(dateTimeColumnFilterComponent.filterDate).toEqual(filterDate);
        });
    });
    it('toggleMenu should show/hide menu', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        var menuVisible = false;
        dateTimeColumnFilterComponent.menuVisible = menuVisible;
        fixture.detectChanges();
        dateTimeColumnFilterComponent.toggleMenu();
        expect(dateTimeColumnFilterComponent.menuVisible).toEqual(!menuVisible);
    });
    it('getConditionMenuState should return opened when menu is visible', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        dateTimeColumnFilterComponent.menuVisible = true;
        fixture.detectChanges();
        var menuState = dateTimeColumnFilterComponent.getConditionMenuState();
        expect(menuState).toEqual('opened');
    });
    it('getConditionMenuState should return hidden when menu is not visible', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        dateTimeColumnFilterComponent.menuVisible = false;
        fixture.detectChanges();
        var menuState = dateTimeColumnFilterComponent.getConditionMenuState();
        expect(menuState).toEqual('hidden');
    });
    it('buttonFocusedOut should hide the menu', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        dateTimeColumnFilterComponent.menuVisible = true;
        fixture.detectChanges();
        dateTimeColumnFilterComponent.buttonFocusedOut();
        expect(dateTimeColumnFilterComponent.menuVisible).toBeFalsy();
    });
    it('customTemplateFilter should create new filter state', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        var templateFilterData = {};
        var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        dateTimeColumnFilterComponent.customTemplateFilter(templateFilterData);
        expect(raiseFilterChangeSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangeSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2]).toEqual(templateFilterData);
    });
    it('filterBlur should not create new filter state when filter date is not changed', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        dateTimeColumnFilterComponent.filterDate = new Date();
        var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');
        var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');
        fixture.detectChanges();
        dateTimeColumnFilterComponent.filterFocusedOut({});
        dateTimeColumnFilterComponent.filterBlur();
        expect(raiseFilterChangeSpy).not.toHaveBeenCalled();
        expect(clearColumnFilterSpy).not.toHaveBeenCalled();
    });
    it('filterBlur should clear filter for this column when the selected date is null or empty', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');
        fixture.detectChanges();
        dateTimeColumnFilterComponent.filterDate = new Date();
        dateTimeColumnFilterComponent.filterFocusedOut({});
        dateTimeColumnFilterComponent.filterDate = null;
        dateTimeColumnFilterComponent.filterBlur();
        expect(clearColumnFilterSpy).toHaveBeenCalled();
        expect(clearColumnFilterSpy.calls.first().args[0]).toEqual(dateTimeColumnFilterComponent.columnId);
    });
    it('filterBlur should create new filter state when selected date is not null or empty', function () {
        var propertyName = 'foo';
        var searchTermType = date_time_search_term_type_1.DateTimeSearchTermType.MoreThanOrEqual;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        var filterDate = new Date();
        dateTimeColumnFilterComponent.filterDate = filterDate;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        dateTimeColumnFilterComponent.filterBlur();
        expect(raiseFilterChangedSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangedSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2].selectedDate).toEqual(filterDate);
        expect(filterCall.args[2].searchTermType).toEqual(searchTermType);
    });
    it('selectedSearchType should be set on init', function () {
        var propertyName = 'foo';
        var searchTermType = date_time_search_term_type_1.DateTimeSearchTermType.MoreThanOrEqual;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        fixture.detectChanges();
        expect(dateTimeColumnFilterComponent.selectedSearchType).toEqual(searchTermType);
    });
    it('selectSearchType should not create new filter state when there is no change in the selected search type', function () {
        var propertyName = 'foo';
        var searchTermType = date_time_search_term_type_1.DateTimeSearchTermType.MoreThanOrEqual;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        dateTimeColumnFilterComponent.filterDate = new Date();
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        dateTimeColumnFilterComponent.selectSearchType(searchTermType);
        expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
    });
    it('selectSearchType should not create new filter state when there is no filter date selected', function () {
        var propertyName = 'foo';
        var searchTermType = date_time_search_term_type_1.DateTimeSearchTermType.MoreThanOrEqual;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        dateTimeColumnFilterComponent.selectSearchType(4);
        expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
    });
    it('selectSearchType should create new filter state when there is filter date selected and the search term type is changed', function () {
        var propertyName = 'foo';
        var searchTermType = date_time_search_term_type_1.DateTimeSearchTermType.MoreThanOrEqual;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        dateTimeColumnFilterComponent.filterData = data;
        var columnId = '123';
        dateTimeColumnFilterComponent.columnId = columnId;
        var filterDate = new Date();
        dateTimeColumnFilterComponent.filterDate = filterDate;
        var selectedSearchTermType = 4;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        dateTimeColumnFilterComponent.selectSearchType(selectedSearchTermType);
        expect(raiseFilterChangedSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangedSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2].selectedDate).toEqual(filterDate);
        expect(filterCall.args[2].searchTermType).toEqual(selectedSearchTermType);
    });
});
//# sourceMappingURL=date-time-column-filter.component.spec.js.map