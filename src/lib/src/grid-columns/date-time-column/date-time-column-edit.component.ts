import { 
  Component, 
  OnInit,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { EditComponent } from '../contracts/edit-component';
import { DateTimeType } from './date-time-type';

@Component({
  selector: 'mg-date-time-column-edit',
  templateUrl: './date-time-column-edit.component.html'
})
export class DateTimeColumnEditComponent extends EditComponent implements OnInit {
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;

  constructor() {
    super();
  }
  
  ngOnInit() {
    if (this.data.editTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item,
        editForm: this.editForm
      };
      this.containerRef.createEmbeddedView(this.data.editTemplate, { $implicit: templateContext });
    }
  }

  getValue() {
    if (this.item == null || !this.item[this.data.propertyName]) {
      return null;
    }

    var date: Date = this.item[this.data.propertyName];

    var year: string = date.getFullYear().toString();
    var month: string = (date.getMonth() + 1).toString();
    var day: string = date.getDate().toString();
    var dateString: string = year + '-' + this.padStart(month, 2, '0') + '-' + this.padStart(day, 2, '0');

    var hours = date.getHours().toString();
    var minutes = date.getMinutes().toString();
    var seconds = date.getSeconds().toString();
    var milliseconds = date.getMilliseconds().toString();
    var timeString: string = this.padStart(hours, 2, '0') + ':' + this.padStart(minutes, 2, '0');

    if (this.data.step < 60) {
      timeString += ':' + this.padStart(seconds, 2, '0');
    }

    if (this.data.step < 1) {
      timeString += '.' + this.padStart(milliseconds, 3, '0');
    }
      
    var formatted: string = '';
    switch (this.data.type) {
      case 'date':
        formatted = dateString;
        break;
      case 'time':
        formatted = timeString;
        break;
      case 'datetime-local':
        formatted = dateString + 'T' + timeString;
        break;
    }

    return formatted;
  }

  private padStart(target: string, length: number, fill: string) {
    if (target.length >= length) {
      return target;
    }

    var fillLength = length - target.length;
    while (fillLength > fill.length) {
      fill += fill;
    }

    var truncatedFill: string = fill.substr(0, fillLength);
    return truncatedFill + target;
  }
}