import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { DateTimeColumnFilterComponent } from './date-time-column-filter.component';
import { FilterStateService } from '../../filter-state.service';
import { FilterState } from '../../states/filter-state';
import { DateTimeSearchTermType } from './date-time-search-term-type';

@Component({
  selector: 'mgt-test-filter',
  template: `
    <ng-template #filterTemplate>
      <div id="test-filter-template"></div>
    </ng-template>
  `
})
class TestFilterComponent {
  @ViewChild('filterTemplate') filterTemplate: TemplateRef<any>;
}

describe('Component: DateTimeColumnFilterComponent', () => {
  let dateTimeColumnFilterComponent: DateTimeColumnFilterComponent;
  let fixture: ComponentFixture<DateTimeColumnFilterComponent>;
  let filterTemplate: TemplateRef<any>;
  let filterStateService: FilterStateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        providers: [
          FilterStateService
        ],
        declarations: [
          TestFilterComponent
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(DateTimeColumnFilterComponent);
        dateTimeColumnFilterComponent = fixture.componentInstance;

        var templateComponent: TestFilterComponent = TestBed.createComponent(TestFilterComponent).componentInstance;
        filterTemplate = templateComponent.filterTemplate;

        filterStateService = fixture.debugElement.injector.get(FilterStateService);
      });
  }));
  
  it('custom filter template should be rendered when it exists', () => {
    var data = {
      filterTemplate: filterTemplate,
      enableFiltering: true
    };
    dateTimeColumnFilterComponent.filterData = data;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-filter-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.filterData).toEqual(data);
    expect(customTemplate.context.$implicit.columnId).toEqual(dateTimeColumnFilterComponent.columnId);
    expect(customTemplate.context.$implicit.filter).toEqual(dateTimeColumnFilterComponent.customTemplateFilter);
  });

  it('changing filter state should update filterDate', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    var filterDate = new Date();

    fixture.detectChanges();
    filterStateService.raiseFilterChanged(columnId, propertyName, { selectedDate: filterDate }, '');

    fixture.whenStable().then(() => {
      expect(dateTimeColumnFilterComponent.filterDate).toEqual(filterDate);
    });
  });

  it('toggleMenu should show/hide menu', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    var menuVisible = false;
    dateTimeColumnFilterComponent.menuVisible = menuVisible;

    fixture.detectChanges();
    dateTimeColumnFilterComponent.toggleMenu();    

    expect(dateTimeColumnFilterComponent.menuVisible).toEqual(!menuVisible);
  });

  it('getConditionMenuState should return opened when menu is visible', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    dateTimeColumnFilterComponent.menuVisible = true;

    fixture.detectChanges();
    var menuState = dateTimeColumnFilterComponent.getConditionMenuState();    

    expect(menuState).toEqual('opened');
  });

  it('getConditionMenuState should return hidden when menu is not visible', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    dateTimeColumnFilterComponent.menuVisible = false;

    fixture.detectChanges();
    var menuState = dateTimeColumnFilterComponent.getConditionMenuState();    

    expect(menuState).toEqual('hidden');
  });

  it('buttonFocusedOut should hide the menu', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    dateTimeColumnFilterComponent.menuVisible = true;

    fixture.detectChanges();
    dateTimeColumnFilterComponent.buttonFocusedOut();

    expect(dateTimeColumnFilterComponent.menuVisible).toBeFalsy();
  });

  it('customTemplateFilter should create new filter state', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    var templateFilterData = {};
    var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    dateTimeColumnFilterComponent.customTemplateFilter(templateFilterData);

    expect(raiseFilterChangeSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangeSpy.calls.first();
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2]).toEqual(templateFilterData);
  });

  it('filterBlur should not create new filter state when filter date is not changed', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    dateTimeColumnFilterComponent.filterDate = new Date();
    var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');
    var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');

    fixture.detectChanges();
    dateTimeColumnFilterComponent.filterFocusedOut({});
    dateTimeColumnFilterComponent.filterBlur();

    expect(raiseFilterChangeSpy).not.toHaveBeenCalled();
    expect(clearColumnFilterSpy).not.toHaveBeenCalled();
  });

  it('filterBlur should clear filter for this column when the selected date is null or empty', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');

    fixture.detectChanges();
    dateTimeColumnFilterComponent.filterDate = new Date();
    dateTimeColumnFilterComponent.filterFocusedOut({});
    dateTimeColumnFilterComponent.filterDate = null;
    dateTimeColumnFilterComponent.filterBlur();

    expect(clearColumnFilterSpy).toHaveBeenCalled();
    expect(clearColumnFilterSpy.calls.first().args[0]).toEqual(dateTimeColumnFilterComponent.columnId);
  });

  it('filterBlur should create new filter state when selected date is not null or empty', () => {
    var propertyName = 'foo';
    var searchTermType = DateTimeSearchTermType.MoreThanOrEqual;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    var filterDate = new Date();
    dateTimeColumnFilterComponent.filterDate = filterDate;
    var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    dateTimeColumnFilterComponent.filterBlur();

    expect(raiseFilterChangedSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangedSpy.calls.first()
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2].selectedDate).toEqual(filterDate);
    expect(filterCall.args[2].searchTermType).toEqual(searchTermType);
  });

  it('selectedSearchType should be set on init', () => {
    var propertyName = 'foo';
    var searchTermType = DateTimeSearchTermType.MoreThanOrEqual;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    
    fixture.detectChanges();
    
    expect(dateTimeColumnFilterComponent.selectedSearchType).toEqual(searchTermType);
  });

  it('selectSearchType should not create new filter state when there is no change in the selected search type', () => {
    var propertyName = 'foo';
    var searchTermType = DateTimeSearchTermType.MoreThanOrEqual;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    dateTimeColumnFilterComponent.filterDate = new Date();
    var raiseFilterChangedSpy: jasmine.Spy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    dateTimeColumnFilterComponent.selectSearchType(searchTermType);

    expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
  });

  it('selectSearchType should not create new filter state when there is no filter date selected', () => {
    var propertyName = 'foo';
    var searchTermType = DateTimeSearchTermType.MoreThanOrEqual;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    var raiseFilterChangedSpy: jasmine.Spy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    dateTimeColumnFilterComponent.selectSearchType(4);

    expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
  });

  it('selectSearchType should create new filter state when there is filter date selected and the search term type is changed', () => {
    var propertyName = 'foo';
    var searchTermType = DateTimeSearchTermType.MoreThanOrEqual;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    dateTimeColumnFilterComponent.filterData = data;
    var columnId = '123';
    dateTimeColumnFilterComponent.columnId = columnId;
    var filterDate = new Date();
    dateTimeColumnFilterComponent.filterDate = filterDate;
    var selectedSearchTermType = 4;
    var raiseFilterChangedSpy: jasmine.Spy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    dateTimeColumnFilterComponent.selectSearchType(selectedSearchTermType);

    expect(raiseFilterChangedSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangedSpy.calls.first();
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2].selectedDate).toEqual(filterDate);
    expect(filterCall.args[2].searchTermType).toEqual(selectedSearchTermType);
  });
});