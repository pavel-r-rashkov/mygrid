"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var grid_column_1 = require("../grid-column");
var default_column_header_component_1 = require("../shared/default-column-header.component");
var date_time_column_filter_component_1 = require("./date-time-column-filter.component");
var date_time_column_data_component_1 = require("./date-time-column-data.component");
var date_time_column_edit_component_1 = require("./date-time-column-edit.component");
var guid_1 = require("../../guid");
var default_comparer_service_1 = require("../shared/default-comparer.service");
var date_time_search_term_type_1 = require("./date-time-search-term-type");
var DateTimeColumnDirective = (function (_super) {
    __extends(DateTimeColumnDirective, _super);
    function DateTimeColumnDirective(guidService, comparer) {
        var _this = _super.call(this, default_column_header_component_1.DefaultColumnHeaderComponent, date_time_column_filter_component_1.DateTimeColumnFilterComponent, date_time_column_data_component_1.DateTimeColumnDataComponent, date_time_column_edit_component_1.DateTimeColumnEditComponent, guidService.newGuid()) || this;
        _this.guidService = guidService;
        _this.comparer = comparer;
        _this.type = 'date';
        _this.step = 1;
        return _this;
    }
    DateTimeColumnDirective_1 = DateTimeColumnDirective;
    DateTimeColumnDirective.prototype.ngOnInit = function () {
        if (!this.format) {
            switch (this.type) {
                case 'time':
                    this.format = 'hh:mm:ss';
                    break;
                case 'date':
                    this.format = 'dd/MM/y';
                    break;
                case 'datetime-local':
                    this.format = 'dd/MM/y hh:mm:ss';
                    break;
                default:
                    console.error('Unrecognized date time type', this.format);
                    break;
            }
        }
    };
    DateTimeColumnDirective.prototype.getHeaderData = function () {
        return {
            caption: this.caption,
            headerTemplate: this.headerTemplate,
            propertyName: this.propertyName,
            enableOrdering: this.enableOrdering == null ? true : this.enableOrdering,
            enableGrouping: this.enableGrouping == null ? true : this.enableGrouping
        };
    };
    DateTimeColumnDirective.prototype.getData = function () {
        return {
            propertyName: this.propertyName,
            dataTemplate: this.dataTemplate,
            enableEditing: this.enableEditing == null ? true : this.enableEditing,
            editTemplate: this.editTemplate,
            validationErrorsTemplate: this.validationErrorsTemplate,
            format: this.format,
            type: this.type,
            step: this.step
        };
    };
    DateTimeColumnDirective.prototype.getFilterData = function () {
        return {
            filterTemplate: this.filterTemplate,
            propertyName: this.propertyName,
            enableFiltering: this.enableFiltering == null ? true : this.enableFiltering,
            enableSearchTermTypeMenu: this.enableSearchTermTypeMenu == null ? true : this.enableSearchTermTypeMenu,
            searchTermType: this.searchTermType == null ? date_time_search_term_type_1.DateTimeSearchTermType.Equals : this.searchTermType,
            type: this.type,
            step: this.step
        };
    };
    DateTimeColumnDirective.prototype.getGroupData = function () {
        var groupData = this.getData();
        groupData.dataTemplate = this.groupHeaderTemplate;
        return groupData;
    };
    DateTimeColumnDirective.prototype.filter = function (val, filterData, item) {
        if (this.customFilter != null) {
            return this.customFilter(val, filterData, item);
        }
        if (filterData != null && filterData.selectedDate != null && filterData.selectedDate != '') {
            if (val != null) {
                val = this.convertToComparableDateTime(new Date(val));
                var selectedDate = this.convertToComparableDateTime(this.convertToDateTime(filterData.selectedDate));
                switch (filterData.searchTermType) {
                    case date_time_search_term_type_1.DateTimeSearchTermType.LessThan:
                        return val < selectedDate;
                    case date_time_search_term_type_1.DateTimeSearchTermType.LessThanOrEqual:
                        return val <= selectedDate;
                    case date_time_search_term_type_1.DateTimeSearchTermType.Equals:
                        return val.valueOf() == selectedDate.valueOf();
                    case date_time_search_term_type_1.DateTimeSearchTermType.MoreThanOrEqual:
                        return val >= selectedDate;
                    case date_time_search_term_type_1.DateTimeSearchTermType.MoreThan:
                        return val > selectedDate;
                    default:
                        throw new Error('unrecognized search type');
                }
            }
            else {
                return false;
            }
        }
        return true;
    };
    DateTimeColumnDirective.prototype.order = function (firstValue, secondValue, isAscending, firstItem, secondItem) {
        if (this.customOrder != null) {
            return this.customOrder(firstValue, secondValue, isAscending, firstItem, secondItem);
        }
        else {
            return this.comparer.compare(this.convertToComparableDateTime(firstValue), this.convertToComparableDateTime(secondValue), isAscending);
        }
    };
    DateTimeColumnDirective.prototype.group = function (firstValue, secondValue, isAscending, firstItem, secondItem) {
        if (this.customGroup != null) {
            return this.customGroup(firstValue, secondValue, isAscending, firstItem, secondItem);
        }
        else {
            return this.comparer.compare(this.convertToComparableDateTime(firstValue), this.convertToComparableDateTime(secondValue), isAscending);
        }
    };
    DateTimeColumnDirective.prototype.getValue = function (item) {
        if (item[this.propertyName] == undefined) {
            return null;
        }
        return item[this.propertyName];
    };
    DateTimeColumnDirective.prototype.convertToComparableDateTime = function (date) {
        if (!date) {
            return date;
        }
        var baseDate = new Date(1990, 5, 6, 0, 0, 0, 0);
        if (this.type == 'date' || this.type == 'datetime-local') {
            baseDate.setFullYear(date.getFullYear());
            baseDate.setMonth(date.getMonth());
            baseDate.setDate(date.getDate());
        }
        if (this.type == 'time' || this.type == 'datetime-local') {
            baseDate.setHours(date.getHours());
            if (this.step < 3600) {
                baseDate.setMinutes(date.getMinutes());
            }
            if (this.step < 60) {
                baseDate.setSeconds(date.getSeconds());
            }
            if (this.step < 1) {
                baseDate.setMilliseconds(date.getMilliseconds());
            }
        }
        return baseDate;
    };
    DateTimeColumnDirective.prototype.convertToDateTime = function (dateString) {
        if (this.type == 'date' || this.type == 'datetime-local') {
            return new Date(dateString);
        }
        var timeParts = dateString.split(/[\:\.]+/);
        var date = new Date(1990, 5, 6, 0, 0, 0, 0);
        date.setHours(parseInt(this.trimLeft(timeParts[0], '0')));
        if (this.step < 3600) {
            date.setMinutes(parseInt(this.trimLeft(timeParts[1], '0')));
        }
        if (this.step < 60) {
            date.setSeconds(parseInt(this.trimLeft(timeParts[2], '0')));
        }
        if (this.step < 1) {
            date.setMilliseconds(parseInt(this.trimLeft(timeParts[3], '0')));
        }
        return date;
    };
    DateTimeColumnDirective.prototype.trimLeft = function (target, charsToRemove) {
        return target.replace(new RegExp('^[' + charsToRemove + ']+'), '');
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], DateTimeColumnDirective.prototype, "propertyName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], DateTimeColumnDirective.prototype, "enableOrdering", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], DateTimeColumnDirective.prototype, "enableFiltering", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], DateTimeColumnDirective.prototype, "enableEditing", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], DateTimeColumnDirective.prototype, "enableGrouping", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], DateTimeColumnDirective.prototype, "enableSearchTermTypeMenu", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], DateTimeColumnDirective.prototype, "searchTermType", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], DateTimeColumnDirective.prototype, "customFilter", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], DateTimeColumnDirective.prototype, "customOrder", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], DateTimeColumnDirective.prototype, "customGroup", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], DateTimeColumnDirective.prototype, "format", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], DateTimeColumnDirective.prototype, "type", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], DateTimeColumnDirective.prototype, "step", void 0);
    DateTimeColumnDirective = DateTimeColumnDirective_1 = __decorate([
        core_1.Directive({
            selector: 'mg-date-time-column',
            providers: [{ provide: grid_column_1.GridColumn, useExisting: core_1.forwardRef(function () { return DateTimeColumnDirective_1; }) }]
        }),
        __metadata("design:paramtypes", [guid_1.GuidService,
            default_comparer_service_1.DefaultComparerService])
    ], DateTimeColumnDirective);
    return DateTimeColumnDirective;
    var DateTimeColumnDirective_1;
}(grid_column_1.GridColumn));
exports.DateTimeColumnDirective = DateTimeColumnDirective;
//# sourceMappingURL=date-time-column.directive.js.map