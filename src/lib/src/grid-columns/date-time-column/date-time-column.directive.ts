import { 
  Directive,
  TemplateRef,
  Input,
  ContentChild,
  forwardRef,
  OnInit
} from '@angular/core';

import { GridColumn } from '../grid-column';
import { DefaultColumnHeaderComponent } from '../shared/default-column-header.component';
import { DateTimeColumnFilterComponent } from './date-time-column-filter.component';
import { DateTimeColumnDataComponent } from './date-time-column-data.component';
import { DateTimeColumnEditComponent } from './date-time-column-edit.component';
import { GuidService } from '../../guid';
import { DefaultComparerService } from '../shared/default-comparer.service';
import { DateTimeSearchTermType } from './date-time-search-term-type';
import { DateTimeType } from './date-time-type';

@Directive({
  selector: 'mg-date-time-column',
  providers: [{provide: GridColumn, useExisting: forwardRef(() => DateTimeColumnDirective)}]
})
export class DateTimeColumnDirective extends GridColumn implements OnInit {
  @Input() propertyName: string;
  @Input() enableOrdering: boolean;
  @Input() enableFiltering: boolean;
  @Input() enableEditing: boolean;
  @Input() enableGrouping: boolean;
  @Input() enableSearchTermTypeMenu: boolean;
  @Input() searchTermType: DateTimeSearchTermType;
  @Input() customFilter: (val: any, filterData: any, item: any) => boolean;
  @Input() customOrder: (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) => number;
  @Input() customGroup: (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) => number;
  @Input() format: string;
  @Input() type: DateTimeType = 'date';
  @Input() step: number = 1;

  constructor(
    private guidService: GuidService,
    private comparer: DefaultComparerService) {
    super(
      DefaultColumnHeaderComponent,
      DateTimeColumnFilterComponent,
      DateTimeColumnDataComponent,
      DateTimeColumnEditComponent,
      guidService.newGuid());
  }

  ngOnInit() {
    if (!this.format) {
      switch (this.type) {
        case 'time':
          this.format = 'hh:mm:ss';
          break;
        case 'date':
          this.format = 'dd/MM/y';
          break;
        case 'datetime-local':
          this.format = 'dd/MM/y hh:mm:ss';
          break;
        default:
          console.error('Unrecognized date time type', this.format);
          break;
      }
    }
  }

  public getHeaderData(): any {
    return {
      caption: this.caption,
      headerTemplate: this.headerTemplate,
      propertyName: this.propertyName,
      enableOrdering: this.enableOrdering == null ? true : this.enableOrdering,
      enableGrouping: this.enableGrouping == null ? true : this.enableGrouping
    };
  }  

  public getData(): any {
    return {
      propertyName: this.propertyName,
      dataTemplate: this.dataTemplate,
      enableEditing: this.enableEditing == null ? true : this.enableEditing,
      editTemplate: this.editTemplate,
      validationErrorsTemplate: this.validationErrorsTemplate,
      format: this.format,
      type: this.type,
      step: this.step
    }
  }

  public getFilterData(): any {
    return {
      filterTemplate: this.filterTemplate,
      propertyName: this.propertyName,
      enableFiltering: this.enableFiltering == null ? true : this.enableFiltering,
      enableSearchTermTypeMenu: this.enableSearchTermTypeMenu == null ? true : this.enableSearchTermTypeMenu,
      searchTermType: this.searchTermType == null ? DateTimeSearchTermType.Equals : this.searchTermType,
      type: this.type,
      step: this.step
    }
  }

  public getGroupData(): any {
    let groupData = this.getData();
    groupData.dataTemplate = this.groupHeaderTemplate;
    return groupData;
  }

  public filter(val: any, filterData: any, item: any): boolean {
    if (this.customFilter != null) {
      return this.customFilter(val, filterData, item);
    }

    if (filterData != null && filterData.selectedDate != null && filterData.selectedDate != '') {
      if (val != null) {
        val = this.convertToComparableDateTime(new Date(val));
        let selectedDate = this.convertToComparableDateTime(this.convertToDateTime(filterData.selectedDate));
        
        switch (filterData.searchTermType) {
          case DateTimeSearchTermType.LessThan:
            return val < selectedDate;
          case DateTimeSearchTermType.LessThanOrEqual:
            return val <= selectedDate;
          case DateTimeSearchTermType.Equals:
            return val.valueOf() == selectedDate.valueOf();
          case DateTimeSearchTermType.MoreThanOrEqual:
            return val >= selectedDate;
          case DateTimeSearchTermType.MoreThan:
            return val > selectedDate;
          default:
            throw new Error('unrecognized search type');
        }
      } else {
        return false;
      }
    }

    return true;
  }

  public order(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    if (this.customOrder != null) {
      return this.customOrder(firstValue, secondValue, isAscending, firstItem, secondItem);
    } else {
      return this.comparer.compare(
        this.convertToComparableDateTime(firstValue),
        this.convertToComparableDateTime(secondValue),
        isAscending);
    }
  }

  public group(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    if (this.customGroup != null) {
      return this.customGroup(firstValue, secondValue, isAscending, firstItem, secondItem);
    } else {
      return this.comparer.compare(
        this.convertToComparableDateTime(firstValue), 
        this.convertToComparableDateTime(secondValue),
        isAscending);
    }
  }

  public getValue(item: any) {
    if (item[this.propertyName] == undefined) {
      return null;
    }

    return item[this.propertyName];
  }

  private convertToComparableDateTime(date: Date): Date {
    if (!date) {
      return date;
    }

    var baseDate = new Date(1990, 5, 6, 0, 0, 0, 0);
    
    if (this.type == 'date' || this.type == 'datetime-local') {
      baseDate.setFullYear(date.getFullYear());
      baseDate.setMonth(date.getMonth());
      baseDate.setDate(date.getDate());
    }

    if (this.type == 'time' || this.type == 'datetime-local') {
      baseDate.setHours(date.getHours());
      
      if (this.step < 3600) {
        baseDate.setMinutes(date.getMinutes());
      }

      if (this.step < 60) {
        baseDate.setSeconds(date.getSeconds());
      }

      if (this.step < 1) {
        baseDate.setMilliseconds(date.getMilliseconds());
      }
    }

    return baseDate;
  }

  private convertToDateTime(dateString: string): Date {
    if (this.type == 'date' || this.type == 'datetime-local') {
      return new Date(dateString);
    }

    var timeParts: string[] = dateString.split(/[\:\.]+/);
    var date = new Date(1990, 5, 6, 0, 0, 0, 0);
    date.setHours(parseInt(this.trimLeft(timeParts[0], '0')));

    if (this.step < 3600) {
      date.setMinutes(parseInt(this.trimLeft(timeParts[1], '0')));
    }

    if (this.step < 60) {
      date.setSeconds(parseInt(this.trimLeft(timeParts[2], '0')));
    }

    if (this.step < 1) {
      date.setMilliseconds(parseInt(this.trimLeft(timeParts[3], '0')));
    }

    return date;
  }

  private trimLeft(target: string, charsToRemove: string): string {
    return target.replace( new RegExp('^[' + charsToRemove + ']+'), '');
  }
}