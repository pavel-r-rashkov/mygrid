import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { DateTimeColumnDirective } from './date-time-column.directive';
import { GuidService } from '../../guid';
import { DefaultComparerService } from '../shared/default-comparer.service';
import { DateTimeSearchTermType } from './date-time-search-term-type';
import { DateTimeType } from './date-time-type';

@Component({
  selector: 'mgt-test-text-column',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
    <ng-template #editTemplate>
      <div id="test-edit-template"></div>
    </ng-template>
    <ng-template #validationErrorsTemplate>
      <div id="test-validation-error-template"></div>
    </ng-template>
    <ng-template #filterTemplate>
      <div id="test-filter-template"></div>
    </ng-template>
    <ng-template #groupHeaderTemplate>
      <div id="test-group-header-template"></div>
    </ng-template>
  `
}) 
class TestDateTimeColumnComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
  @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
  @ViewChild('validationErrorsTemplate') validationErrorsTemplate: TemplateRef<any>;
  @ViewChild('filterTemplate') filterTemplate: TemplateRef<any>;
  @ViewChild('groupHeaderTemplate') groupHeaderTemplate: TemplateRef<any>;
}

describe('Directive: DateTimeColumnDirective', () => {
  let dateTimeColumnDirective: DateTimeColumnDirective;
  let comparer: DefaultComparerService;
  let dataTemplate: TemplateRef<any>;
  let headerTemplate: TemplateRef<any>;
  let editTemplate: TemplateRef<any>;
  let validationErrorsTemplate: TemplateRef<any>;
  let filterTemplate: TemplateRef<any>;
  let groupHeaderTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestDateTimeColumnComponent
        ]
      })
      .compileComponents()
      .then(() => {
        comparer = new DefaultComparerService();
        dateTimeColumnDirective = new DateTimeColumnDirective(new GuidService(), comparer);

        var testComponent = TestBed.createComponent(TestDateTimeColumnComponent).componentInstance;
        dataTemplate = testComponent.dataTemplate;
        headerTemplate = testComponent.headerTemplate;
        editTemplate = testComponent.editTemplate;
        validationErrorsTemplate = testComponent.validationErrorsTemplate;
        filterTemplate = testComponent.filterTemplate;
        groupHeaderTemplate = testComponent.groupHeaderTemplate;
      });
  }));

  it('getHeaderData should return date time column header data', () => {
    var caption = 'foo';
    dateTimeColumnDirective.caption = caption;
    dateTimeColumnDirective.headerTemplate = headerTemplate;
    var propertyName = 'bar';
    dateTimeColumnDirective.propertyName = propertyName;
    var orderingEnabled = false;
    dateTimeColumnDirective.enableOrdering = orderingEnabled;
    var groupingEnabled = false;
    dateTimeColumnDirective.enableGrouping = groupingEnabled;

    var headerData: any = dateTimeColumnDirective.getHeaderData();

    expect(headerData.caption).toEqual(caption);
    expect(headerData.headerTemplate).toEqual(headerTemplate);
    expect(headerData.propertyName).toEqual(propertyName);
    expect(headerData.enableOrdering).toEqual(orderingEnabled);
    expect(headerData.enableGrouping).toEqual(groupingEnabled);
  });

  it('ordering and grouping should be enabled by default', () => {
    var headerData: any = dateTimeColumnDirective.getHeaderData();

    expect(headerData.enableOrdering).toBeTruthy();
    expect(headerData.enableGrouping).toBeTruthy();
  });

  it('default format when type is time should be hh:mm:ss', () => {
    dateTimeColumnDirective.type = 'time';
    dateTimeColumnDirective.ngOnInit();
    
    expect(dateTimeColumnDirective.format).toEqual('hh:mm:ss');
  });

  it('default format when type is date should be dd/MM/y', () => {
    dateTimeColumnDirective.type = 'date';
    dateTimeColumnDirective.ngOnInit();
    
    expect(dateTimeColumnDirective.format).toEqual('dd/MM/y');
  });

  it('default format when type is datetime-local should be dd/MM/y hh:mm:ss', () => {
    dateTimeColumnDirective.type = 'datetime-local';
    dateTimeColumnDirective.ngOnInit();
    
    expect(dateTimeColumnDirective.format).toEqual('dd/MM/y hh:mm:ss');
  });

  it('getData should return date time column data', () => {
    dateTimeColumnDirective.dataTemplate = dataTemplate;
    dateTimeColumnDirective.editTemplate = editTemplate;
    dateTimeColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
    var propertyName = 'bar';
    dateTimeColumnDirective.propertyName = propertyName;
    var editionEnabled = false;
    dateTimeColumnDirective.enableEditing = editionEnabled;
    var format = 'MM-yyyy';
    dateTimeColumnDirective.format = format;
    var type: DateTimeType = 'time';
    dateTimeColumnDirective.type = type;
    var step = 0.1;
    dateTimeColumnDirective.step = step;

    var data: any = dateTimeColumnDirective.getData();

    expect(data.dataTemplate).toEqual(dataTemplate);
    expect(data.editTemplate).toEqual(editTemplate);
    expect(data.validationErrorsTemplate).toEqual(validationErrorsTemplate);
    expect(data.propertyName).toEqual(propertyName);
    expect(data.enableEditing).toEqual(editionEnabled);
    expect(data.format).toEqual(format);
    expect(data.type).toEqual(type);
    expect(data.step).toEqual(step);
  });

  it('editing should be enabled by default, default step should be 1, default type should be date', () => {
    dateTimeColumnDirective.ngOnInit();
    var data: any = dateTimeColumnDirective.getData();

    expect(data.enableEditing).toBeTruthy();
    expect(data.step).toEqual(1);
    expect(data.type).toEqual('date');
  });

  it('getFilterData should return date time column filter data', () => {
    dateTimeColumnDirective.filterTemplate = filterTemplate;
    var propertyName = 'bar';
    dateTimeColumnDirective.propertyName = propertyName;
    var filteringEnabled = false;
    dateTimeColumnDirective.enableFiltering = filteringEnabled;
    var searchTermTypeMenuEnabled = false;
    dateTimeColumnDirective.enableSearchTermTypeMenu = searchTermTypeMenuEnabled;
    var searchTermType = DateTimeSearchTermType.LessThan;
    dateTimeColumnDirective.searchTermType = searchTermType;
    var type: DateTimeType = 'time';
    dateTimeColumnDirective.type = type;
    var step = 0.1;
    dateTimeColumnDirective.step = step;

    var filterData: any = dateTimeColumnDirective.getFilterData();

    expect(filterData.filterTemplate).toEqual(filterTemplate);
    expect(filterData.propertyName).toEqual(propertyName);
    expect(filterData.enableFiltering).toEqual(filteringEnabled);
    expect(filterData.enableSearchTermTypeMenu).toEqual(searchTermTypeMenuEnabled);
    expect(filterData.searchTermType).toEqual(searchTermType);
    expect(filterData.type).toEqual(type);
    expect(filterData.step).toEqual(step);
  });

  it(`filtering should be enabled by default, search term type default option should be equals, 
    search term type menu should be enabled by default, default step should be 1, 
    default type should be date`, () => {
    var filterData: any = dateTimeColumnDirective.getFilterData();

    expect(filterData.enableFiltering).toBeTruthy();
    expect(filterData.enableSearchTermTypeMenu).toBeTruthy();
    expect(filterData.searchTermType).toEqual(DateTimeSearchTermType.Equals);
    expect(filterData.step).toEqual(1);
    expect(filterData.type).toEqual('date');
  });

  it('getGroupData should return date time column group data', () => {
    dateTimeColumnDirective.groupHeaderTemplate = groupHeaderTemplate;
    dateTimeColumnDirective.editTemplate = editTemplate;
    dateTimeColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
    var propertyName = 'bar';
    dateTimeColumnDirective.propertyName = propertyName;
    var editionEnabled = false;
    dateTimeColumnDirective.enableEditing = editionEnabled;

    var groupData: any = dateTimeColumnDirective.getGroupData();

    expect(groupData.dataTemplate).toEqual(groupHeaderTemplate);
    expect(groupData.editTemplate).toEqual(editTemplate);
    expect(groupData.validationErrorsTemplate).toEqual(validationErrorsTemplate);
    expect(groupData.propertyName).toEqual(propertyName);
    expect(groupData.enableEditing).toEqual(editionEnabled);
  });

  it('getValue should return null when the column is not bound to a property', () => {
    var item = { foo: new Date() };

    var columnValue = dateTimeColumnDirective.getValue(item);

    expect(columnValue).toEqual(null);
  });

  it('getValue should return the value of the property that the column is bound to', () => {
    var item = { foo: new Date() };
    var propertyName = 'foo';
    dateTimeColumnDirective.propertyName = propertyName;

    var columnValue = dateTimeColumnDirective.getValue(item);

    expect(columnValue).toEqual(item[propertyName]);
  });

  it('order should call customOrder callback if it is provided', () => {
    dateTimeColumnDirective.customOrder = (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any): number => { return 0; };
    var orderResult = 1;
    var orderSpy: jasmine.Spy = spyOn(dateTimeColumnDirective, 'customOrder').and.returnValue(orderResult);

    var result = dateTimeColumnDirective.order(new Date(), new Date(), true, {}, {});

    expect(result).toEqual(orderResult);
  });

  it('order should call default comparer', () => {
    var firstValue = new Date(1990, 7, 10);
    var secondValue = new Date(1992, 3, 12);
    var isAsecneding = true;
    var comparerSpy: jasmine.Spy = spyOn(comparer, 'compare');

    var result = dateTimeColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(comparerSpy).toHaveBeenCalled();
    var compareArgs = comparerSpy.calls.first().args;
    expect(compareArgs[2]).toEqual(isAsecneding);
  });

  it('order should order by date data when type is date', () => {
    dateTimeColumnDirective.type = 'date';
    var firstValue = new Date(1992, 6, 3, 12, 30, 25, 123);
    var secondValue = new Date(1992, 6, 3, 8, 25, 45, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('order should order by hours data when type is time and step >= 3600', () => {
    dateTimeColumnDirective.type = 'time';
    dateTimeColumnDirective.step = 3600;
    var firstValue = new Date(1992, 5, 3, 12, 30, 25, 123);
    var secondValue = new Date(1990, 2, 1, 12, 25, 45, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('order should order by hours and minutes data when type is time and step >= 60', () => {
    dateTimeColumnDirective.type = 'time';
    dateTimeColumnDirective.step = 60;
    var firstValue = new Date(1992, 5, 3, 12, 15, 25, 123);
    var secondValue = new Date(1990, 2, 1, 12, 15, 45, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('order should order by hours, minutes and seconds data when type is time and step >= 1', () => {
    dateTimeColumnDirective.type = 'time';
    dateTimeColumnDirective.step = 1;
    var firstValue = new Date(1992, 5, 3, 12, 15, 12, 123);
    var secondValue = new Date(1990, 2, 1, 12, 15, 12, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('order should order by hours, minutes, seconds and milliseconds data when type is time and step < 1', () => {
    dateTimeColumnDirective.type = 'time';
    dateTimeColumnDirective.step = 0.001;
    var firstValue = new Date(1992, 5, 3, 12, 15, 12, 123);
    var secondValue = new Date(1990, 2, 1, 12, 15, 12, 123);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('order should order by date and hours data when type is datetime-local and step >= 3600', () => {
    dateTimeColumnDirective.type = 'datetime-local';
    dateTimeColumnDirective.step = 3600;
    var firstValue = new Date(1992, 2, 1, 12, 30, 25, 123);
    var secondValue = new Date(1992, 2, 1, 12, 25, 45, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('order should order by date, hours and minutes data when type is datetime-local and step >= 60', () => {
    dateTimeColumnDirective.type = 'datetime-local';
    dateTimeColumnDirective.step = 60;
    var firstValue = new Date(1992, 2, 1, 12, 30, 25, 123);
    var secondValue = new Date(1992, 2, 1, 12, 30, 45, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('order should order by date, hours, minutes and seconds data when type is datetime-local and step >= 1', () => {
    dateTimeColumnDirective.type = 'datetime-local';
    dateTimeColumnDirective.step = 1;
    var firstValue = new Date(1992, 2, 1, 12, 30, 25, 123);
    var secondValue = new Date(1992, 2, 1, 12, 30, 25, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('order should order by date, hours, minutes, seconds and milliseconds data when type is datetime-local and step < 1', () => {
    dateTimeColumnDirective.type = 'datetime-local';
    dateTimeColumnDirective.step = 0.001;
    var firstValue = new Date(1992, 5, 3, 12, 15, 12, 123);
    var secondValue = new Date(1992, 5, 3, 12, 15, 12, 123);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('group should call customGroup callback if it is provided', () => {
    dateTimeColumnDirective.customGroup = (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any): number => { return 0; };
    var groupResult = 1;
    var orderSpy: jasmine.Spy = spyOn(dateTimeColumnDirective, 'customGroup').and.returnValue(groupResult);

    var result = dateTimeColumnDirective.group(new Date(), new Date(), true, {}, {});

    expect(result).toEqual(groupResult);
  });

  it('group should call default comparer', () => {
    var firstValue = new Date();
    var secondValue = new Date();
    var isAsecneding = true;
    var comparerSpy: jasmine.Spy = spyOn(comparer, 'compare');

    var result = dateTimeColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(comparerSpy).toHaveBeenCalled();
    var compareArgs = comparerSpy.calls.first().args;
    expect(compareArgs[2]).toEqual(isAsecneding);
  });

  it('group should group by date data when type is date', () => {
    dateTimeColumnDirective.type = 'date';
    var firstValue = new Date(1992, 6, 3, 12, 30, 25, 123);
    var secondValue = new Date(1992, 6, 3, 8, 25, 45, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('group should group by hours data when type is time and step >= 3600', () => {
    dateTimeColumnDirective.type = 'time';
    dateTimeColumnDirective.step = 3600;
    var firstValue = new Date(1992, 5, 3, 12, 30, 25, 123);
    var secondValue = new Date(1990, 2, 1, 12, 25, 45, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('group should group by hours and minutes data when type is time and step >= 60', () => {
    dateTimeColumnDirective.type = 'time';
    dateTimeColumnDirective.step = 60;
    var firstValue = new Date(1992, 5, 3, 12, 15, 25, 123);
    var secondValue = new Date(1990, 2, 1, 12, 15, 45, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('group should group by hours, minutes and seconds data when type is time and step >= 1', () => {
    dateTimeColumnDirective.type = 'time';
    dateTimeColumnDirective.step = 1;
    var firstValue = new Date(1992, 5, 3, 12, 15, 12, 123);
    var secondValue = new Date(1990, 2, 1, 12, 15, 12, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('group should group by hours, minutes, seconds and milliseconds data when type is time and step < 1', () => {
    dateTimeColumnDirective.type = 'time';
    dateTimeColumnDirective.step = 0.001;
    var firstValue = new Date(1992, 5, 3, 12, 15, 12, 123);
    var secondValue = new Date(1990, 2, 1, 12, 15, 12, 123);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('group should group by date and hours data when type is datetime-local and step >= 3600', () => {
    dateTimeColumnDirective.type = 'datetime-local';
    dateTimeColumnDirective.step = 3600;
    var firstValue = new Date(1992, 2, 1, 12, 30, 25, 123);
    var secondValue = new Date(1992, 2, 1, 12, 25, 45, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('group should group by date, hours and minutes data when type is datetime-local and step >= 60', () => {
    dateTimeColumnDirective.type = 'datetime-local';
    dateTimeColumnDirective.step = 60;
    var firstValue = new Date(1992, 2, 1, 12, 30, 25, 123);
    var secondValue = new Date(1992, 2, 1, 12, 30, 45, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('group should group by date, hours, minutes and seconds data when type is datetime-local and step >= 1', () => {
    dateTimeColumnDirective.type = 'datetime-local';
    dateTimeColumnDirective.step = 1;
    var firstValue = new Date(1992, 2, 1, 12, 30, 25, 123);
    var secondValue = new Date(1992, 2, 1, 12, 30, 25, 654);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('group should group by date, hours, minutes, seconds and milliseconds data when type is datetime-local and step < 1', () => {
    dateTimeColumnDirective.type = 'datetime-local';
    dateTimeColumnDirective.step = 0.001;
    var firstValue = new Date(1992, 5, 3, 12, 15, 12, 123);
    var secondValue = new Date(1992, 5, 3, 12, 15, 12, 123);
    var isAsecneding = true;

    var result = dateTimeColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(result).toEqual(0);
  });

  it('filter should call customFilter callback if it is provided', () => {
    dateTimeColumnDirective.customFilter = (val: any, filterData: any, item: any): boolean => { return false; };
    var filterResult = true;
    var filterSpy: jasmine.Spy = spyOn(dateTimeColumnDirective, 'customFilter').and.returnValue(filterResult);
    var value = new Date();
    var filterData = {};
    var item = {};

    var result = dateTimeColumnDirective.filter(value, filterData, item);

    expect(filterSpy).toHaveBeenCalled();
    var filterArgs = filterSpy.calls.first().args;
    expect(filterArgs[0]).toEqual(value);
    expect(filterArgs[1]).toEqual(filterData);
    expect(filterArgs[2]).toEqual(item);
  });

  it('filter with no filter data should return true', () => {
    var result = dateTimeColumnDirective.filter(new Date(), null, {});

    expect(result).toBeTruthy();
  });

  it('filter with no filter date should return true', () => {
    var result = dateTimeColumnDirective.filter(new Date(), {}, {});

    expect(result).toBeTruthy();
  });

  it('filter with empty filter date should return true', () => {
    var result = dateTimeColumnDirective.filter(new Date(), { selectedDate: '' }, {});

    expect(result).toBeTruthy();
  });

  it('filter with no item value should return false', () => {
    var result = dateTimeColumnDirective.filter(null, { selectedDate: new Date() }, {});

    expect(result).toBeFalsy();
  });

  it('filter with less search term type should return true when the value is smaller than the filter value', () => {
    var result = dateTimeColumnDirective.filter(new Date(1990, 11, 7), { selectedDate: new Date(1990, 11, 8), searchTermType: DateTimeSearchTermType.LessThan }, {});

    expect(result).toBeTruthy();
  });

  it('filter with less search term type should return false when the value is bigger or equal to the filter value', () => {
    var result = dateTimeColumnDirective.filter(new Date(1990, 11, 8), { selectedDate: new Date(1990, 11, 8), searchTermType: DateTimeSearchTermType.LessThan }, {});

    expect(result).toBeFalsy();
  });

  it('filter with less than or equal search term type should return true when the value is smaller or equal to the filter value', () => {
    var result = dateTimeColumnDirective.filter(new Date(1990, 11, 7), { selectedDate: new Date(1990, 11, 7), searchTermType: DateTimeSearchTermType.LessThanOrEqual }, {});

    expect(result).toBeTruthy();
  });

  it('filter with less than or equal search term type should return false when the value is bigger than the filter value', () => {
    var result = dateTimeColumnDirective.filter(new Date(1990, 11, 8), { selectedDate: new Date(1990, 11, 7), searchTermType: DateTimeSearchTermType.LessThanOrEqual }, {});

    expect(result).toBeFalsy();
  });

  it('filter with equal search term type should return true when the value is equal to the filter value', () => {
    var date = new Date(1990, 11, 7);
    var result = dateTimeColumnDirective.filter(date, { selectedDate: date, searchTermType: DateTimeSearchTermType.Equals }, {});

    expect(result).toBeTruthy();
  });

  it('filter with equal search term type should return false when the value is not equal to the filter value', () => {
    var result = dateTimeColumnDirective.filter(new Date(1990, 11, 7), { selectedDate: new Date(1990, 11, 8), searchTermType: DateTimeSearchTermType.Equals }, {});

    expect(result).toBeFalsy();
  });

  it('filter with bigger or equal search term type should return true when the value is bigger or equal to the filter value', () => {
    var result = dateTimeColumnDirective.filter(new Date(1990, 11, 7), { selectedDate: new Date(1990, 11, 7), searchTermType: DateTimeSearchTermType.MoreThanOrEqual }, {});

    expect(result).toBeTruthy();
  });

  it('filter with bigger or equal search term type should return false when the value is not bigger or equal to the filter value', () => {
    var result = dateTimeColumnDirective.filter(new Date(1990, 11, 6), { selectedDate: new Date(1990, 11, 7), searchTermType: DateTimeSearchTermType.MoreThanOrEqual }, {});

    expect(result).toBeFalsy();
  });

  it('filter with bigger search term type should return true when the value is bigger than the filter value', () => {
    var result = dateTimeColumnDirective.filter(new Date(1990, 11, 8), { selectedDate: new Date(1990, 11, 7), searchTermType: DateTimeSearchTermType.MoreThan }, {});

    expect(result).toBeTruthy();
  });

  it('filter with bigger search term type should return false when the value is not bigger than the filter value', () => {
    var result = dateTimeColumnDirective.filter(new Date(1990, 11, 7), { selectedDate: new Date(1990, 11, 7), searchTermType: DateTimeSearchTermType.MoreThan }, {});

    expect(result).toBeFalsy();
  });

  it('filter should throw an Error if the search term type does not exist', () => {
    expect(function() { dateTimeColumnDirective.filter(new Date(), { selectedDate: new Date(), searchTermType: 5 }, {}); }).toThrowError();
  });
});