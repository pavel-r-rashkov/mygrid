import { 
  Component, 
  OnInit,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { DataComponent } from '../contracts/data-component';

@Component({
  selector: 'mg-date-time-column-data',
  templateUrl: './date-time-column-data.component.html'
})
export class DateTimeColumnDataComponent extends DataComponent implements OnInit {
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;    

  constructor() {
    super();
  }
  
  ngOnInit() {
    if (this.data.dataTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item
      };
      this.containerRef.createEmbeddedView(this.data.dataTemplate, { $implicit: templateContext });
    }
  }
}