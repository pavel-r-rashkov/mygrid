import { 
  Component, 
  OnInit,
  OnDestroy,
  AfterViewInit,
  Input,
  TemplateRef,
  ViewChild,
  ViewContainerRef 
} from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import { filterConditionMenuAnimation } from '../animations';
import { FilterComponent } from '../contracts/filter-component';
import { FilterStateService } from '../../filter-state.service';
import { FilterState } from '../../states/filter-state';
import { TextSearchTermType } from './text-search-term-type';

@Component({
  selector: 'mg-text-column-filter',
  templateUrl: './text-column-filter.component.html',
  animations: [
    filterConditionMenuAnimation
  ]
})
export class TextColumnFilterComponent extends FilterComponent implements OnInit, AfterViewInit, OnDestroy {
  private readonly filterKey: string = 'TextBox';
  private filterStateSubscription: Subscription;
  private cachedFilterText: string;
  private currentFilterState: FilterState;
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;
  filterText: string;
  menuVisible: boolean;
  selectedSearchType: TextSearchTermType;
  textSearchTermType = TextSearchTermType;

  constructor(
    private filterStateService: FilterStateService) {
    super();
    this.menuVisible = false;
  }

  ngOnInit() {  
    this.selectedSearchType = this.filterData.searchTermType;

    this.filterStateSubscription = this.filterStateService.filterState.subscribe((newState) => {
      this.currentFilterState = newState;
      let currentTextFilter = newState.columnFilters[this.columnId];
      this.filterText = currentTextFilter == null ? null : currentTextFilter.filterData.text;
    });
  }

  ngAfterViewInit() {
    if (this.filterData.filterTemplate != null) {
      let templateContext = {
        columnId: this.columnId,
        filterData: this.filterData,
        filter: this.customTemplateFilter
      }
      this.containerRef.createEmbeddedView(this.filterData.filterTemplate, { $implicit: templateContext });
    }
  }

  ngOnDestroy() {
    this.filterStateSubscription.unsubscribe();
  }

  filterFocusIn() {
    this.cachedFilterText = this.filterText;
  }

  filterFocusedOut($event: any) {
    if (this.filterText == '') {
      this.filterText = null;
    }

    if (this.cachedFilterText == this.filterText) {
      return;
    }

    if (this.filterText == null) {
      this.filterStateService.clearColumnFilter(this.columnId);
    } else {
      this.filterStateService.raiseFilterChanged(
        this.columnId,
        this.filterData.propertyName,
        { text: this.filterText, searchTermType: this.selectedSearchType },
        this.filterKey);
    }
  }

  selectSearchType(selectedType: number) {
    if (selectedType != this.selectedSearchType && this.filterText != null) {
      this.filterStateService.raiseFilterChanged(
        this.columnId,
        this.filterData.propertyName, 
        { text: this.filterText, searchTermType: selectedType },
        this.filterKey);
    }

    this.selectedSearchType = selectedType;
  }

  toggleMenu() {
    this.menuVisible = !this.menuVisible;
  }

  buttonFocusedOut() {
    this.menuVisible = false;
  }

  getConditionMenuState(): string {
    return this.menuVisible ? 'opened' : 'hidden';
  }

  customTemplateFilter(filterData: any) {
    this.filterStateService.raiseFilterChanged(
      this.columnId,
      this.filterData.propertyName,
      filterData,
      this.filterKey);
  }
}