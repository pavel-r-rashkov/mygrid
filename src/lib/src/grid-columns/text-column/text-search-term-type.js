"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TextSearchTermType;
(function (TextSearchTermType) {
    TextSearchTermType[TextSearchTermType["StartsWith"] = 0] = "StartsWith";
    TextSearchTermType[TextSearchTermType["Contains"] = 1] = "Contains";
    TextSearchTermType[TextSearchTermType["Equals"] = 2] = "Equals";
    TextSearchTermType[TextSearchTermType["EndsWith"] = 3] = "EndsWith";
})(TextSearchTermType = exports.TextSearchTermType || (exports.TextSearchTermType = {}));
//# sourceMappingURL=text-search-term-type.js.map