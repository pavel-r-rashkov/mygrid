import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { TextColumnEditComponent } from './text-column-edit.component';

@Component({
  selector: 'mgt-test-edit',
  template: `
    <ng-template #editTemplate>
      <div id="test-edit-template"></div>
    </ng-template>
  `
})
class TestEditComponent {
  @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
}

describe('Component: TextColumnEditComponent', () => {
  let textColumnEditComponent: TextColumnEditComponent;
  let fixture: ComponentFixture<TextColumnEditComponent>;
  let editTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestEditComponent
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TextColumnEditComponent);
        textColumnEditComponent = fixture.componentInstance;

        var templateComponent: TestEditComponent = TestBed.createComponent(TestEditComponent).componentInstance;
        editTemplate = templateComponent.editTemplate;
      });
  }));
  
  it('custom edit template should be rendered when it exists', () => {
    var item = {};
    var data = {
      editTemplate: editTemplate
    };
    var form = new FormGroup({});
    textColumnEditComponent.data = data;
    textColumnEditComponent.item = item;
    textColumnEditComponent.editForm = form;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-edit-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.data).toEqual(data);
    expect(customTemplate.context.$implicit.item).toEqual(item);
    expect(customTemplate.context.$implicit.editForm).toEqual(form);
  });
});