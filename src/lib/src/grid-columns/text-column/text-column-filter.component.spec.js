"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var my_grid_module_1 = require("../../my-grid.module");
var text_column_filter_component_1 = require("./text-column-filter.component");
var filter_state_service_1 = require("../../filter-state.service");
var TestFilterComponent = (function () {
    function TestFilterComponent() {
    }
    __decorate([
        core_1.ViewChild('filterTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestFilterComponent.prototype, "filterTemplate", void 0);
    TestFilterComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-filter',
            template: "\n    <ng-template #filterTemplate>\n      <div id=\"test-filter-template\"></div>\n    </ng-template>\n  "
        })
    ], TestFilterComponent);
    return TestFilterComponent;
}());
describe('Component: TextColumnFilterComponent', function () {
    var textColumnFilterComponent;
    var fixture;
    var filterTemplate;
    var filterStateService;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [
                filter_state_service_1.FilterStateService
            ],
            declarations: [
                TestFilterComponent
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(text_column_filter_component_1.TextColumnFilterComponent);
            textColumnFilterComponent = fixture.componentInstance;
            var templateComponent = testing_1.TestBed.createComponent(TestFilterComponent).componentInstance;
            filterTemplate = templateComponent.filterTemplate;
            filterStateService = fixture.debugElement.injector.get(filter_state_service_1.FilterStateService);
        });
    }));
    it('custom filter template should be rendered when it exists', function () {
        var data = {
            filterTemplate: filterTemplate,
            enableFiltering: true
        };
        textColumnFilterComponent.filterData = data;
        fixture.detectChanges();
        var customTemplate = fixture.debugElement.query(platform_browser_1.By.css('#test-filter-template'));
        expect(customTemplate).not.toBeNull();
        expect(customTemplate.context.$implicit.filterData).toEqual(data);
        expect(customTemplate.context.$implicit.columnId).toEqual(textColumnFilterComponent.columnId);
        expect(customTemplate.context.$implicit.filter).toEqual(textColumnFilterComponent.customTemplateFilter);
    });
    it('selectedSearchType should be set on init', function () {
        var propertyName = 'foo';
        var searchTermType = 3;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        fixture.detectChanges();
        expect(textColumnFilterComponent.selectedSearchType).toEqual(searchTermType);
    });
    it('changing filter state should update filterText', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        var filterText = 'foo';
        fixture.detectChanges();
        filterStateService.raiseFilterChanged(columnId, propertyName, { text: filterText }, '');
        fixture.whenStable().then(function () {
            expect(textColumnFilterComponent.filterText).toEqual(filterText);
        });
    });
    it('getConditionMenuState should return opened when menu is visible', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        textColumnFilterComponent.menuVisible = true;
        fixture.detectChanges();
        var menuState = textColumnFilterComponent.getConditionMenuState();
        expect(menuState).toEqual('opened');
    });
    it('getConditionMenuState should return hidden when menu is not visible', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        textColumnFilterComponent.menuVisible = false;
        fixture.detectChanges();
        var menuState = textColumnFilterComponent.getConditionMenuState();
        expect(menuState).toEqual('hidden');
    });
    it('buttonFocusedOut should hide the menu', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        textColumnFilterComponent.menuVisible = true;
        fixture.detectChanges();
        textColumnFilterComponent.buttonFocusedOut();
        expect(textColumnFilterComponent.menuVisible).toBeFalsy();
    });
    it('toggleMenu should show/hide menu', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        var menuVisible = false;
        textColumnFilterComponent.menuVisible = menuVisible;
        fixture.detectChanges();
        textColumnFilterComponent.toggleMenu();
        expect(textColumnFilterComponent.menuVisible).toEqual(!menuVisible);
    });
    it('customTemplateFilter should create new filter state', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        var templateFilterData = {};
        var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        textColumnFilterComponent.customTemplateFilter(templateFilterData);
        expect(raiseFilterChangeSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangeSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2]).toEqual(templateFilterData);
    });
    it('selectedSearchType should be set on init', function () {
        var propertyName = 'foo';
        var searchTermType = 3;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        fixture.detectChanges();
        expect(textColumnFilterComponent.selectedSearchType).toEqual(searchTermType);
    });
    it('selectSearchType should not create new filter state when there is no change in the selected search type', function () {
        var propertyName = 'foo';
        var searchTermType = 3;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        textColumnFilterComponent.filterText = 'foo';
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        textColumnFilterComponent.selectSearchType(searchTermType);
        expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
    });
    it('selectSearchType should not create new filter state when there is no filter text entered', function () {
        var propertyName = 'foo';
        var searchTermType = 3;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        textColumnFilterComponent.selectSearchType(4);
        expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
    });
    it('selectSearchType should create new filter state when there is filter text entered and the search term type is changed', function () {
        var propertyName = 'foo';
        var searchTermType = 3;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        var filterText = 'foo';
        textColumnFilterComponent.filterText = filterText;
        var selectedSearchTermType = 4;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        textColumnFilterComponent.selectSearchType(selectedSearchTermType);
        expect(raiseFilterChangedSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangedSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2].text).toEqual(filterText);
        expect(filterCall.args[2].searchTermType).toEqual(selectedSearchTermType);
    });
    it('filterFocusedOut should not create new filter state when filter text is not changed', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        textColumnFilterComponent.filterText = 'foo';
        var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');
        var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');
        fixture.detectChanges();
        textColumnFilterComponent.filterFocusIn();
        textColumnFilterComponent.filterFocusedOut({});
        expect(raiseFilterChangeSpy).not.toHaveBeenCalled();
        expect(clearColumnFilterSpy).not.toHaveBeenCalled();
    });
    it('filterFocusedOut should clear filter for this column when the entered text is null or empty', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');
        fixture.detectChanges();
        textColumnFilterComponent.filterText = 'foo';
        textColumnFilterComponent.filterFocusIn();
        textColumnFilterComponent.filterText = null;
        textColumnFilterComponent.filterFocusedOut({});
        expect(clearColumnFilterSpy).toHaveBeenCalled();
        expect(clearColumnFilterSpy.calls.first().args[0]).toEqual(textColumnFilterComponent.columnId);
    });
    it('filterFocusedOut should create new filter state when entered text is not null or empty', function () {
        var propertyName = 'foo';
        var searchTermType = 3;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        textColumnFilterComponent.filterData = data;
        var columnId = '123';
        textColumnFilterComponent.columnId = columnId;
        var filterText = 'foo';
        textColumnFilterComponent.filterText = filterText;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        textColumnFilterComponent.filterFocusedOut({});
        expect(raiseFilterChangedSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangedSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2].text).toEqual(filterText);
        expect(filterCall.args[2].searchTermType).toEqual(searchTermType);
    });
});
//# sourceMappingURL=text-column-filter.component.spec.js.map