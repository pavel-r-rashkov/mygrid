"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var text_column_directive_1 = require("./text-column.directive");
var guid_1 = require("../../guid");
var default_comparer_service_1 = require("../shared/default-comparer.service");
var text_search_term_type_1 = require("./text-search-term-type");
var TestTextColumnComponent = (function () {
    function TestTextColumnComponent() {
    }
    __decorate([
        core_1.ViewChild('dataTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestTextColumnComponent.prototype, "dataTemplate", void 0);
    __decorate([
        core_1.ViewChild('headerTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestTextColumnComponent.prototype, "headerTemplate", void 0);
    __decorate([
        core_1.ViewChild('editTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestTextColumnComponent.prototype, "editTemplate", void 0);
    __decorate([
        core_1.ViewChild('validationErrorsTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestTextColumnComponent.prototype, "validationErrorsTemplate", void 0);
    __decorate([
        core_1.ViewChild('filterTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestTextColumnComponent.prototype, "filterTemplate", void 0);
    __decorate([
        core_1.ViewChild('groupHeaderTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestTextColumnComponent.prototype, "groupHeaderTemplate", void 0);
    TestTextColumnComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-text-column',
            template: "\n    <ng-template #dataTemplate>\n      <div id=\"test-data-template\"></div>\n    </ng-template>\n    <ng-template #headerTemplate>\n      <div id=\"test-header-template\"></div>\n    </ng-template>\n    <ng-template #editTemplate>\n      <div id=\"test-edit-template\"></div>\n    </ng-template>\n    <ng-template #validationErrorsTemplate>\n      <div id=\"test-validation-error-template\"></div>\n    </ng-template>\n    <ng-template #filterTemplate>\n      <div id=\"test-filter-template\"></div>\n    </ng-template>\n    <ng-template #groupHeaderTemplate>\n      <div id=\"test-group-header-template\"></div>\n    </ng-template>\n  "
        })
    ], TestTextColumnComponent);
    return TestTextColumnComponent;
}());
describe('Directive: TextColumnDirective', function () {
    var textColumnDirective;
    var comparer;
    var dataTemplate;
    var headerTemplate;
    var editTemplate;
    var validationErrorsTemplate;
    var filterTemplate;
    var groupHeaderTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestTextColumnComponent
            ]
        })
            .compileComponents()
            .then(function () {
            comparer = new default_comparer_service_1.DefaultComparerService();
            textColumnDirective = new text_column_directive_1.TextColumnDirective(new guid_1.GuidService(), comparer);
            var testComponent = testing_1.TestBed.createComponent(TestTextColumnComponent).componentInstance;
            dataTemplate = testComponent.dataTemplate;
            headerTemplate = testComponent.headerTemplate;
            editTemplate = testComponent.editTemplate;
            validationErrorsTemplate = testComponent.validationErrorsTemplate;
            filterTemplate = testComponent.filterTemplate;
            groupHeaderTemplate = testComponent.groupHeaderTemplate;
        });
    }));
    it('getHeaderData should return text column header data', function () {
        var caption = 'foo';
        textColumnDirective.caption = caption;
        textColumnDirective.headerTemplate = headerTemplate;
        var propertyName = 'bar';
        textColumnDirective.propertyName = propertyName;
        var orderingEnabled = false;
        textColumnDirective.enableOrdering = orderingEnabled;
        var groupingEnabled = false;
        textColumnDirective.enableGrouping = groupingEnabled;
        var headerData = textColumnDirective.getHeaderData();
        expect(headerData.caption).toEqual(caption);
        expect(headerData.headerTemplate).toEqual(headerTemplate);
        expect(headerData.propertyName).toEqual(propertyName);
        expect(headerData.enableOrdering).toEqual(orderingEnabled);
        expect(headerData.enableGrouping).toEqual(groupingEnabled);
    });
    it('ordering and grouping should be enabled by default', function () {
        var headerData = textColumnDirective.getHeaderData();
        expect(headerData.enableOrdering).toBeTruthy();
        expect(headerData.enableGrouping).toBeTruthy();
    });
    it('getData should return text column data', function () {
        textColumnDirective.dataTemplate = dataTemplate;
        textColumnDirective.editTemplate = editTemplate;
        textColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
        var propertyName = 'bar';
        textColumnDirective.propertyName = propertyName;
        var editionEnabled = false;
        textColumnDirective.enableEditing = editionEnabled;
        var data = textColumnDirective.getData();
        expect(data.dataTemplate).toEqual(dataTemplate);
        expect(data.editTemplate).toEqual(editTemplate);
        expect(data.validationErrorsTemplate).toEqual(validationErrorsTemplate);
        expect(data.propertyName).toEqual(propertyName);
        expect(data.enableEditing).toEqual(editionEnabled);
    });
    it('editing should be enabled by default', function () {
        var data = textColumnDirective.getData();
        expect(data.enableEditing).toBeTruthy();
    });
    it('getFilterData should return text column filter data', function () {
        textColumnDirective.filterTemplate = filterTemplate;
        var propertyName = 'bar';
        textColumnDirective.propertyName = propertyName;
        var filteringEnabled = false;
        textColumnDirective.enableFiltering = filteringEnabled;
        var searchTermTypeMenuEnabled = false;
        textColumnDirective.enableSearchTermTypeMenu = searchTermTypeMenuEnabled;
        var searchTermType = text_search_term_type_1.TextSearchTermType.Contains;
        textColumnDirective.searchTermType = searchTermType;
        var filterData = textColumnDirective.getFilterData();
        expect(filterData.filterTemplate).toEqual(filterTemplate);
        expect(filterData.propertyName).toEqual(propertyName);
        expect(filterData.enableFiltering).toEqual(filteringEnabled);
        expect(filterData.enableSearchTermTypeMenu).toEqual(searchTermTypeMenuEnabled);
        expect(filterData.searchTermType).toEqual(searchTermType);
    });
    it('filtering should be enabled by default, default filter\'s step should be 0, search term type default option should be equals, search term type menu should be enabled by default', function () {
        var filterData = textColumnDirective.getFilterData();
        expect(filterData.enableFiltering).toBeTruthy();
        expect(filterData.enableSearchTermTypeMenu).toBeTruthy();
        expect(filterData.searchTermType).toEqual(text_search_term_type_1.TextSearchTermType.Equals);
    });
    it('getGroupData should return text column group data', function () {
        textColumnDirective.groupHeaderTemplate = groupHeaderTemplate;
        textColumnDirective.editTemplate = editTemplate;
        textColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
        var propertyName = 'bar';
        textColumnDirective.propertyName = propertyName;
        var editionEnabled = false;
        textColumnDirective.enableEditing = editionEnabled;
        var groupData = textColumnDirective.getGroupData();
        expect(groupData.dataTemplate).toEqual(groupHeaderTemplate);
        expect(groupData.editTemplate).toEqual(editTemplate);
        expect(groupData.validationErrorsTemplate).toEqual(validationErrorsTemplate);
        expect(groupData.propertyName).toEqual(propertyName);
        expect(groupData.enableEditing).toEqual(editionEnabled);
    });
    it('getValue should return null when the column is not bound to a property', function () {
        var item = { foo: 'foo' };
        var columnValue = textColumnDirective.getValue(item);
        expect(columnValue).toEqual(null);
    });
    it('getValue should return the value of the property that the column is bound to', function () {
        var item = { foo: 'foo' };
        var propertyName = 'foo';
        textColumnDirective.propertyName = propertyName;
        var columnValue = textColumnDirective.getValue(item);
        expect(columnValue).toEqual(item[propertyName]);
    });
    it('order should call customOrder callback if it is provided', function () {
        textColumnDirective.customOrder = function (firstValue, secondValue, isAscending, firstItem, secondItem) { return 0; };
        var orderResult = 1;
        var orderSpy = spyOn(textColumnDirective, 'customOrder').and.returnValue(orderResult);
        var result = textColumnDirective.order('A', 'B', true, {}, {});
        expect(result).toEqual(orderResult);
    });
    it('order should call default comparer', function () {
        var firstValue = 'A';
        var secondValue = 'B';
        var isAsecneding = true;
        var comparerSpy = spyOn(comparer, 'compare');
        var result = textColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});
        expect(comparerSpy).toHaveBeenCalled();
        var compareArgs = comparerSpy.calls.first().args;
        expect(compareArgs[0]).toEqual(firstValue);
        expect(compareArgs[1]).toEqual(secondValue);
        expect(compareArgs[2]).toEqual(isAsecneding);
    });
    it('group should call customGroup callback if it is provided', function () {
        textColumnDirective.customGroup = function (firstValue, secondValue, isAscending, firstItem, secondItem) { return 0; };
        var groupResult = 1;
        var orderSpy = spyOn(textColumnDirective, 'customGroup').and.returnValue(groupResult);
        var result = textColumnDirective.group('A', 'B', true, {}, {});
        expect(result).toEqual(groupResult);
    });
    it('group should call default comparer', function () {
        var firstValue = 'A';
        var secondValue = 'B';
        var isAsecneding = true;
        var comparerSpy = spyOn(comparer, 'compare');
        var result = textColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});
        expect(comparerSpy).toHaveBeenCalled();
        var compareArgs = comparerSpy.calls.first().args;
        expect(compareArgs[0]).toEqual(firstValue);
        expect(compareArgs[1]).toEqual(secondValue);
        expect(compareArgs[2]).toEqual(isAsecneding);
    });
    it('filter should call customFilter callback if it is provided', function () {
        textColumnDirective.customFilter = function (val, filterData, item) { return false; };
        var filterResult = true;
        var filterSpy = spyOn(textColumnDirective, 'customFilter').and.returnValue(filterResult);
        var value = 'A';
        var filterData = {};
        var item = {};
        var result = textColumnDirective.filter(value, filterData, item);
        expect(filterSpy).toHaveBeenCalled();
        var filterArgs = filterSpy.calls.first().args;
        expect(filterArgs[0]).toEqual(value);
        expect(filterArgs[1]).toEqual(filterData);
        expect(filterArgs[2]).toEqual(item);
    });
    it('filter with no filter data should return true', function () {
        var result = textColumnDirective.filter('A', null, {});
        expect(result).toBeTruthy();
    });
    it('filter with no filter text should return true', function () {
        var result = textColumnDirective.filter('A', {}, {});
        expect(result).toBeTruthy();
    });
    it('filter with empty filter string should return true', function () {
        var result = textColumnDirective.filter('A', { text: '' }, {});
        expect(result).toBeTruthy();
    });
    it('filter with no item value should return false', function () {
        var result = textColumnDirective.filter(null, { text: 'asd' }, {});
        expect(result).toBeFalsy();
    });
    it('filter with starts with search term type should return true when the value starts with the search term', function () {
        var result = textColumnDirective.filter('Asdfg', { text: 'Asd', searchTermType: text_search_term_type_1.TextSearchTermType.StartsWith }, {});
        expect(result).toBeTruthy();
    });
    it('filter with starts with search term type should return false when the value does not start with the search term', function () {
        var result = textColumnDirective.filter('AcAsdg', { text: 'Asd', searchTermType: text_search_term_type_1.TextSearchTermType.StartsWith }, {});
        expect(result).toBeFalsy();
    });
    it('filter with contains search term type should return true when the value contains the search term', function () {
        var result = textColumnDirective.filter('AcAsdg', { text: 'Asd', searchTermType: text_search_term_type_1.TextSearchTermType.Contains }, {});
        expect(result).toBeTruthy();
    });
    it('filter with contains search term type should return false when the value does not contain the search term', function () {
        var result = textColumnDirective.filter('AcAddg', { text: 'Asd', searchTermType: text_search_term_type_1.TextSearchTermType.Contains }, {});
        expect(result).toBeFalsy();
    });
    it('filter with ends with search term type should return true when the value ends with the search term', function () {
        var result = textColumnDirective.filter('AcAAsd', { text: 'Asd', searchTermType: text_search_term_type_1.TextSearchTermType.EndsWith }, {});
        expect(result).toBeTruthy();
    });
    it('filter with ends with search term type should return false when the value does not end with the search term', function () {
        var result = textColumnDirective.filter('AcAddg', { text: 'Asd', searchTermType: text_search_term_type_1.TextSearchTermType.EndsWith }, {});
        expect(result).toBeFalsy();
    });
    it('filter should throw an Error if the search term type does not exist', function () {
        expect(function () { textColumnDirective.filter('AcAddg', { text: 'Asd', searchTermType: 4 }, {}); }).toThrowError();
    });
});
//# sourceMappingURL=text-column.directive.spec.js.map