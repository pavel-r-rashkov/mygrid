import { 
  Directive,
  TemplateRef,
  Input,
  ContentChild,
  forwardRef,
  Type
} from '@angular/core';

import { GridColumn } from '../grid-column';
import { DefaultColumnHeaderComponent } from '../shared/default-column-header.component';
import { TextColumnFilterComponent } from './text-column-filter.component';
import { TextColumnDataComponent } from './text-column-data.component';
import { TextColumnEditComponent } from './text-column-edit.component';
import { GuidService } from '../../guid';
import { DefaultComparerService } from '../shared/default-comparer.service';
import { TextSearchTermType } from './text-search-term-type';

@Directive({
  selector: 'mg-text-column',
  providers: [{provide: GridColumn, useExisting: forwardRef(() => TextColumnDirective)}]
})
export class TextColumnDirective extends GridColumn {
  @Input() propertyName: string;
  @Input() enableOrdering: boolean;
  @Input() enableFiltering: boolean;
  @Input() enableEditing: boolean;
  @Input() enableGrouping: boolean;
  @Input() enableSearchTermTypeMenu: boolean;
  @Input() searchTermType: TextSearchTermType;
  @Input() customFilter: (val: any, filterData: any, item: any) => boolean;
  @Input() customOrder: (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) => number;
  @Input() customGroup: (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) => number;
  
  constructor(
    private guidService: GuidService,
    private comparer: DefaultComparerService) {
    super(
      DefaultColumnHeaderComponent, 
      TextColumnFilterComponent, 
      TextColumnDataComponent, 
      TextColumnEditComponent,
      guidService.newGuid());
  }

  public getHeaderData(): any {
    return {
      caption: this.caption,
      headerTemplate: this.headerTemplate,
      propertyName: this.propertyName,
      enableOrdering: this.enableOrdering == null ? true : this.enableOrdering,
      enableGrouping: this.enableGrouping == null ? true : this.enableGrouping
    };
  }

  public getData(): any {
    return {
      propertyName: this.propertyName,
      dataTemplate: this.dataTemplate,
      enableEditing: this.enableEditing == null ? true : this.enableEditing,
      editTemplate: this.editTemplate,
      validationErrorsTemplate: this.validationErrorsTemplate
    }
  }

  public getFilterData(): any {
    return {
      filterTemplate: this.filterTemplate,
      propertyName: this.propertyName,
      enableFiltering: this.enableFiltering == null ? true : this.enableFiltering,
      enableSearchTermTypeMenu: this.enableSearchTermTypeMenu == null ? true : this.enableSearchTermTypeMenu,
      searchTermType: this.searchTermType == null ? TextSearchTermType.Equals : this.searchTermType
    }
  }

  public getGroupData(): any {
    let groupData = this.getData();
    groupData.dataTemplate = this.groupHeaderTemplate;
    return groupData;
  }

  public filter(val: any, filterData: any, item: any): boolean {
    if (this.customFilter != null) {
      return this.customFilter(val, filterData, item);
    }

    if (filterData != null && filterData.text != null && filterData.text.length > 0) {
      if (val != null) {
        switch (filterData.searchTermType) {
          case TextSearchTermType.StartsWith:
            return val.toString().startsWith(filterData.text);
          case TextSearchTermType.Contains:
            return val.toString().indexOf(filterData.text) >= 0;
          case TextSearchTermType.Equals:
            return val.toString() == filterData.text;
          case TextSearchTermType.EndsWith:
            return val.toString().endsWith(filterData.text);
          default:
            throw new Error('unrecognized search type');
        }
      } else {
        return false;
      } 
    }

    return true;
  }

  public getValue(item: any) {
    if (item[this.propertyName] == undefined) {
      return null;
    }
    
    return item[this.propertyName];
  }

  public order(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    if (this.customOrder != null) {
      return this.customOrder(firstValue, secondValue, isAscending, firstItem, secondItem);
    } else {
      return this.comparer.compare(firstValue, secondValue, isAscending);
    }
  }

  public group(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    if (this.customGroup != null) {
      return this.customGroup(firstValue, secondValue, isAscending, firstItem, secondItem);
    } else {
      return this.comparer.compare(firstValue, secondValue, isAscending);
    }
  }
}