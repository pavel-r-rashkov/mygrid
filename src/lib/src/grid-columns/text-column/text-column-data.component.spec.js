"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var my_grid_module_1 = require("../../my-grid.module");
var text_column_data_component_1 = require("./text-column-data.component");
var TestDataComponent = (function () {
    function TestDataComponent() {
    }
    __decorate([
        core_1.ViewChild('dataTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestDataComponent.prototype, "dataTemplate", void 0);
    TestDataComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-data',
            template: "\n    <ng-template #dataTemplate>\n      <div id=\"test-data-template\"></div>\n    </ng-template>\n  "
        })
    ], TestDataComponent);
    return TestDataComponent;
}());
describe('Component: TextColumnDataComponent', function () {
    var textColumnDataComponent;
    var fixture;
    var dataTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestDataComponent
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(text_column_data_component_1.TextColumnDataComponent);
            textColumnDataComponent = fixture.componentInstance;
            var templateComponent = testing_1.TestBed.createComponent(TestDataComponent).componentInstance;
            dataTemplate = templateComponent.dataTemplate;
        });
    }));
    it('custom data template should be rendered when it exists', function () {
        var item = {};
        var data = {
            dataTemplate: dataTemplate
        };
        textColumnDataComponent.data = data;
        textColumnDataComponent.item = item;
        fixture.detectChanges();
        var customTemplate = fixture.debugElement.query(platform_browser_1.By.css('#test-data-template'));
        expect(customTemplate).not.toBeNull();
        expect(customTemplate.context.$implicit.data).toEqual(data);
        expect(customTemplate.context.$implicit.item).toEqual(item);
    });
});
//# sourceMappingURL=text-column-data.component.spec.js.map