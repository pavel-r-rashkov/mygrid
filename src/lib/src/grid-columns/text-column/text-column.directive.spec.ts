import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { TextColumnDirective } from './text-column.directive';
import { GuidService } from '../../guid';
import { DefaultComparerService } from '../shared/default-comparer.service';
import { TextSearchTermType } from './text-search-term-type';

@Component({
  selector: 'mgt-test-text-column',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
    <ng-template #editTemplate>
      <div id="test-edit-template"></div>
    </ng-template>
    <ng-template #validationErrorsTemplate>
      <div id="test-validation-error-template"></div>
    </ng-template>
    <ng-template #filterTemplate>
      <div id="test-filter-template"></div>
    </ng-template>
    <ng-template #groupHeaderTemplate>
      <div id="test-group-header-template"></div>
    </ng-template>
  `
}) 
class TestTextColumnComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
  @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
  @ViewChild('validationErrorsTemplate') validationErrorsTemplate: TemplateRef<any>;
  @ViewChild('filterTemplate') filterTemplate: TemplateRef<any>;
  @ViewChild('groupHeaderTemplate') groupHeaderTemplate: TemplateRef<any>;
}

describe('Directive: TextColumnDirective', () => {
  let textColumnDirective: TextColumnDirective;
  let comparer: DefaultComparerService;
  let dataTemplate: TemplateRef<any>;
  let headerTemplate: TemplateRef<any>;
  let editTemplate: TemplateRef<any>;
  let validationErrorsTemplate: TemplateRef<any>;
  let filterTemplate: TemplateRef<any>;
  let groupHeaderTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestTextColumnComponent
        ]
      })
      .compileComponents()
      .then(() => {
        comparer = new DefaultComparerService();
        textColumnDirective = new TextColumnDirective(new GuidService(), comparer);

        var testComponent = TestBed.createComponent(TestTextColumnComponent).componentInstance;
        dataTemplate = testComponent.dataTemplate;
        headerTemplate = testComponent.headerTemplate;
        editTemplate = testComponent.editTemplate;
        validationErrorsTemplate = testComponent.validationErrorsTemplate;
        filterTemplate = testComponent.filterTemplate;
        groupHeaderTemplate = testComponent.groupHeaderTemplate;
      });
  }));

  it('getHeaderData should return text column header data', () => {
    var caption = 'foo';
    textColumnDirective.caption = caption;
    textColumnDirective.headerTemplate = headerTemplate;
    var propertyName = 'bar';
    textColumnDirective.propertyName = propertyName;
    var orderingEnabled = false;
    textColumnDirective.enableOrdering = orderingEnabled;
    var groupingEnabled = false;
    textColumnDirective.enableGrouping = groupingEnabled;

    var headerData: any = textColumnDirective.getHeaderData();

    expect(headerData.caption).toEqual(caption);
    expect(headerData.headerTemplate).toEqual(headerTemplate);
    expect(headerData.propertyName).toEqual(propertyName);
    expect(headerData.enableOrdering).toEqual(orderingEnabled);
    expect(headerData.enableGrouping).toEqual(groupingEnabled);
  });

  it('ordering and grouping should be enabled by default', () => {
    var headerData: any = textColumnDirective.getHeaderData();

    expect(headerData.enableOrdering).toBeTruthy();
    expect(headerData.enableGrouping).toBeTruthy();
  });

  it('getData should return text column data', () => {
    textColumnDirective.dataTemplate = dataTemplate;
    textColumnDirective.editTemplate = editTemplate;
    textColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
    var propertyName = 'bar';
    textColumnDirective.propertyName = propertyName;
    var editionEnabled = false;
    textColumnDirective.enableEditing = editionEnabled;

    var data: any = textColumnDirective.getData();

    expect(data.dataTemplate).toEqual(dataTemplate);
    expect(data.editTemplate).toEqual(editTemplate);
    expect(data.validationErrorsTemplate).toEqual(validationErrorsTemplate);
    expect(data.propertyName).toEqual(propertyName);
    expect(data.enableEditing).toEqual(editionEnabled);
  });

  it('editing should be enabled by default', () => {
    var data: any = textColumnDirective.getData();

    expect(data.enableEditing).toBeTruthy();
  });

  it('getFilterData should return text column filter data', () => {
    textColumnDirective.filterTemplate = filterTemplate;
    var propertyName = 'bar';
    textColumnDirective.propertyName = propertyName;
    var filteringEnabled = false;
    textColumnDirective.enableFiltering = filteringEnabled;
    var searchTermTypeMenuEnabled = false;
    textColumnDirective.enableSearchTermTypeMenu = searchTermTypeMenuEnabled;
    var searchTermType = TextSearchTermType.Contains;
    textColumnDirective.searchTermType = searchTermType;

    var filterData: any = textColumnDirective.getFilterData();

    expect(filterData.filterTemplate).toEqual(filterTemplate);
    expect(filterData.propertyName).toEqual(propertyName);
    expect(filterData.enableFiltering).toEqual(filteringEnabled);
    expect(filterData.enableSearchTermTypeMenu).toEqual(searchTermTypeMenuEnabled);
    expect(filterData.searchTermType).toEqual(searchTermType);
  });

  it('filtering should be enabled by default, default filter\'s step should be 0, search term type default option should be equals, search term type menu should be enabled by default', () => {
    var filterData: any = textColumnDirective.getFilterData();

    expect(filterData.enableFiltering).toBeTruthy();
    expect(filterData.enableSearchTermTypeMenu).toBeTruthy();
    expect(filterData.searchTermType).toEqual(TextSearchTermType.Equals);
  });

  it('getGroupData should return text column group data', () => {
    textColumnDirective.groupHeaderTemplate = groupHeaderTemplate;
    textColumnDirective.editTemplate = editTemplate;
    textColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
    var propertyName = 'bar';
    textColumnDirective.propertyName = propertyName;
    var editionEnabled = false;
    textColumnDirective.enableEditing = editionEnabled;

    var groupData: any = textColumnDirective.getGroupData();

    expect(groupData.dataTemplate).toEqual(groupHeaderTemplate);
    expect(groupData.editTemplate).toEqual(editTemplate);
    expect(groupData.validationErrorsTemplate).toEqual(validationErrorsTemplate);
    expect(groupData.propertyName).toEqual(propertyName);
    expect(groupData.enableEditing).toEqual(editionEnabled);
  });

  it('getValue should return null when the column is not bound to a property', () => {
    var item = { foo: 'foo' };

    var columnValue = textColumnDirective.getValue(item);

    expect(columnValue).toEqual(null);
  });

  it('getValue should return the value of the property that the column is bound to', () => {
    var item = { foo: 'foo' };
    var propertyName = 'foo';
    textColumnDirective.propertyName = propertyName;

    var columnValue = textColumnDirective.getValue(item);

    expect(columnValue).toEqual(item[propertyName]);
  });

  it('order should call customOrder callback if it is provided', () => {
    textColumnDirective.customOrder = (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any): number => { return 0; };
    var orderResult = 1;
    var orderSpy: jasmine.Spy = spyOn(textColumnDirective, 'customOrder').and.returnValue(orderResult);

    var result = textColumnDirective.order('A', 'B', true, {}, {});

    expect(result).toEqual(orderResult);
  });

  it('order should call default comparer', () => {
    var firstValue = 'A';
    var secondValue = 'B';
    var isAsecneding = true;
    var comparerSpy: jasmine.Spy = spyOn(comparer, 'compare');

    var result = textColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(comparerSpy).toHaveBeenCalled();
    var compareArgs = comparerSpy.calls.first().args;
    expect(compareArgs[0]).toEqual(firstValue);
    expect(compareArgs[1]).toEqual(secondValue);
    expect(compareArgs[2]).toEqual(isAsecneding);
  });

  it('group should call customGroup callback if it is provided', () => {
    textColumnDirective.customGroup = (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any): number => { return 0; };
    var groupResult = 1;
    var orderSpy: jasmine.Spy = spyOn(textColumnDirective, 'customGroup').and.returnValue(groupResult);

    var result = textColumnDirective.group('A', 'B', true, {}, {});

    expect(result).toEqual(groupResult);
  });

  it('group should call default comparer', () => {
    var firstValue = 'A';
    var secondValue = 'B';
    var isAsecneding = true;
    var comparerSpy: jasmine.Spy = spyOn(comparer, 'compare');

    var result = textColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(comparerSpy).toHaveBeenCalled();
    var compareArgs = comparerSpy.calls.first().args;
    expect(compareArgs[0]).toEqual(firstValue);
    expect(compareArgs[1]).toEqual(secondValue);
    expect(compareArgs[2]).toEqual(isAsecneding);
  });

  it('filter should call customFilter callback if it is provided', () => {
    textColumnDirective.customFilter = (val: any, filterData: any, item: any): boolean => { return false; };
    var filterResult = true;
    var filterSpy: jasmine.Spy = spyOn(textColumnDirective, 'customFilter').and.returnValue(filterResult);
    var value = 'A';
    var filterData = {};
    var item = {};

    var result = textColumnDirective.filter(value, filterData, item);

    expect(filterSpy).toHaveBeenCalled();
    var filterArgs = filterSpy.calls.first().args;
    expect(filterArgs[0]).toEqual(value);
    expect(filterArgs[1]).toEqual(filterData);
    expect(filterArgs[2]).toEqual(item);
  });

  it('filter with no filter data should return true', () => {
    var result = textColumnDirective.filter('A', null, {});

    expect(result).toBeTruthy();
  });

  it('filter with no filter text should return true', () => {
    var result = textColumnDirective.filter('A', {}, {});

    expect(result).toBeTruthy();
  });

  it('filter with empty filter string should return true', () => {
    var result = textColumnDirective.filter('A', { text: '' }, {});

    expect(result).toBeTruthy();
  });

  it('filter with no item value should return false', () => {
    var result = textColumnDirective.filter(null, { text: 'asd' }, {});

    expect(result).toBeFalsy();
  });

  it('filter with starts with search term type should return true when the value starts with the search term', () => {
    var result = textColumnDirective.filter('Asdfg', { text: 'Asd', searchTermType: TextSearchTermType.StartsWith }, {});

    expect(result).toBeTruthy();
  });

  it('filter with starts with search term type should return false when the value does not start with the search term', () => {
    var result = textColumnDirective.filter('AcAsdg', { text: 'Asd', searchTermType: TextSearchTermType.StartsWith }, {});

    expect(result).toBeFalsy();
  });

  it('filter with contains search term type should return true when the value contains the search term', () => {
    var result = textColumnDirective.filter('AcAsdg', { text: 'Asd', searchTermType: TextSearchTermType.Contains }, {});

    expect(result).toBeTruthy();
  });

  it('filter with contains search term type should return false when the value does not contain the search term', () => {
    var result = textColumnDirective.filter('AcAddg', { text: 'Asd', searchTermType: TextSearchTermType.Contains }, {});

    expect(result).toBeFalsy();
  });

  it('filter with ends with search term type should return true when the value ends with the search term', () => {
    var result = textColumnDirective.filter('AcAAsd', { text: 'Asd', searchTermType: TextSearchTermType.EndsWith }, {});

    expect(result).toBeTruthy();
  });

  it('filter with ends with search term type should return false when the value does not end with the search term', () => {
    var result = textColumnDirective.filter('AcAddg', { text: 'Asd', searchTermType: TextSearchTermType.EndsWith }, {});

    expect(result).toBeFalsy();
  });

  it('filter should throw an Error if the search term type does not exist', () => {
    expect(function() { textColumnDirective.filter('AcAddg', { text: 'Asd', searchTermType: 4 }, {}); }).toThrowError();
  });
});