"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var grid_column_1 = require("../grid-column");
var default_column_header_component_1 = require("../shared/default-column-header.component");
var text_column_filter_component_1 = require("./text-column-filter.component");
var text_column_data_component_1 = require("./text-column-data.component");
var text_column_edit_component_1 = require("./text-column-edit.component");
var guid_1 = require("../../guid");
var default_comparer_service_1 = require("../shared/default-comparer.service");
var text_search_term_type_1 = require("./text-search-term-type");
var TextColumnDirective = (function (_super) {
    __extends(TextColumnDirective, _super);
    function TextColumnDirective(guidService, comparer) {
        var _this = _super.call(this, default_column_header_component_1.DefaultColumnHeaderComponent, text_column_filter_component_1.TextColumnFilterComponent, text_column_data_component_1.TextColumnDataComponent, text_column_edit_component_1.TextColumnEditComponent, guidService.newGuid()) || this;
        _this.guidService = guidService;
        _this.comparer = comparer;
        return _this;
    }
    TextColumnDirective_1 = TextColumnDirective;
    TextColumnDirective.prototype.getHeaderData = function () {
        return {
            caption: this.caption,
            headerTemplate: this.headerTemplate,
            propertyName: this.propertyName,
            enableOrdering: this.enableOrdering == null ? true : this.enableOrdering,
            enableGrouping: this.enableGrouping == null ? true : this.enableGrouping
        };
    };
    TextColumnDirective.prototype.getData = function () {
        return {
            propertyName: this.propertyName,
            dataTemplate: this.dataTemplate,
            enableEditing: this.enableEditing == null ? true : this.enableEditing,
            editTemplate: this.editTemplate,
            validationErrorsTemplate: this.validationErrorsTemplate
        };
    };
    TextColumnDirective.prototype.getFilterData = function () {
        return {
            filterTemplate: this.filterTemplate,
            propertyName: this.propertyName,
            enableFiltering: this.enableFiltering == null ? true : this.enableFiltering,
            enableSearchTermTypeMenu: this.enableSearchTermTypeMenu == null ? true : this.enableSearchTermTypeMenu,
            searchTermType: this.searchTermType == null ? text_search_term_type_1.TextSearchTermType.Equals : this.searchTermType
        };
    };
    TextColumnDirective.prototype.getGroupData = function () {
        var groupData = this.getData();
        groupData.dataTemplate = this.groupHeaderTemplate;
        return groupData;
    };
    TextColumnDirective.prototype.filter = function (val, filterData, item) {
        if (this.customFilter != null) {
            return this.customFilter(val, filterData, item);
        }
        if (filterData != null && filterData.text != null && filterData.text.length > 0) {
            if (val != null) {
                switch (filterData.searchTermType) {
                    case text_search_term_type_1.TextSearchTermType.StartsWith:
                        return val.toString().startsWith(filterData.text);
                    case text_search_term_type_1.TextSearchTermType.Contains:
                        return val.toString().indexOf(filterData.text) >= 0;
                    case text_search_term_type_1.TextSearchTermType.Equals:
                        return val.toString() == filterData.text;
                    case text_search_term_type_1.TextSearchTermType.EndsWith:
                        return val.toString().endsWith(filterData.text);
                    default:
                        throw new Error('unrecognized search type');
                }
            }
            else {
                return false;
            }
        }
        return true;
    };
    TextColumnDirective.prototype.getValue = function (item) {
        if (item[this.propertyName] == undefined) {
            return null;
        }
        return item[this.propertyName];
    };
    TextColumnDirective.prototype.order = function (firstValue, secondValue, isAscending, firstItem, secondItem) {
        if (this.customOrder != null) {
            return this.customOrder(firstValue, secondValue, isAscending, firstItem, secondItem);
        }
        else {
            return this.comparer.compare(firstValue, secondValue, isAscending);
        }
    };
    TextColumnDirective.prototype.group = function (firstValue, secondValue, isAscending, firstItem, secondItem) {
        if (this.customGroup != null) {
            return this.customGroup(firstValue, secondValue, isAscending, firstItem, secondItem);
        }
        else {
            return this.comparer.compare(firstValue, secondValue, isAscending);
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], TextColumnDirective.prototype, "propertyName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], TextColumnDirective.prototype, "enableOrdering", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], TextColumnDirective.prototype, "enableFiltering", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], TextColumnDirective.prototype, "enableEditing", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], TextColumnDirective.prototype, "enableGrouping", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], TextColumnDirective.prototype, "enableSearchTermTypeMenu", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], TextColumnDirective.prototype, "searchTermType", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], TextColumnDirective.prototype, "customFilter", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], TextColumnDirective.prototype, "customOrder", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], TextColumnDirective.prototype, "customGroup", void 0);
    TextColumnDirective = TextColumnDirective_1 = __decorate([
        core_1.Directive({
            selector: 'mg-text-column',
            providers: [{ provide: grid_column_1.GridColumn, useExisting: core_1.forwardRef(function () { return TextColumnDirective_1; }) }]
        }),
        __metadata("design:paramtypes", [guid_1.GuidService,
            default_comparer_service_1.DefaultComparerService])
    ], TextColumnDirective);
    return TextColumnDirective;
    var TextColumnDirective_1;
}(grid_column_1.GridColumn));
exports.TextColumnDirective = TextColumnDirective;
//# sourceMappingURL=text-column.directive.js.map