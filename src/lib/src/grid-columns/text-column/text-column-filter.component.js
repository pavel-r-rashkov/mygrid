"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("../animations");
var filter_component_1 = require("../contracts/filter-component");
var filter_state_service_1 = require("../../filter-state.service");
var text_search_term_type_1 = require("./text-search-term-type");
var TextColumnFilterComponent = (function (_super) {
    __extends(TextColumnFilterComponent, _super);
    function TextColumnFilterComponent(filterStateService) {
        var _this = _super.call(this) || this;
        _this.filterStateService = filterStateService;
        _this.filterKey = 'TextBox';
        _this.textSearchTermType = text_search_term_type_1.TextSearchTermType;
        _this.menuVisible = false;
        return _this;
    }
    TextColumnFilterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.selectedSearchType = this.filterData.searchTermType;
        this.filterStateSubscription = this.filterStateService.filterState.subscribe(function (newState) {
            _this.currentFilterState = newState;
            var currentTextFilter = newState.columnFilters[_this.columnId];
            _this.filterText = currentTextFilter == null ? null : currentTextFilter.filterData.text;
        });
    };
    TextColumnFilterComponent.prototype.ngAfterViewInit = function () {
        if (this.filterData.filterTemplate != null) {
            var templateContext = {
                columnId: this.columnId,
                filterData: this.filterData,
                filter: this.customTemplateFilter
            };
            this.containerRef.createEmbeddedView(this.filterData.filterTemplate, { $implicit: templateContext });
        }
    };
    TextColumnFilterComponent.prototype.ngOnDestroy = function () {
        this.filterStateSubscription.unsubscribe();
    };
    TextColumnFilterComponent.prototype.filterFocusIn = function () {
        this.cachedFilterText = this.filterText;
    };
    TextColumnFilterComponent.prototype.filterFocusedOut = function ($event) {
        if (this.filterText == '') {
            this.filterText = null;
        }
        if (this.cachedFilterText == this.filterText) {
            return;
        }
        if (this.filterText == null) {
            this.filterStateService.clearColumnFilter(this.columnId);
        }
        else {
            this.filterStateService.raiseFilterChanged(this.columnId, this.filterData.propertyName, { text: this.filterText, searchTermType: this.selectedSearchType }, this.filterKey);
        }
    };
    TextColumnFilterComponent.prototype.selectSearchType = function (selectedType) {
        if (selectedType != this.selectedSearchType && this.filterText != null) {
            this.filterStateService.raiseFilterChanged(this.columnId, this.filterData.propertyName, { text: this.filterText, searchTermType: selectedType }, this.filterKey);
        }
        this.selectedSearchType = selectedType;
    };
    TextColumnFilterComponent.prototype.toggleMenu = function () {
        this.menuVisible = !this.menuVisible;
    };
    TextColumnFilterComponent.prototype.buttonFocusedOut = function () {
        this.menuVisible = false;
    };
    TextColumnFilterComponent.prototype.getConditionMenuState = function () {
        return this.menuVisible ? 'opened' : 'hidden';
    };
    TextColumnFilterComponent.prototype.customTemplateFilter = function (filterData) {
        this.filterStateService.raiseFilterChanged(this.columnId, this.filterData.propertyName, filterData, this.filterKey);
    };
    __decorate([
        core_1.ViewChild('customTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], TextColumnFilterComponent.prototype, "containerRef", void 0);
    TextColumnFilterComponent = __decorate([
        core_1.Component({
            selector: 'mg-text-column-filter',
            templateUrl: './text-column-filter.component.html',
            animations: [
                animations_1.filterConditionMenuAnimation
            ]
        }),
        __metadata("design:paramtypes", [filter_state_service_1.FilterStateService])
    ], TextColumnFilterComponent);
    return TextColumnFilterComponent;
}(filter_component_1.FilterComponent));
exports.TextColumnFilterComponent = TextColumnFilterComponent;
//# sourceMappingURL=text-column-filter.component.js.map