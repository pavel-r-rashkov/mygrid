export enum TextSearchTermType {
  StartsWith = 0,
  Contains = 1,
  Equals = 2,
  EndsWith = 3
}