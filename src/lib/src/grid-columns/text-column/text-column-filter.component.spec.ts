import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { TextColumnFilterComponent } from './text-column-filter.component';
import { FilterStateService } from '../../filter-state.service';
import { FilterState } from '../../states/filter-state';

@Component({
  selector: 'mgt-test-filter',
  template: `
    <ng-template #filterTemplate>
      <div id="test-filter-template"></div>
    </ng-template>
  `
})
class TestFilterComponent {
  @ViewChild('filterTemplate') filterTemplate: TemplateRef<any>;
}

describe('Component: TextColumnFilterComponent', () => {
  let textColumnFilterComponent: TextColumnFilterComponent;
  let fixture: ComponentFixture<TextColumnFilterComponent>;
  let filterTemplate: TemplateRef<any>;
  let filterStateService: FilterStateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        providers: [
          FilterStateService
        ],
        declarations: [
          TestFilterComponent
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(TextColumnFilterComponent);
        textColumnFilterComponent = fixture.componentInstance;

        var templateComponent: TestFilterComponent = TestBed.createComponent(TestFilterComponent).componentInstance;
        filterTemplate = templateComponent.filterTemplate;

        filterStateService = fixture.debugElement.injector.get(FilterStateService);
      });
  }));
  
  it('custom filter template should be rendered when it exists', () => {
    var data = {
      filterTemplate: filterTemplate,
      enableFiltering: true
    };
    textColumnFilterComponent.filterData = data;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-filter-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.filterData).toEqual(data);
    expect(customTemplate.context.$implicit.columnId).toEqual(textColumnFilterComponent.columnId);
    expect(customTemplate.context.$implicit.filter).toEqual(textColumnFilterComponent.customTemplateFilter);
  });

  it('selectedSearchType should be set on init', () => {
    var propertyName = 'foo';
    var searchTermType = 3;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    
    fixture.detectChanges();
    
    expect(textColumnFilterComponent.selectedSearchType).toEqual(searchTermType);
  });

  it('changing filter state should update filterText', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    var filterText = 'foo';

    fixture.detectChanges();
    filterStateService.raiseFilterChanged(columnId, propertyName, { text: filterText }, '');

    fixture.whenStable().then(() => {
      expect(textColumnFilterComponent.filterText).toEqual(filterText);
    });
  });

  it('getConditionMenuState should return opened when menu is visible', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    textColumnFilterComponent.menuVisible = true;

    fixture.detectChanges();
    var menuState = textColumnFilterComponent.getConditionMenuState();    

    expect(menuState).toEqual('opened');
  });

  it('getConditionMenuState should return hidden when menu is not visible', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    textColumnFilterComponent.menuVisible = false;

    fixture.detectChanges();
    var menuState = textColumnFilterComponent.getConditionMenuState();

    expect(menuState).toEqual('hidden');
  });

  it('buttonFocusedOut should hide the menu', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    textColumnFilterComponent.menuVisible = true;

    fixture.detectChanges();
    textColumnFilterComponent.buttonFocusedOut();

    expect(textColumnFilterComponent.menuVisible).toBeFalsy();
  });

  it('toggleMenu should show/hide menu', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    var menuVisible = false;
    textColumnFilterComponent.menuVisible = menuVisible;

    fixture.detectChanges();
    textColumnFilterComponent.toggleMenu();    

    expect(textColumnFilterComponent.menuVisible).toEqual(!menuVisible);
  });

  it('customTemplateFilter should create new filter state', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    var templateFilterData = {};
    var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    textColumnFilterComponent.customTemplateFilter(templateFilterData);

    expect(raiseFilterChangeSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangeSpy.calls.first();
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2]).toEqual(templateFilterData);
  });


  it('selectedSearchType should be set on init', () => {
    var propertyName = 'foo';
    var searchTermType = 3;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    
    fixture.detectChanges();
    
    expect(textColumnFilterComponent.selectedSearchType).toEqual(searchTermType);
  });

  it('selectSearchType should not create new filter state when there is no change in the selected search type', () => {
    var propertyName = 'foo';
    var searchTermType = 3;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    textColumnFilterComponent.filterText = 'foo';
    var raiseFilterChangedSpy: jasmine.Spy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    textColumnFilterComponent.selectSearchType(searchTermType);

    expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
  });

  it('selectSearchType should not create new filter state when there is no filter text entered', () => {
    var propertyName = 'foo';
    var searchTermType = 3;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    var raiseFilterChangedSpy: jasmine.Spy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    textColumnFilterComponent.selectSearchType(4);

    expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
  });

  it('selectSearchType should create new filter state when there is filter text entered and the search term type is changed', () => {
    var propertyName = 'foo';
    var searchTermType = 3;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    var filterText = 'foo';
    textColumnFilterComponent.filterText = filterText;
    var selectedSearchTermType = 4;
    var raiseFilterChangedSpy: jasmine.Spy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    textColumnFilterComponent.selectSearchType(selectedSearchTermType);

    expect(raiseFilterChangedSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangedSpy.calls.first();
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2].text).toEqual(filterText);
    expect(filterCall.args[2].searchTermType).toEqual(selectedSearchTermType);
  });

  it('filterFocusedOut should not create new filter state when filter text is not changed', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    textColumnFilterComponent.filterText = 'foo';
    var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');
    var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');

    fixture.detectChanges();
    textColumnFilterComponent.filterFocusIn();
    textColumnFilterComponent.filterFocusedOut({});

    expect(raiseFilterChangeSpy).not.toHaveBeenCalled();
    expect(clearColumnFilterSpy).not.toHaveBeenCalled();
  });

  it('filterFocusedOut should clear filter for this column when the entered text is null or empty', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');

    fixture.detectChanges();
    textColumnFilterComponent.filterText = 'foo';
    textColumnFilterComponent.filterFocusIn();
    textColumnFilterComponent.filterText = null;
    textColumnFilterComponent.filterFocusedOut({});

    expect(clearColumnFilterSpy).toHaveBeenCalled();
    expect(clearColumnFilterSpy.calls.first().args[0]).toEqual(textColumnFilterComponent.columnId);
  });

  it('filterFocusedOut should create new filter state when entered text is not null or empty', () => {
    var propertyName = 'foo';
    var searchTermType = 3;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    textColumnFilterComponent.filterData = data;
    var columnId = '123';
    textColumnFilterComponent.columnId = columnId;
    var filterText = 'foo';
    textColumnFilterComponent.filterText = filterText;
    var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    textColumnFilterComponent.filterFocusedOut({});

    expect(raiseFilterChangedSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangedSpy.calls.first()
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2].text).toEqual(filterText);
    expect(filterCall.args[2].searchTermType).toEqual(searchTermType);
  });
});