import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { IndexColumnDirective } from './index-column.directive';
import { GuidService } from '../../guid';

@Component({
  selector: 'mgt-test-index-column',
  template: `
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
  `
}) 
class TestIndexColumnComponent {
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
}

describe('Directive: IndexColumnDirective', () => {
  let indexColumnDirective: IndexColumnDirective;
  let headerTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestIndexColumnComponent
        ]
      })
      .compileComponents()
      .then(() => {
        indexColumnDirective = new IndexColumnDirective(new GuidService());

        var testComponent = TestBed.createComponent(TestIndexColumnComponent).componentInstance;
        headerTemplate = testComponent.headerTemplate;
      });
  }));

  it('getHeaderData should return index column header data', () => {
    var caption = 'foo';
    indexColumnDirective.caption = caption;
    indexColumnDirective.headerTemplate = headerTemplate;

    var headerData: any = indexColumnDirective.getHeaderData();

    expect(headerData.caption).toEqual(caption);
    expect(headerData.headerTemplate).toEqual(headerTemplate);
    expect(headerData.enableOrdering).toBeFalsy();
    expect(headerData.enableGrouping).toBeFalsy();
    expect(headerData.propertyName).toBeNull();
  });
});