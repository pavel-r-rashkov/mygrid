import { Component } from '@angular/core';

import { DataComponent } from '../contracts/data-component';

@Component({
	selector: 'mg-index-column-data',
	templateUrl: './index-column-data.component.html'
})
export class IndexColumnDataComponent extends DataComponent {
	constructor() {
		super();
	}

	ngOnInit() {
		
	}
}