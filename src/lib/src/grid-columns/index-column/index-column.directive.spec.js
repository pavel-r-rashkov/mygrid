"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var index_column_directive_1 = require("./index-column.directive");
var guid_1 = require("../../guid");
var TestIndexColumnComponent = (function () {
    function TestIndexColumnComponent() {
    }
    __decorate([
        core_1.ViewChild('headerTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestIndexColumnComponent.prototype, "headerTemplate", void 0);
    TestIndexColumnComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-index-column',
            template: "\n    <ng-template #headerTemplate>\n      <div id=\"test-header-template\"></div>\n    </ng-template>\n  "
        })
    ], TestIndexColumnComponent);
    return TestIndexColumnComponent;
}());
describe('Directive: IndexColumnDirective', function () {
    var indexColumnDirective;
    var headerTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestIndexColumnComponent
            ]
        })
            .compileComponents()
            .then(function () {
            indexColumnDirective = new index_column_directive_1.IndexColumnDirective(new guid_1.GuidService());
            var testComponent = testing_1.TestBed.createComponent(TestIndexColumnComponent).componentInstance;
            headerTemplate = testComponent.headerTemplate;
        });
    }));
    it('getHeaderData should return index column header data', function () {
        var caption = 'foo';
        indexColumnDirective.caption = caption;
        indexColumnDirective.headerTemplate = headerTemplate;
        var headerData = indexColumnDirective.getHeaderData();
        expect(headerData.caption).toEqual(caption);
        expect(headerData.headerTemplate).toEqual(headerTemplate);
        expect(headerData.enableOrdering).toBeFalsy();
        expect(headerData.enableGrouping).toBeFalsy();
        expect(headerData.propertyName).toBeNull();
    });
});
//# sourceMappingURL=index-column.directive.spec.js.map