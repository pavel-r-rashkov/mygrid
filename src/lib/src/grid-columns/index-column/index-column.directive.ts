import { 
	Directive, 
	forwardRef 
} from '@angular/core';

import { GridColumn } from '../grid-column';
import { DefaultColumnHeaderComponent } from '../shared/default-column-header.component';
import { IndexColumnDataComponent } from './index-column-data.component';
import { GuidService } from '../../guid';

@Directive({
	selector: 'mg-index-column',
	providers: [{provide: GridColumn, useExisting: forwardRef(() => IndexColumnDirective)}]
})
export class IndexColumnDirective extends GridColumn {
	constructor(
    guidService: GuidService) {
		super(
			DefaultColumnHeaderComponent, 
			null, 
			IndexColumnDataComponent, 
			null,
      guidService.newGuid());
	}

	public getHeaderData(): any {
    return {
      caption: this.caption == null ? '#' : this.caption,
      headerTemplate: this.headerTemplate,
      propertyName: null,
      enableOrdering: false,
      enableGrouping: false
    };
  }
}