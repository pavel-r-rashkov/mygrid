"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var GridColumn = (function () {
    function GridColumn(headerComponentType, filterComponentType, dataComponentType, editComponentType, columnId) {
        this.isVisible = true;
        this.headerComponentType = headerComponentType;
        this.filterComponentType = filterComponentType;
        this.dataComponentType = dataComponentType;
        this.editComponentType = editComponentType;
        this.columnId = columnId;
    }
    GridColumn.prototype.getHeaderData = function () {
        return {};
    };
    GridColumn.prototype.getData = function () {
        return {};
    };
    GridColumn.prototype.getFilterData = function () {
        return {};
    };
    GridColumn.prototype.getGroupData = function () {
        return {};
    };
    GridColumn.prototype.filter = function (val, filterData, item) {
        return true;
    };
    GridColumn.prototype.order = function (firstValue, secondValue, isAscending, firstItem, secondItem) {
        return 0;
    };
    GridColumn.prototype.group = function (firstValue, secondValue, isAscending, firstItem, secondItem) {
        return 0;
    };
    GridColumn.prototype.getValue = function (item) {
        return null;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], GridColumn.prototype, "caption", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], GridColumn.prototype, "isVisible", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], GridColumn.prototype, "width", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.Type)
    ], GridColumn.prototype, "headerComponentType", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.Type)
    ], GridColumn.prototype, "filterComponentType", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.Type)
    ], GridColumn.prototype, "dataComponentType", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.Type)
    ], GridColumn.prototype, "editComponentType", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], GridColumn.prototype, "headerTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], GridColumn.prototype, "filterTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], GridColumn.prototype, "dataTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], GridColumn.prototype, "editTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], GridColumn.prototype, "validationErrorsTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], GridColumn.prototype, "groupHeaderTemplate", void 0);
    return GridColumn;
}());
exports.GridColumn = GridColumn;
//# sourceMappingURL=grid-column.js.map