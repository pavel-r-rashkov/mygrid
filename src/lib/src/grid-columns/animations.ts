import {
  trigger,
  state,
  style,
  animate,
  transition,
  AnimationTriggerMetadata
} from '@angular/animations';

export const filterConditionMenuAnimation: AnimationTriggerMetadata =
  trigger('filterConditionMenuState', [
    state('hidden', style({
      top: '50%',
      opacity: '0'
    })),
    state('opened', style({
      top: '100%',
      opacity: '1'
    })),
    transition('hidden => opened', animate('150ms ease-in')),
    transition('opened => hidden', animate('150ms ease-out'))
  ]);