import { 
  Type,
  Input,
  TemplateRef
} from '@angular/core';

export class GridColumn {
  columnId: string;
  @Input() caption: string;
  @Input() isVisible: boolean = true;
  @Input() width: string;
  @Input() headerComponentType: Type<any>;
  @Input() filterComponentType: Type<any>;
  @Input() dataComponentType: Type<any>;
  @Input() editComponentType: Type<any>;
  @Input() headerTemplate: TemplateRef<any>;
  @Input() filterTemplate: TemplateRef<any>;
  @Input() dataTemplate: TemplateRef<any>;
  @Input() editTemplate: TemplateRef<any>;
  @Input() validationErrorsTemplate: TemplateRef<any>;
  @Input() groupHeaderTemplate: TemplateRef<any>;

  constructor(
    headerComponentType: Type<any>,
    filterComponentType: Type<any>,
    dataComponentType: Type<any>,
    editComponentType: Type<any>,
    columnId: string) {

    this.headerComponentType = headerComponentType;
    this.filterComponentType = filterComponentType;
    this.dataComponentType = dataComponentType;
    this.editComponentType = editComponentType;
    this.columnId = columnId;
  }

  public getHeaderData(): any {
    return {};
  }

  public getData(): any {
    return {};
  }

  public getFilterData(): any {
    return {};
  }

  public getGroupData(): any {
    return {};
  }

  public filter(val: any, filterData: any, item: any): boolean {
    return true;
  }

  public order(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    return 0;
  }

  public group(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    return 0;
  }

  public getValue(item: any): any {
    return null;
  }
}