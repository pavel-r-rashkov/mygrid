import { 
  Component, 
  OnInit,
  OnDestroy,
  AfterViewInit,
  Input,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { NgModel } from '@angular/forms';
import { Subscription } from 'rxjs/Rx';

import { FilterComponent } from '../contracts/filter-component';
import { FilterStateService } from '../../filter-state.service';
import { FilterState } from '../../states/filter-state';

@Component({
  selector: 'mg-combo-box-column-filter',
  templateUrl: './combo-box-column-filter.component.html'
})
export class ComboBoxColumnFilterComponent extends FilterComponent implements OnInit, AfterViewInit, OnDestroy {
  private readonly filterKey: string = 'ComboBox';
  private filterStateSubscription: Subscription;
  private currentFilterState: FilterState;
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;
  comboBoxValue: any;

  constructor(
    private filterStateService: FilterStateService) {
    super();
  }

  ngOnInit() {
    this.filterStateSubscription = this.filterStateService.filterState.subscribe((newState) => {
      let comboBoxColumnFilter = newState.columnFilters[this.columnId];
      this.comboBoxValue = comboBoxColumnFilter == null ? null : comboBoxColumnFilter.filterData.comboBoxValue;
    });
  }

  ngAfterViewInit() {
    if (this.filterData.filterTemplate != null) {
      let templateContext = {
        columnId: this.columnId,
        filterData: this.filterData,
        filter: this.customTemplateFilter
      }

      this.containerRef.createEmbeddedView(this.filterData.filterTemplate, { $implicit: templateContext });
    }
  }

  ngOnDestroy() {
    this.filterStateSubscription.unsubscribe();
  }

  filterChanged() {
    let isEmptySelection = this.comboBoxValue == '' || this.comboBoxValue == null;

    if (isEmptySelection) {
      this.filterStateService.clearColumnFilter(this.columnId);
    } else {
      this.filterStateService.raiseFilterChanged(
        this.columnId,
        this.filterData.propertyName, 
        { comboBoxValue: this.comboBoxValue == '' ? null : this.comboBoxValue },
        this.filterKey);
    }
  }

  customTemplateFilter(filterData: any) {
    this.filterStateService.raiseFilterChanged(
      this.columnId,
      this.filterData.propertyName,
      filterData,
      this.filterKey);
  }
}