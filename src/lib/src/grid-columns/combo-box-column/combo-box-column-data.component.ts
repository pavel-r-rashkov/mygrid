import { 
  Component, 
  OnInit,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { DataComponent } from '../contracts/data-component';

@Component({
  selector: 'mg-combo-box-column-data',
  templateUrl: './combo-box-column-data.component.html'
})
export class ComboBoxColumnDataComponent extends DataComponent implements OnInit {
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;

  constructor() {
    super();
  }

  ngOnInit() {
    if (this.data.dataTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item
      };
      this.containerRef.createEmbeddedView(this.data.dataTemplate, { $implicit: templateContext });
    }
  }

  getDisplayName() {
    if (this.data.comboBoxItems == null) {
      return '';
    }

    let comboBoxItemsCount = this.data.comboBoxItems.length;
    let comboBoxItem = null;
    for (var i = 0; i < comboBoxItemsCount; ++i) {
      let currentItem = this.data.comboBoxItems[i];
      
      if (currentItem[this.data.valueProperty] == this.item[this.data.propertyName]) {
        comboBoxItem = currentItem;
        break;
      }
    }

    if (comboBoxItem == null) {
      return '';
    }

    return comboBoxItem[this.data.displayProperty];
  }
}