import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { ComboBoxColumnDirective } from './combo-box-column.directive';
import { GuidService } from '../../guid';
import { DefaultComparerService } from '../shared/default-comparer.service';

@Component({
  selector: 'mgt-test-text-column',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
    <ng-template #editTemplate>
      <div id="test-edit-template"></div>
    </ng-template>
    <ng-template #validationErrorsTemplate>
      <div id="test-validation-error-template"></div>
    </ng-template>
    <ng-template #filterTemplate>
      <div id="test-filter-template"></div>
    </ng-template>
    <ng-template #groupHeaderTemplate>
      <div id="test-group-header-template"></div>
    </ng-template>
  `
}) 
class TestComboBoxColumnComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
  @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
  @ViewChild('validationErrorsTemplate') validationErrorsTemplate: TemplateRef<any>;
  @ViewChild('filterTemplate') filterTemplate: TemplateRef<any>;
  @ViewChild('groupHeaderTemplate') groupHeaderTemplate: TemplateRef<any>;
}

describe('Directive: ComboBoxColumnDirective', () => {
  let comboBoxColumnDirective: ComboBoxColumnDirective;
  let comparer: DefaultComparerService;
  let dataTemplate: TemplateRef<any>;
  let headerTemplate: TemplateRef<any>;
  let editTemplate: TemplateRef<any>;
  let validationErrorsTemplate: TemplateRef<any>;
  let filterTemplate: TemplateRef<any>;
  let groupHeaderTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestComboBoxColumnComponent
        ]
      })
      .compileComponents()
      .then(() => {
        comparer = new DefaultComparerService();
        comboBoxColumnDirective = new ComboBoxColumnDirective(new GuidService(), comparer);

        var testComponent = TestBed.createComponent(TestComboBoxColumnComponent).componentInstance;
        dataTemplate = testComponent.dataTemplate;
        headerTemplate = testComponent.headerTemplate;
        editTemplate = testComponent.editTemplate;
        validationErrorsTemplate = testComponent.validationErrorsTemplate;
        filterTemplate = testComponent.filterTemplate;
        groupHeaderTemplate = testComponent.groupHeaderTemplate;
      });
  }));

  it('getHeaderData should return combo box column header data', () => {
    var caption = 'foo';
    comboBoxColumnDirective.caption = caption;
    comboBoxColumnDirective.headerTemplate = headerTemplate;
    var propertyName = 'bar';
    comboBoxColumnDirective.propertyName = propertyName;
    var orderingEnabled = false;
    comboBoxColumnDirective.enableOrdering = orderingEnabled;
    var groupingEnabled = false;
    comboBoxColumnDirective.enableGrouping = groupingEnabled;

    var headerData: any = comboBoxColumnDirective.getHeaderData();

    expect(headerData.caption).toEqual(caption);
    expect(headerData.headerTemplate).toEqual(headerTemplate);
    expect(headerData.propertyName).toEqual(propertyName);
    expect(headerData.enableOrdering).toEqual(orderingEnabled);
    expect(headerData.enableGrouping).toEqual(groupingEnabled);
  });

  it('ordering and grouping should be enabled by default', () => {
    var headerData: any = comboBoxColumnDirective.getHeaderData();

    expect(headerData.enableOrdering).toBeTruthy();
    expect(headerData.enableGrouping).toBeTruthy();
  });

  it('getData should return combo box column data', () => {
    comboBoxColumnDirective.dataTemplate = dataTemplate;
    comboBoxColumnDirective.editTemplate = editTemplate;
    comboBoxColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
    var propertyName = 'bar';
    comboBoxColumnDirective.propertyName = propertyName;
    var comboBoxItems: any[] = [];
    comboBoxColumnDirective.comboBoxItems = comboBoxItems;
    var displayProperty = 'foo';
    comboBoxColumnDirective.displayProperty = displayProperty;
    var valueProperty = 'baz';
    comboBoxColumnDirective.valueProperty = valueProperty;
    var editionEnabled = false;
    comboBoxColumnDirective.enableEditing = editionEnabled;

    var data: any = comboBoxColumnDirective.getData();

    expect(data.dataTemplate).toEqual(dataTemplate);
    expect(data.editTemplate).toEqual(editTemplate);
    expect(data.validationErrorsTemplate).toEqual(validationErrorsTemplate);
    expect(data.propertyName).toEqual(propertyName);
    expect(data.comboBoxItems).toEqual(comboBoxItems);
    expect(data.displayProperty).toEqual(displayProperty);
    expect(data.valueProperty).toEqual(valueProperty);
    expect(data.enableEditing).toEqual(editionEnabled);
  });

  it('editing should be enabled by default', () => {
    var data: any = comboBoxColumnDirective.getData();

    expect(data.enableEditing).toBeTruthy();
  });

  it('getFilterData should return combo box column filter data', () => {
    comboBoxColumnDirective.filterTemplate = filterTemplate;
    var propertyName = 'bar';
    comboBoxColumnDirective.propertyName = propertyName;
    var filteringEnabled = false;
    comboBoxColumnDirective.enableFiltering = filteringEnabled;
    var comboBoxItems: any[] = [];
    comboBoxColumnDirective.comboBoxItems = comboBoxItems;
    var displayProperty = 'foo';
    comboBoxColumnDirective.displayProperty = displayProperty;
    var valueProperty = 'baz';
    comboBoxColumnDirective.valueProperty = valueProperty;

    var filterData: any = comboBoxColumnDirective.getFilterData();

    expect(filterData.filterTemplate).toEqual(filterTemplate);
    expect(filterData.propertyName).toEqual(propertyName);
    expect(filterData.enableFiltering).toEqual(filteringEnabled);
    expect(filterData.comboBoxItems).toEqual(comboBoxItems);
    expect(filterData.displayProperty).toEqual(displayProperty);
    expect(filterData.valueProperty).toEqual(valueProperty);
  });

  it('filtering should be enabled by default', () => {
    var filterData: any = comboBoxColumnDirective.getFilterData();

    expect(filterData.enableFiltering).toBeTruthy();
  });

  it('getGroupData should return combo box column group data', () => {
    comboBoxColumnDirective.groupHeaderTemplate = groupHeaderTemplate;
    comboBoxColumnDirective.editTemplate = editTemplate;
    comboBoxColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
    var propertyName = 'bar';
    comboBoxColumnDirective.propertyName = propertyName;
    var editionEnabled = false;
    comboBoxColumnDirective.enableEditing = editionEnabled;

    var groupData: any = comboBoxColumnDirective.getGroupData();

    expect(groupData.dataTemplate).toEqual(groupHeaderTemplate);
    expect(groupData.editTemplate).toEqual(editTemplate);
    expect(groupData.validationErrorsTemplate).toEqual(validationErrorsTemplate);
    expect(groupData.propertyName).toEqual(propertyName);
    expect(groupData.enableEditing).toEqual(editionEnabled);
  });

  it('getValue should return null when the column is not bound to a property', () => {
    var item = { foo: 'foo' };

    var columnValue = comboBoxColumnDirective.getValue(item);

    expect(columnValue).toEqual(null);
  });

  it('getValue should return the value of the property that the column is bound to', () => {
    var item = { foo: 'foo' };
    var propertyName = 'foo';
    comboBoxColumnDirective.propertyName = propertyName;

    var columnValue = comboBoxColumnDirective.getValue(item);

    expect(columnValue).toEqual(item[propertyName]);
  });

  it('order should call customOrder callback if it is provided', () => {
    comboBoxColumnDirective.customOrder = (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any): number => { return 0; };
    var orderResult = 1;
    var orderSpy: jasmine.Spy = spyOn(comboBoxColumnDirective, 'customOrder').and.returnValue(orderResult);

    var result = comboBoxColumnDirective.order(1, 2, true, {}, {});

    expect(result).toEqual(orderResult);
  });

  it('order should call default comparer', () => {
    var firstValue = 1;
    var secondValue = 2;
    var isAsecneding = true;
    var comparerSpy: jasmine.Spy = spyOn(comparer, 'compare');

    var result = comboBoxColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(comparerSpy).toHaveBeenCalled();
    var compareArgs = comparerSpy.calls.first().args;
    expect(compareArgs[0]).toEqual(firstValue);
    expect(compareArgs[1]).toEqual(secondValue);
    expect(compareArgs[2]).toEqual(isAsecneding);
  });

  it('group should call customGroup callback if it is provided', () => {
    comboBoxColumnDirective.customGroup = (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any): number => { return 0; };
    var groupResult = 1;
    var orderSpy: jasmine.Spy = spyOn(comboBoxColumnDirective, 'customGroup').and.returnValue(groupResult);

    var result = comboBoxColumnDirective.group(1, 2, true, {}, {});

    expect(result).toEqual(groupResult);
  });

  it('group should call default comparer', () => {
    var firstValue = 1;
    var secondValue = 2;
    var isAsecneding = true;
    var comparerSpy: jasmine.Spy = spyOn(comparer, 'compare');

    var result = comboBoxColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(comparerSpy).toHaveBeenCalled();
    var compareArgs = comparerSpy.calls.first().args;
    expect(compareArgs[0]).toEqual(firstValue);
    expect(compareArgs[1]).toEqual(secondValue);
    expect(compareArgs[2]).toEqual(isAsecneding);
  });

  it('filter should call customFilter callback if it is provided', () => {
    comboBoxColumnDirective.customFilter = (val: any, filterData: any, item: any): boolean => { return false; };
    var filterResult = true;
    var filterSpy: jasmine.Spy = spyOn(comboBoxColumnDirective, 'customFilter').and.returnValue(filterResult);
    var value = 1;
    var filterData = {};
    var item = {};

    var result = comboBoxColumnDirective.filter(value, filterData, item);

    expect(filterSpy).toHaveBeenCalled();
    var filterArgs = filterSpy.calls.first().args;
    expect(filterArgs[0]).toEqual(value);
    expect(filterArgs[1]).toEqual(filterData);
    expect(filterArgs[2]).toEqual(item);
  });

  it('filter with no filter data should return true', () => {
    var result = comboBoxColumnDirective.filter(1, null, {});

    expect(result).toBeTruthy();
  });

  it('filter with no combo box filter value should return true', () => {
    var result = comboBoxColumnDirective.filter(1, {}, {});

    expect(result).toBeTruthy();
  });

  it('filter with combo box filter value different than column value should return false', () => {
    var result = comboBoxColumnDirective.filter(1, { comboBoxValue: 2 }, {});

    expect(result).toBeFalsy();
  });

  it('filter with combo box filter value equal to column value should return true', () => {
    var result = comboBoxColumnDirective.filter(1, { comboBoxValue: 1 }, {});

    expect(result).toBeTruthy();
  });
});