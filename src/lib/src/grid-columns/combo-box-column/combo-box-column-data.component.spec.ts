import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { ComboBoxColumnDataComponent } from './combo-box-column-data.component';

@Component({
  selector: 'mgt-test-data',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
  `
})
class TestDataComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
}

describe('Component: SpinColumnDataComponent', () => {
  let comboBoxColumnDataComponent: ComboBoxColumnDataComponent;
  let fixture: ComponentFixture<ComboBoxColumnDataComponent>;
  let dataTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestDataComponent
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ComboBoxColumnDataComponent);
        comboBoxColumnDataComponent = fixture.componentInstance;

        var templateComponent: TestDataComponent = TestBed.createComponent(TestDataComponent).componentInstance;
        dataTemplate = templateComponent.dataTemplate;
      });
  }));

  it('custom data template should be rendered when it exists', () => {
    var item = {};
    var data = {
      dataTemplate: dataTemplate
    };
    comboBoxColumnDataComponent.data = data;
    comboBoxColumnDataComponent.item = item;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-data-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.data).toEqual(data);
    expect(customTemplate.context.$implicit.item).toEqual(item);
  });

  it('getDisplayName should return empty string when there are no combo box items', () => {
    var item = {};
    var data = {};
    comboBoxColumnDataComponent.data = data;
    comboBoxColumnDataComponent.item = item;

    fixture.detectChanges();
    var displayName = comboBoxColumnDataComponent.getDisplayName();    

    expect(displayName).toEqual('');
  });

  it('getDisplayName should return empty string when there are no combo box item with value property matching the property name of the column', () => {
    var item = { foo: 4 };
    var data = {
      propertyName: 'foo',
      valueProperty: 'bar',
      displayProperty: 'baz',
      comboBoxItems: [ 
        { bar: 1, baz: 'A' },
        { bar: 2, baz: 'B' },
        { bar: 3, baz: 'C' }
      ]
    };
    comboBoxColumnDataComponent.data = data;
    comboBoxColumnDataComponent.item = item;

    fixture.detectChanges();
    var displayName = comboBoxColumnDataComponent.getDisplayName();

    expect(displayName).toEqual('');
  });

  it('getDisplayName should return the value of the display property of the combo box item', () => {
    var item = { foo: 3 };
    var data = {
      propertyName: 'foo',
      valueProperty: 'bar',
      displayProperty: 'baz',
      comboBoxItems: [ 
        { bar: 1, baz: 'A' },
        { bar: 2, baz: 'B' },
        { bar: 3, baz: 'C' }
      ]
    };
    comboBoxColumnDataComponent.data = data;
    comboBoxColumnDataComponent.item = item;

    fixture.detectChanges();
    var displayName = comboBoxColumnDataComponent.getDisplayName();

    expect(displayName).toEqual('C');
  });
});