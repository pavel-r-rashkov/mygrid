import { 
  Component, 
  OnInit,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { EditComponent } from '../contracts/edit-component';

@Component({
  selector: 'mg-combo-box-column-edit',
  templateUrl: './combo-box-column-edit.component.html'
})
export class ComboBoxColumnEditComponent extends EditComponent implements OnInit {
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;

  constructor() {
    super();
  }
  
  ngOnInit() {
    if (this.data.editTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item,
        editForm: this.editForm
      };
      this.containerRef.createEmbeddedView(this.data.editTemplate, { $implicit: templateContext });
    }
  }
}