import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { ComboBoxColumnFilterComponent } from './combo-box-column-filter.component';
import { FilterStateService } from '../../filter-state.service';
import { FilterState } from '../../states/filter-state';

@Component({
  selector: 'mgt-test-filter',
  template: `
    <ng-template #filterTemplate>
      <div id="test-filter-template"></div>
    </ng-template>
  `
})
class TestFilterComponent {
  @ViewChild('filterTemplate') filterTemplate: TemplateRef<any>;
}

describe('Component: ComboBoxColumnFilterComponent', () => {
  let comboBoxColumnFilterComponent: ComboBoxColumnFilterComponent;
  let fixture: ComponentFixture<ComboBoxColumnFilterComponent>;
  let filterTemplate: TemplateRef<any>;
  let filterStateService: FilterStateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        providers: [
          FilterStateService
        ],
        declarations: [
          TestFilterComponent
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ComboBoxColumnFilterComponent);
        comboBoxColumnFilterComponent = fixture.componentInstance;

        var templateComponent: TestFilterComponent = TestBed.createComponent(TestFilterComponent).componentInstance;
        filterTemplate = templateComponent.filterTemplate;

        filterStateService = fixture.debugElement.injector.get(FilterStateService);
      });
  }));
  
  it('custom filter template should be rendered when it exists', () => {
    var data = {
      filterTemplate: filterTemplate,
      enableFiltering: true
    };
    comboBoxColumnFilterComponent.filterData = data;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-filter-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.filterData).toEqual(data);
    expect(customTemplate.context.$implicit.columnId).toEqual(comboBoxColumnFilterComponent.columnId);
    expect(customTemplate.context.$implicit.filter).toEqual(comboBoxColumnFilterComponent.customTemplateFilter);
  });

  it('changing filter state should update comboBoxValue', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    comboBoxColumnFilterComponent.filterData = data;
    var columnId = '123';
    comboBoxColumnFilterComponent.columnId = columnId;
    var comboBoxValue = 5;

    fixture.detectChanges();
    filterStateService.raiseFilterChanged(columnId, propertyName, { comboBoxValue: comboBoxValue }, '');

    fixture.whenStable().then(() => {
      expect(comboBoxColumnFilterComponent.comboBoxValue).toEqual(comboBoxValue);
    });
  });

  it('customTemplateFilter should create new filter state', () => {
    var columnId = '123';
    comboBoxColumnFilterComponent.columnId = columnId;
    comboBoxColumnFilterComponent.filterData = { enableFiltering: true };
    var propertyName = 'foo';
    var filterData = {
      propertyName: propertyName
    };
    comboBoxColumnFilterComponent.filterData = filterData;
    var templateFilterData = {};
    var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    comboBoxColumnFilterComponent.customTemplateFilter(templateFilterData);

    expect(raiseFilterChangeSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangeSpy.calls.first();
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2]).toEqual(templateFilterData);
  });

  it('filterChanged with empty combo box value should clear filter for this column', () => {
    var columnId = '123';
    comboBoxColumnFilterComponent.columnId = columnId;
    var filterData = {
      enableFiltering: true
    };
    comboBoxColumnFilterComponent.filterData = filterData;
    var propertyName = 'foo';
    comboBoxColumnFilterComponent.comboBoxValue = null;
    var clearFilterSpy = spyOn(filterStateService, 'clearColumnFilter');

    fixture.detectChanges();
    comboBoxColumnFilterComponent.filterChanged();

    expect(clearFilterSpy).toHaveBeenCalled();
    var clearFilterCall = clearFilterSpy.calls.first();
    expect(clearFilterCall.args[0]).toEqual(comboBoxColumnFilterComponent.columnId);
  });

  it('filterChanged with non empty combo box value should create new filter state', () => {
    var columnId = '123';
    comboBoxColumnFilterComponent.columnId = columnId;
    var propertyName = 'foo';
    var filterData = {
      propertyName: propertyName,
      enableFiltering: true
    };
    comboBoxColumnFilterComponent.filterData = filterData;
    var comboBoxValue = 5;
    comboBoxColumnFilterComponent.comboBoxValue = comboBoxValue;
    var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    comboBoxColumnFilterComponent.filterChanged();

    expect(raiseFilterChangedSpy).toHaveBeenCalled();
    var raiseFilterChangedCall = raiseFilterChangedSpy.calls.first();
    expect(raiseFilterChangedCall.args[0]).toEqual(columnId);
    expect(raiseFilterChangedCall.args[1]).toEqual(propertyName);
    expect(raiseFilterChangedCall.args[2].comboBoxValue).toEqual(comboBoxValue);
  });
});