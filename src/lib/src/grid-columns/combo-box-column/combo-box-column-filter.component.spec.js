"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var my_grid_module_1 = require("../../my-grid.module");
var combo_box_column_filter_component_1 = require("./combo-box-column-filter.component");
var filter_state_service_1 = require("../../filter-state.service");
var TestFilterComponent = (function () {
    function TestFilterComponent() {
    }
    __decorate([
        core_1.ViewChild('filterTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestFilterComponent.prototype, "filterTemplate", void 0);
    TestFilterComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-filter',
            template: "\n    <ng-template #filterTemplate>\n      <div id=\"test-filter-template\"></div>\n    </ng-template>\n  "
        })
    ], TestFilterComponent);
    return TestFilterComponent;
}());
describe('Component: ComboBoxColumnFilterComponent', function () {
    var comboBoxColumnFilterComponent;
    var fixture;
    var filterTemplate;
    var filterStateService;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [
                filter_state_service_1.FilterStateService
            ],
            declarations: [
                TestFilterComponent
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(combo_box_column_filter_component_1.ComboBoxColumnFilterComponent);
            comboBoxColumnFilterComponent = fixture.componentInstance;
            var templateComponent = testing_1.TestBed.createComponent(TestFilterComponent).componentInstance;
            filterTemplate = templateComponent.filterTemplate;
            filterStateService = fixture.debugElement.injector.get(filter_state_service_1.FilterStateService);
        });
    }));
    it('custom filter template should be rendered when it exists', function () {
        var data = {
            filterTemplate: filterTemplate,
            enableFiltering: true
        };
        comboBoxColumnFilterComponent.filterData = data;
        fixture.detectChanges();
        var customTemplate = fixture.debugElement.query(platform_browser_1.By.css('#test-filter-template'));
        expect(customTemplate).not.toBeNull();
        expect(customTemplate.context.$implicit.filterData).toEqual(data);
        expect(customTemplate.context.$implicit.columnId).toEqual(comboBoxColumnFilterComponent.columnId);
        expect(customTemplate.context.$implicit.filter).toEqual(comboBoxColumnFilterComponent.customTemplateFilter);
    });
    it('changing filter state should update comboBoxValue', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        comboBoxColumnFilterComponent.filterData = data;
        var columnId = '123';
        comboBoxColumnFilterComponent.columnId = columnId;
        var comboBoxValue = 5;
        fixture.detectChanges();
        filterStateService.raiseFilterChanged(columnId, propertyName, { comboBoxValue: comboBoxValue }, '');
        fixture.whenStable().then(function () {
            expect(comboBoxColumnFilterComponent.comboBoxValue).toEqual(comboBoxValue);
        });
    });
    it('customTemplateFilter should create new filter state', function () {
        var columnId = '123';
        comboBoxColumnFilterComponent.columnId = columnId;
        comboBoxColumnFilterComponent.filterData = { enableFiltering: true };
        var propertyName = 'foo';
        var filterData = {
            propertyName: propertyName
        };
        comboBoxColumnFilterComponent.filterData = filterData;
        var templateFilterData = {};
        var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        comboBoxColumnFilterComponent.customTemplateFilter(templateFilterData);
        expect(raiseFilterChangeSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangeSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2]).toEqual(templateFilterData);
    });
    it('filterChanged with empty combo box value should clear filter for this column', function () {
        var columnId = '123';
        comboBoxColumnFilterComponent.columnId = columnId;
        var filterData = {
            enableFiltering: true
        };
        comboBoxColumnFilterComponent.filterData = filterData;
        var propertyName = 'foo';
        comboBoxColumnFilterComponent.comboBoxValue = null;
        var clearFilterSpy = spyOn(filterStateService, 'clearColumnFilter');
        fixture.detectChanges();
        comboBoxColumnFilterComponent.filterChanged();
        expect(clearFilterSpy).toHaveBeenCalled();
        var clearFilterCall = clearFilterSpy.calls.first();
        expect(clearFilterCall.args[0]).toEqual(comboBoxColumnFilterComponent.columnId);
    });
    it('filterChanged with non empty combo box value should create new filter state', function () {
        var columnId = '123';
        comboBoxColumnFilterComponent.columnId = columnId;
        var propertyName = 'foo';
        var filterData = {
            propertyName: propertyName,
            enableFiltering: true
        };
        comboBoxColumnFilterComponent.filterData = filterData;
        var comboBoxValue = 5;
        comboBoxColumnFilterComponent.comboBoxValue = comboBoxValue;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        comboBoxColumnFilterComponent.filterChanged();
        expect(raiseFilterChangedSpy).toHaveBeenCalled();
        var raiseFilterChangedCall = raiseFilterChangedSpy.calls.first();
        expect(raiseFilterChangedCall.args[0]).toEqual(columnId);
        expect(raiseFilterChangedCall.args[1]).toEqual(propertyName);
        expect(raiseFilterChangedCall.args[2].comboBoxValue).toEqual(comboBoxValue);
    });
});
//# sourceMappingURL=combo-box-column-filter.component.spec.js.map