"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var grid_column_1 = require("../grid-column");
var default_column_header_component_1 = require("../shared/default-column-header.component");
var combo_box_column_filter_component_1 = require("./combo-box-column-filter.component");
var combo_box_column_data_component_1 = require("./combo-box-column-data.component");
var combo_box_column_edit_component_1 = require("./combo-box-column-edit.component");
var guid_1 = require("../../guid");
var default_comparer_service_1 = require("../shared/default-comparer.service");
var ComboBoxColumnDirective = (function (_super) {
    __extends(ComboBoxColumnDirective, _super);
    function ComboBoxColumnDirective(guidService, comparer) {
        var _this = _super.call(this, default_column_header_component_1.DefaultColumnHeaderComponent, combo_box_column_filter_component_1.ComboBoxColumnFilterComponent, combo_box_column_data_component_1.ComboBoxColumnDataComponent, combo_box_column_edit_component_1.ComboBoxColumnEditComponent, guidService.newGuid()) || this;
        _this.guidService = guidService;
        _this.comparer = comparer;
        return _this;
    }
    ComboBoxColumnDirective_1 = ComboBoxColumnDirective;
    ComboBoxColumnDirective.prototype.getHeaderData = function () {
        return {
            caption: this.caption,
            headerTemplate: this.headerTemplate,
            propertyName: this.propertyName,
            enableOrdering: this.enableOrdering == null ? true : this.enableOrdering,
            enableGrouping: this.enableGrouping == null ? true : this.enableGrouping
        };
    };
    ComboBoxColumnDirective.prototype.getData = function () {
        return {
            propertyName: this.propertyName,
            dataTemplate: this.dataTemplate,
            comboBoxItems: this.comboBoxItems,
            valueProperty: this.valueProperty,
            displayProperty: this.displayProperty,
            enableEditing: this.enableEditing == null ? true : this.enableEditing,
            editTemplate: this.editTemplate,
            validationErrorsTemplate: this.validationErrorsTemplate
        };
    };
    ComboBoxColumnDirective.prototype.getFilterData = function () {
        return {
            filterTemplate: this.filterTemplate,
            propertyName: this.propertyName,
            comboBoxItems: this.comboBoxItems,
            valueProperty: this.valueProperty,
            displayProperty: this.displayProperty,
            enableFiltering: this.enableFiltering == null ? true : this.enableFiltering
        };
    };
    ComboBoxColumnDirective.prototype.getGroupData = function () {
        var groupData = this.getData();
        groupData.dataTemplate = this.groupHeaderTemplate;
        return groupData;
    };
    ComboBoxColumnDirective.prototype.filter = function (val, filterData, item) {
        if (this.customFilter != null) {
            return this.customFilter(val, filterData, item);
        }
        if (filterData != null && filterData.comboBoxValue != null) {
            return val == filterData.comboBoxValue;
        }
        return true;
    };
    ComboBoxColumnDirective.prototype.order = function (firstValue, secondValue, isAscending, firstItem, secondItem) {
        if (this.customOrder != null) {
            return this.customOrder(firstValue, secondValue, isAscending, firstItem, secondItem);
        }
        else {
            return this.comparer.compare(firstValue, secondValue, isAscending);
        }
    };
    ComboBoxColumnDirective.prototype.group = function (firstValue, secondValue, isAscending, firstItem, secondItem) {
        if (this.customGroup != null) {
            return this.customGroup(firstValue, secondValue, isAscending, firstItem, secondItem);
        }
        else {
            return this.comparer.compare(firstValue, secondValue, isAscending);
        }
    };
    ComboBoxColumnDirective.prototype.getValue = function (item) {
        if (item[this.propertyName] == undefined) {
            return null;
        }
        return item[this.propertyName];
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ComboBoxColumnDirective.prototype, "propertyName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], ComboBoxColumnDirective.prototype, "enableOrdering", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], ComboBoxColumnDirective.prototype, "enableFiltering", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], ComboBoxColumnDirective.prototype, "enableEditing", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], ComboBoxColumnDirective.prototype, "enableGrouping", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], ComboBoxColumnDirective.prototype, "comboBoxItems", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ComboBoxColumnDirective.prototype, "valueProperty", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ComboBoxColumnDirective.prototype, "displayProperty", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], ComboBoxColumnDirective.prototype, "customFilter", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], ComboBoxColumnDirective.prototype, "customOrder", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], ComboBoxColumnDirective.prototype, "customGroup", void 0);
    ComboBoxColumnDirective = ComboBoxColumnDirective_1 = __decorate([
        core_1.Directive({
            selector: 'mg-combo-box-column',
            providers: [{ provide: grid_column_1.GridColumn, useExisting: core_1.forwardRef(function () { return ComboBoxColumnDirective_1; }) }]
        }),
        __metadata("design:paramtypes", [guid_1.GuidService,
            default_comparer_service_1.DefaultComparerService])
    ], ComboBoxColumnDirective);
    return ComboBoxColumnDirective;
    var ComboBoxColumnDirective_1;
}(grid_column_1.GridColumn));
exports.ComboBoxColumnDirective = ComboBoxColumnDirective;
//# sourceMappingURL=combo-box-column.directive.js.map