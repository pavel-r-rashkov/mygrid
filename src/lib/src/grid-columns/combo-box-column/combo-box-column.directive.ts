import { 
  Directive, 
  forwardRef,
  Input,
  TemplateRef
} from '@angular/core';

import { GridColumn } from '../grid-column';
import { DefaultColumnHeaderComponent } from '../shared/default-column-header.component';
import { ComboBoxColumnFilterComponent } from './combo-box-column-filter.component';
import { ComboBoxColumnDataComponent } from './combo-box-column-data.component';
import { ComboBoxColumnEditComponent } from './combo-box-column-edit.component';
import { GuidService } from '../../guid';
import { DefaultComparerService } from '../shared/default-comparer.service';

@Directive({
  selector: 'mg-combo-box-column',
  providers: [{provide: GridColumn, useExisting: forwardRef(() => ComboBoxColumnDirective)}]
})
export class ComboBoxColumnDirective extends GridColumn {
  @Input() propertyName: string;
  @Input() enableOrdering: boolean;
  @Input() enableFiltering: boolean;
  @Input() enableEditing: boolean;
  @Input() enableGrouping: boolean;
  @Input() comboBoxItems: any[];
  @Input() valueProperty: string;
  @Input() displayProperty: string;
  @Input() customFilter: (val: any, filterData: any, item: any) => boolean;
  @Input() customOrder: (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) => number;
  @Input() customGroup: (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) => number;

  constructor(
    private guidService: GuidService,
    private comparer: DefaultComparerService) {
    super(
      DefaultColumnHeaderComponent, 
      ComboBoxColumnFilterComponent, 
      ComboBoxColumnDataComponent,
      ComboBoxColumnEditComponent,
      guidService.newGuid());
  }

  public getHeaderData(): any {
    return {
      caption: this.caption,
      headerTemplate: this.headerTemplate,
      propertyName: this.propertyName,
      enableOrdering: this.enableOrdering == null ? true : this.enableOrdering,
      enableGrouping: this.enableGrouping == null ? true : this.enableGrouping
    };
  }

  public getData(): any {
    return {
      propertyName: this.propertyName,
      dataTemplate: this.dataTemplate,
      comboBoxItems: this.comboBoxItems,
      valueProperty: this.valueProperty,
      displayProperty: this.displayProperty,
      enableEditing: this.enableEditing == null ? true : this.enableEditing,
      editTemplate: this.editTemplate,
      validationErrorsTemplate: this.validationErrorsTemplate
    }
  }

  public getFilterData(): any {
    return {
      filterTemplate: this.filterTemplate,
      propertyName: this.propertyName,
      comboBoxItems: this.comboBoxItems,
      valueProperty: this.valueProperty,
      displayProperty: this.displayProperty,
      enableFiltering: this.enableFiltering == null ? true : this.enableFiltering
    }
  }

  public getGroupData(): any {
    let groupData = this.getData();
    groupData.dataTemplate = this.groupHeaderTemplate;
    return groupData;
  }

  public filter(val: any, filterData: any, item: any): boolean {
    if (this.customFilter != null) {
      return this.customFilter(val, filterData, item);
    }

    if (filterData != null && filterData.comboBoxValue != null) {
      return val == filterData.comboBoxValue;
    }

    return true;
  }

  public order(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    if (this.customOrder != null) {
      return this.customOrder(firstValue, secondValue, isAscending, firstItem, secondItem);
    } else {
      return this.comparer.compare(firstValue, secondValue, isAscending);
    }
  }

  public group(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    if (this.customGroup != null) {
      return this.customGroup(firstValue, secondValue, isAscending, firstItem, secondItem);
    } else {
      return this.comparer.compare(firstValue, secondValue, isAscending);
    }
  }

  public getValue(item: any) {
    if (item[this.propertyName] == undefined) {
      return null;
    }
    
    return item[this.propertyName];
  }
}