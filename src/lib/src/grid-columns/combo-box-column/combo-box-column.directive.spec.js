"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var combo_box_column_directive_1 = require("./combo-box-column.directive");
var guid_1 = require("../../guid");
var default_comparer_service_1 = require("../shared/default-comparer.service");
var TestComboBoxColumnComponent = (function () {
    function TestComboBoxColumnComponent() {
    }
    __decorate([
        core_1.ViewChild('dataTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestComboBoxColumnComponent.prototype, "dataTemplate", void 0);
    __decorate([
        core_1.ViewChild('headerTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestComboBoxColumnComponent.prototype, "headerTemplate", void 0);
    __decorate([
        core_1.ViewChild('editTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestComboBoxColumnComponent.prototype, "editTemplate", void 0);
    __decorate([
        core_1.ViewChild('validationErrorsTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestComboBoxColumnComponent.prototype, "validationErrorsTemplate", void 0);
    __decorate([
        core_1.ViewChild('filterTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestComboBoxColumnComponent.prototype, "filterTemplate", void 0);
    __decorate([
        core_1.ViewChild('groupHeaderTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestComboBoxColumnComponent.prototype, "groupHeaderTemplate", void 0);
    TestComboBoxColumnComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-text-column',
            template: "\n    <ng-template #dataTemplate>\n      <div id=\"test-data-template\"></div>\n    </ng-template>\n    <ng-template #headerTemplate>\n      <div id=\"test-header-template\"></div>\n    </ng-template>\n    <ng-template #editTemplate>\n      <div id=\"test-edit-template\"></div>\n    </ng-template>\n    <ng-template #validationErrorsTemplate>\n      <div id=\"test-validation-error-template\"></div>\n    </ng-template>\n    <ng-template #filterTemplate>\n      <div id=\"test-filter-template\"></div>\n    </ng-template>\n    <ng-template #groupHeaderTemplate>\n      <div id=\"test-group-header-template\"></div>\n    </ng-template>\n  "
        })
    ], TestComboBoxColumnComponent);
    return TestComboBoxColumnComponent;
}());
describe('Directive: ComboBoxColumnDirective', function () {
    var comboBoxColumnDirective;
    var comparer;
    var dataTemplate;
    var headerTemplate;
    var editTemplate;
    var validationErrorsTemplate;
    var filterTemplate;
    var groupHeaderTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestComboBoxColumnComponent
            ]
        })
            .compileComponents()
            .then(function () {
            comparer = new default_comparer_service_1.DefaultComparerService();
            comboBoxColumnDirective = new combo_box_column_directive_1.ComboBoxColumnDirective(new guid_1.GuidService(), comparer);
            var testComponent = testing_1.TestBed.createComponent(TestComboBoxColumnComponent).componentInstance;
            dataTemplate = testComponent.dataTemplate;
            headerTemplate = testComponent.headerTemplate;
            editTemplate = testComponent.editTemplate;
            validationErrorsTemplate = testComponent.validationErrorsTemplate;
            filterTemplate = testComponent.filterTemplate;
            groupHeaderTemplate = testComponent.groupHeaderTemplate;
        });
    }));
    it('getHeaderData should return combo box column header data', function () {
        var caption = 'foo';
        comboBoxColumnDirective.caption = caption;
        comboBoxColumnDirective.headerTemplate = headerTemplate;
        var propertyName = 'bar';
        comboBoxColumnDirective.propertyName = propertyName;
        var orderingEnabled = false;
        comboBoxColumnDirective.enableOrdering = orderingEnabled;
        var groupingEnabled = false;
        comboBoxColumnDirective.enableGrouping = groupingEnabled;
        var headerData = comboBoxColumnDirective.getHeaderData();
        expect(headerData.caption).toEqual(caption);
        expect(headerData.headerTemplate).toEqual(headerTemplate);
        expect(headerData.propertyName).toEqual(propertyName);
        expect(headerData.enableOrdering).toEqual(orderingEnabled);
        expect(headerData.enableGrouping).toEqual(groupingEnabled);
    });
    it('ordering and grouping should be enabled by default', function () {
        var headerData = comboBoxColumnDirective.getHeaderData();
        expect(headerData.enableOrdering).toBeTruthy();
        expect(headerData.enableGrouping).toBeTruthy();
    });
    it('getData should return combo box column data', function () {
        comboBoxColumnDirective.dataTemplate = dataTemplate;
        comboBoxColumnDirective.editTemplate = editTemplate;
        comboBoxColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
        var propertyName = 'bar';
        comboBoxColumnDirective.propertyName = propertyName;
        var comboBoxItems = [];
        comboBoxColumnDirective.comboBoxItems = comboBoxItems;
        var displayProperty = 'foo';
        comboBoxColumnDirective.displayProperty = displayProperty;
        var valueProperty = 'baz';
        comboBoxColumnDirective.valueProperty = valueProperty;
        var editionEnabled = false;
        comboBoxColumnDirective.enableEditing = editionEnabled;
        var data = comboBoxColumnDirective.getData();
        expect(data.dataTemplate).toEqual(dataTemplate);
        expect(data.editTemplate).toEqual(editTemplate);
        expect(data.validationErrorsTemplate).toEqual(validationErrorsTemplate);
        expect(data.propertyName).toEqual(propertyName);
        expect(data.comboBoxItems).toEqual(comboBoxItems);
        expect(data.displayProperty).toEqual(displayProperty);
        expect(data.valueProperty).toEqual(valueProperty);
        expect(data.enableEditing).toEqual(editionEnabled);
    });
    it('editing should be enabled by default', function () {
        var data = comboBoxColumnDirective.getData();
        expect(data.enableEditing).toBeTruthy();
    });
    it('getFilterData should return combo box column filter data', function () {
        comboBoxColumnDirective.filterTemplate = filterTemplate;
        var propertyName = 'bar';
        comboBoxColumnDirective.propertyName = propertyName;
        var filteringEnabled = false;
        comboBoxColumnDirective.enableFiltering = filteringEnabled;
        var comboBoxItems = [];
        comboBoxColumnDirective.comboBoxItems = comboBoxItems;
        var displayProperty = 'foo';
        comboBoxColumnDirective.displayProperty = displayProperty;
        var valueProperty = 'baz';
        comboBoxColumnDirective.valueProperty = valueProperty;
        var filterData = comboBoxColumnDirective.getFilterData();
        expect(filterData.filterTemplate).toEqual(filterTemplate);
        expect(filterData.propertyName).toEqual(propertyName);
        expect(filterData.enableFiltering).toEqual(filteringEnabled);
        expect(filterData.comboBoxItems).toEqual(comboBoxItems);
        expect(filterData.displayProperty).toEqual(displayProperty);
        expect(filterData.valueProperty).toEqual(valueProperty);
    });
    it('filtering should be enabled by default', function () {
        var filterData = comboBoxColumnDirective.getFilterData();
        expect(filterData.enableFiltering).toBeTruthy();
    });
    it('getGroupData should return combo box column group data', function () {
        comboBoxColumnDirective.groupHeaderTemplate = groupHeaderTemplate;
        comboBoxColumnDirective.editTemplate = editTemplate;
        comboBoxColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
        var propertyName = 'bar';
        comboBoxColumnDirective.propertyName = propertyName;
        var editionEnabled = false;
        comboBoxColumnDirective.enableEditing = editionEnabled;
        var groupData = comboBoxColumnDirective.getGroupData();
        expect(groupData.dataTemplate).toEqual(groupHeaderTemplate);
        expect(groupData.editTemplate).toEqual(editTemplate);
        expect(groupData.validationErrorsTemplate).toEqual(validationErrorsTemplate);
        expect(groupData.propertyName).toEqual(propertyName);
        expect(groupData.enableEditing).toEqual(editionEnabled);
    });
    it('getValue should return null when the column is not bound to a property', function () {
        var item = { foo: 'foo' };
        var columnValue = comboBoxColumnDirective.getValue(item);
        expect(columnValue).toEqual(null);
    });
    it('getValue should return the value of the property that the column is bound to', function () {
        var item = { foo: 'foo' };
        var propertyName = 'foo';
        comboBoxColumnDirective.propertyName = propertyName;
        var columnValue = comboBoxColumnDirective.getValue(item);
        expect(columnValue).toEqual(item[propertyName]);
    });
    it('order should call customOrder callback if it is provided', function () {
        comboBoxColumnDirective.customOrder = function (firstValue, secondValue, isAscending, firstItem, secondItem) { return 0; };
        var orderResult = 1;
        var orderSpy = spyOn(comboBoxColumnDirective, 'customOrder').and.returnValue(orderResult);
        var result = comboBoxColumnDirective.order(1, 2, true, {}, {});
        expect(result).toEqual(orderResult);
    });
    it('order should call default comparer', function () {
        var firstValue = 1;
        var secondValue = 2;
        var isAsecneding = true;
        var comparerSpy = spyOn(comparer, 'compare');
        var result = comboBoxColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});
        expect(comparerSpy).toHaveBeenCalled();
        var compareArgs = comparerSpy.calls.first().args;
        expect(compareArgs[0]).toEqual(firstValue);
        expect(compareArgs[1]).toEqual(secondValue);
        expect(compareArgs[2]).toEqual(isAsecneding);
    });
    it('group should call customGroup callback if it is provided', function () {
        comboBoxColumnDirective.customGroup = function (firstValue, secondValue, isAscending, firstItem, secondItem) { return 0; };
        var groupResult = 1;
        var orderSpy = spyOn(comboBoxColumnDirective, 'customGroup').and.returnValue(groupResult);
        var result = comboBoxColumnDirective.group(1, 2, true, {}, {});
        expect(result).toEqual(groupResult);
    });
    it('group should call default comparer', function () {
        var firstValue = 1;
        var secondValue = 2;
        var isAsecneding = true;
        var comparerSpy = spyOn(comparer, 'compare');
        var result = comboBoxColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});
        expect(comparerSpy).toHaveBeenCalled();
        var compareArgs = comparerSpy.calls.first().args;
        expect(compareArgs[0]).toEqual(firstValue);
        expect(compareArgs[1]).toEqual(secondValue);
        expect(compareArgs[2]).toEqual(isAsecneding);
    });
    it('filter should call customFilter callback if it is provided', function () {
        comboBoxColumnDirective.customFilter = function (val, filterData, item) { return false; };
        var filterResult = true;
        var filterSpy = spyOn(comboBoxColumnDirective, 'customFilter').and.returnValue(filterResult);
        var value = 1;
        var filterData = {};
        var item = {};
        var result = comboBoxColumnDirective.filter(value, filterData, item);
        expect(filterSpy).toHaveBeenCalled();
        var filterArgs = filterSpy.calls.first().args;
        expect(filterArgs[0]).toEqual(value);
        expect(filterArgs[1]).toEqual(filterData);
        expect(filterArgs[2]).toEqual(item);
    });
    it('filter with no filter data should return true', function () {
        var result = comboBoxColumnDirective.filter(1, null, {});
        expect(result).toBeTruthy();
    });
    it('filter with no combo box filter value should return true', function () {
        var result = comboBoxColumnDirective.filter(1, {}, {});
        expect(result).toBeTruthy();
    });
    it('filter with combo box filter value different than column value should return false', function () {
        var result = comboBoxColumnDirective.filter(1, { comboBoxValue: 2 }, {});
        expect(result).toBeFalsy();
    });
    it('filter with combo box filter value equal to column value should return true', function () {
        var result = comboBoxColumnDirective.filter(1, { comboBoxValue: 1 }, {});
        expect(result).toBeTruthy();
    });
});
//# sourceMappingURL=combo-box-column.directive.spec.js.map