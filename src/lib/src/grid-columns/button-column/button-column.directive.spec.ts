import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { ButtonColumnDirective } from './button-column.directive';
import { GuidService } from '../../guid';

@Component({
  selector: 'mgt-test-button',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
  `
}) 
class TestButtonComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
}

describe('Directive: ButtonColumnDirective', () => {
  let buttonColumnDirective: ButtonColumnDirective;
  let dataTemplate: TemplateRef<any>;
  let headerTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestButtonComponent
        ]
      })
      .compileComponents()
      .then(() => {
        buttonColumnDirective = new ButtonColumnDirective(new GuidService());

        var testComponent = TestBed.createComponent(TestButtonComponent).componentInstance;
        dataTemplate = testComponent.dataTemplate;
        headerTemplate = testComponent.headerTemplate;
      });
  }));

  it('getHeaderData should return button header data', () => {
    var caption = 'foo';
    buttonColumnDirective.caption = caption;
    buttonColumnDirective.headerTemplate = headerTemplate;

    var headerData: any = buttonColumnDirective.getHeaderData();

    expect(headerData.caption).toEqual(caption);
    expect(headerData.headerTemplate).toEqual(headerTemplate);
    expect(headerData.enableOrdering).toBeFalsy();
  });

  it('getData should return button data', () => {
    var buttonText: string = 'foo';
    buttonColumnDirective.buttonText = buttonText;
    var clickCallback = (item: any): void => {};
    buttonColumnDirective.clickCallback = clickCallback;
    buttonColumnDirective.dataTemplate = dataTemplate;

    var data: any = buttonColumnDirective.getData();

    expect(data.buttonText).toEqual(buttonText);
    expect(data.dataTemplate).toEqual(dataTemplate);
    expect(data.clickCallback).toEqual(clickCallback);
  });
});