import { 
  Component, 
  OnInit,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { DataComponent } from '../contracts/data-component';

@Component({
  selector: 'mg-button-column-data',
  templateUrl: './button-column-data.component.html'
})
export class ButtonColumnDataComponent extends DataComponent implements OnInit {
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;    

  constructor() {
    super();
  }

  ngOnInit() {
    if (this.data.dataTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item,
        buttonClick: this.buttonClick
      };
      this.containerRef.createEmbeddedView(this.data.dataTemplate, { $implicit: templateContext });
    }
  }

  buttonClick() {
    if (this.data.clickCallback != null) {
      this.data.clickCallback(this.item);
    }
  }
}