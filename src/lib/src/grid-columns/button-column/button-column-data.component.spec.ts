import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { ButtonColumnDataComponent } from './button-column-data.component';

@Component({
  selector: 'mgt-test-data',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
  `
})
class TestDataComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
}

describe('Component: ButtonColumnDataComponent', () => {
  let buttonColumnDataComponent: ButtonColumnDataComponent;
  let fixture: ComponentFixture<ButtonColumnDataComponent>;
  let dataTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestDataComponent
        ],
        providers: [
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ButtonColumnDataComponent);
        buttonColumnDataComponent = fixture.componentInstance;

        var templateComponent: TestDataComponent = TestBed.createComponent(TestDataComponent).componentInstance;
        dataTemplate = templateComponent.dataTemplate;
      });
  }));

  it('buttonClick should call clickCallback when exists', () => {
    var item = {};
    var data = {
      clickCallback: (item: any) => {}
    };
    buttonColumnDataComponent.item = item;
    buttonColumnDataComponent.data = data;
    var buttonClickSpy: jasmine.Spy = spyOn(buttonColumnDataComponent.data, 'clickCallback');

    fixture.detectChanges();
    buttonColumnDataComponent.buttonClick();

    expect(buttonClickSpy).toHaveBeenCalled();
    expect(buttonClickSpy.calls.first().args[0]).toEqual(item);
  });

  it('custom data template should be rendered when it exists', () => {
    var item = {};
    var data = {
      dataTemplate: dataTemplate
    };
    buttonColumnDataComponent.data = data;
    buttonColumnDataComponent.item = item;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-data-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.data).toEqual(data);
    expect(customTemplate.context.$implicit.item).toEqual(item);
    expect(customTemplate.context.$implicit.buttonClick).toEqual(buttonColumnDataComponent.buttonClick);
  });
});