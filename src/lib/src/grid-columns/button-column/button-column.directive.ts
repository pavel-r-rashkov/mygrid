import { 
  Directive, 
  OnInit,
  forwardRef,
  Input,
  TemplateRef
} from '@angular/core';

import { GridColumn } from '../grid-column';
import { ButtonColumnDataComponent } from './button-column-data.component';
import { DefaultColumnHeaderComponent } from '../shared/default-column-header.component';
import { GuidService } from '../../guid';

@Directive({
  selector: 'mg-button-column',
  providers: [{provide: GridColumn, useExisting: forwardRef(() => ButtonColumnDirective)}]
})
export class ButtonColumnDirective extends GridColumn implements OnInit {
  @Input() buttonText: string;
  @Input() clickCallback: any;

  constructor(
    guidService: GuidService) {
    super(
      DefaultColumnHeaderComponent,
      null,
      ButtonColumnDataComponent,
      null,
      guidService.newGuid());
  }

  ngOnInit() {
  }

  public getHeaderData(): any {
    return {
      caption: this.caption,
      headerTemplate: this.headerTemplate,
      enableOrdering: false
    };
  }

  public getData(): any {
    return {
      dataTemplate: this.dataTemplate,
      buttonText: this.buttonText,
      clickCallback: this.clickCallback
    }
  }
}