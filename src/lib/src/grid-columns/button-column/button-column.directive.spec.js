"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var button_column_directive_1 = require("./button-column.directive");
var guid_1 = require("../../guid");
var TestButtonComponent = (function () {
    function TestButtonComponent() {
    }
    __decorate([
        core_1.ViewChild('dataTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestButtonComponent.prototype, "dataTemplate", void 0);
    __decorate([
        core_1.ViewChild('headerTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestButtonComponent.prototype, "headerTemplate", void 0);
    TestButtonComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-button',
            template: "\n    <ng-template #dataTemplate>\n      <div id=\"test-data-template\"></div>\n    </ng-template>\n    <ng-template #headerTemplate>\n      <div id=\"test-header-template\"></div>\n    </ng-template>\n  "
        })
    ], TestButtonComponent);
    return TestButtonComponent;
}());
describe('Directive: ButtonColumnDirective', function () {
    var buttonColumnDirective;
    var dataTemplate;
    var headerTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestButtonComponent
            ]
        })
            .compileComponents()
            .then(function () {
            buttonColumnDirective = new button_column_directive_1.ButtonColumnDirective(new guid_1.GuidService());
            var testComponent = testing_1.TestBed.createComponent(TestButtonComponent).componentInstance;
            dataTemplate = testComponent.dataTemplate;
            headerTemplate = testComponent.headerTemplate;
        });
    }));
    it('getHeaderData should return button header data', function () {
        var caption = 'foo';
        buttonColumnDirective.caption = caption;
        buttonColumnDirective.headerTemplate = headerTemplate;
        var headerData = buttonColumnDirective.getHeaderData();
        expect(headerData.caption).toEqual(caption);
        expect(headerData.headerTemplate).toEqual(headerTemplate);
        expect(headerData.enableOrdering).toBeFalsy();
    });
    it('getData should return button data', function () {
        var buttonText = 'foo';
        buttonColumnDirective.buttonText = buttonText;
        var clickCallback = function (item) { };
        buttonColumnDirective.clickCallback = clickCallback;
        buttonColumnDirective.dataTemplate = dataTemplate;
        var data = buttonColumnDirective.getData();
        expect(data.buttonText).toEqual(buttonText);
        expect(data.dataTemplate).toEqual(dataTemplate);
        expect(data.clickCallback).toEqual(clickCallback);
    });
});
//# sourceMappingURL=button-column.directive.spec.js.map