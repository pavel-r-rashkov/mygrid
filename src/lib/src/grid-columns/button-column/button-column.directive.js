"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var grid_column_1 = require("../grid-column");
var button_column_data_component_1 = require("./button-column-data.component");
var default_column_header_component_1 = require("../shared/default-column-header.component");
var guid_1 = require("../../guid");
var ButtonColumnDirective = (function (_super) {
    __extends(ButtonColumnDirective, _super);
    function ButtonColumnDirective(guidService) {
        return _super.call(this, default_column_header_component_1.DefaultColumnHeaderComponent, null, button_column_data_component_1.ButtonColumnDataComponent, null, guidService.newGuid()) || this;
    }
    ButtonColumnDirective_1 = ButtonColumnDirective;
    ButtonColumnDirective.prototype.ngOnInit = function () {
    };
    ButtonColumnDirective.prototype.getHeaderData = function () {
        return {
            caption: this.caption,
            headerTemplate: this.headerTemplate,
            enableOrdering: false
        };
    };
    ButtonColumnDirective.prototype.getData = function () {
        return {
            dataTemplate: this.dataTemplate,
            buttonText: this.buttonText,
            clickCallback: this.clickCallback
        };
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ButtonColumnDirective.prototype, "buttonText", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ButtonColumnDirective.prototype, "clickCallback", void 0);
    ButtonColumnDirective = ButtonColumnDirective_1 = __decorate([
        core_1.Directive({
            selector: 'mg-button-column',
            providers: [{ provide: grid_column_1.GridColumn, useExisting: core_1.forwardRef(function () { return ButtonColumnDirective_1; }) }]
        }),
        __metadata("design:paramtypes", [guid_1.GuidService])
    ], ButtonColumnDirective);
    return ButtonColumnDirective;
    var ButtonColumnDirective_1;
}(grid_column_1.GridColumn));
exports.ButtonColumnDirective = ButtonColumnDirective;
//# sourceMappingURL=button-column.directive.js.map