import { 
  Directive,
  TemplateRef,
  Input,
  ContentChild,
  forwardRef 
} from '@angular/core';

import { GridColumn } from '../grid-column';
import { DefaultColumnHeaderComponent } from '../shared/default-column-header.component';
import { EditColumnDataComponent } from './edit-column-data.component';
import { EditColumnEditComponent } from './edit-column-edit.component';
import { GuidService } from '../../guid';

@Directive({
  selector: 'mg-edit-column',
  providers: [{provide: GridColumn, useExisting: forwardRef(() => EditColumnDirective)}]
})
export class EditColumnDirective extends GridColumn {
  @Input() editButtonText: string = 'EDIT';
  @Input() cancelButtonText: string = 'CANCEL';
  @Input() saveButtonText: string = 'SAVE';

  constructor(
    guidService: GuidService) {
    super(
      DefaultColumnHeaderComponent, 
      null, 
      EditColumnDataComponent, 
      EditColumnEditComponent,
      guidService.newGuid());
  }

  public getHeaderData(): any {
    return {
      caption: this.caption,
      headerTemplate: this.headerTemplate,
      enableOrdering: false
    };
  }

  public getData(): any {
    return {
      dataTemplate: this.dataTemplate,
      editButtonText: this.editButtonText,
      cancelButtonText: this.cancelButtonText,
      saveButtonText: this.saveButtonText
    }
  }
}