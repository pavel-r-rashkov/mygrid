import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { 
  ReactiveFormsModule,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { EditColumnEditComponent } from './edit-column-edit.component';
import { RowEditionService } from '../../edition/row-edition.service';
import { GridSettingsService } from '../../grid-settings.service';

@Component({
  selector: 'mgt-test-edit',
  template: `
    <ng-template #editTemplate>
      <div id="test-edit-template"></div>
    </ng-template>
  `
})
class TestEditComponent {
  @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
}

describe('Component: EditColumnEditComponent', () => {
  let editColumnEditComponent: EditColumnEditComponent;
  let fixture: ComponentFixture<EditColumnEditComponent>;
  let rowEditionService: RowEditionService;
  let gridSettingsService: GridSettingsService;
  let editTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestEditComponent
        ],
        providers: [
          RowEditionService,
          GridSettingsService
        ],
        imports: [
          MyGridModule, 
          ReactiveFormsModule
        ]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(EditColumnEditComponent);
        editColumnEditComponent = fixture.componentInstance;

        rowEditionService = fixture.debugElement.injector.get(RowEditionService);
        gridSettingsService = fixture.debugElement.injector.get(GridSettingsService);

        var templateComponent: TestEditComponent = TestBed.createComponent(TestEditComponent).componentInstance;
        editTemplate = templateComponent.editTemplate;
      });
  }));

  it('custom edit template should be rendered when it exists', () => {
    var item = {};
    var data = {
      editTemplate: editTemplate
    };
    var form = new FormGroup({});
    editColumnEditComponent.data = data;
    editColumnEditComponent.item = item;
    editColumnEditComponent.editForm = form;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-edit-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.data).toEqual(data);
    expect(customTemplate.context.$implicit.item).toEqual(item);
    expect(customTemplate.context.$implicit.editForm).toEqual(form);
    expect(customTemplate.context.$implicit.saveChanges).toEqual(editColumnEditComponent.saveChanges);
    expect(customTemplate.context.$implicit.cancelEdit).toEqual(editColumnEditComponent.cancelEdit);
  });

  it('cancelEdit should raise cancelRowEdition with item\'s key when an item is edited', () => {
    var key = 'foo';
    var item = { foo: 'foo' };
    gridSettingsService.key = key;
    editColumnEditComponent.item = item;
    editColumnEditComponent.data = {};
    var cancelRowEditionSpy: jasmine.Spy = spyOn(rowEditionService, 'cancelRowEdition');

    fixture.detectChanges();
    editColumnEditComponent.cancelEdit();

    expect(cancelRowEditionSpy).toHaveBeenCalled();
    expect(cancelRowEditionSpy.calls.first().args[0]).toEqual(item[key]);
  });

  it('cancelEdit should raise cancelRowEdition with null key when a new item is being added', () => {
    var key = 'foo';
    gridSettingsService.key = key;
    editColumnEditComponent.data = {};
    var cancelRowEditionSpy: jasmine.Spy = spyOn(rowEditionService, 'cancelRowEdition');

    fixture.detectChanges();
    editColumnEditComponent.cancelEdit();

    expect(cancelRowEditionSpy).toHaveBeenCalled();
    expect(cancelRowEditionSpy.calls.first().args[0]).toBeNull();
  });

  it('saveChanges should raise endRowEdition with item\'s key when an item is edited and the form is valid, form should be marked as dirty', () => {
    var key = 'foo';
    var item = { foo: 'foo' };
    gridSettingsService.key = key;
    editColumnEditComponent.item = item;
    editColumnEditComponent.data = { propertyName: key };
    var formGroup = {};
    formGroup[key] = new FormControl();
    editColumnEditComponent.editForm = new FormGroup(formGroup);
    var endRowEditionSpy: jasmine.Spy = spyOn(rowEditionService, 'endRowEdition');

    fixture.detectChanges();
    editColumnEditComponent.saveChanges();

    expect(endRowEditionSpy).toHaveBeenCalled();
    expect(endRowEditionSpy.calls.first().args[0]).toEqual(item[key]);
    expect(editColumnEditComponent.editForm.dirty).toBeTruthy();
  });

  it('saveChanges should raise endRowEdition with null key when a new item is being added and the form is valid, form should be marked as dirty', () => {
    var key = 'foo';
    gridSettingsService.key = key;
    editColumnEditComponent.data = { propertyName: key };
    var formGroup = {};
    formGroup[key] = new FormControl();
    editColumnEditComponent.editForm = new FormGroup(formGroup);
    var cancelRowEditionSpy: jasmine.Spy = spyOn(rowEditionService, 'endRowEdition');

    fixture.detectChanges();
    editColumnEditComponent.saveChanges();

    expect(cancelRowEditionSpy).toHaveBeenCalled();
    expect(cancelRowEditionSpy.calls.first().args[0]).toBeNull();
    expect(editColumnEditComponent.editForm.dirty).toBeTruthy();
  });

  it('saveChanges with validation errors should mark the form dirty', () => {
    var key = 'foo';
    gridSettingsService.key = key;
    editColumnEditComponent.data = { propertyName: key };
    var formGroup = {};
    formGroup[key] = new FormControl(null, Validators.required);
    editColumnEditComponent.editForm = new FormGroup(formGroup);
    var cancelRowEditionSpy: jasmine.Spy = spyOn(rowEditionService, 'endRowEdition');

    fixture.detectChanges();
    editColumnEditComponent.saveChanges();

    expect(cancelRowEditionSpy).not.toHaveBeenCalled();
    expect(editColumnEditComponent.editForm.dirty).toBeTruthy();
  });
});