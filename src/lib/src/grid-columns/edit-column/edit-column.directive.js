"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var grid_column_1 = require("../grid-column");
var default_column_header_component_1 = require("../shared/default-column-header.component");
var edit_column_data_component_1 = require("./edit-column-data.component");
var edit_column_edit_component_1 = require("./edit-column-edit.component");
var guid_1 = require("../../guid");
var EditColumnDirective = (function (_super) {
    __extends(EditColumnDirective, _super);
    function EditColumnDirective(guidService) {
        var _this = _super.call(this, default_column_header_component_1.DefaultColumnHeaderComponent, null, edit_column_data_component_1.EditColumnDataComponent, edit_column_edit_component_1.EditColumnEditComponent, guidService.newGuid()) || this;
        _this.editButtonText = 'EDIT';
        _this.cancelButtonText = 'CANCEL';
        _this.saveButtonText = 'SAVE';
        return _this;
    }
    EditColumnDirective_1 = EditColumnDirective;
    EditColumnDirective.prototype.getHeaderData = function () {
        return {
            caption: this.caption,
            headerTemplate: this.headerTemplate,
            enableOrdering: false
        };
    };
    EditColumnDirective.prototype.getData = function () {
        return {
            dataTemplate: this.dataTemplate,
            editButtonText: this.editButtonText,
            cancelButtonText: this.cancelButtonText,
            saveButtonText: this.saveButtonText
        };
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], EditColumnDirective.prototype, "editButtonText", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], EditColumnDirective.prototype, "cancelButtonText", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], EditColumnDirective.prototype, "saveButtonText", void 0);
    EditColumnDirective = EditColumnDirective_1 = __decorate([
        core_1.Directive({
            selector: 'mg-edit-column',
            providers: [{ provide: grid_column_1.GridColumn, useExisting: core_1.forwardRef(function () { return EditColumnDirective_1; }) }]
        }),
        __metadata("design:paramtypes", [guid_1.GuidService])
    ], EditColumnDirective);
    return EditColumnDirective;
    var EditColumnDirective_1;
}(grid_column_1.GridColumn));
exports.EditColumnDirective = EditColumnDirective;
//# sourceMappingURL=edit-column.directive.js.map