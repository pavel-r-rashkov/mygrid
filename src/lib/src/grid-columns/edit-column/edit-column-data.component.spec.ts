import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { EditColumnDataComponent } from './edit-column-data.component';
import { RowEditionService } from '../../edition/row-edition.service';
import { GridSettingsService } from '../../grid-settings.service';

@Component({
  selector: 'mgt-test-data',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
  `
})
class TestDataComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
}

describe('Component: EditColumnDataComponent', () => {
  let editColumnDataComponent: EditColumnDataComponent;
  let fixture: ComponentFixture<EditColumnDataComponent>;
  let dataTemplate: TemplateRef<any>;
  let rowEditionService: RowEditionService;
  let gridSettingsService: GridSettingsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestDataComponent
        ],
        providers: [
          RowEditionService,
          GridSettingsService
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(EditColumnDataComponent);
        editColumnDataComponent = fixture.componentInstance;

        rowEditionService = fixture.debugElement.injector.get(RowEditionService);
        gridSettingsService = fixture.debugElement.injector.get(GridSettingsService);

        var templateComponent: TestDataComponent = TestBed.createComponent(TestDataComponent).componentInstance;
        dataTemplate = templateComponent.dataTemplate;
      });
  }));

  it('editRow should raise startRowEdition with item\'s key', () => {
    var key = 'foo';
    var item = { foo: 'foo' };
    gridSettingsService.key = key;
    editColumnDataComponent.item = item;
    editColumnDataComponent.data = {};
    var startRowEditionSpy: jasmine.Spy = spyOn(rowEditionService, 'startRowEdition');

    fixture.detectChanges();
    editColumnDataComponent.editRow();

    expect(startRowEditionSpy).toHaveBeenCalled();
    expect(startRowEditionSpy.calls.first().args[0]).toEqual(item[key]);
  });

  it('custom data template should be rendered when it exists', () => {
    var item = {};
    var data = {
      dataTemplate: dataTemplate
    };
    editColumnDataComponent.data = data;
    editColumnDataComponent.item = item;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-data-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.data).toEqual(data);
    expect(customTemplate.context.$implicit.item).toEqual(item);
  });
});