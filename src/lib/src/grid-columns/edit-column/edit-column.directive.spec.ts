import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { EditColumnDirective } from './edit-column.directive';
import { GuidService } from '../../guid';

@Component({
  selector: 'mgt-test-edit-column',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
  `
}) 
class TestEditColumnComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
}

describe('Directive: EditColumnDirective', () => {
  let editColumnDirective: EditColumnDirective;
  let dataTemplate: TemplateRef<any>;
  let headerTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestEditColumnComponent
        ]
      })
      .compileComponents()
      .then(() => {
        editColumnDirective = new EditColumnDirective(new GuidService());

        var testComponent = TestBed.createComponent(TestEditColumnComponent).componentInstance;
        dataTemplate = testComponent.dataTemplate;
        headerTemplate = testComponent.headerTemplate;
      });
  }));

  it('getHeaderData should return edit column header data', () => {
    var caption = 'foo';
    editColumnDirective.caption = caption;
    editColumnDirective.headerTemplate = headerTemplate;

    var headerData: any = editColumnDirective.getHeaderData();

    expect(headerData.caption).toEqual(caption);
    expect(headerData.headerTemplate).toEqual(headerTemplate);
    expect(headerData.enableOrdering).toBeFalsy();
  });

  it('getData should return edit column data', () => {
    var editButtonText: string = 'foo';
    editColumnDirective.editButtonText = editButtonText;
    var cancelButtonText: string = 'bar';
    editColumnDirective.cancelButtonText = cancelButtonText;
    var saveButtonText: string = 'baz';
    editColumnDirective.saveButtonText = saveButtonText;
    editColumnDirective.dataTemplate = dataTemplate;

    var data: any = editColumnDirective.getData();

    expect(data.editButtonText).toEqual(editButtonText);
    expect(data.cancelButtonText).toEqual(cancelButtonText);
    expect(data.saveButtonText).toEqual(saveButtonText);
    expect(data.dataTemplate).toEqual(dataTemplate);
  });
});