import { 
  Component, 
  Input,
  ViewChild,
  ViewContainerRef,
  OnInit
} from '@angular/core';

import { AbstractControl } from '@angular/forms';

import { EditComponent } from '../contracts/edit-component';
import { RowEditionService } from '../../edition/row-edition.service';
import { GridSettingsService } from '../../grid-settings.service';

@Component({
  selector: 'mg-edit-column-edit',
  templateUrl: './edit-column-edit.component.html'
})
export class EditColumnEditComponent extends EditComponent implements OnInit {
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;

  constructor(
    private rowEditionService: RowEditionService,
    private gridSettingsService: GridSettingsService) {
    super();
  }

  ngOnInit() {
    if (this.data.editTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item,
        editForm: this.editForm,
        saveChanges: this.saveChanges,
        cancelEdit: this.cancelEdit
      };
      this.containerRef.createEmbeddedView(this.data.editTemplate, { $implicit: templateContext });
    }
  }

  saveChanges() {
    let itemKey = null;
    if (this.item != null) {
      itemKey = this.item[this.gridSettingsService.key];
    }

    this.markAsDirtyRecursive(this.editForm);

    if (this.editForm.valid) {
      this.rowEditionService.endRowEdition(itemKey);
    }
  }

  cancelEdit() {
    let itemKey = null;
    if (this.item != null) {
      itemKey = this.item[this.gridSettingsService.key];
    }
    this.rowEditionService.cancelRowEdition(itemKey);
  }

  private markAsDirtyRecursive(control: AbstractControl): void {
    control.markAsDirty();

    if (control.hasOwnProperty('controls')) {
      for (let child in (<any>control).controls) {
        this.markAsDirtyRecursive((<any>control).controls[child] as AbstractControl);
      }
    }
  }
}