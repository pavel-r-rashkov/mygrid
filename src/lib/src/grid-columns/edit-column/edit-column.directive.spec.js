"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var edit_column_directive_1 = require("./edit-column.directive");
var guid_1 = require("../../guid");
var TestEditColumnComponent = (function () {
    function TestEditColumnComponent() {
    }
    __decorate([
        core_1.ViewChild('dataTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestEditColumnComponent.prototype, "dataTemplate", void 0);
    __decorate([
        core_1.ViewChild('headerTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestEditColumnComponent.prototype, "headerTemplate", void 0);
    TestEditColumnComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-edit-column',
            template: "\n    <ng-template #dataTemplate>\n      <div id=\"test-data-template\"></div>\n    </ng-template>\n    <ng-template #headerTemplate>\n      <div id=\"test-header-template\"></div>\n    </ng-template>\n  "
        })
    ], TestEditColumnComponent);
    return TestEditColumnComponent;
}());
describe('Directive: EditColumnDirective', function () {
    var editColumnDirective;
    var dataTemplate;
    var headerTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestEditColumnComponent
            ]
        })
            .compileComponents()
            .then(function () {
            editColumnDirective = new edit_column_directive_1.EditColumnDirective(new guid_1.GuidService());
            var testComponent = testing_1.TestBed.createComponent(TestEditColumnComponent).componentInstance;
            dataTemplate = testComponent.dataTemplate;
            headerTemplate = testComponent.headerTemplate;
        });
    }));
    it('getHeaderData should return edit column header data', function () {
        var caption = 'foo';
        editColumnDirective.caption = caption;
        editColumnDirective.headerTemplate = headerTemplate;
        var headerData = editColumnDirective.getHeaderData();
        expect(headerData.caption).toEqual(caption);
        expect(headerData.headerTemplate).toEqual(headerTemplate);
        expect(headerData.enableOrdering).toBeFalsy();
    });
    it('getData should return edit column data', function () {
        var editButtonText = 'foo';
        editColumnDirective.editButtonText = editButtonText;
        var cancelButtonText = 'bar';
        editColumnDirective.cancelButtonText = cancelButtonText;
        var saveButtonText = 'baz';
        editColumnDirective.saveButtonText = saveButtonText;
        editColumnDirective.dataTemplate = dataTemplate;
        var data = editColumnDirective.getData();
        expect(data.editButtonText).toEqual(editButtonText);
        expect(data.cancelButtonText).toEqual(cancelButtonText);
        expect(data.saveButtonText).toEqual(saveButtonText);
        expect(data.dataTemplate).toEqual(dataTemplate);
    });
});
//# sourceMappingURL=edit-column.directive.spec.js.map