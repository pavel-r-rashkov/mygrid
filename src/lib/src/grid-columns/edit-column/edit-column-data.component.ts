import { 
  Component, 
  OnInit,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { DataComponent } from '../contracts/data-component';
import { RowEditionState } from '../../edition/row-edition-state';
import { RowEditionService } from '../../edition/row-edition.service';
import { GridSettingsService } from '../../grid-settings.service';

@Component({
  selector: 'mg-edit-column-data',
  templateUrl: './edit-column-data.component.html'
})
export class EditColumnDataComponent extends DataComponent implements OnInit {
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;
  
  constructor(
    private rowEditionService: RowEditionService,
    private gridSettingsService: GridSettingsService) {
    super();
  }

  ngOnInit() {
    if (this.data.dataTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item
      };
      this.containerRef.createEmbeddedView(this.data.dataTemplate, { $implicit: templateContext });
    }
  }

  editRow() {
    let itemKey = this.item[this.gridSettingsService.key];
    this.rowEditionService.startRowEdition(itemKey);
  }
}