"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
require("rxjs/add/observable/of");
var my_grid_module_1 = require("../../my-grid.module");
var edit_column_edit_component_1 = require("./edit-column-edit.component");
var row_edition_service_1 = require("../../edition/row-edition.service");
var grid_settings_service_1 = require("../../grid-settings.service");
var TestEditComponent = (function () {
    function TestEditComponent() {
    }
    __decorate([
        core_1.ViewChild('editTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestEditComponent.prototype, "editTemplate", void 0);
    TestEditComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-edit',
            template: "\n    <ng-template #editTemplate>\n      <div id=\"test-edit-template\"></div>\n    </ng-template>\n  "
        })
    ], TestEditComponent);
    return TestEditComponent;
}());
describe('Component: EditColumnEditComponent', function () {
    var editColumnEditComponent;
    var fixture;
    var rowEditionService;
    var gridSettingsService;
    var editTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestEditComponent
            ],
            providers: [
                row_edition_service_1.RowEditionService,
                grid_settings_service_1.GridSettingsService
            ],
            imports: [
                my_grid_module_1.MyGridModule,
                forms_1.ReactiveFormsModule
            ]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(edit_column_edit_component_1.EditColumnEditComponent);
            editColumnEditComponent = fixture.componentInstance;
            rowEditionService = fixture.debugElement.injector.get(row_edition_service_1.RowEditionService);
            gridSettingsService = fixture.debugElement.injector.get(grid_settings_service_1.GridSettingsService);
            var templateComponent = testing_1.TestBed.createComponent(TestEditComponent).componentInstance;
            editTemplate = templateComponent.editTemplate;
        });
    }));
    it('custom edit template should be rendered when it exists', function () {
        var item = {};
        var data = {
            editTemplate: editTemplate
        };
        var form = new forms_1.FormGroup({});
        editColumnEditComponent.data = data;
        editColumnEditComponent.item = item;
        editColumnEditComponent.editForm = form;
        fixture.detectChanges();
        var customTemplate = fixture.debugElement.query(platform_browser_1.By.css('#test-edit-template'));
        expect(customTemplate).not.toBeNull();
        expect(customTemplate.context.$implicit.data).toEqual(data);
        expect(customTemplate.context.$implicit.item).toEqual(item);
        expect(customTemplate.context.$implicit.editForm).toEqual(form);
        expect(customTemplate.context.$implicit.saveChanges).toEqual(editColumnEditComponent.saveChanges);
        expect(customTemplate.context.$implicit.cancelEdit).toEqual(editColumnEditComponent.cancelEdit);
    });
    it('cancelEdit should raise cancelRowEdition with item\'s key when an item is edited', function () {
        var key = 'foo';
        var item = { foo: 'foo' };
        gridSettingsService.key = key;
        editColumnEditComponent.item = item;
        editColumnEditComponent.data = {};
        var cancelRowEditionSpy = spyOn(rowEditionService, 'cancelRowEdition');
        fixture.detectChanges();
        editColumnEditComponent.cancelEdit();
        expect(cancelRowEditionSpy).toHaveBeenCalled();
        expect(cancelRowEditionSpy.calls.first().args[0]).toEqual(item[key]);
    });
    it('cancelEdit should raise cancelRowEdition with null key when a new item is being added', function () {
        var key = 'foo';
        gridSettingsService.key = key;
        editColumnEditComponent.data = {};
        var cancelRowEditionSpy = spyOn(rowEditionService, 'cancelRowEdition');
        fixture.detectChanges();
        editColumnEditComponent.cancelEdit();
        expect(cancelRowEditionSpy).toHaveBeenCalled();
        expect(cancelRowEditionSpy.calls.first().args[0]).toBeNull();
    });
    it('saveChanges should raise endRowEdition with item\'s key when an item is edited and the form is valid, form should be marked as dirty', function () {
        var key = 'foo';
        var item = { foo: 'foo' };
        gridSettingsService.key = key;
        editColumnEditComponent.item = item;
        editColumnEditComponent.data = { propertyName: key };
        var formGroup = {};
        formGroup[key] = new forms_1.FormControl();
        editColumnEditComponent.editForm = new forms_1.FormGroup(formGroup);
        var endRowEditionSpy = spyOn(rowEditionService, 'endRowEdition');
        fixture.detectChanges();
        editColumnEditComponent.saveChanges();
        expect(endRowEditionSpy).toHaveBeenCalled();
        expect(endRowEditionSpy.calls.first().args[0]).toEqual(item[key]);
        expect(editColumnEditComponent.editForm.dirty).toBeTruthy();
    });
    it('saveChanges should raise endRowEdition with null key when a new item is being added and the form is valid, form should be marked as dirty', function () {
        var key = 'foo';
        gridSettingsService.key = key;
        editColumnEditComponent.data = { propertyName: key };
        var formGroup = {};
        formGroup[key] = new forms_1.FormControl();
        editColumnEditComponent.editForm = new forms_1.FormGroup(formGroup);
        var cancelRowEditionSpy = spyOn(rowEditionService, 'endRowEdition');
        fixture.detectChanges();
        editColumnEditComponent.saveChanges();
        expect(cancelRowEditionSpy).toHaveBeenCalled();
        expect(cancelRowEditionSpy.calls.first().args[0]).toBeNull();
        expect(editColumnEditComponent.editForm.dirty).toBeTruthy();
    });
    it('saveChanges with validation errors should mark the form dirty', function () {
        var key = 'foo';
        gridSettingsService.key = key;
        editColumnEditComponent.data = { propertyName: key };
        var formGroup = {};
        formGroup[key] = new forms_1.FormControl(null, forms_1.Validators.required);
        editColumnEditComponent.editForm = new forms_1.FormGroup(formGroup);
        var cancelRowEditionSpy = spyOn(rowEditionService, 'endRowEdition');
        fixture.detectChanges();
        editColumnEditComponent.saveChanges();
        expect(cancelRowEditionSpy).not.toHaveBeenCalled();
        expect(editColumnEditComponent.editForm.dirty).toBeTruthy();
    });
});
//# sourceMappingURL=edit-column-edit.component.spec.js.map