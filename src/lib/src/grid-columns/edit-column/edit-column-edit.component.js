"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var edit_component_1 = require("../contracts/edit-component");
var row_edition_service_1 = require("../../edition/row-edition.service");
var grid_settings_service_1 = require("../../grid-settings.service");
var EditColumnEditComponent = (function (_super) {
    __extends(EditColumnEditComponent, _super);
    function EditColumnEditComponent(rowEditionService, gridSettingsService) {
        var _this = _super.call(this) || this;
        _this.rowEditionService = rowEditionService;
        _this.gridSettingsService = gridSettingsService;
        return _this;
    }
    EditColumnEditComponent.prototype.ngOnInit = function () {
        if (this.data.editTemplate != null) {
            var templateContext = {
                data: this.data,
                item: this.item,
                editForm: this.editForm,
                saveChanges: this.saveChanges,
                cancelEdit: this.cancelEdit
            };
            this.containerRef.createEmbeddedView(this.data.editTemplate, { $implicit: templateContext });
        }
    };
    EditColumnEditComponent.prototype.saveChanges = function () {
        var itemKey = null;
        if (this.item != null) {
            itemKey = this.item[this.gridSettingsService.key];
        }
        this.markAsDirtyRecursive(this.editForm);
        if (this.editForm.valid) {
            this.rowEditionService.endRowEdition(itemKey);
        }
    };
    EditColumnEditComponent.prototype.cancelEdit = function () {
        var itemKey = null;
        if (this.item != null) {
            itemKey = this.item[this.gridSettingsService.key];
        }
        this.rowEditionService.cancelRowEdition(itemKey);
    };
    EditColumnEditComponent.prototype.markAsDirtyRecursive = function (control) {
        control.markAsDirty();
        if (control.hasOwnProperty('controls')) {
            for (var child in control.controls) {
                this.markAsDirtyRecursive(control.controls[child]);
            }
        }
    };
    __decorate([
        core_1.ViewChild('customTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], EditColumnEditComponent.prototype, "containerRef", void 0);
    EditColumnEditComponent = __decorate([
        core_1.Component({
            selector: 'mg-edit-column-edit',
            templateUrl: './edit-column-edit.component.html'
        }),
        __metadata("design:paramtypes", [row_edition_service_1.RowEditionService,
            grid_settings_service_1.GridSettingsService])
    ], EditColumnEditComponent);
    return EditColumnEditComponent;
}(edit_component_1.EditComponent));
exports.EditColumnEditComponent = EditColumnEditComponent;
//# sourceMappingURL=edit-column-edit.component.js.map