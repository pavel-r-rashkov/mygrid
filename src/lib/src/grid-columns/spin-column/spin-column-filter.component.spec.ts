import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { SpinColumnFilterComponent } from './spin-column-filter.component';
import { FilterStateService } from '../../filter-state.service';
import { FilterState } from '../../states/filter-state';
import { SpinSearchTermType } from './spin-search-term-type';

@Component({
  selector: 'mgt-test-filter',
  template: `
    <ng-template #filterTemplate>
      <div id="test-filter-template"></div>
    </ng-template>
  `
})
class TestFilterComponent {
  @ViewChild('filterTemplate') filterTemplate: TemplateRef<any>;
}

describe('Component: SpinColumnFilterComponent', () => {
  let spinColumnFilterComponent: SpinColumnFilterComponent;
  let fixture: ComponentFixture<SpinColumnFilterComponent>;
  let filterTemplate: TemplateRef<any>;
  let filterStateService: FilterStateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        providers: [
          FilterStateService
        ],
        declarations: [
          TestFilterComponent
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SpinColumnFilterComponent);
        spinColumnFilterComponent = fixture.componentInstance;

        var templateComponent: TestFilterComponent = TestBed.createComponent(TestFilterComponent).componentInstance;
        filterTemplate = templateComponent.filterTemplate;

        filterStateService = fixture.debugElement.injector.get(FilterStateService);
      });
  }));
  
  it('custom filter template should be rendered when it exists', () => {
    var data = {
      filterTemplate: filterTemplate,
      enableFiltering: true
    };
    spinColumnFilterComponent.filterData = data;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-filter-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.filterData).toEqual(data);
    expect(customTemplate.context.$implicit.columnId).toEqual(spinColumnFilterComponent.columnId);
    expect(customTemplate.context.$implicit.filter).toEqual(spinColumnFilterComponent.customTemplateFilter);
  });

  it('selectedSearchType should be set on init', () => {
    var propertyName = 'foo';
    var searchTermType = SpinSearchTermType.MoreThanOrEqual;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    
    fixture.detectChanges();
    
    expect(spinColumnFilterComponent.selectedSearchType).toEqual(searchTermType);
  });

  it('changing filter state should update filterNumber', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    var filterNumber = 10;

    fixture.detectChanges();
    filterStateService.raiseFilterChanged(columnId, propertyName, { number: filterNumber }, '');

    fixture.whenStable().then(() => {
      expect(spinColumnFilterComponent.filterNumber).toEqual(filterNumber);
    });
  });

  it('getConditionMenuState should return opened when menu is visible', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    spinColumnFilterComponent.menuVisible = true;

    fixture.detectChanges();
    var menuState = spinColumnFilterComponent.getConditionMenuState();    

    expect(menuState).toEqual('opened');
  });

  it('getConditionMenuState should return hidden when menu is not visible', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    spinColumnFilterComponent.menuVisible = false;

    fixture.detectChanges();
    var menuState = spinColumnFilterComponent.getConditionMenuState();

    expect(menuState).toEqual('hidden');
  });

  it('buttonFocusedOut should hide the menu', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    spinColumnFilterComponent.menuVisible = true;

    fixture.detectChanges();
    spinColumnFilterComponent.buttonFocusedOut();

    expect(spinColumnFilterComponent.menuVisible).toBeFalsy();
  });

  it('toggleMenu should show/hide menu', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    var menuVisible = false;
    spinColumnFilterComponent.menuVisible = menuVisible;

    fixture.detectChanges();
    spinColumnFilterComponent.toggleMenu();    

    expect(spinColumnFilterComponent.menuVisible).toEqual(!menuVisible);
  });

  it('customTemplateFilter should create new filter state', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    var templateFilterData = {};
    var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    spinColumnFilterComponent.customTemplateFilter(templateFilterData);

    expect(raiseFilterChangeSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangeSpy.calls.first();
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2]).toEqual(templateFilterData);
  });


  it('selectedSearchType should be set on init', () => {
    var propertyName = 'foo';
    var searchTermType = SpinSearchTermType.MoreThanOrEqual;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    
    fixture.detectChanges();
    
    expect(spinColumnFilterComponent.selectedSearchType).toEqual(searchTermType);
  });

  it('selectSearchType should not create new filter state when there is no change in the selected search type', () => {
    var propertyName = 'foo';
    var searchTermType = SpinSearchTermType.MoreThanOrEqual;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    spinColumnFilterComponent.filterNumber = 10;
    var raiseFilterChangedSpy: jasmine.Spy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    spinColumnFilterComponent.selectSearchType(searchTermType);

    expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
  });

  it('selectSearchType should not create new filter state when there is no filter number selected', () => {
    var propertyName = 'foo';
    var searchTermType = SpinSearchTermType.MoreThanOrEqual;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    var raiseFilterChangedSpy: jasmine.Spy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    spinColumnFilterComponent.selectSearchType(4);

    expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
  });

  it('selectSearchType should create new filter state when there is filter number selected and the search term type is changed', () => {
    var propertyName = 'foo';
    var searchTermType = SpinSearchTermType.MoreThanOrEqual;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    var filterNumber = 12;
    spinColumnFilterComponent.filterNumber = filterNumber;
    var selectedSearchTermType = SpinSearchTermType.MoreThan;
    var raiseFilterChangedSpy: jasmine.Spy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    spinColumnFilterComponent.selectSearchType(selectedSearchTermType);

    expect(raiseFilterChangedSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangedSpy.calls.first();
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2].number).toEqual(filterNumber);
    expect(filterCall.args[2].searchTermType).toEqual(selectedSearchTermType);
  });

  it('filterFocusedOut should not create new filter state when filter number is not changed', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    spinColumnFilterComponent.filterNumber = 10;
    var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');
    var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');

    fixture.detectChanges();
    spinColumnFilterComponent.filterFocusedIn();
    spinColumnFilterComponent.filterFocusedOut({});

    expect(raiseFilterChangeSpy).not.toHaveBeenCalled();
    expect(clearColumnFilterSpy).not.toHaveBeenCalled();
  });

  it('filterFocusedOut should clear filter for this column when the selected number is null', () => {
    var propertyName = 'foo';
    var data = {
      propertyName: propertyName,
      enableFiltering: true
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');

    fixture.detectChanges();
    spinColumnFilterComponent.filterNumber = 10;
    spinColumnFilterComponent.filterFocusedIn();
    spinColumnFilterComponent.filterNumber = null;
    spinColumnFilterComponent.filterFocusedOut({});

    expect(clearColumnFilterSpy).toHaveBeenCalled();
    expect(clearColumnFilterSpy.calls.first().args[0]).toEqual(spinColumnFilterComponent.columnId);
  });

  it('filterFocusedOut should create new filter state when selected number is not null', () => {
    var propertyName = 'foo';
    var searchTermType = SpinSearchTermType.MoreThanOrEqual;
    var data = {
      propertyName: propertyName,
      enableFiltering: true,
      searchTermType: searchTermType
    };
    spinColumnFilterComponent.filterData = data;
    var columnId = '123';
    spinColumnFilterComponent.columnId = columnId;
    var filterNumber = 10;
    spinColumnFilterComponent.filterNumber = filterNumber;
    var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');

    fixture.detectChanges();
    spinColumnFilterComponent.filterFocusedOut({});

    expect(raiseFilterChangedSpy).toHaveBeenCalled();
    var filterCall = raiseFilterChangedSpy.calls.first()
    expect(filterCall.args[0]).toEqual(columnId);
    expect(filterCall.args[1]).toEqual(propertyName);
    expect(filterCall.args[2].number).toEqual(filterNumber);
    expect(filterCall.args[2].searchTermType).toEqual(searchTermType);
  });
});