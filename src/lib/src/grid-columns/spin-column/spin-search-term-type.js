"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SpinSearchTermType;
(function (SpinSearchTermType) {
    SpinSearchTermType[SpinSearchTermType["LessThan"] = 0] = "LessThan";
    SpinSearchTermType[SpinSearchTermType["LessThanOrEqual"] = 1] = "LessThanOrEqual";
    SpinSearchTermType[SpinSearchTermType["Equals"] = 2] = "Equals";
    SpinSearchTermType[SpinSearchTermType["MoreThanOrEqual"] = 3] = "MoreThanOrEqual";
    SpinSearchTermType[SpinSearchTermType["MoreThan"] = 4] = "MoreThan";
})(SpinSearchTermType = exports.SpinSearchTermType || (exports.SpinSearchTermType = {}));
//# sourceMappingURL=spin-search-term-type.js.map