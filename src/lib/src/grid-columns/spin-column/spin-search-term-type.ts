export enum SpinSearchTermType {
  LessThan = 0,
  LessThanOrEqual = 1,
  Equals = 2,
  MoreThanOrEqual = 3,
  MoreThan = 4
}