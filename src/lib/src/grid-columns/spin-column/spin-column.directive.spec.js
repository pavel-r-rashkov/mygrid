"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var spin_column_directive_1 = require("./spin-column.directive");
var guid_1 = require("../../guid");
var default_comparer_service_1 = require("../shared/default-comparer.service");
var spin_search_term_type_1 = require("./spin-search-term-type");
var TestSpinColumnComponent = (function () {
    function TestSpinColumnComponent() {
    }
    __decorate([
        core_1.ViewChild('dataTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestSpinColumnComponent.prototype, "dataTemplate", void 0);
    __decorate([
        core_1.ViewChild('headerTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestSpinColumnComponent.prototype, "headerTemplate", void 0);
    __decorate([
        core_1.ViewChild('editTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestSpinColumnComponent.prototype, "editTemplate", void 0);
    __decorate([
        core_1.ViewChild('validationErrorsTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestSpinColumnComponent.prototype, "validationErrorsTemplate", void 0);
    __decorate([
        core_1.ViewChild('filterTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestSpinColumnComponent.prototype, "filterTemplate", void 0);
    __decorate([
        core_1.ViewChild('groupHeaderTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestSpinColumnComponent.prototype, "groupHeaderTemplate", void 0);
    TestSpinColumnComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-spin-column',
            template: "\n    <ng-template #dataTemplate>\n      <div id=\"test-data-template\"></div>\n    </ng-template>\n    <ng-template #headerTemplate>\n      <div id=\"test-header-template\"></div>\n    </ng-template>\n    <ng-template #editTemplate>\n      <div id=\"test-edit-template\"></div>\n    </ng-template>\n    <ng-template #validationErrorsTemplate>\n      <div id=\"test-validation-error-template\"></div>\n    </ng-template>\n    <ng-template #filterTemplate>\n      <div id=\"test-filter-template\"></div>\n    </ng-template>\n    <ng-template #groupHeaderTemplate>\n      <div id=\"test-group-header-template\"></div>\n    </ng-template>\n  "
        })
    ], TestSpinColumnComponent);
    return TestSpinColumnComponent;
}());
describe('Directive: SpinColumnDirective', function () {
    var spinColumnDirective;
    var comparer;
    var dataTemplate;
    var headerTemplate;
    var editTemplate;
    var validationErrorsTemplate;
    var filterTemplate;
    var groupHeaderTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                TestSpinColumnComponent
            ]
        })
            .compileComponents()
            .then(function () {
            comparer = new default_comparer_service_1.DefaultComparerService();
            spinColumnDirective = new spin_column_directive_1.SpinColumnDirective(new guid_1.GuidService(), comparer);
            var testComponent = testing_1.TestBed.createComponent(TestSpinColumnComponent).componentInstance;
            dataTemplate = testComponent.dataTemplate;
            headerTemplate = testComponent.headerTemplate;
            editTemplate = testComponent.editTemplate;
            validationErrorsTemplate = testComponent.validationErrorsTemplate;
            filterTemplate = testComponent.filterTemplate;
            groupHeaderTemplate = testComponent.groupHeaderTemplate;
        });
    }));
    it('getHeaderData should return spin column header data', function () {
        var caption = 'foo';
        spinColumnDirective.caption = caption;
        spinColumnDirective.headerTemplate = headerTemplate;
        var propertyName = 'bar';
        spinColumnDirective.propertyName = propertyName;
        var orderingEnabled = false;
        spinColumnDirective.enableOrdering = orderingEnabled;
        var groupingEnabled = false;
        spinColumnDirective.enableGrouping = groupingEnabled;
        var headerData = spinColumnDirective.getHeaderData();
        expect(headerData.caption).toEqual(caption);
        expect(headerData.headerTemplate).toEqual(headerTemplate);
        expect(headerData.propertyName).toEqual(propertyName);
        expect(headerData.enableOrdering).toEqual(orderingEnabled);
        expect(headerData.enableGrouping).toEqual(groupingEnabled);
    });
    it('ordering and grouping should be enabled by default', function () {
        var headerData = spinColumnDirective.getHeaderData();
        expect(headerData.enableOrdering).toBeTruthy();
        expect(headerData.enableGrouping).toBeTruthy();
    });
    it('getData should return spin column data', function () {
        spinColumnDirective.dataTemplate = dataTemplate;
        spinColumnDirective.editTemplate = editTemplate;
        spinColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
        var propertyName = 'bar';
        spinColumnDirective.propertyName = propertyName;
        var editionEnabled = false;
        spinColumnDirective.enableEditing = editionEnabled;
        var step = 5;
        spinColumnDirective.step = step;
        var min = -10;
        spinColumnDirective.min = min;
        var max = 20;
        spinColumnDirective.max = max;
        var data = spinColumnDirective.getData();
        expect(data.dataTemplate).toEqual(dataTemplate);
        expect(data.editTemplate).toEqual(editTemplate);
        expect(data.validationErrorsTemplate).toEqual(validationErrorsTemplate);
        expect(data.propertyName).toEqual(propertyName);
        expect(data.enableEditing).toEqual(editionEnabled);
        expect(data.step).toEqual(step);
        expect(data.max).toEqual(max);
        expect(data.min).toEqual(min);
    });
    it('editing should be enabled by default, step\'s default value should be 1', function () {
        var data = spinColumnDirective.getData();
        expect(data.enableEditing).toBeTruthy();
        expect(data.step).toEqual(1);
    });
    it('getFilterData should return spin column filter data', function () {
        spinColumnDirective.filterTemplate = filterTemplate;
        var propertyName = 'bar';
        spinColumnDirective.propertyName = propertyName;
        var filteringEnabled = false;
        spinColumnDirective.enableFiltering = filteringEnabled;
        var step = 5;
        spinColumnDirective.step = step;
        var min = -10;
        spinColumnDirective.min = min;
        var max = 20;
        spinColumnDirective.max = max;
        var searchTermTypeMenuEnabled = false;
        spinColumnDirective.enableSearchTermTypeMenu = searchTermTypeMenuEnabled;
        var searchTermType = spin_search_term_type_1.SpinSearchTermType.LessThanOrEqual;
        spinColumnDirective.searchTermType = searchTermType;
        var filterData = spinColumnDirective.getFilterData();
        expect(filterData.filterTemplate).toEqual(filterTemplate);
        expect(filterData.propertyName).toEqual(propertyName);
        expect(filterData.enableFiltering).toEqual(filteringEnabled);
        expect(filterData.step).toEqual(step);
        expect(filterData.max).toEqual(max);
        expect(filterData.min).toEqual(min);
        expect(filterData.enableSearchTermTypeMenu).toEqual(searchTermTypeMenuEnabled);
        expect(filterData.searchTermType).toEqual(searchTermType);
    });
    it('filtering should be enabled by default, default filter\'s step should be 1, search term type default option should be equals, search term type menu should be enabled by default', function () {
        var filterData = spinColumnDirective.getFilterData();
        expect(filterData.enableFiltering).toBeTruthy();
        expect(filterData.step).toEqual(1);
        expect(filterData.enableSearchTermTypeMenu).toBeTruthy();
        expect(filterData.searchTermType).toEqual(spin_search_term_type_1.SpinSearchTermType.Equals);
    });
    it('getGroupData should return spin column group data', function () {
        spinColumnDirective.groupHeaderTemplate = groupHeaderTemplate;
        spinColumnDirective.editTemplate = editTemplate;
        spinColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
        var propertyName = 'bar';
        spinColumnDirective.propertyName = propertyName;
        var editionEnabled = false;
        spinColumnDirective.enableEditing = editionEnabled;
        var groupData = spinColumnDirective.getGroupData();
        expect(groupData.dataTemplate).toEqual(groupHeaderTemplate);
        expect(groupData.editTemplate).toEqual(editTemplate);
        expect(groupData.validationErrorsTemplate).toEqual(validationErrorsTemplate);
        expect(groupData.propertyName).toEqual(propertyName);
        expect(groupData.enableEditing).toEqual(editionEnabled);
    });
    it('getValue should return null when the column is not bound to a property', function () {
        var item = { foo: 5 };
        var columnValue = spinColumnDirective.getValue(item);
        expect(columnValue).toEqual(null);
    });
    it('getValue should return the value of the property that the column is bound to', function () {
        var item = { foo: 5 };
        var propertyName = 'foo';
        spinColumnDirective.propertyName = propertyName;
        var columnValue = spinColumnDirective.getValue(item);
        expect(columnValue).toEqual(item[propertyName]);
    });
    it('order should call customOrder callback if it is provided', function () {
        spinColumnDirective.customOrder = function (firstValue, secondValue, isAscending, firstItem, secondItem) { return 0; };
        var orderResult = 1;
        var orderSpy = spyOn(spinColumnDirective, 'customOrder').and.returnValue(orderResult);
        var result = spinColumnDirective.order(3, 10, true, {}, {});
        expect(result).toEqual(orderResult);
    });
    it('order should call default comparer', function () {
        var firstValue = 5;
        var secondValue = 10;
        var isAsecneding = true;
        var comparerSpy = spyOn(comparer, 'compare');
        var result = spinColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});
        expect(comparerSpy).toHaveBeenCalled();
        var compareArgs = comparerSpy.calls.first().args;
        expect(compareArgs[0]).toEqual(firstValue);
        expect(compareArgs[1]).toEqual(secondValue);
        expect(compareArgs[2]).toEqual(isAsecneding);
    });
    it('group should call customGroup callback if it is provided', function () {
        spinColumnDirective.customGroup = function (firstValue, secondValue, isAscending, firstItem, secondItem) { return 0; };
        var groupResult = 1;
        var orderSpy = spyOn(spinColumnDirective, 'customGroup').and.returnValue(groupResult);
        var result = spinColumnDirective.group(5, 10, true, {}, {});
        expect(result).toEqual(groupResult);
    });
    it('group should call default comparer', function () {
        var firstValue = 5;
        var secondValue = 10;
        var isAsecneding = true;
        var comparerSpy = spyOn(comparer, 'compare');
        var result = spinColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});
        expect(comparerSpy).toHaveBeenCalled();
        var compareArgs = comparerSpy.calls.first().args;
        expect(compareArgs[0]).toEqual(firstValue);
        expect(compareArgs[1]).toEqual(secondValue);
        expect(compareArgs[2]).toEqual(isAsecneding);
    });
    it('filter should call customFilter callback if it is provided', function () {
        spinColumnDirective.customFilter = function (val, filterData, item) { return false; };
        var filterResult = true;
        var filterSpy = spyOn(spinColumnDirective, 'customFilter').and.returnValue(filterResult);
        var value = 3;
        var filterData = {};
        var item = {};
        var result = spinColumnDirective.filter(value, filterData, item);
        expect(filterSpy).toHaveBeenCalled();
        var filterArgs = filterSpy.calls.first().args;
        expect(filterArgs[0]).toEqual(value);
        expect(filterArgs[1]).toEqual(filterData);
        expect(filterArgs[2]).toEqual(item);
    });
    it('filter with no filter data should return true', function () {
        var result = spinColumnDirective.filter('A', null, {});
        expect(result).toBeTruthy();
    });
    it('filter with no filter number should return true', function () {
        var result = spinColumnDirective.filter('A', {}, {});
        expect(result).toBeTruthy();
    });
    it('filter with no item value should return false', function () {
        var result = spinColumnDirective.filter(null, { number: 3 }, {});
        expect(result).toBeFalsy();
    });
    it('filter with less search term type should return true when the value is smaller than the filter value', function () {
        var result = spinColumnDirective.filter(5, { number: 6, searchTermType: spin_search_term_type_1.SpinSearchTermType.LessThan }, {});
        expect(result).toBeTruthy();
    });
    it('filter with less search term type should return false when the value is bigger or equal to the filter value', function () {
        var result = spinColumnDirective.filter(5, { number: 5, searchTermType: spin_search_term_type_1.SpinSearchTermType.LessThan }, {});
        expect(result).toBeFalsy();
    });
    it('filter with less than or equal search term type should return true when the value is smaller or equal to the filter value', function () {
        var result = spinColumnDirective.filter(5, { number: 5, searchTermType: spin_search_term_type_1.SpinSearchTermType.LessThanOrEqual }, {});
        expect(result).toBeTruthy();
    });
    it('filter with less than or equal search term type should return false when the value is bigger than the filter value', function () {
        var result = spinColumnDirective.filter(6, { number: 5, searchTermType: spin_search_term_type_1.SpinSearchTermType.LessThanOrEqual }, {});
        expect(result).toBeFalsy();
    });
    it('filter with equal search term type should return true when the value is equal to the filter value', function () {
        var result = spinColumnDirective.filter(5, { number: 5, searchTermType: spin_search_term_type_1.SpinSearchTermType.Equals }, {});
        expect(result).toBeTruthy();
    });
    it('filter with equal search term type should return false when the value is not equal to the filter value', function () {
        var result = spinColumnDirective.filter(5, { number: 6, searchTermType: spin_search_term_type_1.SpinSearchTermType.Equals }, {});
        expect(result).toBeFalsy();
    });
    it('filter with bigger or equal search term type should return true when the value is bigger or equal to the filter value', function () {
        var result = spinColumnDirective.filter(5, { number: 5, searchTermType: spin_search_term_type_1.SpinSearchTermType.MoreThanOrEqual }, {});
        expect(result).toBeTruthy();
    });
    it('filter with bigger or equal search term type should return false when the value is not bigger or equal to the filter value', function () {
        var result = spinColumnDirective.filter(5, { number: 6, searchTermType: spin_search_term_type_1.SpinSearchTermType.MoreThanOrEqual }, {});
        expect(result).toBeFalsy();
    });
    it('filter with bigger search term type should return true when the value is bigger than the filter value', function () {
        var result = spinColumnDirective.filter(6, { number: 5, searchTermType: spin_search_term_type_1.SpinSearchTermType.MoreThan }, {});
        expect(result).toBeTruthy();
    });
    it('filter with bigger search term type should return false when the value is not bigger than the filter value', function () {
        var result = spinColumnDirective.filter(5, { number: 5, searchTermType: spin_search_term_type_1.SpinSearchTermType.MoreThan }, {});
        expect(result).toBeFalsy();
    });
    it('filter should throw an Error if the search term type does not exist', function () {
        expect(function () { spinColumnDirective.filter(5, { number: 3, searchTermType: 5 }, {}); }).toThrowError();
    });
});
//# sourceMappingURL=spin-column.directive.spec.js.map