import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { SpinColumnDirective } from './spin-column.directive';
import { GuidService } from '../../guid';
import { DefaultComparerService } from '../shared/default-comparer.service';
import { SpinSearchTermType } from './spin-search-term-type';

@Component({
  selector: 'mgt-test-spin-column',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
    <ng-template #editTemplate>
      <div id="test-edit-template"></div>
    </ng-template>
    <ng-template #validationErrorsTemplate>
      <div id="test-validation-error-template"></div>
    </ng-template>
    <ng-template #filterTemplate>
      <div id="test-filter-template"></div>
    </ng-template>
    <ng-template #groupHeaderTemplate>
      <div id="test-group-header-template"></div>
    </ng-template>
  `
}) 
class TestSpinColumnComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
  @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
  @ViewChild('validationErrorsTemplate') validationErrorsTemplate: TemplateRef<any>;
  @ViewChild('filterTemplate') filterTemplate: TemplateRef<any>;
  @ViewChild('groupHeaderTemplate') groupHeaderTemplate: TemplateRef<any>;
}

describe('Directive: SpinColumnDirective', () => {
  let spinColumnDirective: SpinColumnDirective;
  let comparer: DefaultComparerService;
  let dataTemplate: TemplateRef<any>;
  let headerTemplate: TemplateRef<any>;
  let editTemplate: TemplateRef<any>;
  let validationErrorsTemplate: TemplateRef<any>;
  let filterTemplate: TemplateRef<any>;
  let groupHeaderTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestSpinColumnComponent
        ]
      })
      .compileComponents()
      .then(() => {
        comparer = new DefaultComparerService();
        spinColumnDirective = new SpinColumnDirective(new GuidService(), comparer);

        var testComponent = TestBed.createComponent(TestSpinColumnComponent).componentInstance;
        dataTemplate = testComponent.dataTemplate;
        headerTemplate = testComponent.headerTemplate;
        editTemplate = testComponent.editTemplate;
        validationErrorsTemplate = testComponent.validationErrorsTemplate;
        filterTemplate = testComponent.filterTemplate;
        groupHeaderTemplate = testComponent.groupHeaderTemplate;
      });
  }));

  it('getHeaderData should return spin column header data', () => {
    var caption = 'foo';
    spinColumnDirective.caption = caption;
    spinColumnDirective.headerTemplate = headerTemplate;
    var propertyName = 'bar';
    spinColumnDirective.propertyName = propertyName;
    var orderingEnabled = false;
    spinColumnDirective.enableOrdering = orderingEnabled;
    var groupingEnabled = false;
    spinColumnDirective.enableGrouping = groupingEnabled;

    var headerData: any = spinColumnDirective.getHeaderData();

    expect(headerData.caption).toEqual(caption);
    expect(headerData.headerTemplate).toEqual(headerTemplate);
    expect(headerData.propertyName).toEqual(propertyName);
    expect(headerData.enableOrdering).toEqual(orderingEnabled);
    expect(headerData.enableGrouping).toEqual(groupingEnabled);
  });

  it('ordering and grouping should be enabled by default', () => {
    var headerData: any = spinColumnDirective.getHeaderData();

    expect(headerData.enableOrdering).toBeTruthy();
    expect(headerData.enableGrouping).toBeTruthy();
  });

  it('getData should return spin column data', () => {
    spinColumnDirective.dataTemplate = dataTemplate;
    spinColumnDirective.editTemplate = editTemplate;
    spinColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
    var propertyName = 'bar';
    spinColumnDirective.propertyName = propertyName;
    var editionEnabled = false;
    spinColumnDirective.enableEditing = editionEnabled;
    var step = 5;
    spinColumnDirective.step = step;
    var min = -10;
    spinColumnDirective.min = min;
    var max = 20;
    spinColumnDirective.max = max;

    var data: any = spinColumnDirective.getData();

    expect(data.dataTemplate).toEqual(dataTemplate);
    expect(data.editTemplate).toEqual(editTemplate);
    expect(data.validationErrorsTemplate).toEqual(validationErrorsTemplate);
    expect(data.propertyName).toEqual(propertyName);
    expect(data.enableEditing).toEqual(editionEnabled);
    expect(data.step).toEqual(step);
    expect(data.max).toEqual(max);
    expect(data.min).toEqual(min);
  });

  it('editing should be enabled by default, step\'s default value should be 1', () => {
    var data: any = spinColumnDirective.getData();

    expect(data.enableEditing).toBeTruthy();
    expect(data.step).toEqual(1);
  });

  it('getFilterData should return spin column filter data', () => {
    spinColumnDirective.filterTemplate = filterTemplate;
    var propertyName = 'bar';
    spinColumnDirective.propertyName = propertyName;
    var filteringEnabled = false;
    spinColumnDirective.enableFiltering = filteringEnabled;
    var step = 5;
    spinColumnDirective.step = step;
    var min = -10;
    spinColumnDirective.min = min;
    var max = 20;
    spinColumnDirective.max = max;
    var searchTermTypeMenuEnabled = false;
    spinColumnDirective.enableSearchTermTypeMenu = searchTermTypeMenuEnabled;
    var searchTermType = SpinSearchTermType.LessThanOrEqual;
    spinColumnDirective.searchTermType = searchTermType;

    var filterData: any = spinColumnDirective.getFilterData();

    expect(filterData.filterTemplate).toEqual(filterTemplate);
    expect(filterData.propertyName).toEqual(propertyName);
    expect(filterData.enableFiltering).toEqual(filteringEnabled);
    expect(filterData.step).toEqual(step);
    expect(filterData.max).toEqual(max);
    expect(filterData.min).toEqual(min);
    expect(filterData.enableSearchTermTypeMenu).toEqual(searchTermTypeMenuEnabled);
    expect(filterData.searchTermType).toEqual(searchTermType);
  });

  it('filtering should be enabled by default, default filter\'s step should be 1, search term type default option should be equals, search term type menu should be enabled by default', () => {
    var filterData: any = spinColumnDirective.getFilterData();

    expect(filterData.enableFiltering).toBeTruthy();
    expect(filterData.step).toEqual(1);
    expect(filterData.enableSearchTermTypeMenu).toBeTruthy();
    expect(filterData.searchTermType).toEqual(SpinSearchTermType.Equals);
  });

  it('getGroupData should return spin column group data', () => {
    spinColumnDirective.groupHeaderTemplate = groupHeaderTemplate;
    spinColumnDirective.editTemplate = editTemplate;
    spinColumnDirective.validationErrorsTemplate = validationErrorsTemplate;
    var propertyName = 'bar';
    spinColumnDirective.propertyName = propertyName;
    var editionEnabled = false;
    spinColumnDirective.enableEditing = editionEnabled;

    var groupData: any = spinColumnDirective.getGroupData();

    expect(groupData.dataTemplate).toEqual(groupHeaderTemplate);
    expect(groupData.editTemplate).toEqual(editTemplate);
    expect(groupData.validationErrorsTemplate).toEqual(validationErrorsTemplate);
    expect(groupData.propertyName).toEqual(propertyName);
    expect(groupData.enableEditing).toEqual(editionEnabled);
  });

  it('getValue should return null when the column is not bound to a property', () => {
    var item = { foo: 5 };

    var columnValue = spinColumnDirective.getValue(item);

    expect(columnValue).toEqual(null);
  });

  it('getValue should return the value of the property that the column is bound to', () => {
    var item = { foo: 5 };
    var propertyName = 'foo';
    spinColumnDirective.propertyName = propertyName;

    var columnValue = spinColumnDirective.getValue(item);

    expect(columnValue).toEqual(item[propertyName]);
  });

  it('order should call customOrder callback if it is provided', () => {
    spinColumnDirective.customOrder = (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any): number => { return 0; };
    var orderResult = 1;
    var orderSpy: jasmine.Spy = spyOn(spinColumnDirective, 'customOrder').and.returnValue(orderResult);

    var result = spinColumnDirective.order(3, 10, true, {}, {});

    expect(result).toEqual(orderResult);
  });

  it('order should call default comparer', () => {
    var firstValue = 5;
    var secondValue = 10;
    var isAsecneding = true;
    var comparerSpy: jasmine.Spy = spyOn(comparer, 'compare');

    var result = spinColumnDirective.order(firstValue, secondValue, isAsecneding, {}, {});

    expect(comparerSpy).toHaveBeenCalled();
    var compareArgs = comparerSpy.calls.first().args;
    expect(compareArgs[0]).toEqual(firstValue);
    expect(compareArgs[1]).toEqual(secondValue);
    expect(compareArgs[2]).toEqual(isAsecneding);
  });

  it('group should call customGroup callback if it is provided', () => {
    spinColumnDirective.customGroup = (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any): number => { return 0; };
    var groupResult = 1;
    var orderSpy: jasmine.Spy = spyOn(spinColumnDirective, 'customGroup').and.returnValue(groupResult);

    var result = spinColumnDirective.group(5, 10, true, {}, {});

    expect(result).toEqual(groupResult);
  });

  it('group should call default comparer', () => {
    var firstValue = 5;
    var secondValue = 10;
    var isAsecneding = true;
    var comparerSpy: jasmine.Spy = spyOn(comparer, 'compare');

    var result = spinColumnDirective.group(firstValue, secondValue, isAsecneding, {}, {});

    expect(comparerSpy).toHaveBeenCalled();
    var compareArgs = comparerSpy.calls.first().args;
    expect(compareArgs[0]).toEqual(firstValue);
    expect(compareArgs[1]).toEqual(secondValue);
    expect(compareArgs[2]).toEqual(isAsecneding);
  });

  it('filter should call customFilter callback if it is provided', () => {
    spinColumnDirective.customFilter = (val: any, filterData: any, item: any): boolean => { return false; };
    var filterResult = true;
    var filterSpy: jasmine.Spy = spyOn(spinColumnDirective, 'customFilter').and.returnValue(filterResult);
    var value = 3;
    var filterData = {};
    var item = {};

    var result = spinColumnDirective.filter(value, filterData, item);

    expect(filterSpy).toHaveBeenCalled();
    var filterArgs = filterSpy.calls.first().args;
    expect(filterArgs[0]).toEqual(value);
    expect(filterArgs[1]).toEqual(filterData);
    expect(filterArgs[2]).toEqual(item);
  });

  it('filter with no filter data should return true', () => {
    var result = spinColumnDirective.filter('A', null, {});

    expect(result).toBeTruthy();
  });

  it('filter with no filter number should return true', () => {
    var result = spinColumnDirective.filter('A', {}, {});

    expect(result).toBeTruthy();
  });

  it('filter with no item value should return false', () => {
    var result = spinColumnDirective.filter(null, { number: 3 }, {});

    expect(result).toBeFalsy();
  });

  it('filter with less search term type should return true when the value is smaller than the filter value', () => {
    var result = spinColumnDirective.filter(5, { number: 6, searchTermType: SpinSearchTermType.LessThan }, {});

    expect(result).toBeTruthy();
  });

  it('filter with less search term type should return false when the value is bigger or equal to the filter value', () => {
    var result = spinColumnDirective.filter(5, { number: 5, searchTermType: SpinSearchTermType.LessThan }, {});

    expect(result).toBeFalsy();
  });

  it('filter with less than or equal search term type should return true when the value is smaller or equal to the filter value', () => {
    var result = spinColumnDirective.filter(5, { number: 5, searchTermType: SpinSearchTermType.LessThanOrEqual }, {});

    expect(result).toBeTruthy();
  });

  it('filter with less than or equal search term type should return false when the value is bigger than the filter value', () => {
    var result = spinColumnDirective.filter(6, { number: 5, searchTermType: SpinSearchTermType.LessThanOrEqual }, {});

    expect(result).toBeFalsy();
  });

  it('filter with equal search term type should return true when the value is equal to the filter value', () => {
    var result = spinColumnDirective.filter(5, { number: 5, searchTermType: SpinSearchTermType.Equals }, {});

    expect(result).toBeTruthy();
  });

  it('filter with equal search term type should return false when the value is not equal to the filter value', () => {
    var result = spinColumnDirective.filter(5, { number: 6, searchTermType: SpinSearchTermType.Equals }, {});

    expect(result).toBeFalsy();
  });

  it('filter with bigger or equal search term type should return true when the value is bigger or equal to the filter value', () => {
    var result = spinColumnDirective.filter(5, { number: 5, searchTermType: SpinSearchTermType.MoreThanOrEqual }, {});

    expect(result).toBeTruthy();
  });

  it('filter with bigger or equal search term type should return false when the value is not bigger or equal to the filter value', () => {
    var result = spinColumnDirective.filter(5, { number: 6, searchTermType: SpinSearchTermType.MoreThanOrEqual }, {});

    expect(result).toBeFalsy();
  });

  it('filter with bigger search term type should return true when the value is bigger than the filter value', () => {
    var result = spinColumnDirective.filter(6, { number: 5, searchTermType: SpinSearchTermType.MoreThan }, {});

    expect(result).toBeTruthy();
  });

  it('filter with bigger search term type should return false when the value is not bigger than the filter value', () => {
    var result = spinColumnDirective.filter(5, { number: 5, searchTermType: SpinSearchTermType.MoreThan }, {});

    expect(result).toBeFalsy();
  });

  it('filter should throw an Error if the search term type does not exist', () => {
    expect(function() { spinColumnDirective.filter(5, { number: 3, searchTermType: 5 }, {}); }).toThrowError();
  });
});