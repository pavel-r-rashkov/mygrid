"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("../animations");
var filter_component_1 = require("../contracts/filter-component");
var filter_state_service_1 = require("../../filter-state.service");
var spin_search_term_type_1 = require("./spin-search-term-type");
var SpinColumnFilterComponent = (function (_super) {
    __extends(SpinColumnFilterComponent, _super);
    function SpinColumnFilterComponent(filterStateService) {
        var _this = _super.call(this) || this;
        _this.filterStateService = filterStateService;
        _this.filterKey = 'Spin';
        _this.spinSearchTermType = spin_search_term_type_1.SpinSearchTermType;
        _this.menuVisible = false;
        return _this;
    }
    SpinColumnFilterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.selectedSearchType = this.filterData.searchTermType;
        this.filterStateSubscription = this.filterStateService.filterState.subscribe(function (newState) {
            _this.currentFilterState = newState;
            var spinColumnFilter = newState.columnFilters[_this.columnId];
            _this.filterNumber = spinColumnFilter == null ? null : spinColumnFilter.filterData.number;
        });
    };
    SpinColumnFilterComponent.prototype.ngAfterViewInit = function () {
        if (this.filterData.filterTemplate != null) {
            var templateContext = {
                columnId: this.columnId,
                filterData: this.filterData,
                filter: this.customTemplateFilter
            };
            this.containerRef.createEmbeddedView(this.filterData.filterTemplate, { $implicit: templateContext });
        }
    };
    SpinColumnFilterComponent.prototype.ngOnDestroy = function () {
        this.filterStateSubscription.unsubscribe();
    };
    SpinColumnFilterComponent.prototype.filterFocusedIn = function () {
        this.cachedFilterNumber = this.filterNumber;
    };
    SpinColumnFilterComponent.prototype.filterFocusedOut = function ($event) {
        if (this.cachedFilterNumber == this.filterNumber) {
            return;
        }
        if (this.filterNumber == null) {
            this.filterStateService.clearColumnFilter(this.columnId);
        }
        else {
            this.filterStateService.raiseFilterChanged(this.columnId, this.filterData.propertyName, { number: this.filterNumber, searchTermType: this.selectedSearchType }, this.filterKey);
        }
    };
    SpinColumnFilterComponent.prototype.selectSearchType = function (selectedType) {
        if (selectedType != this.selectedSearchType && this.filterNumber != null) {
            this.filterStateService.raiseFilterChanged(this.columnId, this.filterData.propertyName, { number: this.filterNumber, searchTermType: selectedType }, this.filterKey);
        }
        this.selectedSearchType = selectedType;
    };
    SpinColumnFilterComponent.prototype.toggleMenu = function () {
        this.menuVisible = !this.menuVisible;
    };
    SpinColumnFilterComponent.prototype.buttonFocusedOut = function () {
        this.menuVisible = false;
    };
    SpinColumnFilterComponent.prototype.getConditionMenuState = function () {
        return this.menuVisible ? 'opened' : 'hidden';
    };
    SpinColumnFilterComponent.prototype.customTemplateFilter = function (filterData) {
        this.filterStateService.raiseFilterChanged(this.columnId, this.filterData.propertyName, filterData, this.filterKey);
    };
    __decorate([
        core_1.ViewChild('customTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], SpinColumnFilterComponent.prototype, "containerRef", void 0);
    SpinColumnFilterComponent = __decorate([
        core_1.Component({
            selector: 'mg-spin-column-filter',
            templateUrl: './spin-column-filter.component.html',
            animations: [
                animations_1.filterConditionMenuAnimation
            ]
        }),
        __metadata("design:paramtypes", [filter_state_service_1.FilterStateService])
    ], SpinColumnFilterComponent);
    return SpinColumnFilterComponent;
}(filter_component_1.FilterComponent));
exports.SpinColumnFilterComponent = SpinColumnFilterComponent;
//# sourceMappingURL=spin-column-filter.component.js.map