import { 
  Directive, 
  forwardRef,
  Input,
  TemplateRef
} from '@angular/core';

import { GridColumn } from '../grid-column';
import { DefaultColumnHeaderComponent } from '../shared/default-column-header.component';
import { SpinColumnFilterComponent } from './spin-column-filter.component';
import { SpinColumnDataComponent } from './spin-column-data.component';
import { SpinColumnEditComponent } from './spin-column-edit.component';
import { GuidService } from '../../guid';
import { DefaultComparerService } from '../shared/default-comparer.service';
import { SpinSearchTermType } from './spin-search-term-type';

@Directive({
  selector: 'mg-spin-column',
  providers: [{provide: GridColumn, useExisting: forwardRef(() => SpinColumnDirective)}]
})
export class SpinColumnDirective extends GridColumn {
  @Input() propertyName: string;
  @Input() step: number;
  @Input() min: number;
  @Input() max: number;
  @Input() enableOrdering: boolean;
  @Input() enableFiltering: boolean;
  @Input() enableEditing: boolean;
  @Input() enableGrouping: boolean;
  @Input() enableSearchTermTypeMenu: boolean;
  @Input() searchTermType: SpinSearchTermType;
  @Input() customFilter: (val: any, filterData: any, item: any) => boolean;
  @Input() customOrder: (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) => number;
  @Input() customGroup: (firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) => number;

  constructor(
    private guidService: GuidService,
    private comparer: DefaultComparerService) {
    super(
      DefaultColumnHeaderComponent, 
      SpinColumnFilterComponent, 
      SpinColumnDataComponent, 
      SpinColumnEditComponent,
      guidService.newGuid());
  }

  public getHeaderData(): any {
    return {
      caption: this.caption,
      headerTemplate: this.headerTemplate,
      propertyName: this.propertyName,
      enableOrdering: this.enableOrdering == null ? true : this.enableOrdering,
      enableGrouping: this.enableGrouping == null ? true : this.enableGrouping
    };
  }

  public getData(): any {
    return {
      propertyName: this.propertyName,
      dataTemplate: this.dataTemplate,
      enableEditing: this.enableEditing == null ? true : this.enableEditing,
      step: this.step == null ? 1 : this.step,
      min: this.min,
      max: this.max,
      editTemplate: this.editTemplate,
      validationErrorsTemplate: this.validationErrorsTemplate
    }
  }

  public getFilterData(): any {
    return {
      filterTemplate: this.filterTemplate,
      propertyName: this.propertyName,
      step: this.step == null ? 1 : this.step,
      min: this.min,
      max: this.max,
      enableFiltering: this.enableFiltering == null ? true : this.enableFiltering,
      enableSearchTermTypeMenu: this.enableSearchTermTypeMenu == null ? true : this.enableSearchTermTypeMenu,
      searchTermType: this.searchTermType == null ? SpinSearchTermType.Equals : this.searchTermType
    }
  }

  public getGroupData(): any {
    let groupData = this.getData();
    groupData.dataTemplate = this.groupHeaderTemplate;
    return groupData;
  }

  public filter(val: any, filterData: any, item: any): boolean {
    if (this.customFilter != null) {
      return this.customFilter(val, filterData, item);
    }

    if (filterData != null && filterData.number != null) {
      if (val != null) {
        switch (filterData.searchTermType) {
          case SpinSearchTermType.LessThan:
            return val < filterData.number;
          case SpinSearchTermType.LessThanOrEqual:
            return val <= filterData.number;
          case SpinSearchTermType.Equals:
            return val == filterData.number;
          case SpinSearchTermType.MoreThanOrEqual:
            return val >= filterData.number;
          case SpinSearchTermType.MoreThan:
            return val > filterData.number;
          default:
            throw new Error('unrecognized search type');
        }
      } else {
        return false;
      }
    }

    return true;
  }

  public order(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    if (this.customOrder != null) {
      return this.customOrder(firstValue, secondValue, isAscending, firstItem, secondItem);
    } else {
      return this.comparer.compare(firstValue, secondValue, isAscending);
    }
  }

  public group(firstValue: any, secondValue: any, isAscending: boolean, firstItem: any, secondItem: any) {
    if (this.customGroup != null) {
      return this.customGroup(firstValue, secondValue, isAscending, firstItem, secondItem);
    } else {
      return this.comparer.compare(firstValue, secondValue, isAscending);
    }
  }

  public getValue(item: any) {
    if (item[this.propertyName] == undefined) {
      return null;
    }
    
    return item[this.propertyName];
  }
}