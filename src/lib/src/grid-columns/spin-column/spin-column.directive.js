"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var grid_column_1 = require("../grid-column");
var default_column_header_component_1 = require("../shared/default-column-header.component");
var spin_column_filter_component_1 = require("./spin-column-filter.component");
var spin_column_data_component_1 = require("./spin-column-data.component");
var spin_column_edit_component_1 = require("./spin-column-edit.component");
var guid_1 = require("../../guid");
var default_comparer_service_1 = require("../shared/default-comparer.service");
var spin_search_term_type_1 = require("./spin-search-term-type");
var SpinColumnDirective = (function (_super) {
    __extends(SpinColumnDirective, _super);
    function SpinColumnDirective(guidService, comparer) {
        var _this = _super.call(this, default_column_header_component_1.DefaultColumnHeaderComponent, spin_column_filter_component_1.SpinColumnFilterComponent, spin_column_data_component_1.SpinColumnDataComponent, spin_column_edit_component_1.SpinColumnEditComponent, guidService.newGuid()) || this;
        _this.guidService = guidService;
        _this.comparer = comparer;
        return _this;
    }
    SpinColumnDirective_1 = SpinColumnDirective;
    SpinColumnDirective.prototype.getHeaderData = function () {
        return {
            caption: this.caption,
            headerTemplate: this.headerTemplate,
            propertyName: this.propertyName,
            enableOrdering: this.enableOrdering == null ? true : this.enableOrdering,
            enableGrouping: this.enableGrouping == null ? true : this.enableGrouping
        };
    };
    SpinColumnDirective.prototype.getData = function () {
        return {
            propertyName: this.propertyName,
            dataTemplate: this.dataTemplate,
            enableEditing: this.enableEditing == null ? true : this.enableEditing,
            step: this.step == null ? 1 : this.step,
            min: this.min,
            max: this.max,
            editTemplate: this.editTemplate,
            validationErrorsTemplate: this.validationErrorsTemplate
        };
    };
    SpinColumnDirective.prototype.getFilterData = function () {
        return {
            filterTemplate: this.filterTemplate,
            propertyName: this.propertyName,
            step: this.step == null ? 1 : this.step,
            min: this.min,
            max: this.max,
            enableFiltering: this.enableFiltering == null ? true : this.enableFiltering,
            enableSearchTermTypeMenu: this.enableSearchTermTypeMenu == null ? true : this.enableSearchTermTypeMenu,
            searchTermType: this.searchTermType == null ? spin_search_term_type_1.SpinSearchTermType.Equals : this.searchTermType
        };
    };
    SpinColumnDirective.prototype.getGroupData = function () {
        var groupData = this.getData();
        groupData.dataTemplate = this.groupHeaderTemplate;
        return groupData;
    };
    SpinColumnDirective.prototype.filter = function (val, filterData, item) {
        if (this.customFilter != null) {
            return this.customFilter(val, filterData, item);
        }
        if (filterData != null && filterData.number != null) {
            if (val != null) {
                switch (filterData.searchTermType) {
                    case spin_search_term_type_1.SpinSearchTermType.LessThan:
                        return val < filterData.number;
                    case spin_search_term_type_1.SpinSearchTermType.LessThanOrEqual:
                        return val <= filterData.number;
                    case spin_search_term_type_1.SpinSearchTermType.Equals:
                        return val == filterData.number;
                    case spin_search_term_type_1.SpinSearchTermType.MoreThanOrEqual:
                        return val >= filterData.number;
                    case spin_search_term_type_1.SpinSearchTermType.MoreThan:
                        return val > filterData.number;
                    default:
                        throw new Error('unrecognized search type');
                }
            }
            else {
                return false;
            }
        }
        return true;
    };
    SpinColumnDirective.prototype.order = function (firstValue, secondValue, isAscending, firstItem, secondItem) {
        if (this.customOrder != null) {
            return this.customOrder(firstValue, secondValue, isAscending, firstItem, secondItem);
        }
        else {
            return this.comparer.compare(firstValue, secondValue, isAscending);
        }
    };
    SpinColumnDirective.prototype.group = function (firstValue, secondValue, isAscending, firstItem, secondItem) {
        if (this.customGroup != null) {
            return this.customGroup(firstValue, secondValue, isAscending, firstItem, secondItem);
        }
        else {
            return this.comparer.compare(firstValue, secondValue, isAscending);
        }
    };
    SpinColumnDirective.prototype.getValue = function (item) {
        if (item[this.propertyName] == undefined) {
            return null;
        }
        return item[this.propertyName];
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], SpinColumnDirective.prototype, "propertyName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], SpinColumnDirective.prototype, "step", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], SpinColumnDirective.prototype, "min", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], SpinColumnDirective.prototype, "max", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], SpinColumnDirective.prototype, "enableOrdering", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], SpinColumnDirective.prototype, "enableFiltering", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], SpinColumnDirective.prototype, "enableEditing", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], SpinColumnDirective.prototype, "enableGrouping", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], SpinColumnDirective.prototype, "enableSearchTermTypeMenu", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], SpinColumnDirective.prototype, "searchTermType", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], SpinColumnDirective.prototype, "customFilter", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], SpinColumnDirective.prototype, "customOrder", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], SpinColumnDirective.prototype, "customGroup", void 0);
    SpinColumnDirective = SpinColumnDirective_1 = __decorate([
        core_1.Directive({
            selector: 'mg-spin-column',
            providers: [{ provide: grid_column_1.GridColumn, useExisting: core_1.forwardRef(function () { return SpinColumnDirective_1; }) }]
        }),
        __metadata("design:paramtypes", [guid_1.GuidService,
            default_comparer_service_1.DefaultComparerService])
    ], SpinColumnDirective);
    return SpinColumnDirective;
    var SpinColumnDirective_1;
}(grid_column_1.GridColumn));
exports.SpinColumnDirective = SpinColumnDirective;
//# sourceMappingURL=spin-column.directive.js.map