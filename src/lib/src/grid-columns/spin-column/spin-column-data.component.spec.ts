import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { SpinColumnDataComponent } from './spin-column-data.component';

@Component({
  selector: 'mgt-test-data',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
  `
})
class TestDataComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
}

describe('Component: SpinColumnDataComponent', () => {
  let spinColumnDataComponent: SpinColumnDataComponent;
  let fixture: ComponentFixture<SpinColumnDataComponent>;
  let dataTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestDataComponent
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SpinColumnDataComponent);
        spinColumnDataComponent = fixture.componentInstance;

        var templateComponent: TestDataComponent = TestBed.createComponent(TestDataComponent).componentInstance;
        dataTemplate = templateComponent.dataTemplate;
      });
  }));

  it('custom data template should be rendered when it exists', () => {
    var item = {};
    var data = {
      dataTemplate: dataTemplate
    };
    spinColumnDataComponent.data = data;
    spinColumnDataComponent.item = item;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-data-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.data).toEqual(data);
    expect(customTemplate.context.$implicit.item).toEqual(item);
  });
});