import { 
  Component, 
  OnInit,
  OnDestroy,
  AfterViewInit,
  Input,
  TemplateRef,
  ViewChild,
  ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import { filterConditionMenuAnimation } from '../animations';
import { FilterComponent } from '../contracts/filter-component';
import { FilterStateService } from '../../filter-state.service';
import { FilterState } from '../../states/filter-state';
import { SpinSearchTermType } from './spin-search-term-type';

@Component({
  selector: 'mg-spin-column-filter',
  templateUrl: './spin-column-filter.component.html',
  animations: [
    filterConditionMenuAnimation
  ]
})
export class SpinColumnFilterComponent extends FilterComponent implements OnInit, AfterViewInit, OnDestroy {
  private readonly filterKey: string = 'Spin';
  private filterStateSubscription: Subscription;
  private currentFilterState: FilterState;
  private cachedFilterNumber: number;
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;
  filterNumber: number;
  menuVisible: boolean;
  selectedSearchType: SpinSearchTermType;
  spinSearchTermType = SpinSearchTermType;

  constructor(
    private filterStateService: FilterStateService) {
    super();
    this.menuVisible = false;
  }

  ngOnInit() {
    this.selectedSearchType = this.filterData.searchTermType;

    this.filterStateSubscription = this.filterStateService.filterState.subscribe((newState) => {
      this.currentFilterState = newState;
      let spinColumnFilter = newState.columnFilters[this.columnId];
      this.filterNumber = spinColumnFilter == null ? null : spinColumnFilter.filterData.number;
    });
  }

  ngAfterViewInit() {
    if (this.filterData.filterTemplate != null) {
      let templateContext = {
        columnId: this.columnId,
        filterData: this.filterData,
        filter: this.customTemplateFilter
      }

      this.containerRef.createEmbeddedView(this.filterData.filterTemplate, { $implicit: templateContext });
    }
  }

  ngOnDestroy() {
    this.filterStateSubscription.unsubscribe();
  }

  filterFocusedIn() {
    this.cachedFilterNumber = this.filterNumber;
  }

  filterFocusedOut($event: any) {
    if (this.cachedFilterNumber == this.filterNumber) {
      return;
    }

    if (this.filterNumber == null) {
      this.filterStateService.clearColumnFilter(this.columnId);
    } else {
      this.filterStateService.raiseFilterChanged(
        this.columnId,
        this.filterData.propertyName, 
        { number: this.filterNumber, searchTermType: this.selectedSearchType },
        this.filterKey);
    }
    
  }

  selectSearchType(selectedType: number) {
    if (selectedType != this.selectedSearchType && this.filterNumber != null) {
      this.filterStateService.raiseFilterChanged(
        this.columnId,
        this.filterData.propertyName,
        { number: this.filterNumber, searchTermType: selectedType },
        this.filterKey);
    }

    this.selectedSearchType = selectedType;
  }

  toggleMenu() {
    this.menuVisible = !this.menuVisible;
  }

  buttonFocusedOut() {
    this.menuVisible = false;
  }

  getConditionMenuState(): string {
    return this.menuVisible ? 'opened' : 'hidden';
  }

  customTemplateFilter(filterData: any) {
    this.filterStateService.raiseFilterChanged(
      this.columnId,
      this.filterData.propertyName,
      filterData,
      this.filterKey);
  }
}