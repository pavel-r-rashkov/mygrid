"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var my_grid_module_1 = require("../../my-grid.module");
var spin_column_filter_component_1 = require("./spin-column-filter.component");
var filter_state_service_1 = require("../../filter-state.service");
var spin_search_term_type_1 = require("./spin-search-term-type");
var TestFilterComponent = (function () {
    function TestFilterComponent() {
    }
    __decorate([
        core_1.ViewChild('filterTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], TestFilterComponent.prototype, "filterTemplate", void 0);
    TestFilterComponent = __decorate([
        core_1.Component({
            selector: 'mgt-test-filter',
            template: "\n    <ng-template #filterTemplate>\n      <div id=\"test-filter-template\"></div>\n    </ng-template>\n  "
        })
    ], TestFilterComponent);
    return TestFilterComponent;
}());
describe('Component: SpinColumnFilterComponent', function () {
    var spinColumnFilterComponent;
    var fixture;
    var filterTemplate;
    var filterStateService;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [
                filter_state_service_1.FilterStateService
            ],
            declarations: [
                TestFilterComponent
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(spin_column_filter_component_1.SpinColumnFilterComponent);
            spinColumnFilterComponent = fixture.componentInstance;
            var templateComponent = testing_1.TestBed.createComponent(TestFilterComponent).componentInstance;
            filterTemplate = templateComponent.filterTemplate;
            filterStateService = fixture.debugElement.injector.get(filter_state_service_1.FilterStateService);
        });
    }));
    it('custom filter template should be rendered when it exists', function () {
        var data = {
            filterTemplate: filterTemplate,
            enableFiltering: true
        };
        spinColumnFilterComponent.filterData = data;
        fixture.detectChanges();
        var customTemplate = fixture.debugElement.query(platform_browser_1.By.css('#test-filter-template'));
        expect(customTemplate).not.toBeNull();
        expect(customTemplate.context.$implicit.filterData).toEqual(data);
        expect(customTemplate.context.$implicit.columnId).toEqual(spinColumnFilterComponent.columnId);
        expect(customTemplate.context.$implicit.filter).toEqual(spinColumnFilterComponent.customTemplateFilter);
    });
    it('selectedSearchType should be set on init', function () {
        var propertyName = 'foo';
        var searchTermType = spin_search_term_type_1.SpinSearchTermType.MoreThanOrEqual;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        fixture.detectChanges();
        expect(spinColumnFilterComponent.selectedSearchType).toEqual(searchTermType);
    });
    it('changing filter state should update filterNumber', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        var filterNumber = 10;
        fixture.detectChanges();
        filterStateService.raiseFilterChanged(columnId, propertyName, { number: filterNumber }, '');
        fixture.whenStable().then(function () {
            expect(spinColumnFilterComponent.filterNumber).toEqual(filterNumber);
        });
    });
    it('getConditionMenuState should return opened when menu is visible', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        spinColumnFilterComponent.menuVisible = true;
        fixture.detectChanges();
        var menuState = spinColumnFilterComponent.getConditionMenuState();
        expect(menuState).toEqual('opened');
    });
    it('getConditionMenuState should return hidden when menu is not visible', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        spinColumnFilterComponent.menuVisible = false;
        fixture.detectChanges();
        var menuState = spinColumnFilterComponent.getConditionMenuState();
        expect(menuState).toEqual('hidden');
    });
    it('buttonFocusedOut should hide the menu', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        spinColumnFilterComponent.menuVisible = true;
        fixture.detectChanges();
        spinColumnFilterComponent.buttonFocusedOut();
        expect(spinColumnFilterComponent.menuVisible).toBeFalsy();
    });
    it('toggleMenu should show/hide menu', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        var menuVisible = false;
        spinColumnFilterComponent.menuVisible = menuVisible;
        fixture.detectChanges();
        spinColumnFilterComponent.toggleMenu();
        expect(spinColumnFilterComponent.menuVisible).toEqual(!menuVisible);
    });
    it('customTemplateFilter should create new filter state', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        var templateFilterData = {};
        var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        spinColumnFilterComponent.customTemplateFilter(templateFilterData);
        expect(raiseFilterChangeSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangeSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2]).toEqual(templateFilterData);
    });
    it('selectedSearchType should be set on init', function () {
        var propertyName = 'foo';
        var searchTermType = spin_search_term_type_1.SpinSearchTermType.MoreThanOrEqual;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        fixture.detectChanges();
        expect(spinColumnFilterComponent.selectedSearchType).toEqual(searchTermType);
    });
    it('selectSearchType should not create new filter state when there is no change in the selected search type', function () {
        var propertyName = 'foo';
        var searchTermType = spin_search_term_type_1.SpinSearchTermType.MoreThanOrEqual;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        spinColumnFilterComponent.filterNumber = 10;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        spinColumnFilterComponent.selectSearchType(searchTermType);
        expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
    });
    it('selectSearchType should not create new filter state when there is no filter number selected', function () {
        var propertyName = 'foo';
        var searchTermType = spin_search_term_type_1.SpinSearchTermType.MoreThanOrEqual;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        spinColumnFilterComponent.selectSearchType(4);
        expect(raiseFilterChangedSpy).not.toHaveBeenCalled();
    });
    it('selectSearchType should create new filter state when there is filter number selected and the search term type is changed', function () {
        var propertyName = 'foo';
        var searchTermType = spin_search_term_type_1.SpinSearchTermType.MoreThanOrEqual;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        var filterNumber = 12;
        spinColumnFilterComponent.filterNumber = filterNumber;
        var selectedSearchTermType = spin_search_term_type_1.SpinSearchTermType.MoreThan;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        spinColumnFilterComponent.selectSearchType(selectedSearchTermType);
        expect(raiseFilterChangedSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangedSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2].number).toEqual(filterNumber);
        expect(filterCall.args[2].searchTermType).toEqual(selectedSearchTermType);
    });
    it('filterFocusedOut should not create new filter state when filter number is not changed', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        spinColumnFilterComponent.filterNumber = 10;
        var raiseFilterChangeSpy = spyOn(filterStateService, 'raiseFilterChanged');
        var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');
        fixture.detectChanges();
        spinColumnFilterComponent.filterFocusedIn();
        spinColumnFilterComponent.filterFocusedOut({});
        expect(raiseFilterChangeSpy).not.toHaveBeenCalled();
        expect(clearColumnFilterSpy).not.toHaveBeenCalled();
    });
    it('filterFocusedOut should clear filter for this column when the selected number is null', function () {
        var propertyName = 'foo';
        var data = {
            propertyName: propertyName,
            enableFiltering: true
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        var clearColumnFilterSpy = spyOn(filterStateService, 'clearColumnFilter');
        fixture.detectChanges();
        spinColumnFilterComponent.filterNumber = 10;
        spinColumnFilterComponent.filterFocusedIn();
        spinColumnFilterComponent.filterNumber = null;
        spinColumnFilterComponent.filterFocusedOut({});
        expect(clearColumnFilterSpy).toHaveBeenCalled();
        expect(clearColumnFilterSpy.calls.first().args[0]).toEqual(spinColumnFilterComponent.columnId);
    });
    it('filterFocusedOut should create new filter state when selected number is not null', function () {
        var propertyName = 'foo';
        var searchTermType = spin_search_term_type_1.SpinSearchTermType.MoreThanOrEqual;
        var data = {
            propertyName: propertyName,
            enableFiltering: true,
            searchTermType: searchTermType
        };
        spinColumnFilterComponent.filterData = data;
        var columnId = '123';
        spinColumnFilterComponent.columnId = columnId;
        var filterNumber = 10;
        spinColumnFilterComponent.filterNumber = filterNumber;
        var raiseFilterChangedSpy = spyOn(filterStateService, 'raiseFilterChanged');
        fixture.detectChanges();
        spinColumnFilterComponent.filterFocusedOut({});
        expect(raiseFilterChangedSpy).toHaveBeenCalled();
        var filterCall = raiseFilterChangedSpy.calls.first();
        expect(filterCall.args[0]).toEqual(columnId);
        expect(filterCall.args[1]).toEqual(propertyName);
        expect(filterCall.args[2].number).toEqual(filterNumber);
        expect(filterCall.args[2].searchTermType).toEqual(searchTermType);
    });
});
//# sourceMappingURL=spin-column-filter.component.spec.js.map