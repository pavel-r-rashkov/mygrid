import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';

import { DetailsButtonColumnDirective } from './details-button-column.directive';
import { GuidService } from '../../guid';

@Component({
  selector: 'mgt-test-details-button-column',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>
    <ng-template #headerTemplate>
      <div id="test-header-template"></div>
    </ng-template>
  `
}) 
class TestDetailsButtonColumnComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
}

describe('Directive: DetailsButtonColumnDirective', () => {
  let detailsButtonColumnDirective: DetailsButtonColumnDirective;
  let dataTemplate: TemplateRef<any>;
  let headerTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestDetailsButtonColumnComponent
        ]
      })
      .compileComponents()
      .then(() => {
        detailsButtonColumnDirective = new DetailsButtonColumnDirective(new GuidService());

        var testComponent = TestBed.createComponent(TestDetailsButtonColumnComponent).componentInstance;
        dataTemplate = testComponent.dataTemplate;
        headerTemplate = testComponent.headerTemplate;
      });
  }));

  it('getHeaderData should return details button column header data', () => {
    var caption = 'foo';
    detailsButtonColumnDirective.caption = caption;
    detailsButtonColumnDirective.headerTemplate = headerTemplate;

    var headerData: any = detailsButtonColumnDirective.getHeaderData();

    expect(headerData.caption).toEqual(caption);
    expect(headerData.headerTemplate).toEqual(headerTemplate);
    expect(headerData.enableOrdering).toBeFalsy();
  });

  it('getData should return details button column data', () => {
    detailsButtonColumnDirective.dataTemplate = dataTemplate;

    var data: any = detailsButtonColumnDirective.getData();

    expect(data.dataTemplate).toEqual(dataTemplate);
  });
});