import { 
  Directive, 
  forwardRef,
  Input,
  TemplateRef
} from '@angular/core';

import { GridColumn } from '../grid-column';
import { DetailsButtonColumnDataComponent } from './details-button-column-data.component';
import { DefaultColumnHeaderComponent } from '../shared/default-column-header.component';
import { GuidService } from '../../guid';

@Directive({
  selector: 'mg-details-button-column',
  providers: [{provide: GridColumn, useExisting: forwardRef(() => DetailsButtonColumnDirective)}]
})
export class DetailsButtonColumnDirective extends GridColumn {
  constructor(
    guidService: GuidService) {
    super(
      DefaultColumnHeaderComponent, 
      null, 
      DetailsButtonColumnDataComponent,
      null,
      guidService.newGuid());
  }

  public getHeaderData(): any {
    return {
      caption: this.caption,
      headerTemplate: this.headerTemplate,
      enableOrdering: false
    };
  }

  public getData(): any {
    return {
      dataTemplate: this.dataTemplate
    }
  }
}