"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_component_1 = require("../contracts/data-component");
var grid_settings_service_1 = require("../../grid-settings.service");
var master_detail_service_1 = require("../../master-detail/master-detail.service");
var details_state_1 = require("../../master-detail/details-state");
var DetailsButtonColumnDataComponent = (function (_super) {
    __extends(DetailsButtonColumnDataComponent, _super);
    function DetailsButtonColumnDataComponent(gridSettings, masterDetailService) {
        var _this = _super.call(this) || this;
        _this.gridSettings = gridSettings;
        _this.masterDetailService = masterDetailService;
        _this.detailsStateSubscription = _this.masterDetailService.detailsStateSubject.subscribe(function (newState) {
            _this.currentDetailsState = newState;
        });
        return _this;
    }
    DetailsButtonColumnDataComponent.prototype.ngOnInit = function () {
        if (this.data.dataTemplate != null) {
            var templateContext = {
                data: this.data,
                item: this.item
            };
            this.containerRef.createEmbeddedView(this.data.dataTemplate, { $implicit: templateContext });
        }
    };
    DetailsButtonColumnDataComponent.prototype.ngOnDestroy = function () {
        this.detailsStateSubscription.unsubscribe();
    };
    DetailsButtonColumnDataComponent.prototype.toggleDetails = function () {
        var key = this.item[this.gridSettings.key];
        var keyIndex = this.currentDetailsState.openedDetails.indexOf(key);
        if (keyIndex < 0) {
            this.currentDetailsState.openedDetails.push(key);
        }
        else {
            this.currentDetailsState.openedDetails.splice(keyIndex, 1);
        }
        this.masterDetailService.detailsStateSubject.next(new details_state_1.DetailsState(this.currentDetailsState.openedDetails));
    };
    DetailsButtonColumnDataComponent.prototype.areDetailsOpened = function () {
        var key = this.item[this.gridSettings.key];
        return this.currentDetailsState.openedDetails.indexOf(key) >= 0;
    };
    __decorate([
        core_1.ViewChild('customTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], DetailsButtonColumnDataComponent.prototype, "containerRef", void 0);
    DetailsButtonColumnDataComponent = __decorate([
        core_1.Component({
            selector: 'mg-details-button-column-data',
            templateUrl: './details-button-column-data.component.html'
        }),
        __metadata("design:paramtypes", [grid_settings_service_1.GridSettingsService,
            master_detail_service_1.MasterDetailService])
    ], DetailsButtonColumnDataComponent);
    return DetailsButtonColumnDataComponent;
}(data_component_1.DataComponent));
exports.DetailsButtonColumnDataComponent = DetailsButtonColumnDataComponent;
//# sourceMappingURL=details-button-column-data.component.js.map