import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from '../../my-grid.component';
import { MyGridModule } from '../../my-grid.module';
import { DetailsButtonColumnDataComponent } from './details-button-column-data.component';
import { MasterDetailService } from '../../master-detail/master-detail.service';
import { GridSettingsService } from '../../grid-settings.service';
import { DetailsState } from '../../master-detail/details-state';

@Component({
  selector: 'mgt-test-data',
  template: `
    <ng-template #dataTemplate>
      <div id="test-data-template"></div>
    </ng-template>  
  `
})
class TestDataComponent {
  @ViewChild('dataTemplate') dataTemplate: TemplateRef<any>;
}

describe('Component: DetailsButtonColumnDataComponent', () => {
  let detailsButtonColumnDataComponent: DetailsButtonColumnDataComponent;
  let fixture: ComponentFixture<DetailsButtonColumnDataComponent>;
  let dataTemplate: TemplateRef<any>;
  let masterDetailService: MasterDetailService;
  let gridSettingsService: GridSettingsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          TestDataComponent
        ],
        providers: [
          MasterDetailService,
          GridSettingsService
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(DetailsButtonColumnDataComponent);
        detailsButtonColumnDataComponent = fixture.componentInstance;

        masterDetailService = fixture.debugElement.injector.get(MasterDetailService);
        gridSettingsService = fixture.debugElement.injector.get(GridSettingsService);

        var templateComponent: TestDataComponent = TestBed.createComponent(TestDataComponent).componentInstance;
        dataTemplate = templateComponent.dataTemplate;
      });
  }));

  it('custom data template should be rendered when it exists', () => {
    var item = {};
    var data = {
      dataTemplate: dataTemplate
    };
    detailsButtonColumnDataComponent.data = data;
    detailsButtonColumnDataComponent.item = item;

    fixture.detectChanges();

    var customTemplate: DebugElement = fixture.debugElement.query(By.css('#test-data-template'));
    expect(customTemplate).not.toBeNull();
    expect(customTemplate.context.$implicit.data).toEqual(data);
    expect(customTemplate.context.$implicit.item).toEqual(item);
  });

  it('areDetailsOpened should return false when the details for this item are closed', () => {
    var key = 'foo';
    var item = { foo: 'foo' };
    gridSettingsService.key = key;
    detailsButtonColumnDataComponent.item = item;
    detailsButtonColumnDataComponent.data = {};

    fixture.detectChanges();
    var areOpened = detailsButtonColumnDataComponent.areDetailsOpened();

    expect(areOpened).toBeFalsy();
  });

  it('areDetailsOpened should return true when the details for this item are opened', async(() => {
    var key = 'foo';
    var item = { foo: 'foo' };
    gridSettingsService.key = key;
    detailsButtonColumnDataComponent.item = item;
    detailsButtonColumnDataComponent.data = {};

    fixture.detectChanges();
    masterDetailService.detailsStateSubject.next(new DetailsState([item[key]]));

    fixture.whenStable().then(() => {
      var areOpened = detailsButtonColumnDataComponent.areDetailsOpened();
      expect(areOpened).toBeTruthy();
    });
  }));

  it('toggleDetails with closed details should add the item\'s key to the currently opened details list', async(() => {
    var key = 'foo';
    var item = { foo: 'foo' };
    gridSettingsService.key = key;
    detailsButtonColumnDataComponent.item = item;
    detailsButtonColumnDataComponent.data = {};
    var detailsStateSpy: jasmine.Spy = spyOn(masterDetailService.detailsStateSubject, 'next');

    fixture.detectChanges();
    detailsButtonColumnDataComponent.toggleDetails();

    expect(detailsStateSpy.calls.count()).toEqual(1);
    var detailsState: DetailsState = detailsStateSpy.calls.first().args[0];
    expect(detailsState.openedDetails.indexOf(item[key]) >= 0).toBeTruthy();
  }));

  it('toggleDetails with opened details should remove the item\'s key from the currently opened details list', async(() => {
    var key = 'foo';
    var item = { foo: 'foo' };
    gridSettingsService.key = key;
    detailsButtonColumnDataComponent.item = item;
    detailsButtonColumnDataComponent.data = {};
    var detailsStateSpy: jasmine.Spy = spyOn(masterDetailService.detailsStateSubject, 'next').and.callThrough();

    fixture.detectChanges();
    masterDetailService.detailsStateSubject.next(new DetailsState([item[key]]));

    fixture.whenStable().then(() => {
      detailsStateSpy.calls.reset();      
      detailsButtonColumnDataComponent.toggleDetails();

      expect(detailsStateSpy.calls.count()).toEqual(1);
      var detailsState: DetailsState = detailsStateSpy.calls.first().args[0];
      expect(detailsState.openedDetails.indexOf(item[key]) < 0).toBeTruthy();  
    });
  }));
});