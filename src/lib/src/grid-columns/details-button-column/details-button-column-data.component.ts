import { 
  Component, 
  OnInit,
  OnDestroy,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import { DataComponent } from '../contracts/data-component';
import { GridSettingsService } from '../../grid-settings.service';
import { MasterDetailService } from '../../master-detail/master-detail.service';
import { DetailsState } from '../../master-detail/details-state';

@Component({
  selector: 'mg-details-button-column-data',
  templateUrl: './details-button-column-data.component.html'
})
export class DetailsButtonColumnDataComponent extends DataComponent implements OnInit, OnDestroy {
  private detailsStateSubscription: Subscription;
	private currentDetailsState: DetailsState;
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;

  constructor(
  	private gridSettings: GridSettingsService,
  	private masterDetailService: MasterDetailService) {
    super();
  	this.detailsStateSubscription = this.masterDetailService.detailsStateSubject.subscribe((newState) => {
  		this.currentDetailsState = newState;
  	});
  }

  ngOnInit() {
    if (this.data.dataTemplate != null) {
      let templateContext = {
        data: this.data,
        item: this.item
      };
      this.containerRef.createEmbeddedView(this.data.dataTemplate, { $implicit: templateContext });
    }
  }

  ngOnDestroy() {
    this.detailsStateSubscription.unsubscribe();
  }

  toggleDetails() {
  	let key = this.item[this.gridSettings.key];
  	let keyIndex = this.currentDetailsState.openedDetails.indexOf(key);

  	if (keyIndex < 0) {
  		this.currentDetailsState.openedDetails.push(key);
  	} else {
  		this.currentDetailsState.openedDetails.splice(keyIndex, 1);
  	}

  	this.masterDetailService.detailsStateSubject.next(
  			new DetailsState(this.currentDetailsState.openedDetails));
  }

  areDetailsOpened() {
  	let key = this.item[this.gridSettings.key];
  	return this.currentDetailsState.openedDetails.indexOf(key) >= 0;
  }
}