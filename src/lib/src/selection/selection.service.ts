import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { SelectionState } from './selection-state';

@Injectable()
export class SelectionService {
	public selectionSubject: BehaviorSubject<SelectionState>;

	constructor() {
		this.selectionSubject = new BehaviorSubject<SelectionState>(new SelectionState([]));
	}
}