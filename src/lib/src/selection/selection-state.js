"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SelectionState = (function () {
    function SelectionState(selectedItems) {
        this.selectedItems = selectedItems;
    }
    return SelectionState;
}());
exports.SelectionState = SelectionState;
//# sourceMappingURL=selection-state.js.map