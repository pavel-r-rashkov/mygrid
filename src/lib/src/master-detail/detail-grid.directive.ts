import { 
	Directive, 
	OnInit,
	ContentChild,
	TemplateRef,
	ViewChild,
	ViewChildren,
	forwardRef,
	ContentChildren,
	QueryList,
	ElementRef
} from '@angular/core';

import { GridColumn } from '../grid-columns/grid-column';
import { MasterDetailService } from './master-detail.service';

import { MyGridComponent } from '../my-grid.component';

@Directive({
	selector: 'mg-detail-grid',
	providers: [MasterDetailService]
})
export class DetailGridDirective {
	@ContentChild(TemplateRef) detailGridTemplate: TemplateRef<any>;

	constructor() {
	}
}