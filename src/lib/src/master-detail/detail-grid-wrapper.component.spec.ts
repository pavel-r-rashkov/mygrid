import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import {
  Component, 
  DebugElement, 
  Type,
  TemplateRef,
  ViewChild
} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import { Observable } from "rxjs/Observable";

import { MyGridComponent } from '../my-grid.component';
import { DetailGridWrapperComponent } from './detail-grid-wrapper.component';
import { GridSettingsService } from '../grid-settings.service';
import { MyGridModule } from '../my-grid.module';

@Component({
  selector: 'mgt-details-grid-template',
  template: `
    <ng-template #detailsTemplate let-context>
      <mg-grid [items]="context.items">
        <mg-text-column [propertyName]="'id'"></mg-text-column>
      </mg-grid>
    </ng-template>
  `
})
class DetailsGridTemplateComponent {
  @ViewChild('detailsTemplate') detailsTemplate: TemplateRef<any>;
}

describe('Component: DetailGridWrapperComponent', () => {
  let detailGridWrapperComponent: DetailGridWrapperComponent;
  let fixture: ComponentFixture<DetailGridWrapperComponent>;
  let gridSettingsService: GridSettingsService;
  let masterItem: any;
  let detailsGridTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          DetailsGridTemplateComponent
        ],
        providers: [
          GridSettingsService
        ],
        imports: [MyGridModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(DetailGridWrapperComponent);
        detailGridWrapperComponent = fixture.componentInstance;

        gridSettingsService = fixture.debugElement.injector.get(GridSettingsService);
        gridSettingsService.key = 'id';
        masterItem = { id: 1, firstName: 'Foo', lastName: 'Bar' };

        detailsGridTemplate = TestBed
          .createComponent(DetailsGridTemplateComponent)
          .componentInstance
          .detailsTemplate;
      });
  }));

  it('DetailGridWrapperComponent should call getDetails and create details grid on initialization', async(() => {
    var detailItems = [
      { id: 1 },
      { id: 2 },
      { id: 3 }
    ]

    var inputs = {
      masterItem: masterItem,
      detailGridTemplate: detailsGridTemplate,
      getDetailItems: () => {
        return Observable.of(detailItems);
      }
    };

    detailGridWrapperComponent.masterItem = inputs.masterItem;
    detailGridWrapperComponent.detailGridTemplate = inputs.detailGridTemplate;
    detailGridWrapperComponent.getDetailItems = inputs.getDetailItems;
    var getDetailsSpy = spyOn(detailGridWrapperComponent, 'getDetailItems').and.callThrough();

    detailGridWrapperComponent.ngOnInit();

    fixture.whenStable().then(() => {
      var masterKey = masterItem[gridSettingsService.key];
      expect(getDetailsSpy.calls.count()).toEqual(1);
      expect(getDetailsSpy.calls.first().args[0]).toEqual(inputs.masterItem[gridSettingsService.key]);
      expect(getDetailsSpy.calls.first().args[1]).toEqual(inputs.masterItem);
      
      fixture.detectChanges();
      var dataCells = fixture.debugElement.queryAll(By.css('.mg-data'));
      expect(dataCells.length).toEqual(detailItems.length);
      for (var i = 0; i >= dataCells.length; i++) {
        expect(Number(dataCells[i].nativeElement.textContent)).toEqual(detailItems[i]);
      }
    });
  }));
});