import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { DetailsState } from './details-state';

@Injectable()
export class MasterDetailService {
  public masterKey : any;
  public masterItem : any;
  public detailsStateSubject: BehaviorSubject<DetailsState>;

  constructor() {
    this.detailsStateSubject = new BehaviorSubject<DetailsState>(new DetailsState([]));
  }
}