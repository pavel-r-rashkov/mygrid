"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
require("rxjs/add/observable/of");
var Observable_1 = require("rxjs/Observable");
var detail_grid_wrapper_component_1 = require("./detail-grid-wrapper.component");
var grid_settings_service_1 = require("../grid-settings.service");
var my_grid_module_1 = require("../my-grid.module");
var DetailsGridTemplateComponent = (function () {
    function DetailsGridTemplateComponent() {
    }
    __decorate([
        core_1.ViewChild('detailsTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], DetailsGridTemplateComponent.prototype, "detailsTemplate", void 0);
    DetailsGridTemplateComponent = __decorate([
        core_1.Component({
            selector: 'mgt-details-grid-template',
            template: "\n    <ng-template #detailsTemplate let-context>\n      <mg-grid [items]=\"context.items\">\n        <mg-text-column [propertyName]=\"'id'\"></mg-text-column>\n      </mg-grid>\n    </ng-template>\n  "
        })
    ], DetailsGridTemplateComponent);
    return DetailsGridTemplateComponent;
}());
describe('Component: DetailGridWrapperComponent', function () {
    var detailGridWrapperComponent;
    var fixture;
    var gridSettingsService;
    var masterItem;
    var detailsGridTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                DetailsGridTemplateComponent
            ],
            providers: [
                grid_settings_service_1.GridSettingsService
            ],
            imports: [my_grid_module_1.MyGridModule]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(detail_grid_wrapper_component_1.DetailGridWrapperComponent);
            detailGridWrapperComponent = fixture.componentInstance;
            gridSettingsService = fixture.debugElement.injector.get(grid_settings_service_1.GridSettingsService);
            gridSettingsService.key = 'id';
            masterItem = { id: 1, firstName: 'Foo', lastName: 'Bar' };
            detailsGridTemplate = testing_1.TestBed
                .createComponent(DetailsGridTemplateComponent)
                .componentInstance
                .detailsTemplate;
        });
    }));
    it('DetailGridWrapperComponent should call getDetails and create details grid on initialization', testing_1.async(function () {
        var detailItems = [
            { id: 1 },
            { id: 2 },
            { id: 3 }
        ];
        var inputs = {
            masterItem: masterItem,
            detailGridTemplate: detailsGridTemplate,
            getDetailItems: function () {
                return Observable_1.Observable.of(detailItems);
            }
        };
        detailGridWrapperComponent.masterItem = inputs.masterItem;
        detailGridWrapperComponent.detailGridTemplate = inputs.detailGridTemplate;
        detailGridWrapperComponent.getDetailItems = inputs.getDetailItems;
        var getDetailsSpy = spyOn(detailGridWrapperComponent, 'getDetailItems').and.callThrough();
        detailGridWrapperComponent.ngOnInit();
        fixture.whenStable().then(function () {
            var masterKey = masterItem[gridSettingsService.key];
            expect(getDetailsSpy.calls.count()).toEqual(1);
            expect(getDetailsSpy.calls.first().args[0]).toEqual(inputs.masterItem[gridSettingsService.key]);
            expect(getDetailsSpy.calls.first().args[1]).toEqual(inputs.masterItem);
            fixture.detectChanges();
            var dataCells = fixture.debugElement.queryAll(platform_browser_1.By.css('.mg-data'));
            expect(dataCells.length).toEqual(detailItems.length);
            for (var i = 0; i >= dataCells.length; i++) {
                expect(Number(dataCells[i].nativeElement.textContent)).toEqual(detailItems[i]);
            }
        });
    }));
});
//# sourceMappingURL=detail-grid-wrapper.component.spec.js.map