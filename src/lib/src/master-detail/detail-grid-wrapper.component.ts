import { 
  Component, 
  OnInit,
  Input,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
  ContentChild,
  ContentChildren,
  Optional,
  QueryList
} from '@angular/core';

import { Observable } from 'rxjs';
import { GridSettingsService } from '../grid-settings.service';

@Component({
  selector: 'mg-detail-grid-wrapper',
  templateUrl: './detail-grid-wrapper.component.html',
})
export class DetailGridWrapperComponent implements OnInit {
  @Input() masterItem: any;
  @Input() detailGridTemplate: TemplateRef<any>;
  @Input() getDetailItems: (masterKey: any, masterItem: any) => Observable<any[]>;
  @ViewChild('detailGridPlaceholder', { read: ViewContainerRef }) detailGridPlaceholder: ViewContainerRef;
  
  constructor(
    private settingsService: GridSettingsService) {
  }

  ngOnInit() {
    if (this.getDetailItems != null) {
      this.getDetailItems(this.masterItem[this.settingsService.key], this.masterItem)
        .toPromise()
        .then((details) => {
          this.createDetailGridView(details);
        });
    } else {
      this.createDetailGridView([]);
    }
  }

  private createDetailGridView(items: any[]) {
    this.detailGridPlaceholder.createEmbeddedView(this.detailGridTemplate, { 
      $implicit: { 
        masterKey: this.masterItem[this.settingsService.key], 
        masterItem: this.masterItem,
        items: items
      } 
    });
  }
}