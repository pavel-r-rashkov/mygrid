"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var grid_settings_service_1 = require("../grid-settings.service");
var DetailGridWrapperComponent = (function () {
    function DetailGridWrapperComponent(settingsService) {
        this.settingsService = settingsService;
    }
    DetailGridWrapperComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.getDetailItems != null) {
            this.getDetailItems(this.masterItem[this.settingsService.key], this.masterItem)
                .toPromise()
                .then(function (details) {
                _this.createDetailGridView(details);
            });
        }
        else {
            this.createDetailGridView([]);
        }
    };
    DetailGridWrapperComponent.prototype.createDetailGridView = function (items) {
        this.detailGridPlaceholder.createEmbeddedView(this.detailGridTemplate, {
            $implicit: {
                masterKey: this.masterItem[this.settingsService.key],
                masterItem: this.masterItem,
                items: items
            }
        });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], DetailGridWrapperComponent.prototype, "masterItem", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], DetailGridWrapperComponent.prototype, "detailGridTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], DetailGridWrapperComponent.prototype, "getDetailItems", void 0);
    __decorate([
        core_1.ViewChild('detailGridPlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], DetailGridWrapperComponent.prototype, "detailGridPlaceholder", void 0);
    DetailGridWrapperComponent = __decorate([
        core_1.Component({
            selector: 'mg-detail-grid-wrapper',
            templateUrl: './detail-grid-wrapper.component.html',
        }),
        __metadata("design:paramtypes", [grid_settings_service_1.GridSettingsService])
    ], DetailGridWrapperComponent);
    return DetailGridWrapperComponent;
}());
exports.DetailGridWrapperComponent = DetailGridWrapperComponent;
//# sourceMappingURL=detail-grid-wrapper.component.js.map