import { 
  Component,
  Type,
  OnInit,
  OnDestroy,
  ContentChild,
  ContentChildren,
  ViewChildren,
  QueryList,
  Input,
  Optional,
  TemplateRef,
  ViewContainerRef,
  ElementRef,
  SimpleChanges,
  EventEmitter,
  Output
} from '@angular/core';
import { 
  Observable, 
  Subscription 
} from 'rxjs';

import { GroupDropTargetComponent } from './grouping/group-drop-target.component';
import { TextColumnDirective } from './grid-columns/text-column/text-column.directive';
import { GridColumn } from './grid-columns/grid-column';
import { HeaderWrapperComponent } from './wrappers/header-wrapper.component';
import { FilterStateService } from './filter-state.service';
import { FilterState } from './states/filter-state';
import { OrderStateService } from './order-state.service';
import { OrderState } from './states/order-state';
import { PaginationService } from './pagination/pagination.service';
import { PaginationState } from './pagination/pagination-state';
import { GroupService } from './grouping/group.service';
import { GroupState } from './grouping/group-state';
import { SummaryItemDirective } from './summary/summary-item.directive';
import { ColumnOrderingService } from './column-ordering/column-ordering.service';
import { ColumnReorderEvent } from './column-ordering/column-reorder-event';
import { RowEditionService } from './edition/row-edition.service';
import { RowEditionState } from './edition/row-edition-state';
import { RowDeletionService } from './edition/row-deletion.service';
import { DetailGridDirective } from './master-detail/detail-grid.directive';
import { GridSettingsService } from './grid-settings.service';
import { HtmlTableElementService } from './html-table-elements/html-table-element.service';
import { LifeCycleState } from './states/life-cycle-state';
import { GridState } from './states/grid-state';
import { SummaryState } from './states/summary-state';
import { MasterDetailService } from './master-detail/master-detail.service';
import { DetailsState } from './master-detail/details-state';
import { SelectionService } from './selection/selection.service';
import { SelectionState } from './selection/selection-state';
import { ColumnsService } from './columns.service';
import { GridApiService } from './grid-api.service';
import { HtmlElementType } from './html-table-elements/html-element-type';
import { ScrollMeasureService } from './scroll-measure.service';
import { GuidService } from './guid';
import { SummaryService } from './summary/summary.service';
import { DefaultComparerService } from './grid-columns/shared/default-comparer.service';

@Component({
  selector: 'mg-grid',
  templateUrl: './my-grid.component.html',
  providers: [
    GridSettingsService,
    FilterStateService,
    OrderStateService,
    PaginationService,
    GroupService,
    ColumnOrderingService,
    RowEditionService,
    RowDeletionService,
    HtmlTableElementService,
    MasterDetailService,
    SelectionService,
    ColumnsService,
    GridApiService,
    ScrollMeasureService,
    GuidService,
    SummaryService,
    DefaultComparerService
  ]
})
export class MyGridComponent implements OnInit, OnDestroy {
  private filterStateSubscription: Subscription;
  private orderStateSubscription: Subscription;
  private paginationStateSubscription: Subscription;
  private groupStateSubscription: Subscription;
  private columnOrderingSubscription: Subscription;
  private editionStateSubscription: Subscription;
  private beforeRowDeleteSubscription: Subscription;
  private itemAddedSubscription: Subscription;
  private detailsStateSubscription: Subscription;
  private selectionStateSubscription: Subscription;
  private currentFilterState: FilterState;
  private currentOrderState: OrderState;
  private currentPaginationState: PaginationState;
  private currentGroupState: GroupState;
  private currentEditionState: RowEditionState;
  private currentDetailState: DetailsState;
  private lifeCycleStateOrder: any[] = [];
  private isLoading: boolean = false;
  private serverSideSummaryResults: any[] = [];
  private filteredItems: any[] = [];
  private groupedItems: any[] = [];
  gridItems: any[] = [];
  htmlElementType: typeof HtmlElementType = HtmlElementType;
  scrollWidth: string;
  @Input() key: string;
  @Input() width: string;
  @Input() items: any[] = [];
  @Input() enableServerSideMode: boolean;
  @Input() enableGrouping: boolean = false;
  @Input() showFilterRow: boolean = true;
  @Input() showAddRow: boolean = false;
  @Input() showAddRowToggle: boolean = false;
  @Input() showClearFilterButton: boolean = true;
  @Input() pageSize: number;
  @Input() dropColumnText: string = 'drag and drop columns here to group';
  @Input() removeGroupingButtonText: string = 'Remove grouping';
  @Input() noDataText: string = 'No data to display';
  @Input() paginationTemplate: TemplateRef<any>;
  @Input() showLoadingBar: boolean = false;
  @Input() scrollHeight: string;
  @Input() enableHighlighting: boolean = false;
  @Input() saveChangesCallback: (changes: any) => Observable<any>;
  @Input() deleteItemCallback: (itemKey: any) => Observable<any>;
  @Input() initFormCallback: (editForm: any) => void;
  @Input() getDetailItems: (masterKey: any, masterItem: any) => Observable<any[]>;
  @Input() initCellCallback: (elementData: any, elementRef: ElementRef, elementType: HtmlElementType) => void;
  @Input() getErrorMessages: (errorKeys: any, propertyName: string) => string[];
  @Input() onGridStateChange: (gridState: GridState) => Observable<any>;
  @Output() selectionChanged: EventEmitter<SelectionState> = new EventEmitter<SelectionState>();
  @Output() beforeFiltering: EventEmitter<any[]> = new EventEmitter<any[]>();
  @Output() beforeGrouping: EventEmitter<any[]> = new EventEmitter<any[]>();
  @Output() beforeSorting: EventEmitter<any[]> = new EventEmitter<any[]>();
  @Output() beforePaging: EventEmitter<any[]> = new EventEmitter<any[]>();
  @ContentChild(DetailGridDirective) detailGrid: DetailGridDirective;
  @ContentChildren(GridColumn) columns: QueryList<GridColumn> = new QueryList<GridColumn>();
  @ContentChildren(SummaryItemDirective) summaryItems: QueryList<SummaryItemDirective> = new QueryList<SummaryItemDirective>();;

  constructor(
    private filterStateService: FilterStateService,
    private orderStateService: OrderStateService,
    private paginationService: PaginationService,
    private groupService: GroupService,
    private columnOrderingService: ColumnOrderingService,
    private rowEditionService: RowEditionService,
    private rowDeletionService: RowDeletionService,
    private gridSettingsService: GridSettingsService,
    private tableElementService: HtmlTableElementService,
    private masterDetailService: MasterDetailService,
    private selectionService: SelectionService,
    private columnsService: ColumnsService,
    private gridApi: GridApiService,
    private scrollMeasureService: ScrollMeasureService) {

    this.scrollWidth = (this.scrollMeasureService.measureScrollWidth() - 1).toString() + 'px';
    this.initLyfeCycleStates();
    this.columns = new QueryList<GridColumn>();

    this.filterStateSubscription = this.filterStateService.filterState.subscribe((newState) => {
      this.currentFilterState = newState;
      this.refresh();
    });

    this.orderStateSubscription = this.orderStateService.orderState.subscribe((newState) => {
      this.currentOrderState = newState;
      this.refresh(LifeCycleState.Sorting);
    });

    this.paginationStateSubscription = this.paginationService.paginationState.subscribe((newState) => {
      this.currentPaginationState = newState;
      this.refresh(LifeCycleState.Pagination);
    });

    this.groupStateSubscription = this.groupService.groupState.subscribe((newState) => {
      this.currentGroupState = newState;
      this.refresh();
    });

    this.columnOrderingSubscription = this.columnOrderingService.columnReorder.subscribe((columnReorderEvent: ColumnReorderEvent) => {
      this.reorderColumns(columnReorderEvent);
    });

    this.currentEditionState = this.rowEditionService.rowEdition.getValue();
    this.editionStateSubscription = this.rowEditionService.rowEdition.subscribe((newState) => {
      this.currentEditionState = newState;
    });

    this.beforeRowDeleteSubscription = this.rowDeletionService.beforeRowDelete.subscribe((deletedRowKey) => {
      this.deleteRow(deletedRowKey);
    });

    this.itemAddedSubscription = this.rowEditionService.itemAdded.subscribe((newItem) => {
      this.addItem(newItem);
    });

    this.detailsStateSubscription = this.masterDetailService.detailsStateSubject.subscribe((newState) => {
      this.currentDetailState = newState;
    });

    this.selectionStateSubscription = this.selectionService.selectionSubject.subscribe((newState: SelectionState) => {
      this.selectionChanged.next(newState);
    });
  }

  public get api(): GridApiService {
    return this.gridApi;
  }
  
  ngOnInit() {
    this.gridSettingsService.key = this.key;
    this.gridSettingsService.enableServerSideMode = this.enableServerSideMode == null ? false : this.enableServerSideMode;
    this.rowEditionService.saveChangesCallback = this.saveChangesCallback;
    this.rowEditionService.initFormCallback = this.initFormCallback;
    this.rowEditionService.getErrorMessages = this.getErrorMessages;
    this.tableElementService.initCellCallback = this.initCellCallback;
    this.paginationService.initialPageSize = this.pageSize;
    this.currentPaginationState = this.pageSize != null ? new PaginationState(1, this.pageSize) : null;
    this.createApi();
  }

  ngAfterContentInit() {
    this.columnsService.columns = this.columns.toArray();
    this.refresh();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes != null 
      && changes['pageSize'] != null 
      && !changes['pageSize'].isFirstChange()) {

      this.paginationService.paginationState.next(new PaginationState(this.currentPaginationState.currentPage, this.pageSize));
    }
  }

  ngOnDestroy() {
    this.filterStateSubscription.unsubscribe();
    this.orderStateSubscription.unsubscribe();
    this.paginationStateSubscription.unsubscribe();
    this.groupStateSubscription.unsubscribe();
    this.columnOrderingSubscription.unsubscribe();
    this.editionStateSubscription.unsubscribe();
    this.beforeRowDeleteSubscription.unsubscribe();
    this.itemAddedSubscription.unsubscribe();
    this.detailsStateSubscription.unsubscribe();
    this.selectionStateSubscription.unsubscribe();
  }

  getItemRows(item: any): any[] {
    if (this.shouldRenderGroupedLayout()) {
      return item[this.groupService.GROUPED_ITEMS_KEY];
    }

    return [item];
  }

  shouldRenderGroupedLayout() {
    return this.currentGroupState != null;
  }

  getVisibleIndex(gridItemIndex: number, rowIndex: number): number {
    if (this.shouldRenderGroupedLayout()) {
      let previousGroupsRowCount = 0;
      for (var i = 0; i < gridItemIndex; i++) {
        previousGroupsRowCount += this.gridItems[i][this.groupService.GROUPED_ITEMS_KEY].length;
      }

      return previousGroupsRowCount + rowIndex + 1;
    } else {
      return gridItemIndex + 1;
    }
  }

  toggleGroup(group: any) {
    group.isOpened = !group.isOpened;
  }

  shouldRenderColumn(column: GridColumn) {
    let isColumnGrouped = this.currentGroupState != null && this.currentGroupState.columnId == column.columnId;
    return column.isVisible && !isColumnGrouped;
  }

  getRenderableColumns(): GridColumn[] {
    let renderableColumns = this.columns.filter((column) => {
      return this.shouldRenderColumn(column);
    });

    return renderableColumns;
  }

  getRenderableColumnCount() {
    return this.getRenderableColumns().length;
  }

  isRowEdited(item: any) {
    let itemKey = item[this.key];
    let isRowEdited = this.currentEditionState.currentlyEditedRows.indexOf(itemKey) >= 0;
    return isRowEdited;
  }

  getSummaryItems(propertyName: string) {
    let summaryItems: any[] = [];

    this.summaryItems.forEach((summaryItem: SummaryItemDirective) => {
      if (summaryItem.propertyName != propertyName) {
        return;
      }

      var serverSideResults = this.serverSideSummaryResults.filter((serverSideSummaryResult) => {
        return serverSideSummaryResult.propertyName == propertyName 
          && serverSideSummaryResult.summaryType == summaryItem.summaryType;
      });

      var serverSideSummary = null;
      if (serverSideResults.length > 0) {
        serverSideSummary = serverSideResults[0].summaryValue;
      }

      summaryItems.push({
        serverSideSummary: serverSideSummary,
        summaryItem: summaryItem
      });
    });

    return summaryItems;
  }

  getItemsToSummarize(isPerPageSummary: boolean) {
    if (this.gridSettingsService.enableServerSideMode && !isPerPageSummary) {
       return [];
    }

    if (this.currentGroupState == null) {
      return isPerPageSummary ? this.gridItems : this.groupedItems;
    } else {
      let itemsToSummarize: any[] = [];
      let sourceItems = isPerPageSummary ? this.gridItems : this.groupedItems;
      for (var i = 0; i < sourceItems.length; ++i) {
        itemsToSummarize.push.apply(itemsToSummarize, sourceItems[i][this.groupService.GROUPED_ITEMS_KEY]);
      }
      return itemsToSummarize;    
    }
  }

  shouldRenderDetailGrid(item: any) {
    let key = item[this.gridSettingsService.key];
    return this.currentDetailState.openedDetails.indexOf(key) >= 0;
  }

  clearFilters() {
    this.filterStateService.filterState.next(null);
  }

  toggleAddRow() {
    this.showAddRow = !this.showAddRow;
  }

  getGroupedColumnData() {
    let groupedColumn = this.columnsService.columns.find((col) => {
      return col.columnId == this.currentGroupState.columnId;
    });

    return groupedColumn.getGroupData();
  }

  getGroupedColumnDataComponentType(): Type<any> {
    let groupedColumn = this.columnsService.columns.find((col) => {
      return col.columnId == this.currentGroupState.columnId;
    });

    return groupedColumn.dataComponentType;
  }

  private refresh(startState: LifeCycleState = LifeCycleState.Filtering) {
    if (this.gridSettingsService.enableServerSideMode) {
      let summaryStates: SummaryState[] = [];
      let globalSummaryItems = this.summaryItems.filter((summaryItem) => {
        return !summaryItem.isPerPageSummary;
      }).forEach((summaryItem) => {
        summaryStates.push(new SummaryState(summaryItem.propertyName, summaryItem.summaryType));
      });

      let gridState = new GridState(
        this.currentFilterState,
        this.currentGroupState,
        this.currentOrderState,
        this.currentPaginationState,
        summaryStates
      );

      this.isLoading = true;
      this.onGridStateChange(gridState)
        .toPromise()
        .then((result: any) => {
          this.gridItems = result.items;
          if (this.pageSize != null) {
            this.paginationService.itemsCount.next(result.count);
          }

          this.serverSideSummaryResults = result.summaryResults || [];
          this.isLoading = false;
        })
        .catch((err) => {
          console.error(err);
          this.isLoading = false;
        });
    }
    else {
      for (var i = startState; i < this.lifeCycleStateOrder.length; ++i) {
        this.lifeCycleStateOrder[i]();
      }
    }
  }

  private getFilteredItems(items: any[]): any[] {
    if (this.currentFilterState == null) {
      return items;
    }

    let filteredItems = this.filterStateService.filterItems(items, this.currentFilterState, this.columns.toArray());
    return filteredItems;
  }

  private sortItems(itemsToSort: any[]) {
    if (this.currentOrderState == null) {
      return;
    }

    if (this.currentGroupState != null && this.currentOrderState.columnId == this.currentGroupState.columnId) {
      return;
    }

    this.orderStateService.sortItems(
      itemsToSort, 
      this.currentOrderState, 
      this.currentGroupState, 
      this.columns.toArray());
  }

  private groupItems(itemsToGroup: any[]): any[] {
    if (this.currentGroupState != null) {
      return this.groupService.groupItems(itemsToGroup, this.currentGroupState, this.columnsService.columns);
    }

    return itemsToGroup;
  }

  private applyPagination(itemsToPage: any[]) {
    if (this.currentPaginationState == null) {
      return itemsToPage;
    } else {
      return this.paginationService.applyPagination(itemsToPage, this.currentPaginationState);
    }
  }

  private initLyfeCycleStates() {
    this.lifeCycleStateOrder[LifeCycleState.Filtering] = () => {
		console.log('filtering');
      this.beforeFiltering.next(this.items);
      this.filteredItems = this.getFilteredItems(this.items);
    };

    this.lifeCycleStateOrder[LifeCycleState.Grouping] = () => {
      this.beforeGrouping.next(this.filteredItems);
      this.groupedItems = this.groupItems(this.filteredItems);
      this.paginationService.itemsCount.next(this.groupedItems.length);
    };

    this.lifeCycleStateOrder[LifeCycleState.Sorting] = () => {
      this.beforeSorting.next(this.groupedItems);
      this.sortItems(this.groupedItems);
    };

    this.lifeCycleStateOrder[LifeCycleState.Pagination] = () => {
      this.beforePaging.next(this.groupedItems);
      this.gridItems = this.applyPagination(this.groupedItems);
    };
  }

  private reorderColumns(columnReorderEvent: ColumnReorderEvent) {
    let columnsToReorder = this.columns.toArray();
    this.columnOrderingService.reorderColumns(columnReorderEvent, columnsToReorder);
    this.columns.reset(columnsToReorder);

    if (this.currentGroupState != null 
      && columnReorderEvent.movedColumn.columnId == this.currentGroupState.columnId) {
      // while single grouped column is supported
      this.groupService.groupState.next(null);
    }
  }

  private addItem(newItem: any) {
    this.items.push(newItem);
    this.refresh();
  }

  private removeRow(rowKey: any) {
    let itemsLength = this.items.length;
      for (var i = 0; i < itemsLength; ++i) {
        if (this.items[i][this.key] == rowKey) {
          this.items.splice(i, 1);
          break;
        }
      }
  }

  private deleteRow(deletedRowKey: any) {
    if (this.gridSettingsService.enableServerSideMode) {
      this.deleteItemCallback(deletedRowKey)
        .toPromise()
        .then(() => {
          this.refresh();
        })
        .catch((error) => {
          console.error('Error deleting item', error);
        });
    } else {
      if (this.deleteItemCallback != null) {
        this.deleteItemCallback(deletedRowKey)
          .toPromise()
          .then(() => {
            this.removeRow(deletedRowKey);
            this.refresh();
          })
          .catch((error) => {
            console.error('Error deleting item', error);
          });
      } else {
        this.removeRow(deletedRowKey);
        this.refresh();
      }
    }
  }

  private createApi() {
    this.gridApi.refresh = this.refresh.bind(this);
  }
}