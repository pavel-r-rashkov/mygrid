import { 
  Injectable, 
  Renderer2,
  ElementRef,
  OnDestroy,
  Inject
} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Injectable()
export class ScrollMeasureService implements OnDestroy {
  private scrollCache: number;

  constructor(
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: any) {
  }

  public measureScrollWidth(): number {
    if (this.scrollCache == null) {
      var scrollDiv = this.renderer.createElement('div');
      this.renderer.addClass(scrollDiv, 'scrollbar-measure');
      this.renderer.appendChild(this.document.body, scrollDiv);
      this.scrollCache = scrollDiv.offsetWidth - scrollDiv.clientWidth;
      this.renderer.removeChild(this.document.body, scrollDiv);
    }

    return this.scrollCache; 
  }

  ngOnDestroy() {
    this.renderer.destroy();
  }
}