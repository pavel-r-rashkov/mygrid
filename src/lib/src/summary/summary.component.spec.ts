import { 
  Component,
  ViewChild,
  TemplateRef
} from '@angular/core';
import {
  ComponentFixture,
  TestBed,
  async
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { SummaryComponent } from './summary.component';
import { SummaryService } from './summary.service';

@Component({
  selector: 'mgt-summary-template',
  template: `
    <ng-template #summaryTemplate let-context>
      <div id="custom-summary-template"></div>
    </ng-template>
  `
})
class SummaryTemplateComponent {
  @ViewChild('summaryTemplate') summaryTemplate: TemplateRef<any>;
}

describe('Component: SummaryComponent', () => {
  let summaryComponent: SummaryComponent;
  let fixture: ComponentFixture<SummaryComponent>;
  let summaryService: SummaryService;
  let summaryServiceSpy: jasmine.Spy;
  let averageMock: number = 10;
  let summaryTemplate: TemplateRef<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [
          SummaryComponent,
          SummaryTemplateComponent
        ],
        providers: [
          SummaryService
        ]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SummaryComponent);
        summaryComponent = fixture.componentInstance;

        summaryService = fixture.debugElement.injector.get(SummaryService);
        summaryServiceSpy = spyOn(summaryService, 'average').and.returnValue(averageMock);

        summaryTemplate = TestBed
          .createComponent(SummaryTemplateComponent)
          .componentInstance
          .summaryTemplate;
      });
  }));

  it('SummaryComponent should recalculate summary value on input changes', () => {
    summaryComponent.items = [];
    summaryComponent.summaryType = 'avg';

    summaryComponent.ngOnChanges({});

    expect(summaryComponent.summaryValue).toEqual(averageMock);
  });

  it('SummaryComponent should use server-side summary value if it exists', () => {
    summaryComponent.items = [];
    summaryComponent.summaryType = 'avg';
    let serverSideSummary: number = 20;
    summaryComponent.serverSideSummary = serverSideSummary;

    summaryComponent.ngOnChanges({});

    expect(summaryComponent.summaryValue).toEqual(serverSideSummary);
  });

  it('isSummaryValueDate should return true when summary value is a date object', () => {
    summaryComponent.items = [
      { birthdate: new Date() }
    ];
    summaryComponent.propertyName = 'birthdate';
    summaryComponent.summaryType = 'min';

    summaryComponent.ngOnChanges({});
    
    expect(summaryComponent.isSummaryValueDate()).toEqual(true);
  });

  it('isSummaryValueDate should return false when summary value is not a date object', () => {
    summaryComponent.items = [
      { age: 20 }
    ];
    summaryComponent.propertyName = 'age';
    summaryComponent.summaryType = 'min';

    summaryComponent.ngOnChanges({});
    
    expect(summaryComponent.isSummaryValueDate()).toEqual(false);
  });

  it('customCalculateSummary should be used when it exists', () => {
    summaryComponent.items = [];
    summaryComponent.summaryType = 'avg';
    var customSummaryValue: number = 30;
    summaryComponent.customCalculateSummary = (items: any[]) => {
      return customSummaryValue;
    };

    summaryComponent.ngOnChanges({});

    expect(summaryComponent.summaryValue).toEqual(customSummaryValue);
  });

  it('customTemplate should be rendered when it exists', () => {
    summaryComponent.items = [];
    summaryComponent.summaryType = 'avg';
    summaryComponent.customTemplate = summaryTemplate;

    summaryComponent.ngOnChanges({});

    var customTemplateElement = fixture.debugElement.query(By.css('#custom-summary-template'));
    expect(customTemplateElement).toBeTruthy();
  });
});