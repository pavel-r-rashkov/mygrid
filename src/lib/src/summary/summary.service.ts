import { Injectable } from '@angular/core';

@Injectable()
export class SummaryService {
  public average(items: any[], propertyName: string): any {
    var nonNullItemsCount = 0;
    var sum = items.reduce((prev, current, index, items: any[]) => {
      if (!current[propertyName]) {
        return prev;
      }
      nonNullItemsCount++;

      if (prev == null) {
        prev = 0;
      }

      return prev + current[propertyName];
    }, null);

    if (sum != null) {
      return sum / nonNullItemsCount;
    } else {
      return null;
    }
  }

  public min(items: any[], propertyName: string): any {
    return items.reduce((prev, current, index, items: any[]) => {
      if (current[propertyName] == null) {
        return prev;
      }

      return (current[propertyName] < prev || prev == null) ? current[propertyName] : prev;
    }, null);
  }

  public max(items: any[], propertyName: string): any {
    return items.reduce((prev, current, index, items: any[]) => {
      if (current[propertyName] == null) {
        return prev;
      }

      return (current[propertyName] > prev || prev == null) ? current[propertyName] : prev;
    }, null);
  }

  public count(items: any[], propertyName: string): any {
    return items.filter((item) => {
      return !!item[propertyName];
    }).length;
  }

  public sum(items: any[], propertyName: string): any {
    return items.reduce((prev, current, index, items: any[]) => {
      if (!current[propertyName]) {
        return prev;
      }

      if (prev == null) {
        prev = 0;
      }

      return prev + Number(current[propertyName]);
    }, null);
  }
}