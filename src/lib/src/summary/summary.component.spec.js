"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var summary_component_1 = require("./summary.component");
var summary_service_1 = require("./summary.service");
var SummaryTemplateComponent = (function () {
    function SummaryTemplateComponent() {
    }
    __decorate([
        core_1.ViewChild('summaryTemplate'),
        __metadata("design:type", core_1.TemplateRef)
    ], SummaryTemplateComponent.prototype, "summaryTemplate", void 0);
    SummaryTemplateComponent = __decorate([
        core_1.Component({
            selector: 'mgt-summary-template',
            template: "\n    <ng-template #summaryTemplate let-context>\n      <div id=\"custom-summary-template\"></div>\n    </ng-template>\n  "
        })
    ], SummaryTemplateComponent);
    return SummaryTemplateComponent;
}());
describe('Component: SummaryComponent', function () {
    var summaryComponent;
    var fixture;
    var summaryService;
    var summaryServiceSpy;
    var averageMock = 10;
    var summaryTemplate;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                summary_component_1.SummaryComponent,
                SummaryTemplateComponent
            ],
            providers: [
                summary_service_1.SummaryService
            ]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(summary_component_1.SummaryComponent);
            summaryComponent = fixture.componentInstance;
            summaryService = fixture.debugElement.injector.get(summary_service_1.SummaryService);
            summaryServiceSpy = spyOn(summaryService, 'average').and.returnValue(averageMock);
            summaryTemplate = testing_1.TestBed
                .createComponent(SummaryTemplateComponent)
                .componentInstance
                .summaryTemplate;
        });
    }));
    it('SummaryComponent should recalculate summary value on input changes', function () {
        summaryComponent.items = [];
        summaryComponent.summaryType = 'avg';
        summaryComponent.ngOnChanges({});
        expect(summaryComponent.summaryValue).toEqual(averageMock);
    });
    it('SummaryComponent should use server-side summary value if it exists', function () {
        summaryComponent.items = [];
        summaryComponent.summaryType = 'avg';
        var serverSideSummary = 20;
        summaryComponent.serverSideSummary = serverSideSummary;
        summaryComponent.ngOnChanges({});
        expect(summaryComponent.summaryValue).toEqual(serverSideSummary);
    });
    it('isSummaryValueDate should return true when summary value is a date object', function () {
        summaryComponent.items = [
            { birthdate: new Date() }
        ];
        summaryComponent.propertyName = 'birthdate';
        summaryComponent.summaryType = 'min';
        summaryComponent.ngOnChanges({});
        expect(summaryComponent.isSummaryValueDate()).toEqual(true);
    });
    it('isSummaryValueDate should return false when summary value is not a date object', function () {
        summaryComponent.items = [
            { age: 20 }
        ];
        summaryComponent.propertyName = 'age';
        summaryComponent.summaryType = 'min';
        summaryComponent.ngOnChanges({});
        expect(summaryComponent.isSummaryValueDate()).toEqual(false);
    });
    it('customCalculateSummary should be used when it exists', function () {
        summaryComponent.items = [];
        summaryComponent.summaryType = 'avg';
        var customSummaryValue = 30;
        summaryComponent.customCalculateSummary = function (items) {
            return customSummaryValue;
        };
        summaryComponent.ngOnChanges({});
        expect(summaryComponent.summaryValue).toEqual(customSummaryValue);
    });
    it('customTemplate should be rendered when it exists', function () {
        summaryComponent.items = [];
        summaryComponent.summaryType = 'avg';
        summaryComponent.customTemplate = summaryTemplate;
        summaryComponent.ngOnChanges({});
        var customTemplateElement = fixture.debugElement.query(platform_browser_1.By.css('#custom-summary-template'));
        expect(customTemplateElement).toBeTruthy();
    });
});
//# sourceMappingURL=summary.component.spec.js.map