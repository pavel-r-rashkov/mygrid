"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SummaryService = (function () {
    function SummaryService() {
    }
    SummaryService.prototype.average = function (items, propertyName) {
        var nonNullItemsCount = 0;
        var sum = items.reduce(function (prev, current, index, items) {
            if (!current[propertyName]) {
                return prev;
            }
            nonNullItemsCount++;
            if (prev == null) {
                prev = 0;
            }
            return prev + current[propertyName];
        }, null);
        if (sum != null) {
            return sum / nonNullItemsCount;
        }
        else {
            return null;
        }
    };
    SummaryService.prototype.min = function (items, propertyName) {
        return items.reduce(function (prev, current, index, items) {
            if (current[propertyName] == null) {
                return prev;
            }
            return (current[propertyName] < prev || prev == null) ? current[propertyName] : prev;
        }, null);
    };
    SummaryService.prototype.max = function (items, propertyName) {
        return items.reduce(function (prev, current, index, items) {
            if (current[propertyName] == null) {
                return prev;
            }
            return (current[propertyName] > prev || prev == null) ? current[propertyName] : prev;
        }, null);
    };
    SummaryService.prototype.count = function (items, propertyName) {
        return items.filter(function (item) {
            return !!item[propertyName];
        }).length;
    };
    SummaryService.prototype.sum = function (items, propertyName) {
        return items.reduce(function (prev, current, index, items) {
            if (!current[propertyName]) {
                return prev;
            }
            if (prev == null) {
                prev = 0;
            }
            return prev + Number(current[propertyName]);
        }, null);
    };
    SummaryService = __decorate([
        core_1.Injectable()
    ], SummaryService);
    return SummaryService;
}());
exports.SummaryService = SummaryService;
//# sourceMappingURL=summary.service.js.map