import { 
  Component, 
  Input,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
  SimpleChanges,
  OnChanges
} from '@angular/core';

import { SummaryService } from './summary.service';

@Component({
  selector: 'mg-summary',
  templateUrl: './summary.component.html'
})
export class SummaryComponent implements OnChanges {
  @Input() items: any[];
  @Input() customTemplate: TemplateRef<any>;
  @Input() summaryType: string;
  @Input() propertyName: string;
  @Input() customCalculateSummary: (items: any[]) => any;
  @Input() serverSideSummary: any;
  @ViewChild('customTemplatePlaceholder', { read: ViewContainerRef }) containerRef: ViewContainerRef;
  summaryValue: any = null;

  constructor(
    private summaryService: SummaryService) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes != null) {
      this.initSummary();
    }
  }

  isSummaryValueDate(): boolean {
    return this.summaryValue instanceof Date;
  } 

  private initSummary(): void {
    this.summaryValue = this.serverSideSummary == null ? this.calculateDisplaySummary() : this.serverSideSummary;

    if (this.customTemplate != null) {
      let templateContext = {
        summaryType: this.summaryType,
        summaryValue: this.summaryValue
      };

      this.containerRef.createEmbeddedView(this.customTemplate, { $implicit: templateContext });
    } 
  }

  private calculateDisplaySummary(): void {
    let summary = null
    if (this.customCalculateSummary == null) {
      switch (this.summaryType.toLowerCase()) {
        case 'avg':
          summary = this.summaryService.average(this.items, this.propertyName);
          break;
        case 'min':
          summary = this.summaryService.min(this.items, this.propertyName);
          break;
        case 'max':
          summary = this.summaryService.max(this.items, this.propertyName);
          break;
        case 'count':
          summary = this.summaryService.count(this.items, this.propertyName);
          break;
        case 'sum':
          summary = this.summaryService.sum(this.items, this.propertyName);
          break;
        default:
          throw new Error('invalid summary type');
      }
    } else {
      summary = this.customCalculateSummary(this.items);
    }
    return summary;
  }
}