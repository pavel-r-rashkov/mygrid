import { 
  Directive, 
  OnInit,
  Input,
  TemplateRef
} from '@angular/core';

@Directive({
  selector: 'mg-summary-item',
})
export class SummaryItemDirective {
  @Input() summaryType: string;
  @Input() propertyName: string;
  @Input() customCalculateSummary: (items: any[]) => any;
  @Input() customTemplate: TemplateRef<any>;
  @Input() isPerPageSummary: boolean = true;
}