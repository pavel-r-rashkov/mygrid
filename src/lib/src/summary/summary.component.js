"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var summary_service_1 = require("./summary.service");
var SummaryComponent = (function () {
    function SummaryComponent(summaryService) {
        this.summaryService = summaryService;
        this.summaryValue = null;
    }
    SummaryComponent.prototype.ngOnChanges = function (changes) {
        if (changes != null) {
            this.initSummary();
        }
    };
    SummaryComponent.prototype.isSummaryValueDate = function () {
        return this.summaryValue instanceof Date;
    };
    SummaryComponent.prototype.initSummary = function () {
        this.summaryValue = this.serverSideSummary == null ? this.calculateDisplaySummary() : this.serverSideSummary;
        if (this.customTemplate != null) {
            var templateContext = {
                summaryType: this.summaryType,
                summaryValue: this.summaryValue
            };
            this.containerRef.createEmbeddedView(this.customTemplate, { $implicit: templateContext });
        }
    };
    SummaryComponent.prototype.calculateDisplaySummary = function () {
        var summary = null;
        if (this.customCalculateSummary == null) {
            switch (this.summaryType.toLowerCase()) {
                case 'avg':
                    summary = this.summaryService.average(this.items, this.propertyName);
                    break;
                case 'min':
                    summary = this.summaryService.min(this.items, this.propertyName);
                    break;
                case 'max':
                    summary = this.summaryService.max(this.items, this.propertyName);
                    break;
                case 'count':
                    summary = this.summaryService.count(this.items, this.propertyName);
                    break;
                case 'sum':
                    summary = this.summaryService.sum(this.items, this.propertyName);
                    break;
                default:
                    throw new Error('invalid summary type');
            }
        }
        else {
            summary = this.customCalculateSummary(this.items);
        }
        return summary;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], SummaryComponent.prototype, "items", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], SummaryComponent.prototype, "customTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], SummaryComponent.prototype, "summaryType", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], SummaryComponent.prototype, "propertyName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], SummaryComponent.prototype, "customCalculateSummary", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], SummaryComponent.prototype, "serverSideSummary", void 0);
    __decorate([
        core_1.ViewChild('customTemplatePlaceholder', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], SummaryComponent.prototype, "containerRef", void 0);
    SummaryComponent = __decorate([
        core_1.Component({
            selector: 'mg-summary',
            templateUrl: './summary.component.html'
        }),
        __metadata("design:paramtypes", [summary_service_1.SummaryService])
    ], SummaryComponent);
    return SummaryComponent;
}());
exports.SummaryComponent = SummaryComponent;
//# sourceMappingURL=summary.component.js.map