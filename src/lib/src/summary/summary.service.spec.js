"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var summary_service_1 = require("./summary.service");
describe('Service: SummaryService', function () {
    var summaryService;
    beforeEach(function () {
        summaryService = new summary_service_1.SummaryService();
    });
    it('average with non null values', function () {
        var items = [
            { id: 1, age: 20 },
            { id: 2, age: 25 },
            { id: 3, age: 22 },
            { id: 4, age: 32 },
            { id: 5, age: 37 },
            { id: 6, age: 45 },
            { id: 7, age: 21 },
            { id: 8, age: 33 }
        ];
        var averageAge = summaryService.average(items, 'age');
        expect(averageAge).toEqual(29.375);
    });
    it('average with null and undefined values should ignore null/undefined values', function () {
        var items = [
            { id: 1, age: 20 },
            { id: 2, age: 25 },
            { id: 3 },
            { id: 4, age: 32 },
            { id: 5, age: 37 },
            { id: 6, age: null },
            { id: 7, age: 21 },
            { id: 8, age: 33 }
        ];
        var averageAge = summaryService.average(items, 'age');
        expect(averageAge).toEqual(28);
    });
    it('average with all values equal to null or undefined should return null', function () {
        var items = [
            { id: 1 },
            { id: 2, age: null },
            { id: 3 },
        ];
        var averageAge = summaryService.average(items, 'age');
        expect(averageAge).toBeNull();
    });
    it('average with no items should return null', function () {
        var items = [];
        var averageAge = summaryService.average(items, 'age');
        expect(averageAge).toBeNull();
    });
    it('min with non null values', function () {
        var items = [
            { id: 1, age: 20 },
            { id: 2, age: 25 },
            { id: 3, age: 22 },
            { id: 4, age: 32 },
            { id: 5, age: 37 },
            { id: 6, age: 45 },
            { id: 7, age: 21 },
            { id: 8, age: 33 }
        ];
        var minAge = summaryService.min(items, 'age');
        expect(minAge).toEqual(20);
    });
    it('min with null and undefined values should ignore null/undefined values', function () {
        var items = [
            { id: 1, age: 20 },
            { id: 2, age: 25 },
            { id: 3 },
            { id: 4, age: 32 },
            { id: 5, age: 37 },
            { id: 6, age: null },
            { id: 7, age: 21 },
            { id: 8, age: 33 }
        ];
        var minAge = summaryService.min(items, 'age');
        expect(minAge).toEqual(20);
    });
    it('min with all values equal to null or undefined should return null', function () {
        var items = [
            { id: 1 },
            { id: 2, age: null },
            { id: 3 },
        ];
        var minAge = summaryService.min(items, 'age');
        expect(minAge).toBeNull();
    });
    it('min with no items should return null', function () {
        var items = [];
        var minAge = summaryService.min(items, 'age');
        expect(minAge).toBeNull();
    });
    it('max with non null values', function () {
        var items = [
            { id: 1, age: 20 },
            { id: 2, age: 25 },
            { id: 3, age: 22 },
            { id: 4, age: 32 },
            { id: 5, age: 37 },
            { id: 6, age: 45 },
            { id: 7, age: 21 },
            { id: 8, age: 33 }
        ];
        var maxAge = summaryService.max(items, 'age');
        expect(maxAge).toEqual(45);
    });
    it('max with null and undefined values should ignore null/undefined values', function () {
        var items = [
            { id: 1, age: 20 },
            { id: 2, age: 25 },
            { id: 3 },
            { id: 4, age: 32 },
            { id: 5, age: 37 },
            { id: 6, age: null },
            { id: 7, age: 21 },
            { id: 8, age: 33 }
        ];
        var maxAge = summaryService.max(items, 'age');
        expect(maxAge).toEqual(37);
    });
    it('max with all values equal to null or undefined should return null', function () {
        var items = [
            { id: 1 },
            { id: 2, age: null },
            { id: 3 },
        ];
        var maxAge = summaryService.max(items, 'age');
        expect(maxAge).toBeNull();
    });
    it('max with no items should return null', function () {
        var items = [];
        var maxAge = summaryService.max(items, 'age');
        expect(maxAge).toBeNull();
    });
    it('sum with non null values', function () {
        var items = [
            { id: 1, age: 20 },
            { id: 2, age: 25 },
            { id: 3, age: 22 },
            { id: 4, age: 32 },
            { id: 5, age: 37 },
            { id: 6, age: 45 },
            { id: 7, age: 21 },
            { id: 8, age: 33 }
        ];
        var ageSum = summaryService.sum(items, 'age');
        expect(ageSum).toEqual(235);
    });
    it('sum with null and undefined values should ignore null/undefined values', function () {
        var items = [
            { id: 1, age: 20 },
            { id: 2, age: 25 },
            { id: 3 },
            { id: 4, age: 32 },
            { id: 5, age: 37 },
            { id: 6, age: null },
            { id: 7, age: 21 },
            { id: 8, age: 33 }
        ];
        var ageSum = summaryService.sum(items, 'age');
        expect(ageSum).toEqual(168);
    });
    it('sum with all values equal to null or undefined should return null', function () {
        var items = [
            { id: 1 },
            { id: 2, age: null },
            { id: 3 },
        ];
        var ageSum = summaryService.sum(items, 'age');
        expect(ageSum).toBeNull();
    });
    it('sum with no items should return null', function () {
        var items = [];
        var ageSum = summaryService.sum(items, 'age');
        expect(ageSum).toBeNull();
    });
    it('count with non null values', function () {
        var items = [
            { id: 1, age: 20 },
            { id: 2, age: 25 },
            { id: 3, age: 22 },
            { id: 4, age: 32 },
            { id: 5, age: 37 },
            { id: 6, age: 45 },
            { id: 7, age: 21 },
            { id: 8, age: 33 }
        ];
        var ageCount = summaryService.count(items, 'age');
        expect(ageCount).toEqual(8);
    });
    it('count with null and undefined values should ignore null/undefined values', function () {
        var items = [
            { id: 1, age: 20 },
            { id: 2, age: 25 },
            { id: 3 },
            { id: 4, age: 32 },
            { id: 5, age: 37 },
            { id: 6, age: null },
            { id: 7, age: 21 },
            { id: 8, age: 33 }
        ];
        var ageCount = summaryService.count(items, 'age');
        expect(ageCount).toEqual(6);
    });
    it('count with all values equal to null or undefined should return 0', function () {
        var items = [
            { id: 1 },
            { id: 2, age: null },
            { id: 3 },
        ];
        var ageCount = summaryService.count(items, 'age');
        expect(ageCount).toEqual(0);
    });
    it('count with no items should return 0', function () {
        var items = [];
        var ageCount = summaryService.count(items, 'age');
        expect(ageCount).toEqual(0);
    });
});
//# sourceMappingURL=summary.service.spec.js.map