"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
var my_grid_1 = require("my-grid");
var my_grid_2 = require("my-grid");
var AppComponent = (function () {
    function AppComponent(httpClient) {
        var _this = this;
        this.title = 'app works!';
        this.gridKey = 'Id';
        this.firstCaption = 'Some caption';
        this.data = [];
        this.comboBoxItems = [];
        this.enableServerSideMode = false;
        this.pageSize = 10;
        this.boolTestItems = [{ Id: true, Name: 'T' }, { Id: false, Name: 'F' }];
        this.guidTestItems = [{ Id: 'c9312f11-c826-47a2-8806-ab209e9d6098', Name: '1' }, { Id: '510fff7d-c108-4dce-8db2-5929febfd19e', Name: '2' }];
        this.charTestItems = [{ Id: 'Aaa', Name: 'aaa' }, { Id: 'Bbb', Name: 'bbb' }];
        this.decimalTestItems = [{ Id: 5.23, Name: '5.23' }, { Id: 3.4, Name: '3.4' }];
        this.someProp = 'IDJEAI';
        this.summaryType = false;
        this.testSearchTerm = my_grid_2.TextSearchTermType;
        this.onGridStateChange = function (state) {
            console.log('STATE', state);
            return _this.httpClient
                .post('http://localhost:57022/api/users', state)
                .map(function (res) { return res.json(); });
        };
        this.httpClient = httpClient;
        this.initCell = function (context, elementRef, type) {
        };
    }
    AppComponent.prototype.beforeFiltering = function (items) {
        console.log('Before filtering', items);
    };
    AppComponent.prototype.ngOnInit = function () {
        this.data = [
            { Id: 1, ComboProp: 1, TextPro: 'abcd', MarkTest: true, detailItems: [{ id: 1, firstName: 'Asdf', lastName: 'Aas' }, { id: 2, firstName: 'Bjd', lastName: 'Bjop' }, { id: 3, firstName: 'Cqdo', lastName: 'Cda' }] },
            { Id: 2, ComboProp: 5, TextPro: 'jfri', MarkTest: null, DateTest: new Date(2017, 1, 6, 1, 5, 2, 33) },
            { Id: 3, ComboProp: 5, TextPro: 'abcd', MarkTest: false, DateTest: new Date(2013, 1, 6, 4, 6, 3, 98) },
            { Id: 4, ComboProp: 5, TextPro: 'dddda', MarkTest: false, age: 23 },
            { Id: 5, ComboProp: 5, TextPro: 'aad', MarkTest: true },
            { Id: 6, ComboProp: 5, TextPro: 'wueu', MarkTest: false },
            { Id: 7, ComboProp: 5, TextPro: 'jfri', MarkTest: false },
            { Id: 8, ComboProp: 5, TextPro: 'add', MarkTest: true },
            { Id: 9, ComboProp: 5, TextPro: 'dddda', MarkTest: null },
            { Id: 10, ComboProp: 5, TextPro: 'dqq', MarkTest: false },
            { Id: 11, ComboProp: 5, TextPro: 'ohcu', MarkTest: true },
            { Id: 12, ComboProp: 5, TextPro: 'ncj', MarkTest: null },
            { Id: 13, ComboProp: 5, TextPro: 'dqq', MarkTest: true },
            { Id: 14, ComboProp: 5, TextPro: 'abcd', MarkTest: false },
            { Id: 15, ComboProp: 5, TextPro: 'ncj', MarkTest: true, age: 7 },
            { Id: 16, ComboProp: 5, TextPro: 'dqq', MarkTest: true },
            { Id: 17, ComboProp: 5, TextPro: 'ohcu', MarkTest: false },
        ];
        this.comboBoxItems = [
            // { Id: 1, Name: '1_Name' },
            // { Id: 5, Name: '5_Name' }
            { Id: 0, Name: 'F0' },
            { Id: 1, Name: 'F1' },
            { Id: 2, Name: 'F2' },
            { Id: 3, Name: 'F3' }
        ];
    };
    AppComponent.prototype.ngAfterViewInit = function () {
    };
    AppComponent.prototype.change = function () {
        this.someProp = 'CHANGED';
    };
    // onGridStateChange(state) {
    //   console.log('STATE', state, this.httpClient, this);
    //   return this.httpClient
    //     .get('http://localhost:51599/api/users')
    //     .map(res => res.json());
    // }
    AppComponent.prototype.fetchData = function (event) {
        var data = [
            { Id: 1, ComboProp: 1, TextPro: 'dawawd', DateTest: new Date() },
            { Id: 2, ComboProp: 5, TextPro: 'add', DateTest: new Date() }
        ];
        event.data = data;
    };
    AppComponent.prototype.firstColumnCustomFilter = function (val, filterData, item) {
        return true;
    };
    AppComponent.prototype.gridButtonClickTest = function (item) {
        console.log('Grid button click', item);
    };
    AppComponent.prototype.testSave = function (item) {
        return Rx_1.Observable.empty();
    };
    AppComponent.prototype.getDetails = function (masterKey, masterItem) {
        console.log('GET DETAILS');
        var details = masterItem['detailItems'];
        if (details == null) {
            return Rx_1.Observable.of([]);
        }
        return Rx_1.Observable.of([]);
    };
    // initCell(context: any, elementRef: any, type: any) {
    //   console.log(this);
    //   // if (type == 4) {
    //   //   console.log(context, elementRef, type);
    //   // }
    // }
    AppComponent.prototype.initFormCallback = function (editForm) {
        editForm.form.controls.age.validator = forms_1.Validators.required;
        //editForm.form.TextPro;
    };
    AppComponent.prototype.getErrorMessages = function (errorKeys, propertyName) {
        if (propertyName == 'age' && errorKeys['required']) {
            return ['Required field', 'Asd', 'a', 'a', 'a'];
        }
        return [];
    };
    AppComponent.prototype.testChangePage = function (context) {
        console.log('!?!?!?!?!', context);
        context.changePage(2, 10);
    };
    AppComponent.prototype.testPageChange = function () {
        this.pageSize = 5;
    };
    AppComponent.prototype.test = function (ctx) {
        console.log(ctx);
    };
    AppComponent.prototype.testGridChange = function (masterKey, gridState) {
        console.log('STATE CHANGE', masterKey, gridState);
        return Rx_1.Observable.of({ items: [], count: 0, summaryResults: [] });
    };
    AppComponent.prototype.getCallback = function (context) {
        return this.testGridChange.bind(this, context.masterKey);
    };
    __decorate([
        core_1.ViewChild('fooGrid'),
        __metadata("design:type", my_grid_1.MyGridComponent)
    ], AppComponent.prototype, "grid", void 0);
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        }),
        __metadata("design:paramtypes", [http_1.Http])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map