import { Component, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { MyGridComponent } from 'my-grid';
import { TextSearchTermType } from 'my-grid';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  gridKey = 'Id';
  firstCaption = 'Some caption';
  data: any[] = [];
  comboBoxItems: any[] = [];
  enableServerSideMode: boolean = false;
  pageSize = 10;
  boolTestItems = [{ Id: true, Name: 'T' }, { Id: false, Name: 'F' }];
  guidTestItems = [{ Id: 'c9312f11-c826-47a2-8806-ab209e9d6098', Name: '1' }, { Id: '510fff7d-c108-4dce-8db2-5929febfd19e', Name: '2' }];
  charTestItems = [{ Id: 'Aaa', Name: 'aaa' }, { Id: 'Bbb', Name: 'bbb' }];
  decimalTestItems = [{ Id: 5.23, Name: '5.23' }, { Id: 3.4, Name: '3.4' }];
  someProp = 'IDJEAI';
  httpClient: Http;
  initCell: (context: any, elementRef: any, type: any) => void;
  summaryType: boolean = false;
  @ViewChild('fooGrid') grid: MyGridComponent;
  testSearchTerm = TextSearchTermType;

  constructor(httpClient: Http) {
    this.httpClient = httpClient;

    this.initCell = (context: any, elementRef: any, type: any) => {
    };
  }

	beforeFiltering(items: any[]) {
		console.log('Before filtering', items);
	}
  
  ngOnInit() {
    this.data = [
      { Id: 1, ComboProp: 1, TextPro: 'abcd', MarkTest: true, detailItems: [ { id: 1, firstName: 'Asdf', lastName: 'Aas' }, { id: 2, firstName: 'Bjd', lastName: 'Bjop' }, { id: 3, firstName: 'Cqdo', lastName: 'Cda' } ] },
      { Id: 2, ComboProp: 5, TextPro: 'jfri', MarkTest: null, DateTest: new Date(2017, 1, 6, 1, 5, 2, 33) },
      { Id: 3, ComboProp: 5, TextPro: 'abcd', MarkTest: false, DateTest: new Date(2013, 1, 6, 4, 6, 3, 98) },
      { Id: 4, ComboProp: 5, TextPro: 'dddda', MarkTest: false, age: 23  },
      { Id: 5, ComboProp: 5, TextPro: 'aad', MarkTest: true },
      { Id: 6, ComboProp: 5, TextPro: 'wueu', MarkTest: false },
      { Id: 7, ComboProp: 5, TextPro: 'jfri', MarkTest: false },
      { Id: 8, ComboProp: 5, TextPro: 'add', MarkTest: true },
      { Id: 9, ComboProp: 5, TextPro: 'dddda', MarkTest: null },
      { Id: 10, ComboProp: 5, TextPro: 'dqq', MarkTest: false },
      { Id: 11, ComboProp: 5, TextPro: 'ohcu', MarkTest: true },
      { Id: 12, ComboProp: 5, TextPro: 'ncj', MarkTest: null },
      { Id: 13, ComboProp: 5, TextPro: 'dqq', MarkTest: true },
      { Id: 14, ComboProp: 5, TextPro: 'abcd', MarkTest: false },
      { Id: 15, ComboProp: 5, TextPro: 'ncj', MarkTest: true, age: 7 },
      { Id: 16, ComboProp: 5, TextPro: 'dqq', MarkTest: true },
      { Id: 17, ComboProp: 5, TextPro: 'ohcu', MarkTest: false },
    ]; 

    this.comboBoxItems = [
      // { Id: 1, Name: '1_Name' },
      // { Id: 5, Name: '5_Name' }
      { Id: 0, Name: 'F0' },
      { Id: 1, Name: 'F1' },
      { Id: 2, Name: 'F2' },
      { Id: 3, Name: 'F3' }
    ];
  }

  ngAfterViewInit() {
  }
  
  change() {
    this.someProp = 'CHANGED';
  }

  onGridStateChange = (state: any) => {
    console.log('STATE', state);
    return this.httpClient
      .post('http://localhost:57022/api/users', state)
      .map(res => res.json());
  }

  // onGridStateChange(state) {
  //   console.log('STATE', state, this.httpClient, this);
  //   return this.httpClient
  //     .get('http://localhost:51599/api/users')
  //     .map(res => res.json());
  // }

  fetchData(event: any) {
    var data = [
      { Id: 1, ComboProp: 1, TextPro: 'dawawd', DateTest: new Date() },
      { Id: 2, ComboProp: 5, TextPro: 'add', DateTest: new Date() }
    ];

    event.data = data;
  }

  firstColumnCustomFilter(val: any, filterData: any, item: any) {
    return true;
  }

  gridButtonClickTest(item: any) {
    console.log('Grid button click', item);
  }

  testSave(item: any): Observable<any> {
    return Observable.empty();
  }

  getDetails(masterKey: any, masterItem: any): Observable<any[]> {
    console.log('GET DETAILS');
    let details = masterItem['detailItems'];
    if (details == null) {
      return Observable.of([]);
    }

    return Observable.of([]);
  }

  // initCell(context: any, elementRef: any, type: any) {
  //   console.log(this);
  //   // if (type == 4) {
  //   //   console.log(context, elementRef, type);
  //   // }
  // }

  initFormCallback(editForm: any) {
    editForm.form.controls.age.validator = Validators.required;
    
    //editForm.form.TextPro;
  }

  getErrorMessages(errorKeys: any, propertyName: string) {
    if (propertyName == 'age' && errorKeys['required']) {
      return ['Required field', 'Asd', 'a', 'a', 'a'];
    }

    return [];
  }

  testChangePage(context: any) {
    console.log('!?!?!?!?!', context);
    context.changePage(2, 10);
  }

  testPageChange() {
    this.pageSize = 5;
  }

  test(ctx: any) {
    console.log(ctx);
  }

  testGridChange(masterKey: any, gridState: any): Observable<any> {
    console.log('STATE CHANGE', masterKey, gridState);
    return Observable.of({items: [], count: 0, summaryResults: []});
  }

  getCallback(context: any) {
    return this.testGridChange.bind(this, context.masterKey);
  }
}