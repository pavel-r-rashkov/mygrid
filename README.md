# MyGrid
MyGrid is an open-source data grid for Angular written in typescript.
## Getting started
Check the project [wiki][wiki] for installation and api details.
## Repository
https://bitbucket.org/pavel-r-rashkov/mygrid
## Examples
[Plunker][plunkerExamples]

or

https://bitbucket.org/pavel-r-rashkov/mygrid.examples
## Server-side extensions
https://bitbucket.org/pavel-r-rashkov/mygrid.serversideextensions
## Npm
https://www.npmjs.com/package/my-grid
## Contacts
If you want to report a bug, make a feature request or just need help - [open an issue][openIssue]. Make sure to check why you are opening the issue:
[x] Bug report  
[ ] Feature request
[ ] I have a question / need help
## License
This project is licensed under MIT license  

[plunkerExamples]: <https://plnkr.co/edit/VBRvx49g5dbbPEbin7lg?p=preview>
[openIssue]: <https://bitbucket.org/pavel-r-rashkov/mygrid/issues>
[wiki]: <https://bitbucket.org/pavel-r-rashkov/mygrid/wiki/Home>

